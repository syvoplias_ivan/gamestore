﻿using GameStore.DTO.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.DTO.Localization
{
    public class GenreTranslateDto : IDtoItem, ILocalizedDtoItem
    {
        public Guid Id { get; set; }

        public bool IsDeleted { get; set; }

        public string Name { get; set; }

        public LanguageCode Language { get; set; }

        public Guid GenreId { get; set; }
    }
}
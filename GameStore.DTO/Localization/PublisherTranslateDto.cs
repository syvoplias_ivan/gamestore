﻿using GameStore.DTO.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.DTO.Localization
{
    public class PublisherTranslateDto : IDtoItem, ILocalizedDtoItem
    {
        public Guid Id { get; set; }

        public bool IsDeleted { get; set; }

        public string CompanyName { get; set; }

        public string Description { get; set; }

        public LanguageCode Language { get; set; }

        public Guid PublisherId { get; set; }
    }
}
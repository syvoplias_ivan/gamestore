﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.DTO
{
    public class FilterDto
    {
        public IEnumerable<string> Genres { get; set; }

        public IEnumerable<string> Platforms { get; set; }

        public IEnumerable<string> Publishers { get; set; }

        public string SortType { get; set; }

        public string Date { get; set; }

        public decimal? PriceLow { get; set; }

        public decimal? PriceHigh { get; set; }

        public string GameName { get; set; }

        public string NumberOnPage { get; set; }

        public int PageNumber { get; set; }
    }
}
﻿using GameStore.Common;
using GameStore.DTO.Abstract;
using System;
using System.Collections.Generic;

namespace GameStore.DTO
{
    public class OrderDto : IDtoItem
    {
        public Guid UserId { get; set; }

        public DateTime OrderDate { get; set; }

        public DateTime? ShippedDate { get; set; }

        public IEnumerable<OrderDetailDto> OrderDetails { get; set; }

        public TypeOfPayment PaymentType { get; set; }

        public Guid BasketId { get; set; }

        public decimal Summ { get; set; }

        public OrderStatus Status { get; set; }

        public Guid Id { get; set; }

        public bool IsDeleted { get; set; }

        public string ObjectId { get; set; }
    }
}
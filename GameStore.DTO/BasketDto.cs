﻿using GameStore.DTO.Abstract;
using System;
using System.Collections.Generic;

namespace GameStore.DTO
{
    public class BasketDto : IDtoItem
    {
        public Guid UserId { get; set; }

        public IEnumerable<OrderDto> Orders { get; set; }

        public decimal Summ { get; set; }

        public Guid Id { get; set; }

        public bool IsDeleted { get; set; }
    }
}
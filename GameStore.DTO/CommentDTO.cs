﻿using GameStore.DTO.Abstract;
using GameStore.DTO.Authentification;
using System;
using System.Collections.Generic;

namespace GameStore.DTO
{
    public class CommentDto : IDtoItem
    {
        public Guid Id { get; set; }

        public bool IsDeleted { get; set; }

        public Guid? UserId { get; set; }

        public UserDto User { get; set; }

        public string Body { get; set; }

        public string GameKey { get; set; }

        public Guid? ParentId { get; set; }

        public string Quote { get; set; }

        public DateTime Date { get; set; }

        public IEnumerable<CommentDto> ChildComments { get; set; }
    }
}
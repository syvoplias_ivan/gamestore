﻿using GameStore.DTO.Abstract;
using GameStore.DTO.Localization;
using System;
using System.Collections.Generic;

namespace GameStore.DTO
{
    public class PublisherDto : IDtoNamedItem
    {
        public IEnumerable<GameDto> Games { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string HomePage { get; set; }

        public Guid Id { get; set; }

        public bool IsDeleted { get; set; }

        public string ObjectId { get; set; }

        public IEnumerable<PublisherTranslateDto> PublisherLocalization { get; set; }
    }
}
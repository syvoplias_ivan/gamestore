﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.DTO
{
    public class OrderHistoryFilterDto
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public string NumberOnPage { get; set; }

        public int PageNumber { get; set; }
    }
}
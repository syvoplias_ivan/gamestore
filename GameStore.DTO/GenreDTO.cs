﻿using GameStore.DTO.Abstract;
using GameStore.DTO.Localization;
using System;
using System.Collections.Generic;

namespace GameStore.DTO
{
    public class GenreDto : IDtoNamedItem
    {
        public Guid Id { get; set; }

        public bool IsDeleted { get; set; }

        public string Name { get; set; }

        public string ParentName { get; set; }

        public IEnumerable<Guid> GameIds { get; set; }

        public string ObjectId { get; set; }

        public IEnumerable<GenreTranslateDto> GenreLocalization { get; set; }
    }
}
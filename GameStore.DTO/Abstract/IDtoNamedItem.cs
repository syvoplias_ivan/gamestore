﻿namespace GameStore.DTO.Abstract
{
    public interface IDtoNamedItem : IDtoItem
    {
        string Name { get; set; }
    }
}
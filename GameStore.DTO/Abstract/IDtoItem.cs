﻿using System;

namespace GameStore.DTO.Abstract
{
    public interface IDtoItem
    {
        Guid Id { get; set; }
        bool IsDeleted { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.DTO.Abstract
{
    public interface ILocalizedDtoItem
    {
        LanguageCode Language { get; set; }
    }
}
﻿using GameStore.Common;
using GameStore.DTO.Abstract;
using System;

namespace GameStore.DTO.Authentification
{
    public class RoleDto : IDtoItem
    {
        public Guid Id { get; set; }

        public bool IsDeleted { get; set; }

        public RoleCode RoleCode { get; set; }
    }
}
﻿using GameStore.Common.Auth;
using GameStore.DTO.Abstract;
using System;
using System.Collections.Generic;

namespace GameStore.DTO.Authentification
{
    public class UserDto : IDtoItem
    {
        public Guid Id { get; set; }

        public bool IsDeleted { get; set; }

        public string NickName { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string FirstName { get; set; }

        public string Lastname { get; set; }

        public Gender Gender { get; set; }

        public string MiddleName { get; set; }

        public IEnumerable<RoleDto> Roles { get; set; }
    }
}
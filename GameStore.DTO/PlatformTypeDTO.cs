﻿using GameStore.Common;
using GameStore.DTO.Abstract;
using System;
using System.Collections.Generic;

namespace GameStore.DTO
{
    public class PlatformTypeDto : IDtoItem
    {
        public Guid Id { get; set; }

        public bool IsDeleted { get; set; }

        public TypeOfPlatform Type { get; set; }

        public IEnumerable<Guid> GameIds { get; set; }
    }
}
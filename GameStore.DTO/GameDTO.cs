﻿using GameStore.DTO.Abstract;
using GameStore.DTO.Localization;
using System;
using System.Collections.Generic;

namespace GameStore.DTO
{
    public class GameDto : IDtoNamedItem
    {
        public Guid Id { get; set; }

        public bool IsDeleted { get; set; }

        public string Key { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public decimal Price { get; set; }

        public short UnitsInStock { get; set; }

        public bool Discontinued { get; set; }

        public int VisitsCount { get; set; }

        public DateTime ReleaseDate { get; set; }

        public DateTime PublishDate { get; set; }

        public IEnumerable<CommentDto> Comments { get; set; }

        public IEnumerable<PlatformTypeDto> PlatformTypes { get; set; }

        public IEnumerable<GenreDto> Genres { get; set; }

        public IEnumerable<GameTranslateDto> GameLocalization { get; set; }

        public Guid? PublisherId { get; set; }

        public string PublisherName { get; set; }

        public string ObjectId { get; set; }
    }
}
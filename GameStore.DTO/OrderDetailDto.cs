﻿using GameStore.DTO.Abstract;
using System;

namespace GameStore.DTO
{
    public class OrderDetailDto : IDtoItem
    {
        public string ProductName { get; set; }

        public string ProductKey { get; set; }

        public decimal Price { get; set; }

        public short Quantity { get; set; }

        public float Discount { get; set; }

        public Guid OrderId { get; set; }

        public Guid Id { get; set; }

        public bool IsDeleted { get; set; }

        public string ObjectId { get; set; }
    }
}
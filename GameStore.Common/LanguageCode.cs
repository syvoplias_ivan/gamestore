﻿using System.ComponentModel;

public enum LanguageCode
{
    [Description("En_LanguageCode")]
    En,

    [Description("Ru_LanguageCode")]
    Ru,

    [Description("Uk_LanguageCode")]
    Uk,

    Fr
}
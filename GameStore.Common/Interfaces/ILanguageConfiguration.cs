﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.Common
{
    public interface ILanguageConfiguration
    {
        LanguageCode DefaultLanguage { get; }
    }
}
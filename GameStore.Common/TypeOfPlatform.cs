﻿using System.ComponentModel;

namespace GameStore.Common
{
    public enum TypeOfPlatform
    {
        [Description("Mobile_TypeOfPlatform")]
        Mobile,

        [Description("Browser_TypeOfPlatform")]
        Browser,

        [Description("Desktop_TypeOfPlatform")]
        Desktop,

        [Description("Console_TypeOfPlatform")]
        Console
    }
}
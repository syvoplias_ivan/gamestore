﻿using System.ComponentModel;

namespace GameStore.Common
{
    public enum BanDuration
    {
        [Description("Hour_BanDuration")]
        Hour,

        [Description("Day_BanDuration")]
        Day,

        [Description("Week_BanDuration")]
        Week,

        [Description("Month_BanDuration")]
        Month,

        [Description("Year_BanDuration")]
        Year,

        [Description("Permanent_BanDuration")]
        Permanent
    }
}
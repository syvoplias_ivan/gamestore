﻿using System.ComponentModel;

namespace GameStore.Common
{
    public enum OrderStatus
    {
        [Description("NotPaid_OrderStatus")]
        NotPaid,

        [Description("Paid_OrderStatus")]
        Paid,

        [Description("Shipped_OrderStatus")]
        Shipped
    }
}
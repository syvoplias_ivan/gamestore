﻿using System.ComponentModel;

namespace GameStore.Common
{
    public enum SortType
    {
        [Description("MostPopular_SortType")]
        MostPopular,

        [Description("MostCommented_SortType")]
        MostCommented,

        [Description("PriceAsc_SortType")]
        PriceAsc,

        [Description("PriceDesc_SortType")]
        PriceDesc,

        [Description("New_SortType")]
        New,

        [Description("None_SortType")]
        None
    }
}
﻿using System.ComponentModel;

namespace GameStore.Common
{
    public enum TypeOfPayment
    {
        [Description("Bank_TypeOfPayment")]
        Bank,

        [Description("IBox_TypeOfPayment")]
        IBox,

        [Description("Visa_TypeOfPayment")]
        Visa,

        [Description("NotSelected_TypeOfPayment")]
        NotSelected
    }
}
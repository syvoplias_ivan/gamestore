﻿using System.ComponentModel;

namespace GameStore.Common.Auth
{
    public enum Gender
    {
        [Description("Gender_Male")]
        Male,

        [Description("Gender_Female")]
        Female,

        [Description("Gender_Other")]
        Other
    }
}
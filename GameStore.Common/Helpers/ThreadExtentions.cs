﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GameStore.Common.Helpers
{
    public static class ThreadExtentions
    {
        public static LanguageCode LanguageFromThread(this Thread thread)
        {
            var culture = thread.CurrentUICulture.TwoLetterISOLanguageName;
            var langs = LanguageCode.En.GetAllValuesOfEnum();
            var result = default(LanguageCode);

            foreach (var lang in langs)
            {
                if (lang.ToString().ToLower() == culture.ToLower())
                {
                    result = lang;
                    break;
                }
            }

            return result;
        }
    }
}
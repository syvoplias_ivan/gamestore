﻿using System.ComponentModel;

namespace GameStore.Common
{
    public enum RoleCode
    {
        [Description("Guest_Role")]
        Guest,

        [Description("User_Role")]
        User,

        [Description("Manager_Role")]
        Manager,

        [Description("Moderator_Role")]
        Moderator,

        [Description("Administrator_Role")]
        Administrator
    }
}
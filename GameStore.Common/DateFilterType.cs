﻿using System.ComponentModel;

namespace GameStore.Common
{
    public enum DateFilterType
    {
        [Description("LastWeek_DateFilterType")]
        LastWeek,

        [Description("LastMonth_DateFilterType")]
        LastMonth,

        [Description("LastYear_DateFilterType")]
        LastYear,

        [Description("Last2Years_DateFilterType")]
        Last2Years,

        [Description("Last3Years_DateFilterType")]
        Last3Years
    }
}
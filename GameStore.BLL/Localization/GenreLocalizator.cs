﻿using GameStore.BLL.Abstract;
using GameStore.Common;
using GameStore.DAL.Entities;
using GameStore.DTO;
using GameStore.DTO.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.BLL.Localization
{
    public class GenreLocalizator : BaseLocalizator<GenreDto, GenreTranslateDto>
    {
        protected override void AddOrUpdateLocale(List<GenreTranslateDto> genreLocalizations, LanguageCode language, GenreDto source)
        {
            var localization = genreLocalizations.FirstOrDefault(x => language == x.Language);

            if (localization != null)
            {
                localization.Name = source.Name;
            }
            else
            {
                genreLocalizations.Add(new GenreTranslateDto()
                {
                    Name = source.Name,
                    IsDeleted = false,
                    Language = language,
                    GenreId = source.Id
                });
            }
        }

        protected override void UpdateLocaleIfNotNull(List<GenreTranslateDto> genreLocalizations, LanguageCode language, GenreDto source)
        {
            var currentLocalization = genreLocalizations.FirstOrDefault(x => x.Language == language);

            if (currentLocalization != null && currentLocalization.Name != null)
            {
                source.Name = currentLocalization.Name;
            }
        }

        protected override List<GenreTranslateDto> GetLocalizations(GenreDto source)
        {
            List<GenreTranslateDto> result;
            if (source.GenreLocalization != null && source.GenreLocalization.Any())
            {
                result = source.GenreLocalization.ToList();
            }
            else
            {
                result = new List<GenreTranslateDto>();
            }
            return result;
        }

        protected override void SaveResults(GenreDto source, List<GenreTranslateDto> localized)
        {
            source.GenreLocalization = localized;
        }
    }
}
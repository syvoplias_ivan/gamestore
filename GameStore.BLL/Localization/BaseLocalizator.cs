﻿using GameStore.BLL.Abstract;
using GameStore.Common;
using GameStore.DTO.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.BLL.Localization
{
    public abstract class BaseLocalizator<TSource, TTranslate> : ILocalizator<TSource> where TTranslate : ILocalizedDtoItem
    {
        protected LanguageCode _defaultLanguage;

        public TSource Localize(TSource source, LanguageCode langCode, LanguageCode defaultLanguage, LocalizationDirection localizationDirection, bool liveEmptyIfNoLocale = false)
        {
            _defaultLanguage = defaultLanguage;
            if (source != null)
            {
                if (localizationDirection == LocalizationDirection.In)
                {
                    LocalizeIn(source, langCode);
                }
                else
                {
                    LocalizeOut(source, langCode);
                }
            }
            return source;
        }

        protected abstract void SaveResults(TSource source, List<TTranslate> localized);

        private void LocalizeIn(TSource source, LanguageCode langCode)
        {
            List<TTranslate> gameLocalizations = GetLocalizations(source);

            gameLocalizations = gameLocalizations.GroupBy(x => x.Language).Select(x => x.First()).ToList();

            var defaultLocalization = gameLocalizations.FirstOrDefault(x => _defaultLanguage == x.Language);

            if (langCode != _defaultLanguage)
            {
                AddOrUpdateLocale(gameLocalizations, langCode, source);
            }

            if (defaultLocalization != null)
            {
                UpdateLocaleIfNotNull(gameLocalizations, _defaultLanguage, source);
                gameLocalizations.RemoveAt(gameLocalizations.FindIndex(x => _defaultLanguage == x.Language));
            }

            SaveResults(source, gameLocalizations);
        }

        private void LocalizeOut(TSource source, LanguageCode langCode)
        {
            List<TTranslate> gameLocalizations = GetLocalizations(source);
            gameLocalizations = gameLocalizations.GroupBy(x => x.Language).Select(x => x.First()).ToList();

            AddOrUpdateLocale(gameLocalizations, _defaultLanguage, source);

            UpdateLocaleIfNotNull(gameLocalizations, langCode, source);

            SaveResults(source, gameLocalizations);
        }

        protected abstract void UpdateLocaleIfNotNull(List<TTranslate> gameLocalizations, LanguageCode langCode, TSource source);

        protected abstract void AddOrUpdateLocale(List<TTranslate> gameLocalizations, LanguageCode _defaultLanguage, TSource source);

        protected abstract List<TTranslate> GetLocalizations(TSource source);
    }
}
﻿using GameStore.BLL.Abstract;
using GameStore.Common;
using GameStore.DAL.Entities;
using GameStore.DTO;
using GameStore.DTO.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.BLL.Localization
{
    public class PublisherLocalizator : BaseLocalizator<PublisherDto, PublisherTranslateDto>
    {
        protected override void AddOrUpdateLocale(List<PublisherTranslateDto> gameLocalizations, LanguageCode language, PublisherDto source)
        {
            var localization = gameLocalizations.FirstOrDefault(x => language == x.Language);

            if (localization != null)
            {
                localization.CompanyName = source.Name;
                localization.Description = source.Description;
            }
            else
            {
                gameLocalizations.Add(new PublisherTranslateDto()
                {
                    Description = source.Description,
                    CompanyName = source.Name,
                    IsDeleted = false,
                    Language = language,
                    PublisherId = source.Id
                });
            }
        }

        protected override void UpdateLocaleIfNotNull(List<PublisherTranslateDto> gameLocalizations, LanguageCode language, PublisherDto source)
        {
            var currentLocalization = gameLocalizations.FirstOrDefault(x => x.Language == language);

            if (currentLocalization != null)
            {
                if (currentLocalization.CompanyName != null)
                {
                    source.Name = currentLocalization.CompanyName;
                }

                if (currentLocalization.Description != null)
                {
                    source.Description = currentLocalization.Description;
                }
            }
        }

        protected override List<PublisherTranslateDto> GetLocalizations(PublisherDto source)
        {
            List<PublisherTranslateDto> result;
            if (source.PublisherLocalization != null && source.PublisherLocalization.Any())
            {
                result = source.PublisherLocalization.ToList();
            }
            else
            {
                result = new List<PublisherTranslateDto>();
            }
            return result;
        }

        protected override void SaveResults(PublisherDto source, List<PublisherTranslateDto> localized)
        {
            source.PublisherLocalization = localized;
        }
    }
}
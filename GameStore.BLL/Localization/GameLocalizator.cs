﻿using GameStore.BLL.Abstract;
using GameStore.Common;
using GameStore.Common.Helpers;
using GameStore.DAL.Entities;
using GameStore.DTO;
using GameStore.DTO.Localization;
using System.Collections.Generic;
using System.Linq;

namespace GameStore.BLL.Localization
{
    public class GameLocalizator : BaseLocalizator<GameDto, GameTranslateDto>
    {
        protected override List<GameTranslateDto> GetLocalizations(GameDto source)
        {
            List<GameTranslateDto> result;
            if (source.GameLocalization != null && source.GameLocalization.Any())
            {
                result = source.GameLocalization.ToList();
            }
            else
            {
                result = new List<GameTranslateDto>();
            }
            return result;
        }

        protected override void AddOrUpdateLocale(List<GameTranslateDto> gameLocalizations, LanguageCode language, GameDto source)
        {
            var localization = gameLocalizations.FirstOrDefault(x => language == x.Language);

            if (localization != null)
            {
                localization.Name = source.Name;
                localization.Description = source.Description;
            }
            else
            {
                gameLocalizations.Add(new GameTranslateDto()
                {
                    Description = source.Description,
                    Name = source.Name,
                    IsDeleted = false,
                    Language = language,
                    GameId = source.Id
                });
            }
        }

        protected override void UpdateLocaleIfNotNull(List<GameTranslateDto> gameLocalizations, LanguageCode language, GameDto source)
        {
            var currentLocalization = gameLocalizations.FirstOrDefault(x => x.Language == language);

            if (currentLocalization != null)
            {
                if (currentLocalization.Name != null)
                {
                    source.Name = currentLocalization.Name;
                }

                if (currentLocalization.Description != null)
                {
                    source.Description = currentLocalization.Description;
                }
            }
        }

        protected override void SaveResults(GameDto source, List<GameTranslateDto> localized)
        {
            source.GameLocalization = localized;
        }
    }
}
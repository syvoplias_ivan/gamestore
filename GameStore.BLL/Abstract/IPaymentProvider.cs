﻿using GameStore.Common;

namespace GameStore.BLL.Abstract
{
    public interface IPaymentProvider
    {
        IPaymentStrategy GetPaymentStrategy(TypeOfPayment payment);
    }
}
﻿using GameStore.DAL.Entities;
using GameStore.DTO;
using System;
using System.Collections.Generic;

namespace GameStore.BLL.Abstract
{
    public interface IGameService
    {
        void AddGame(GameDto item);

        void DeleteGame(Guid id);

        void DeleteGame(string key);

        void UpdateGame(GameDto item);

        GameDto GetGame(string key);

        GameDto GetGame(Guid id);

        IEnumerable<GameDto> GetAllGames();

        IEnumerable<GameDto> GetGamesPage(int pageNumber, int numberOnPage);

        IEnumerable<GameDto> GetGameByGenre(string genreName);

        IEnumerable<GameDto> GetGameByPlatformTypes(params string[] platformTypes);

        IEnumerable<GameDto> GetFilteredGames(FilterDto filter);

        int GetCountOfFilteredGames(FilterDto filterDto);

        int GetAllGamesCount();
    }
}
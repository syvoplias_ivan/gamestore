﻿using GameStore.DTO;
using System;
using System.Collections.Generic;

namespace GameStore.BLL.Abstract
{
    public interface IBasketService
    {
        BasketDto GetBasket(Guid userId);

        bool IfBasketExistsForCurrentUser(Guid id);

        void AddOrderToBasket(OrderDto orderDto);

        void AddOrderDetails(OrderDetailDto orderDetail, OrderDto order);

        void DeleteOrderDetail(OrderDetailDto orderDetail, OrderDto order);

        void UpdateOrderDetail(OrderDetailDto orderDetail, OrderDto order);

        IEnumerable<OrderDto> GetHistory(OrderHistoryFilterDto filter);

        OrderDto GetOrderFromBasket(Guid userGuid);

        void ClearBasket(Guid userGuid);

        void PayOrder(OrderDto order);

        int GetOrdersCount(Guid userGuid);

        void CreateBasketForCurrentUser(Guid id);

        int GetFilteredOrdersCount(OrderHistoryFilterDto filter);

        int GetOrdersCount();
    }
}
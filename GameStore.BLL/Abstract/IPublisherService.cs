﻿using GameStore.DTO;
using System;
using System.Collections.Generic;

namespace GameStore.BLL.Abstract
{
    public interface IPublisherService
    {
        IEnumerable<PublisherDto> GetAllPublishers();

        PublisherDto GetPublisherByName(string publisherName);

        PublisherDto GetPublisherById(Guid id);

        void AddPublisher(PublisherDto publisher);

        void UpdatePublisher(PublisherDto publisher);

        void DeletePublisher(PublisherDto publisher);

        void DeletePublisher(string companyName);

        void DeletePublisher(Guid id);

        IEnumerable<string> GetPublishers();

        IEnumerable<string> GetPublishersExcept(string publisherName);

        IEnumerable<string> GetLocalizedPublishers();

        IEnumerable<string> GetLocalizedPublishersExcept(string publisherName);

        bool IsPublisherNameExists(string publisherName);
    }
}
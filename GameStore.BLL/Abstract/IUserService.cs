﻿using GameStore.Common;
using GameStore.DTO.Authentification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.BLL.Abstract
{
    public interface IUserService
    {
        void CreateUser(UserDto user);

        void UpdateUser(UserDto user);

        void DeleteUser(UserDto user);

        void DeleteUser(string userNickName);

        UserDto GetUser(string userNickName);

        UserDto GetUser(Guid id);

        IEnumerable<UserDto> GetAllUsers();

        bool IsUserNickNameExist(string userNickName);

        bool IsUserInRoles(string userNickName, string roles);

        IEnumerable<string> GetUsersRoles(Guid userId);

        IEnumerable<string> GetAllRoles();

        IEnumerable<RoleCode> GetAllRoleCodes();
    }
}
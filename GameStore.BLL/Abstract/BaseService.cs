﻿using AutoMapper;
using GameStore.BLL.Localization;
using GameStore.Common;
using GameStore.Common.Helpers;
using GameStore.DAL.Abstract;
using GameStore.DAL.Entities;
using GameStore.DAL.UnitOfWork;
using GameStore.DTO;
using GameStore.DTO.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;

namespace GameStore.BLL.Abstract
{
    public abstract class BaseService<TDto, TEntity>
        where TDto : class, IDtoItem, new()
        where TEntity : class, IEntity, new()
    {
        protected IUnitOfWork _unitOfWork;

        
        protected LanguageCode CurrentLanguage => Thread.CurrentThread.LanguageFromThread();
        protected void Add(TDto item)
        {
            var destItem = PrepareForAdd(item);
            _unitOfWork.Repository<TEntity>().Insert(destItem);
            Save();
        }

        protected void Delete(Func<TEntity, bool> expression)
        {
            var itemToDelete = PrepareForDelete(expression);
            _unitOfWork.Repository<TEntity>().Delete(itemToDelete);
            Save();
        }

        protected void Update(TDto item)
        {
            var updated = PrepareForUpdate(item);
            _unitOfWork.Repository<TEntity>().Update(updated);
            Save();
        }

        protected IEnumerable<TDto> GetAll()
        {
            var list = _unitOfWork.Repository<TEntity>().Enumerable();

            IEnumerable<TDto> res = null;
            if (list != null)
            {
                res = Mapper.Map<IEnumerable<TDto>>(list);
            }
            return res;
        }

        protected IEnumerable<TDto> GetAllWithCondition(Expression<Func<TEntity, bool>> expression, Expression<Func<TEntity, object>> orderBy = null)
        {
            var games = _unitOfWork.Repository<TEntity>().Select(expression, orderBy);

            IEnumerable<TDto> res = null;
            if (games != null && games.Any())
            {
                res = Mapper.Map<IEnumerable<TDto>>(games);
            }
            return res;
        }

        protected IEnumerable<TDto> GetPage(int pageNumber, int numberOnPage)
        {
            var pages = _unitOfWork.Repository<TEntity>().Select(page: pageNumber, pageSize: numberOnPage);

            IEnumerable<TDto> res = null;
            if (pages != null && pages.Any())
            {
                res = Mapper.Map<IEnumerable<TDto>>(pages);
            }
            return res;
        }

        protected TDto Get(Guid id)
        {
            var item = _unitOfWork.Repository<TEntity>().Get(x => x.Id == id);

            TDto res = null;
            if (item != null)
            {
                res = Mapper.Map<TDto>(item);
            }
            return res;
        }

        protected TDto Get(Func<TEntity, bool> expression)
        {
            var item = _unitOfWork.Repository<TEntity>().Get(expression);

            TDto res = null;
            if (item != null)
            {
                res = Mapper.Map<TDto>(item);
            }
            return res;
        }

        protected virtual TEntity PrepareForDelete(Func<TEntity, bool> expression)
        {
            var temp = _unitOfWork.Repository<TEntity>().Get(expression);
            TEntity result = null;
            if (temp != null)
            {
                result = temp;
            }
            return result;
        }

        protected virtual TEntity PrepareForUpdate(TDto item)
        {
            var mappedItem = Mapper.Map<TEntity>(item);
            return mappedItem;
        }

        protected virtual TEntity PrepareForAdd(TDto item)
        {
            var mappedItem = Mapper.Map<TEntity>(item);
            return mappedItem;
        }

        private void Save()
        {
            _unitOfWork.SaveChanges();
        }
    }
}
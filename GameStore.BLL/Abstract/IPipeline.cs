﻿namespace GameStore.BLL.Abstract
{
    public interface IPipeline<T>
    {
        IExpressionFilter<T> Filter { get; }

        IOrderFilter<T> OrderFilter { get; }

        IPaginationFilter Pagination { get; }

        void Register(IExpressionFilter<T> filter);

        void Register(IOrderFilter<T> orderFilter);

        void Register(IPaginationFilter paginationFilter);

        IPipeline<T> Process();
    }
}
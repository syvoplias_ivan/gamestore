﻿using System;
using System.Linq.Expressions;

namespace GameStore.BLL.Abstract
{
    public interface IExpressionFilter<T>
    {
        IExpressionFilter<T> Execute(IExpressionFilter<T> filter);

        Expression<Func<T, bool>> FilterBodyExpression { get; set; }
    }
}
﻿using GameStore.DTO;
using System;
using System.Collections.Generic;

namespace GameStore.BLL.Abstract
{
    public interface IGenreService
    {
        IEnumerable<GenreDto> GetAllGenres();

        GenreDto GetGenre(string genreName);

        void CreateGenre(GenreDto genre);

        void UpdateGenre(GenreDto genre);

        void DeleteGenre(GenreDto genre);

        void DeleteGenre(string genreName);

        void DeleteGenre(Guid id);

        IEnumerable<string> GetGenres();

        IEnumerable<string> GetLocalizedGenres();

        IEnumerable<string> GetGenresExcept(string genreName);

        IEnumerable<string> GetLocalizedGenresExcept(string genreName);

        bool IsGenreNameExists(string genreName);
    }
}
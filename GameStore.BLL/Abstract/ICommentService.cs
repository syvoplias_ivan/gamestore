﻿using GameStore.DTO;
using System;
using System.Collections.Generic;

namespace GameStore.BLL.Abstract
{
    public interface ICommentService
    {
        void AddComment(CommentDto comment, String key);

        IEnumerable<CommentDto> GetAllComments();

        IEnumerable<CommentDto> GetCommentsPage(int pageNumber, int numberOnPage);

        CommentDto GetComment(Guid id);

        IEnumerable<CommentDto> GetComments(string key);

        IEnumerable<CommentDto> GetAllCommentWithoutParents(string key);

        void DeleteComment(Guid id);
    }
}
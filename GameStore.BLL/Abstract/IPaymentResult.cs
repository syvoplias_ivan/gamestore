﻿namespace GameStore.BLL.Abstract
{
    public interface IPaymentResult
    {
        T GetResultValue<T>(string valueName);
    }
}
﻿using GameStore.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.BLL.Abstract
{
    public interface ILocalizator<T>
    {
        T Localize(T source, LanguageCode langCode, LanguageCode defaultLanguage, LocalizationDirection localizationDirection, bool liveEmptyIfNoLocale = false);
    }
}
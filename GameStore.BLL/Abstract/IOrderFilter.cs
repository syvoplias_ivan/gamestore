﻿using System;
using System.Linq.Expressions;
using GameStore.Common;

namespace GameStore.BLL.Abstract
{
    public interface IOrderFilter<T>
    {
        SortType SortType { get; }
        Expression<Func<T, object>> Order { get; }
    }
}
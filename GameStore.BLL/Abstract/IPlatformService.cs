﻿using GameStore.DTO;
using System.Collections.Generic;

namespace GameStore.BLL.Abstract
{
    public interface IPlatformService
    {
        IEnumerable<PlatformTypeDto> GetAllPlatforms();

        void AddPlatformType(PlatformTypeDto platformTypeDto);

        void DeletePlatformType(PlatformTypeDto platformTypeDto);

        PlatformTypeDto Get(string typeName);
    }
}
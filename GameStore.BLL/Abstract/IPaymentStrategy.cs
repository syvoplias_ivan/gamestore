﻿using GameStore.DTO;

namespace GameStore.BLL.Abstract
{
    public interface IPaymentStrategy
    {
        string StrategyName { get; }

        IPaymentResult Pay(OrderDto order);
    }
}
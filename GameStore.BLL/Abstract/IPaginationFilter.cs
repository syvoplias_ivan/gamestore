﻿namespace GameStore.BLL.Abstract
{
    public interface IPaginationFilter
    {
        int PageNumber { get; }
        int CountOnPage { get; }
    }
}
﻿using GameStore.DTO.Authentification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.BLL.Abstract.Auth
{
    public interface IAuthenticationService
    {
        UserDto Login(string userNickName, string password, bool isPersistent = false);

        UserDto Login(string userNickName);
    }
}
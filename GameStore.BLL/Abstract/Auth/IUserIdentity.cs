﻿using GameStore.DTO.Authentification;
using System.Security.Principal;

namespace GameStore.BLL.Abstract.Auth
{
    public interface IUserIdentity : IIdentity
    {
        UserDto User { get; set; }

        void Init(string userNickName, IUserService service);
    }
}
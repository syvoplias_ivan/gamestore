﻿using GameStore.Common;
using GameStore.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.BLL.Abstract
{
    public interface IOrderService
    {
        void AddOrder(OrderDto order);

        void DeleteOrder(Guid id);

        void DeleteOrder(OrderDto order);

        void UpdateOrder(OrderDto order);

        void UpdateOrderStatus(Guid orderId, OrderStatus status);

        OrderDto GetOrder(Guid orderId);

        IEnumerable<OrderDto> GetAllOrders();
    }
}
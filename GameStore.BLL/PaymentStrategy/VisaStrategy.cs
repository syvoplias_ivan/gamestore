﻿using GameStore.BLL.Abstract;
using GameStore.DTO;
using System.Collections.Generic;

namespace GameStore.BLL.PaymentStrategy
{
    public class VisaStrategy : IPaymentStrategy
    {
        public string StrategyName => "Visa";

        public IPaymentResult Pay(OrderDto order)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            result["Summ"] = order.Summ;
            result["CustomerId"] = order.UserId;
            result["Bank"] = "Bank";
            return new PaymentResult(result);
        }
    }
}
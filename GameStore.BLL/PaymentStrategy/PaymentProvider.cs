﻿using GameStore.BLL.Abstract;
using GameStore.Common;
using System.Collections.Generic;
using System.Linq;

namespace GameStore.BLL.PaymentStrategy
{
    public class PaymentProvider : IPaymentProvider
    {
        private readonly IEnumerable<IPaymentStrategy> _strategies;

        public PaymentProvider(IEnumerable<IPaymentStrategy> strategies)
        {
            _strategies = strategies;
        }

        public IPaymentStrategy GetPaymentStrategy(TypeOfPayment payment)
        {
            return _strategies.First(x => x.StrategyName == payment.ToString());
        }
    }
}
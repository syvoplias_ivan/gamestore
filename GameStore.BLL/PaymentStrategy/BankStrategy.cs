﻿using GameStore.BLL.Abstract;
using GameStore.DTO;
using Spire.Pdf;
using Spire.Pdf.HtmlConverter;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web.Mvc;

namespace GameStore.BLL.PaymentStrategy
{
    public class BankStrategy : IPaymentStrategy
    {
        public string StrategyName => "Bank";

        public IPaymentResult Pay(OrderDto order)
        {
            var pdf = new PdfDocument();
            var pdfFormat = new PdfHtmlLayoutFormat();
            pdfFormat.IsWaiting = false;
            var pageSettings = new PdfPageSettings();
            pageSettings.Size = PdfPageSize.A4;

            string htmlCode = GetHTMLFromOrder(order);
            Thread thread = new Thread(() =>
            {
                pdf.LoadFromHTML(htmlCode, false, pageSettings, pdfFormat);
            });
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            thread.Join();

            var path = GetGameStoreDirectoryPath() + "/invoces/" + order.BasketId;

            if (Directory.Exists(path))
                Directory.CreateDirectory(path);

            pdf.SaveToFile(path + "/" + order.BasketId + ".pdf");

            return new PaymentResult(new Dictionary<string, object>() { ["filename"] = path + "/" + order.BasketId + ".pdf" });
        }

        private string GetHTMLFromOrder(OrderDto order)
        {
            var heading = new TagBuilder("h1");
            heading.SetInnerText("Order #" + order.Id);
            var table = new TagBuilder("table");
            table.Attributes["style"] = "border: 1px solid black";

            var row = new TagBuilder("tr");
            row.Attributes["style"] = "border: 1px solid black";
            var tdName = new TagBuilder("td");
            tdName.Attributes["style"] = "border: 1px solid black";
            var tdValue = new TagBuilder("td");
            tdValue.Attributes["style"] = "border: 1px solid black";

            tdName.SetInnerText("Date: ");
            tdValue.SetInnerText(order.OrderDate.ToString("D"));

            row.InnerHtml += tdName.ToString() + tdValue.ToString();
            table.InnerHtml += row.ToString();

            tdName.SetInnerText("Payment type: ");
            tdValue.SetInnerText(order.PaymentType.ToString());

            row.InnerHtml = tdName.ToString() + tdValue.ToString();
            table.InnerHtml += row.ToString();

            tdName.SetInnerText("Basket #: ");
            tdValue.SetInnerText(order.BasketId.ToString());

            row.InnerHtml = tdName.ToString() + tdValue.ToString();
            table.InnerHtml += row.ToString();

            tdName.SetInnerText("User Id: ");
            tdValue.SetInnerText(order.UserId.ToString());

            row.InnerHtml = tdName.ToString() + tdValue.ToString();
            table.InnerHtml += row.ToString();

            var detailsTable = new TagBuilder("table");
            detailsTable.Attributes["style"] = "border: 1px solid black";
            var caption = new TagBuilder("caption");
            caption.SetInnerText("Order details");
            detailsTable.InnerHtml += caption.ToString();

            foreach (var orderDetail in order.OrderDetails.Where(x => !x.IsDeleted))
            {
                var temptable = new TagBuilder("table");
                temptable.Attributes["style"] = "border: 1px solid black";
                tdName.SetInnerText("Name of product: ");
                tdValue.SetInnerText(orderDetail.ProductName);

                row.InnerHtml = tdName.ToString() + tdValue.ToString();
                temptable.InnerHtml += row.ToString();

                tdName.SetInnerText("Price: ");
                tdValue.SetInnerText(orderDetail.Price.ToString());

                row.InnerHtml = tdName.ToString() + tdValue.ToString();
                temptable.InnerHtml += row.ToString();

                tdName.SetInnerText("Price: ");
                tdValue.SetInnerText(orderDetail.Price.ToString());

                row.InnerHtml = tdName.ToString() + tdValue.ToString();
                temptable.InnerHtml += row.ToString();

                tdName.SetInnerText("Quantity: ");
                tdValue.SetInnerText(orderDetail.Quantity.ToString());

                row.InnerHtml = tdName.ToString() + tdValue.ToString();
                temptable.InnerHtml += row.ToString();

                tdName.SetInnerText("Discount: ");
                tdValue.SetInnerText(orderDetail.Discount.ToString());

                row.InnerHtml = tdName.ToString() + tdValue.ToString();
                temptable.InnerHtml += row.ToString();

                row.InnerHtml = temptable.ToString();
                detailsTable.InnerHtml += row.ToString();
            }

            table.InnerHtml += detailsTable.ToString();

            tdName.SetInnerText("Total sum: ");
            tdValue.SetInnerText(order.Summ.ToString());
            row.InnerHtml = tdName.ToString() + tdValue.ToString();

            table.InnerHtml += row.ToString();

            return heading.ToString() + table.ToString();
        }

        private string GetGameStoreDirectoryPath()
        {
            return AppDomain.CurrentDomain.BaseDirectory;
        }
    }
}
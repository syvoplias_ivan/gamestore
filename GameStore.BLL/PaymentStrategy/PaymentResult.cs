﻿using GameStore.BLL.Abstract;
using System.Collections.Generic;

namespace GameStore.BLL.PaymentStrategy
{
    public class PaymentResult : IPaymentResult
    {
        private readonly Dictionary<string, object> _results;

        public PaymentResult(Dictionary<string, object> results)
        {
            _results = results;
        }

        public T GetResultValue<T>(string valueName)
        {
            T res = default(T);
            if (_results.ContainsKey(valueName))
            {
                res = (T)_results[valueName];
            }
            return res;
        }
    }
}
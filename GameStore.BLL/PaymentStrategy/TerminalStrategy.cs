﻿using GameStore.BLL.Abstract;
using GameStore.DTO;
using System.Collections.Generic;

namespace GameStore.BLL.PaymentStrategy
{
    public class TerminalStrategy : IPaymentStrategy
    {
        public string StrategyName => "IBox";

        public IPaymentResult Pay(OrderDto order)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            result["AccountNumber"] = order.UserId;
            result["InvoiceNumber"] = order.Id;
            result["Summ"] = order.Summ;

            return new PaymentResult(result);
        }
    }
}
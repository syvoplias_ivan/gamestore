﻿using GameStore.BLL.Abstract;
using GameStore.BLL.Abstract.Auth;
using GameStore.DTO.Authentification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.BLL.Authentication
{
    public class UserIdentity : IUserIdentity
    {
        public UserDto User { get; set; }

        public string AuthenticationType
        {
            get
            {
                return typeof(UserDto).ToString();
            }
        }

        public bool IsAuthenticated
        {
            get
            {
                return User != null;
            }
        }

        public string Name
        {
            get
            {
                if (User != null)
                {
                    return User.NickName;
                }
                return "anonym";
            }
        }

        public void Init(string userNickName, IUserService userService)
        {
            if (!string.IsNullOrEmpty(userNickName))
            {
                User = userService.GetUser(userNickName);
            }
        }
    }
}
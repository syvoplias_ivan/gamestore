﻿using AutoMapper;
using GameStore.BLL.Abstract.Auth;
using GameStore.Common.Hasher;
using GameStore.DAL.Abstract;
using GameStore.DAL.Entities.Authorizaion;
using GameStore.DTO.Authentification;

namespace GameStore.BLL.Authentication
{
    public class AuthenticationService : IAuthenticationService
    {
        private IUnitOfWork _unitOfWork;

        public AuthenticationService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public UserDto Login(string userNickName, string password, bool isPersistent = false)
        {
            var hasher = Sha512Hasher.GetInstance();
            var user = _unitOfWork.Repository<User>().Get(x => !x.IsDeleted && x.NickName == userNickName);
            UserDto res = null;
            if (user != null && hasher.VerifyHash(password, user.Password))
            {
                res = Mapper.Map<UserDto>(user);
            }
            return res;
        }

        public UserDto Login(string userNickName)
        {
            var user = _unitOfWork.Repository<User>().Get(x => !x.IsDeleted && x.NickName == userNickName);
            UserDto res = null;
            if (user != null)
            {
                res = Mapper.Map<UserDto>(user);
            }
            return res;
        }
    }
}
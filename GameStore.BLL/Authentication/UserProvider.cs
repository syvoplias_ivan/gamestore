﻿using GameStore.BLL.Abstract;
using GameStore.BLL.Abstract.Auth;
using GameStore.BLL.Authentication;
using GameStore.Common;
using GameStore.DTO.Authentification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.BLL.Authentication
{
    public class UserProvider : IPrincipal
    {
        private readonly IUserService _userService;
        private readonly IUserIdentity _userIdentity;

        #region IPrincipal Members

        public IIdentity Identity
        {
            get
            {
                return _userIdentity;
            }
        }

        public bool IsInRole(string role)
        {
            if (_userIdentity.User == null && role.ToLower() == RoleCode.Guest.ToString().ToLower())
            {
                return true;
            }

            if (_userIdentity.User == null)
            {
                return false;
            }
            return _userService.IsUserInRoles(_userIdentity.User.NickName, role);
        }

        #endregion IPrincipal Members

        public UserProvider(string name, IUserService service)
        {
            _userService = service;
            _userIdentity = new UserIdentity();
            _userIdentity.Init(name, service);
        }

        public override string ToString()
        {
            return _userIdentity.Name;
        }
    }
}
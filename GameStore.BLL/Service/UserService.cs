﻿using AutoMapper;
using GameStore.BLL.Abstract;
using GameStore.Common;
using GameStore.Common.Hasher;
using GameStore.Common.Helpers;
using GameStore.DAL.Abstract;
using GameStore.DAL.Entities.Authorizaion;
using GameStore.DTO.Authentification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.BLL.Service
{
    public class UserService : BaseService<UserDto, User>, IUserService
    {
        public UserService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void CreateUser(UserDto user)
        {
            Add(user);
        }

        protected override User PrepareForAdd(UserDto item)
        {
            item.Password = Sha512Hasher.GetInstance().ComputeHash(item.Password, null);
            var user = Mapper.Map<User>(item);

            if (item.Roles != null)
            {
                user.Roles.Clear();
            }
            else
            {
                user.Roles = new List<Role>();
            }

            foreach (var role in item.Roles)
            {
                var roleDb = _unitOfWork.Repository<Role>().Get(x => x.RoleCode == role.RoleCode);
                if (roleDb != null)
                {
                    user.Roles.Add(roleDb);
                }
            }

            return user;
        }

        protected override User PrepareForUpdate(UserDto item)
        {
            var user = _unitOfWork.Repository<User>().Get(x => x.Id == item.Id);
            var pass = user.Password;

            Mapper.Map(item, user);
            user.Password = pass;

            var userRoles = user.Roles.ToList();

            if (user.Roles != null)
            {
                userRoles.Clear();
            }
            else
            {
                userRoles = new List<Role>();
            }

            foreach (var role in item.Roles)
            {
                var roleDb = _unitOfWork.Repository<Role>().Get(x => x.RoleCode == role.RoleCode);
                if (roleDb != null)
                {
                    userRoles.Add(roleDb);
                }
            }
            user.Roles = userRoles;

            return user;
        }

        public void DeleteUser(UserDto user)
        {
            Delete(x => !x.IsDeleted && x.NickName == user.NickName);
        }

        public IEnumerable<UserDto> GetAllUsers()
        {
            return GetAll();
        }

        public UserDto GetUser(string userNickName)
        {
            return Get(x => x.NickName == userNickName);
        }

        public bool IsUserInRoles(string userNickName, string roles)
        {
            var user = GetUser(userNickName);
            if (user != null)
            {
                if (string.IsNullOrWhiteSpace(roles))
                {
                    return false;
                }

                var rolesArray = roles.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var role in rolesArray)
                {
                    var hasRole = user.Roles.Any(x => x.RoleCode.ToString().ToLower() == role.ToLower());
                    if (hasRole)
                    {
                        return true;
                    }
                }
                return false;
            }
            return false;
        }

        public bool IsUserNickNameExist(string userNickName)
        {
            var user = Get(x => x.NickName == userNickName);
            return user != null;
        }

        public void UpdateUser(UserDto user)
        {
            Update(user);
        }

        public void DeleteUser(string userNickName)
        {
            Delete(x => x.NickName == userNickName);
        }

        public UserDto GetUser(Guid id)
        {
            return Get(x => x.Id == id);
        }

        public IEnumerable<string> GetUsersRoles(Guid userId)
        {
            var user = GetUser(userId);
            return user.Roles.Select(x => x.RoleCode.ConvertToString()).ToList();
        }

        public IEnumerable<string> GetAllRoles()
        {
            var roles = RoleCode.User.GetAllValuesOfEnum().ToList();
            roles.Remove(RoleCode.Guest);
            return roles.Select(x => x.ConvertToString()).ToList();
        }

        public IEnumerable<RoleCode> GetAllRoleCodes()
        {
            var roles = RoleCode.User.GetAllValuesOfEnum().ToList();
            roles.Remove(RoleCode.Guest);
            return roles;
        }
    }
}
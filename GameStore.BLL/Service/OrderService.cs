﻿using AutoMapper;
using GameStore.BLL.Abstract;
using GameStore.Common;
using GameStore.DAL.Abstract;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Authorizaion;
using GameStore.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.BLL.Service
{
    public class OrderService : BaseService<OrderDto, Order>, IOrderService
    {
        public OrderService(IUnitOfWork workUnit)
        {
            _unitOfWork = workUnit;
        }

        public void AddOrder(OrderDto order)
        {
            Add(order);
        }

        public void DeleteOrder(OrderDto order)
        {
            Delete(x => x.Id == order.Id);
        }

        public void DeleteOrder(Guid id)
        {
            Delete(x => x.Id == id);
        }

        public OrderDto GetOrder(Guid orderId)
        {
            return Get(orderId);
        }

        public void UpdateOrder(OrderDto order)
        {
            Update(order);
        }

        public void UpdateOrderStatus(Guid orderId, OrderStatus status)
        {
            var order = _unitOfWork.Repository<Order>().Get(x => x.Id == orderId);
            order.Status = status;
            if (status == OrderStatus.Shipped)
            {
                order.ShippedDate = DateTime.UtcNow;
            }
            _unitOfWork.Repository<Order>().Update(order);
            _unitOfWork.SaveChanges();
        }

        public IEnumerable<OrderDto> GetAllOrders()
        {
            return GetAll();
        }

        protected override Order PrepareForAdd(OrderDto item)
        {
            var order = Mapper.Map<Order>(item);

            User user = _unitOfWork.Repository<User>().Get(x => x.Id == item.UserId);
            if (user == null)
            {
                order.UserId = null;
                order.User = null;
            }
            return order;
        }

        //todo: add update logic
        protected override Order PrepareForUpdate(OrderDto item)
        {
            return base.PrepareForUpdate(item);
        }
    }
}
﻿using AutoMapper;
using GameStore.BLL.Abstract;
using GameStore.BLL.Localization;
using GameStore.BLL.Pipeline.OrderFilters;
using GameStore.Common;
using GameStore.DAL.Abstract;
using GameStore.DAL.Entities;
using GameStore.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GameStore.BLL.Service
{
    public class BasketService : BaseService<BasketDto, Basket>, IBasketService
    {
        public BasketService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        private void UpdateGameWhileDeletingOrder(Order order)
        {
            foreach (var item in order.OrderDetails)
            {
                item.Game.UnitsInStock += item.Quantity;
            }
        }

        private void UpdateGameWhileDeletingOrderDetail(OrderDetail detail)
        {
            detail.Game.UnitsInStock += detail.Quantity;
        }

        private void UpdateOrderData(Order order, OrderDto orderDto)
        {
            if (!order.IsDeleted && orderDto.IsDeleted) UpdateGameWhileDeletingOrder(order);
            order.IsDeleted = orderDto.IsDeleted;
            order.Status = orderDto.Status;
            order.PaymentType = orderDto.PaymentType;
        }

        private void UpdateOrderDetailData(OrderDetail orderDetail, OrderDetailDto orderDetailDto, ref decimal sumOfOrder)
        {
            var order = orderDetail.Order;
            var quantityPrev = orderDetail.Quantity;
            orderDetail.Quantity = orderDetailDto.Quantity;
            var diff = (short)(orderDetail.Quantity - quantityPrev);
            if (!orderDetail.IsDeleted && orderDetailDto.IsDeleted)
            {
                UpdateGameWhileDeletingOrderDetail(orderDetail);
            }
            else
            {
                orderDetail.Game.UnitsInStock -= diff;
                var deleted = orderDetail.IsDeleted || order.IsDeleted;
                sumOfOrder = UpdateSum(order.Status, deleted, sumOfOrder, orderDetail.Quantity * orderDetail.Price);
            }
            orderDetail.IsDeleted = orderDetailDto.IsDeleted;
        }

        protected override Basket PrepareForUpdate(BasketDto item)
        {
            var basket = _unitOfWork.Repository<Basket>().Get(x => x.UserId == item.UserId && !x.IsDeleted);

            var orders = basket.Orders;

            if (orders != null && orders.Count != 0)
            {
                var itemOrders = item.Orders;
                decimal sum = 0.0M;

                foreach (var order in orders)
                {
                    decimal sumOfOrder = 0.0M;
                    var itemOrder = itemOrders.First(x => x.Id == order.Id);

                    UpdateOrderData(order, itemOrder);

                    var orderDetails = order.OrderDetails;
                    var itemDetails = itemOrder.OrderDetails;

                    foreach (var itemDetail in itemDetails)
                    {
                        var orderDetail = orderDetails.FirstOrDefault(x => x.Game.Key == itemDetail.ProductKey && x.Price == itemDetail.Price && x.Id == itemDetail.Id);
                        if (orderDetail != null)
                        {
                            UpdateOrderDetailData(orderDetail, itemDetail, ref sumOfOrder);
                        }
                        else
                        {
                            var orderDet = Mapper.Map<OrderDetail>(itemDetail);
                            orderDet.ProductId = _unitOfWork.Repository<Game>().Get(x => x.Key == itemDetail.ProductKey).Id;
                            orderDet.Game = _unitOfWork.Repository<Game>().Get(x => x.Key == itemDetail.ProductKey);

                            orderDet.Game.UnitsInStock -= orderDet.Quantity;

                            orderDet.OrderId = order.Id;
                            orderDetails.Add(orderDet);
                            var deleted = orderDet.IsDeleted || order.IsDeleted;

                            sumOfOrder = UpdateSum(order.Status, deleted, sumOfOrder, orderDet.Quantity * orderDet.Price);
                        }
                    }

                    if (orderDetails.Count != itemDetails.Count())
                    {
                        AppendNewOrderDetailsToOrder(order, orderDetails, itemDetails, ref sumOfOrder);
                    }
                    order.Summ = sumOfOrder;
                    sum += sumOfOrder;
                }

                if (orders.Count != itemOrders.Count())
                {
                    basket = AppendNewOrders(basket, itemOrders, ref sum);
                }
                basket.Summ = sum;
            }
            else
            {
                basket = AddOrdersToBasket(basket, item);
            }

            return basket;
        }

        private void AppendNewOrderDetailsToOrder(Order order, ICollection<OrderDetail> orderDetails, IEnumerable<OrderDetailDto> itemDetails, ref decimal sumOfOrder)
        {
            for (int i = orderDetails.Count; i < itemDetails.Count(); i++)
            {
                var orderDetail = Mapper.Map<OrderDetail>(itemDetails.ElementAt(i));
                orderDetail.ProductId = _unitOfWork.Repository<Game>().Get(x => x.Key == orderDetail.Game.Key).Id;
                orderDetail.Game = _unitOfWork.Repository<Game>().Get(x => x.Key == orderDetail.Game.Key);

                orderDetail.Game.UnitsInStock -= orderDetail.Quantity;

                orderDetail.OrderId = order.Id;
                orderDetails.Add(orderDetail);
                var deleted = orderDetail.IsDeleted || order.IsDeleted;

                sumOfOrder += UpdateSum(order.Status, deleted, sumOfOrder, orderDetail.Quantity * orderDetail.Price);
            }
        }

        private Basket AppendNewOrders(Basket basket, IEnumerable<OrderDto> itemOrders, ref decimal sum)
        {
            var orders = basket.Orders;
            for (int i = orders.Count; i < itemOrders.Count(); i++)
            {
                decimal sumOfOrder = 0.0M;
                var newOrder = Mapper.Map<Order>(itemOrders.ElementAt(i));
                foreach (var orderDetail in newOrder.OrderDetails)
                {
                    orderDetail.ProductId = _unitOfWork.Repository<Game>().Get(x => x.Key == orderDetail.Game.Key).Id;
                    orderDetail.Game = _unitOfWork.Repository<Game>().Get(x => x.Key == orderDetail.Game.Key);

                    orderDetail.Game.UnitsInStock -= orderDetail.Quantity;

                    orderDetail.Order = newOrder;
                    sumOfOrder += orderDetail.Price * orderDetail.Quantity;
                }

                newOrder.Summ = sumOfOrder;
                orders.Add(newOrder);
                sum += sumOfOrder;
            }
            return basket;
        }

        private Basket AddOrdersToBasket(Basket basket, BasketDto item)
        {
            decimal summ = 0.0M;
            var list = new List<Order>();

            foreach (var order in item.Orders)
            {
                decimal sumOfOrder = 0.0M;
                var newOrder = Mapper.Map<Order>(order);
                foreach (var orderDetail in newOrder.OrderDetails)
                {
                    orderDetail.ProductId = _unitOfWork.Repository<Game>().Get(x => x.Key == orderDetail.Game.Key).Id;
                    orderDetail.Game = _unitOfWork.Repository<Game>().Get(x => x.Key == orderDetail.Game.Key);

                    orderDetail.Game.UnitsInStock -= orderDetail.Quantity;

                    orderDetail.Order = newOrder;
                    sumOfOrder += orderDetail.Price * orderDetail.Quantity;
                }

                newOrder.Summ = sumOfOrder;
                list.Add(newOrder);
                summ += sumOfOrder;
            }

            basket.Orders = list;
            basket.Summ = summ;
            return basket;
        }

        private decimal UpdateSum(OrderStatus status, bool isDeleted, decimal summ, decimal updated)
        {
            if (!isDeleted && !(status == OrderStatus.Paid)) return summ + updated;
            return summ;
        }

        public bool IfBasketExistsForCurrentUser(Guid id)
        {
            var buskets = GetAll();
            if (buskets != null)
            {
                var busket = Get(x => x.UserId == id && !x.IsDeleted);

                if (busket != null)
                {
                    return true;
                }
            }
            return false;
        }

        public void AddOrderToBasket(OrderDto orderDto)
        {
            var basket = _unitOfWork.Repository<Basket>().Get(x => x.UserId == orderDto.UserId && !x.IsDeleted);

            var basketDto = Mapper.Map<BasketDto>(basket);
            var ordersList = basketDto.Orders.ToList();
            ordersList.Add(orderDto);
            basketDto.Orders = ordersList;

            Update(basketDto);
        }

        public void AddOrderDetails(OrderDetailDto orderDetail, OrderDto order)
        {
            var basket = Mapper.Map<BasketDto>(_unitOfWork.Repository<Basket>().Get(x => x.UserId == order.UserId));

            var orderDto = basket.Orders.First(x => x.Id == order.Id && !x.IsDeleted && !(x.Status == OrderStatus.Paid));
            var orderDetails = orderDto.OrderDetails.ToList();

            var oldDetail = orderDetails.FirstOrDefault(x => x.Price == orderDetail.Price && x.ProductKey == orderDetail.ProductKey && !x.IsDeleted);

            if (oldDetail == null)
            {
                orderDetails.Add(orderDetail);
            }
            else
            {
                oldDetail.Quantity += orderDetail.Quantity;
            }

            AdaptGameInOrderDetail(orderDetails);

            orderDto.OrderDetails = orderDetails;
            var orders = basket.Orders.ToList();
            var index = orders.FindIndex(x => x.Id == orderDto.Id);
            orders.RemoveAt(index);
            orders.Insert(index, orderDto);
            Update(basket);
        }

        public void DeleteOrderDetail(OrderDetailDto orderDetail, OrderDto order)
        {
            var basket = GetBasket(order.UserId);

            var orderDto = basket.Orders.FirstOrDefault(x => x.Id == order.Id);

            var orderDetails = orderDto.OrderDetails.ToList();
            var index = orderDetails.FindIndex(x => x.Id == orderDetail.Id);
            var orderDet = orderDetails[index];
            orderDet.IsDeleted = true;
            orderDetails.RemoveAt(index);
            orderDetails.Insert(index, orderDet);
            orderDto.OrderDetails = orderDetails;

            var orders = basket.Orders.ToList();
            var orderIdx = orders.FindIndex(x => x.Id == order.Id);
            orders.RemoveAt(orderIdx);
            orders.Insert(orderIdx, orderDto);

            basket.Orders = orders;

            Update(basket);
        }

        public void UpdateOrderDetail(OrderDetailDto orderDetail, OrderDto order)
        {
            var basket = GetBasket(order.UserId);

            var orderDto = basket.Orders.FirstOrDefault(x => x.Id == order.Id);

            var orderDetails = orderDto.OrderDetails.ToList();
            var index = orderDetails.FindIndex(x => x.Id == orderDetail.Id);
            var orderDet = orderDetails[index];

            orderDet.Quantity = orderDetail.Quantity;

            orderDetails.RemoveAt(index);
            orderDetails.Insert(index, orderDet);
            orderDto.OrderDetails = orderDetails;

            var orders = basket.Orders.ToList();
            var orderIdx = orders.FindIndex(x => x.Id == order.Id);
            orders.RemoveAt(orderIdx);
            orders.Insert(orderIdx, orderDto);

            basket.Orders = orders;

            Update(basket);
        }

        public OrderDto GetOrderFromBasket(Guid userGuid)
        {
            var order = _unitOfWork.Repository<Order>().Get(x => x.UserId == userGuid && !x.IsDeleted && !(x.Status == OrderStatus.Paid));
            var orderDto = Mapper.Map<OrderDto>(order);
            return orderDto;
        }

        public void PayOrder(OrderDto order)
        {
            var basket = Mapper.Map<BasketDto>(_unitOfWork.Repository<Basket>().Get(x => x.UserId == order.UserId && !x.IsDeleted));
            order.Status = OrderStatus.Paid;
            var orders = basket.Orders.ToList();
            var index = orders.FindIndex(x => x.Id == order.Id);
            orders.RemoveAt(index);
            orders.Insert(index, order);
            basket.Orders = orders;
            Update(basket);
        }

        public int GetOrdersCount(Guid userGuid)
        {
            return _unitOfWork.Repository<Basket>().Get(x => x.UserId == userGuid && !x.IsDeleted).Orders.Count;
        }

        public void CreateBasketForCurrentUser(Guid id)
        {
            var busket = new BasketDto();
            busket.UserId = id;
            busket.Summ = 0;
            busket.IsDeleted = false;
            Add(busket);
        }

        private void AdaptGameInOrderDetail(ICollection<OrderDetailDto> orderDetails)
        {
            foreach (var orderDetail in orderDetails)
            {
                var game = _unitOfWork.Repository<Game>().Get(x => !x.IsDeleted && x.Key == orderDetail.ProductKey);
                if (game.GetType().Name == "Game")
                {
                    var gameGenres = game.Genres.ToList();
                    var gamePlatformTypes = game.PlatformTypes.ToList();

                    Publisher publisher = _unitOfWork.Repository<Publisher>().Get(x => x.CompanyName == game.Publisher.CompanyName && !x.IsDeleted);

                    if (publisher == null)
                    {
                        publisher =
                            _unitOfWork.Repository<Publisher>()
                                .Get(y => y.PublisherLocalization.Any(z => z.CompanyName == game.Publisher.CompanyName));
                    }

                    var itemGenres = game.Genres?.ToList();
                    var itemPlatformTypes = game.PlatformTypes?.ToList();

                    gameGenres.Clear();
                    gamePlatformTypes.Clear();

                    if (game.Genres != null)
                    {
                        foreach (var gameGenre in itemGenres)
                        {
                            Genre genre = _unitOfWork.Repository<Genre>()
                                    .Get(y => y.Name.ToLower() == gameGenre.Name.ToLower() && !y.IsDeleted);
                            if (genre == null)
                            {
                                genre =
                                    _unitOfWork.Repository<Genre>()
                                        .Get(y => y.GenreLocalization.Any(z => z.Name == gameGenre.Name));
                            }

                            if (genre != null)
                            {
                                gameGenres.Add(genre);
                            }
                        }
                    }

                    if (game.PlatformTypes != null)
                    {
                        foreach (var gamePlatformType in itemPlatformTypes)
                        {
                            var platformType = _unitOfWork.Repository<PlatformType>().Get(y => y.Type == gamePlatformType.Type && !y.IsDeleted);
                            if (platformType != null)
                            {
                                gamePlatformTypes.Add(platformType);
                            }
                        }
                    }

                    game.Genres = gameGenres;
                    game.PlatformTypes = gamePlatformTypes;
                    _unitOfWork.Repository<Game>().Insert(game);
                }
            }
        }

        public void ClearBasket(Guid userGuid)
        {
            var basket = Mapper.Map<BasketDto>(_unitOfWork.Repository<Basket>().Get(x => x.UserId == userGuid && !x.IsDeleted));
            foreach (var item in basket.Orders)
            {
                if (!item.IsDeleted && !(item.Status == OrderStatus.Paid)) item.IsDeleted = true;
            }
            Update(basket);
        }

        public BasketDto GetBasket(Guid userId)
        {
            return Get(x => x.UserId == userId && !x.IsDeleted);
        }

        public IEnumerable<OrderDto> GetHistory(OrderHistoryFilterDto filter)
        {
            var pipeline = OrderPipelineConstructor.ConstructPipeline(filter);
            var expressionFilter = pipeline.Filter.FilterBodyExpression;
            var orderFilter = pipeline.OrderFilter.Order;
            var pageSize = pipeline.Pagination.CountOnPage;
            var pageNumber = pipeline.Pagination.PageNumber;

            var orders = _unitOfWork.Repository<Order>().Select(expressionFilter, orderFilter, pageNumber, pageSize);
            var orderResult = Mapper.Map<IEnumerable<OrderDto>>(orders);
            var orderDetails = new List<OrderDetail>();

            foreach (var order in orderResult)
            {
                if (order.Summ == default(decimal))
                {
                    decimal summ = 0;
                    foreach (var detail in order.OrderDetails)
                    {
                        summ += detail.Price * detail.Quantity;
                    }
                    order.Summ = summ;
                }
            }

            return orderResult;
        }

        public int GetFilteredOrdersCount(OrderHistoryFilterDto filter)
        {
            var pipeline = OrderPipelineConstructor.ConstructPipeline(filter);
            var expressionFilter = pipeline.Filter.FilterBodyExpression;

            return _unitOfWork.Repository<Order>().FilteredCount(expressionFilter);
        }

        public int GetOrdersCount()
        {
            return _unitOfWork.Repository<Order>().Count();
        }
    }
}
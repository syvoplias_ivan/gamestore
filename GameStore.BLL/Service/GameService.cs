﻿using AutoMapper;
using GameStore.BLL.Abstract;
using GameStore.BLL.Localization;
using GameStore.BLL.Pipeline;
using GameStore.BLL.Pipeline.GameFilters;
using GameStore.Common;
using GameStore.Common.Helpers;
using GameStore.DAL.Abstract;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Localization;
using GameStore.DTO;
using GameStore.DTO.Localization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;

namespace GameStore.BLL.Service
{
    public class GameService : BaseService<GameDto, Game>, IGameService
    {
        private readonly LanguageCode _defaultLanguage;
        private readonly ILocalizator<GameDto> _localizator;
        private readonly ILocalizator<GenreDto> _genreLocalizator;

        public GameService(IUnitOfWork workUnit, ILanguageConfiguration defaultLanguage,
            ILocalizator<GameDto> localizator, ILocalizator<GenreDto> genreLocalizator)
        {
            _unitOfWork = workUnit;
            _defaultLanguage = defaultLanguage.DefaultLanguage;
            _localizator = localizator;
            _genreLocalizator = genreLocalizator;
        }

        public void AddGame(GameDto item)
        {
            Add(item);
            var path = AppDomain.CurrentDomain.BaseDirectory + "/GameFiles/" + item.Key + "/";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            var filename = "game_" + item.Key + ".txt";

            using (var stream = new FileStream(path + filename, FileMode.Create))
            {
                using (var sw = new StreamWriter(stream))
                {
                    sw.WriteLine("Game with key:" + item.Key);
                }
            }
        }

        private IEnumerable<GameDto> LocalizeGames(IEnumerable<GameDto> games, LocalizationDirection locDirection)
        {
            if (games != null && games.Any())
            {
                foreach (var game in games)
                {
                    LocalizeGame(game, locDirection);
                }
            }
            return games;
        }

        private GameDto LocalizeGame(GameDto game, LocalizationDirection locDirection)
        {
            game = _localizator.Localize(game, CurrentLanguage, _defaultLanguage, locDirection);
            if (game != null)
            {
                foreach (var genre in game.Genres)
                {
                    _genreLocalizator.Localize(genre, CurrentLanguage, _defaultLanguage, locDirection);
                }
            }
            return game;
        }

        public void DeleteGame(Guid id)
        {
            var game = _unitOfWork.Repository<Game>().Get(x => x.Id == id);
            if (game != null)
            {
                _unitOfWork.Repository<Game>().Delete(game);
                _unitOfWork.SaveChanges();
            }
        }

        public void UpdateGame(GameDto item)
        {
            Update(item);
        }

        public GameDto GetGame(string key)
        {
            var game = Get(x => x.Key == key && !x.IsDeleted);
            game = LocalizeGame(game, LocalizationDirection.Out);
            return game;
        }

        public IEnumerable<GameDto> GetAllGames()
        {
            var games = GetAll();
            games = LocalizeGames(games, LocalizationDirection.Out);
            return games;
        }

        public IEnumerable<GameDto> GetGamesPage(int pageNumber, int numberOnPage)
        {
            var games = GetPage(pageNumber, numberOnPage);
            games = LocalizeGames(games, LocalizationDirection.Out);
            return games;
        }

        public void DeleteGame(string key)
        {
            Delete(x => x.Key == key && !x.IsDeleted);
        }

        public GameDto GetGame(Guid id)
        {
            var game = Get(id);
            game = LocalizeGame(game, LocalizationDirection.Out);
            return game;
        }

        public IEnumerable<GameDto> GetGameByGenre(string genreName)
        {
            var games = GetAllWithCondition(x => x.Genres.Any(y => y.Name == genreName && !y.IsDeleted) && !x.IsDeleted);
            games = LocalizeGames(games, LocalizationDirection.Out);
            return games;
        }

        public IEnumerable<GameDto> GetGameByPlatformTypes(params string[] platformTypes)
        {
            var games = GetAllWithCondition(
                    x => x.PlatformTypes.Any(y => platformTypes.Contains(y.Type.ConvertToString()) && !y.IsDeleted) && !x.IsDeleted);
            games = LocalizeGames(games, LocalizationDirection.Out);
            return games;
        }

        protected override Game PrepareForDelete(Func<Game, bool> expression)
        {
            Game itemToDelete = null;
            var temp = _unitOfWork.Repository<Game>().Get(expression);
            if (temp != null)
            {
                itemToDelete = temp;
            }
            return itemToDelete;
        }

        protected override Game PrepareForUpdate(GameDto item)
        {
            LocalizeGame(item, LocalizationDirection.In);
            var game = _unitOfWork.Repository<Game>().Get(x => x.Key == item.Key && !x.IsDeleted);

            if (game == null)
            {
                return null;
            }
            Mapper.Map(item, game);

            var publisher = _unitOfWork.Repository<Publisher>().Get(x => x.CompanyName == item.PublisherName && !x.IsDeleted) ??
                                  _unitOfWork.Repository<Publisher>()
                                      .Get(y => y.PublisherLocalization.Any(z => z.CompanyName == item.PublisherName));

            game.Publisher = publisher;
            game.PublisherId = publisher.Id;
            var gameLocalization = new List<GameTranslate>();
            var comments = new List<Comment>();

            GetLocalizations(item, game, gameLocalization);

            GetGenres(item, game);

            GetPlatforms(item, game);

            GetComments(item, game, comments);
            return game;
        }

        private void GetComments(GameDto item, Game game, List<Comment> comments)
        {
            if (item.Comments != null && item.Comments.Any())
            {
                game.Comments.Clear();
                foreach (var updatedComment in item.Comments)
                {
                    var comment =
                        _unitOfWork.Repository<Comment>()
                            .Get(y => y.Game.Key == updatedComment.GameKey && y.Id == updatedComment.Id);
                    if (comment != null)
                    {
                        comments.Add(comment);
                    }
                }

                game.Comments = comments;
            }
        }

        private void GetLocalizations(GameDto item, Game game, List<GameTranslate> gameLocalization)
        {
            if (item.GameLocalization != null && item.GameLocalization.Any())
            {
                foreach (var updatedLoc in item.GameLocalization)
                {
                    var gameLocale = game.GameLocalization.FirstOrDefault(x => x.Language == updatedLoc.Language);

                    if (gameLocale != null)
                    {
                        gameLocale.Description = updatedLoc.Description;
                        gameLocale.Name = updatedLoc.Name;
                        gameLocalization.Add(gameLocale);
                    }
                    else
                    {
                        var mappedLoc = Mapper.Map<GameTranslate>(updatedLoc);
                        gameLocalization.Add(mappedLoc);
                    }
                }

                game.GameLocalization = gameLocalization;
            }
        }

        private void GetPlatforms(GameDto item, Game game)
        {
            if (item.PlatformTypes != null && item.PlatformTypes.Any())
            {
                game.PlatformTypes.Clear();
                IEnumerable<TypeOfPlatform> platforms = item.PlatformTypes.Select(x => x.Type).ToList();

                game.PlatformTypes =
                    _unitOfWork.Repository<PlatformType>().Select(x => platforms.Contains(x.Type)).ToList();
            }
        }

        private void GetGenres(GameDto item, Game game)
        {
            if (item.Genres != null && item.Genres.Any())
            {
                game.Genres.Clear();

                IEnumerable<string> genreNames = item.Genres.Select(x => x.Name).ToList();

                game.Genres = _unitOfWork.Repository<Genre>().Select(x => genreNames.Contains(x.Name) ||
                                                                          x.GenreLocalization.Any(
                                                                              z => genreNames.Contains(z.Name)))
                    .ToList();
            }
        }

        protected override Game PrepareForAdd(GameDto item)
        {
            LocalizeGame(item, LocalizationDirection.In);
            var game = Mapper.Map<Game>(item);

            var publisher = _unitOfWork.Repository<Publisher>().Get(x => x.CompanyName == item.PublisherName) ??
                                  _unitOfWork.Repository<Publisher>()
                                      .Get(y => y.PublisherLocalization.Any(z => z.CompanyName == item.PublisherName));

            game.Publisher = publisher;
            game.PublisherId = publisher.Id;

            if (item.Genres != null)
            {
                GetGenres(item, game);
            }

            if (item.PlatformTypes != null)
            {
                GetPlatforms(item, game);
            }

            return game;
        }

        public IEnumerable<GameDto> GetFilteredGames(FilterDto filter)
        {
            IEnumerable<GameDto> result = null;

            CheckAndAdoptFilter(filter);

            var pipeline = FilterDtoToPipelineConvertor.Convert(filter);

            if (pipeline != null)
            {
                var order = pipeline.OrderFilter?.Order;
                var pageNumber = pipeline.Pagination?.PageNumber;
                var countOnPage = pipeline.Pagination?.CountOnPage;
                var filterExpression = pipeline.Filter.FilterBodyExpression;

                var filteredResult = _unitOfWork.Repository<Game>()
                    .Select(filterExpression, order, pageNumber, countOnPage);

                result = Mapper.Map<IEnumerable<GameDto>>(filteredResult);
                if (CurrentLanguage != _defaultLanguage)
                {
                    LocalizeGames(result, LocalizationDirection.Out);
                }
            }
            return result;
        }

        public int GetCountOfFilteredGames(FilterDto filter)
        {
            CheckAndAdoptFilter(filter);

            var pipeline = FilterDtoToPipelineConvertor.Convert(filter);
            var filterExpression = pipeline.Filter.FilterBodyExpression;

            return _unitOfWork.Repository<Game>().FilteredCount(filterExpression);
        }

        public int GetAllGamesCount()
        {
            return _unitOfWork.Repository<Game>().Count();
        }

        private void CheckAndAdoptFilter(FilterDto filter)
        {
            if (CurrentLanguage != _defaultLanguage)
            {
                var filterGenres = filter.Genres.ToList();
                var filterPublishers = filter.Publishers.ToList();

                var genres =
                    _unitOfWork.Repository<Genre>()
                        .Select(filter: x => x.GenreLocalization.Any(y => filterGenres.Contains(y.Name)) || filterGenres.Contains(x.Name)).ToList();

                filter.Genres = genres.Select(x => x.Name).ToList();

                var publishers =
                    _unitOfWork.Repository<Publisher>()
                        .Select(filter: x => x.PublisherLocalization.Any(y => filterPublishers.Contains(y.CompanyName)) || filterPublishers.Contains(x.CompanyName)).ToList();

                filter.Publishers = publishers.Select(x => x.CompanyName).ToList();
            }
        }
    }
}
﻿using GameStore.BLL.Abstract;
using NLog;
using System;

namespace GameStore.BLL.Service
{
    public class LoggerService : Logger, ILoggerService
    {
        private static string _loggerName;
        private bool _isTest;

        public void Debug(Exception exception, string format, params object[] args)
        {
            if (!IsDebugEnabled)
            {
                return;
            }
            var logEvent = GetLogEvent(_loggerName, LogLevel.Debug, exception, format, args);
            if (!_isTest)
            {
                Log(typeof(LoggerService), logEvent);
            }
        }

        public void Error(Exception exception, string format, params object[] args)
        {
            if (!IsErrorEnabled)
            {
                return;
            }
            var logEvent = GetLogEvent(_loggerName, LogLevel.Error, exception, format, args);
            if (!_isTest)
            {
                Log(typeof(LoggerService), logEvent);
            }
        }

        public void Fatal(Exception exception, string format, params object[] args)
        {
            if (!IsFatalEnabled)
            {
                return;
            }
            var logEvent = GetLogEvent(_loggerName, LogLevel.Fatal, exception, format, args);
            if (!_isTest)
            {
                Log(typeof(LoggerService), logEvent);
            }
        }

        public void Info(Exception exception, string format, params object[] args)
        {
            if (!IsInfoEnabled)
            {
                return;
            }
            var logEvent = GetLogEvent(_loggerName, LogLevel.Info, exception, format, args);
            if (!_isTest)
            {
                Log(typeof(LoggerService), logEvent);
            }
        }

        public void Trace(Exception exception, string format, params object[] args)
        {
            if (!IsTraceEnabled)
            {
                return;
            }
            var logEvent = GetLogEvent(_loggerName, LogLevel.Trace, exception, format, args);
            if (!_isTest)
            {
                Log(typeof(LoggerService), logEvent);
            }
        }

        public void Warn(Exception exception, string format, params object[] args)
        {
            if (!IsWarnEnabled)
            {
                return;
            }
            var logEvent = GetLogEvent(_loggerName, LogLevel.Warn, exception, format, args);
            if (!_isTest)
            {
                Log(typeof(LoggerService), logEvent);
            }
        }

        public void Debug(Exception exception)
        {
            Debug(exception, string.Empty);
        }

        public void Error(Exception exception)
        {
            Error(exception, string.Empty);
        }

        public void Fatal(Exception exception)
        {
            Fatal(exception, string.Empty);
        }

        public void Info(Exception exception)
        {
            Info(exception, string.Empty);
        }

        public void Trace(Exception exception)
        {
            Trace(exception, string.Empty);
        }

        public void Warn(Exception exception)
        {
            Warn(exception, string.Empty);
        }

        public static ILoggerService GetLoggingService(string logName)
        {
            var logger = (ILoggerService)LogManager.GetLogger(logName, typeof(LoggerService));
            return logger;
        }

        private LogEventInfo GetLogEvent(string loggerName, LogLevel level, Exception exception, string format,
            object[] args)
        {
            var assemblyProp = string.Empty;
            var classProp = string.Empty;
            var methodProp = string.Empty;
            var messageProp = string.Empty;
            var innerMessageProp = string.Empty;

            var logEvent = new LogEventInfo
                (level, loggerName, string.Format(format, args));

            if (exception != null)
            {
                assemblyProp = exception.Source;
                classProp = exception?.TargetSite?.DeclaringType != null
                    ? exception.TargetSite.DeclaringType.FullName
                    : "";
                methodProp = exception?.TargetSite?.Name;
                messageProp = exception.Message;

                if (exception.InnerException != null)
                {
                    innerMessageProp = exception.InnerException.Message;
                }
            }

            logEvent.Properties["error-source"] = assemblyProp;
            logEvent.Properties["error-class"] = classProp;
            logEvent.Properties["error-method"] = methodProp;
            logEvent.Properties["error-message"] = messageProp;
            logEvent.Properties["inner-error-message"] = innerMessageProp;

            return logEvent;
        }

        public void SetTest(bool value)
        {
            _isTest = value;
        }
    }
}
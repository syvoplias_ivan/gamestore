﻿using AutoMapper;
using GameStore.BLL.Abstract;
using GameStore.BLL.Localization;
using GameStore.Common;
using GameStore.Common.Helpers;
using GameStore.DAL.Abstract;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Localization;
using GameStore.DAL.UnitOfWork;
using GameStore.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace GameStore.BLL.Service
{
    public class PublisherService : BaseService<PublisherDto, Publisher>, IPublisherService
    {
        private readonly LanguageCode _defaultLanguage;
        private readonly ILocalizator<PublisherDto> _localizator;

        public PublisherService(IUnitOfWork unitOfWork, ILanguageConfiguration defaultLanguage, ILocalizator<PublisherDto> localizator)
        {
            _unitOfWork = unitOfWork;
            _defaultLanguage = defaultLanguage.DefaultLanguage;
            _localizator = localizator;
        }

        public void AddPublisher(PublisherDto publisher)
        {
            Add(publisher);
        }

        public IEnumerable<PublisherDto> GetAllPublishers()
        {
            var publishers = GetAll();
            publishers = LocalizePublishers(publishers, LocalizationDirection.Out);
            return publishers;
        }

        private IEnumerable<PublisherDto> LocalizePublishers(IEnumerable<PublisherDto> publishers, LocalizationDirection locDirection)
        {
            if (publishers != null && publishers.Any())
            {
                foreach (var publisher in publishers)
                {
                    LocalizePublisher(publisher, locDirection);
                }
            }
            return publishers;
        }

        private PublisherDto LocalizePublisher(PublisherDto publisher, LocalizationDirection locDirection)
        {
            publisher = _localizator.Localize(publisher, CurrentLanguage, _defaultLanguage, locDirection);
            return publisher;
        }

        public PublisherDto GetPublisherByName(string publisherName)
        {
            return Get(x => x.CompanyName.ToLower() == publisherName.ToLower() || x.PublisherLocalization.Any(y => x.CompanyName.ToLower() == publisherName.ToLower()));
        }

        protected override Publisher PrepareForAdd(PublisherDto item)
        {
            LocalizePublisher(item, LocalizationDirection.In);
            var publisher = Mapper.Map<Publisher>(item);
            return publisher;
        }

        protected override Publisher PrepareForUpdate(PublisherDto item)
        {
            LocalizePublisher(item, LocalizationDirection.In);
            Publisher publisher;
            if (item.Id != default(Guid))
            {
                publisher = _unitOfWork.Repository<Publisher>().Get(x => !x.IsDeleted && x.Id == item.Id);
            }
            else
            {
                publisher = _unitOfWork.Repository<Publisher>().Get(x => !x.IsDeleted && x.ObjectId == item.ObjectId);
            }

            var id = publisher.Id;
            var publisherGames = publisher.Games.ToList();
            var publisherLocalizations = new List<PublisherTranslate>();

            Mapper.Map(item, publisher);
            publisher.Id = id;

            GetLocalizations(item, publisher, publisherLocalizations);

            publisherGames = GetPublisherGames(item, publisher, publisherGames);

            publisher.Games = publisherGames;

            return publisher;
        }

        private List<Game> GetPublisherGames(PublisherDto item, Publisher publisher, List<Game> publisherGames)
        {
            if (item.Games != null && item.Games.Any())
            {
                if (publisher.Games != null)
                {
                    publisherGames.Clear();
                }
                else
                {
                    publisherGames = new List<Game>();
                }

                foreach (var game in item.Games)
                {
                    var gameDb = _unitOfWork.Repository<Game>().Get(x => !x.IsDeleted && x.Key == game.Key);
                    if (gameDb != null)
                    {
                        publisherGames.Add(gameDb);
                    }
                }
            }
            return publisherGames;
        }

        private void GetLocalizations(PublisherDto item, Publisher publisher, List<PublisherTranslate> publisherLocalizations)
        {
            if (item.PublisherLocalization != null && item.PublisherLocalization.Any())
            {
                foreach (var updatedLoc in item.PublisherLocalization)
                {
                    var gameLocale = publisher.PublisherLocalization.FirstOrDefault(x => x.Language == updatedLoc.Language);

                    if (gameLocale != null)
                    {
                        gameLocale.Description = updatedLoc.Description;
                        gameLocale.CompanyName = updatedLoc.CompanyName;
                        publisherLocalizations.Add(gameLocale);
                    }
                    else
                    {
                        var mappedLoc = Mapper.Map<PublisherTranslate>(updatedLoc);
                        publisherLocalizations.Add(mappedLoc);
                    }
                }

                publisher.PublisherLocalization = publisherLocalizations;
            }
        }

        public void UpdatePublisher(PublisherDto publisher)
        {
            Update(publisher);
        }

        public void DeletePublisher(PublisherDto publisher)
        {
            Delete(x => !x.IsDeleted && x.Id == publisher.Id);
        }

        public void DeletePublisher(string companyName)
        {
            Delete(x => !x.IsDeleted && x.CompanyName == companyName);
        }

        public void DeletePublisher(Guid id)
        {
            Delete(x => !x.IsDeleted && x.Id == id);
        }

        public IEnumerable<string> GetPublishers()
        {
            var publishers = GetAll();

            return publishers.Select(x => x.Name).ToList();
        }

        public IEnumerable<string> GetPublishersExcept(string publisherName)
        {
            var publishers = GetAll();

            var strings = publishers.Select(x => x.Name).ToList();
            strings.Remove(publisherName);
            return strings;
        }

        public IEnumerable<string> GetLocalizedPublishers()
        {
            var publishers = GetAllPublishers();
            var publisherStrings = publishers.Select(x => x.Name);
            return publisherStrings;
        }

        public IEnumerable<string> GetLocalizedPublishersExcept(string publisherName)
        {
            var publishers = GetAll().ToList();
            publishers.Remove(publishers.Find(x => x.Name == publisherName));

            LocalizePublishers(publishers, LocalizationDirection.Out).ToList();
            var publisherNames = publishers.Select(x => x.Name).ToList();
            return publisherNames;
        }

        public bool IsPublisherNameExists(string publisherName)
        {
            var publisher = Get(x => x.CompanyName == publisherName || x.PublisherLocalization.Any(y => y.CompanyName == publisherName));
            return publisher == null;
        }

        public PublisherDto GetPublisherById(Guid id)
        {
            return Get(x => x.Id == id);
        }
    }
}
﻿using AutoMapper;
using GameStore.BLL.Abstract;
using GameStore.Common;
using GameStore.DAL.Abstract;
using GameStore.DAL.Entities;
using GameStore.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GameStore.BLL.Service
{
    public class GenreService : BaseService<GenreDto, Genre>, IGenreService
    {
        private readonly LanguageCode _defaultLanguage;
        private readonly ILocalizator<GenreDto> _localizator;

        public GenreService(IUnitOfWork unitOfWork, ILanguageConfiguration defaultLanguage, ILocalizator<GenreDto> localizator)
        {
            _unitOfWork = unitOfWork;
            _defaultLanguage = defaultLanguage.DefaultLanguage;
            _localizator = localizator;
        }

        protected override Genre PrepareForDelete(Func<Genre, bool> expression)
        {
            var genre = _unitOfWork.Repository<Genre>().Get(expression);
            return genre;
        }

        protected override Genre PrepareForUpdate(GenreDto item)
        {
            LocalizeGenre(item, LocalizationDirection.In);
            Genre original = _unitOfWork.Repository<Genre>().Get(x => x.Id == item.Id && !x.IsDeleted);

            original.Name = item.Name;
            var parent = _unitOfWork.Repository<Genre>().Get(x => x.Name == item.ParentName && !x.IsDeleted);

            if (parent == null)
            {
                parent =
                    _unitOfWork.Repository<Genre>().Get(x => x.GenreLocalization.Any(y => y.Name == item.Name));
            }

            original.Parent = parent;
            original.ParentId = parent?.Id;
            original.IsDeleted = item.IsDeleted;

            return original;
        }

        private IEnumerable<GenreDto> LocalizeGenres(IEnumerable<GenreDto> genres, LocalizationDirection locDirection)
        {
            if (genres != null && genres.Any())
            {
                foreach (var genre in genres)
                {
                    LocalizeGenre(genre, locDirection);
                }
            }
            return genres;
        }

        private GenreDto LocalizeGenre(GenreDto genre, LocalizationDirection locDirection)
        {
            genre = _localizator.Localize(genre, CurrentLanguage, _defaultLanguage, locDirection);
            return genre;
        }

        protected override Genre PrepareForAdd(GenreDto item)
        {
            LocalizeGenre(item, LocalizationDirection.In);
            var genre = Mapper.Map<Genre>(item);
            if (genre.Parent != null)
            {
                var parent =
                    _unitOfWork.Repository<Genre>().Get(x => x.Name == item.ParentName && !x.IsDeleted);

                if (parent == null)
                {
                    parent = _unitOfWork.Repository<Genre>().Get(x => x.GenreLocalization.Any(y => y.Name == item.ParentName) && !x.IsDeleted);
                }
                genre.Parent = parent;
                genre.ParentId = parent.Id;
            }

            return genre;
        }

        public IEnumerable<GenreDto> GetAllGenres()
        {
            var genres = GetAll();
            genres = LocalizeGenres(genres, LocalizationDirection.Out);
            return genres;
        }

        public void CreateGenre(GenreDto genre)
        {
            Add(genre);
        }

        public void UpdateGenre(GenreDto genre)
        {
            Update(genre);
        }

        public void DeleteGenre(GenreDto genre)
        {
            Delete(x => !x.IsDeleted && x.Id == genre.Id);
        }

        public void DeleteGenre(string genreName)
        {
            Delete(x => !x.IsDeleted && x.Name == genreName);
        }

        public void DeleteGenre(Guid id)
        {
            Delete(x => !x.IsDeleted && x.Id == id);
        }

        public bool IsGenreNameExists(string genreName)
        {
            return _unitOfWork.Repository<Genre>().Get(x => x.Name == genreName) != null;
        }

        public GenreDto GetGenre(string genreName)
        {
            var genre = Get(x => x.Name == genreName);
            if (genre == null)
            {
                genre = Get(x => x.GenreLocalization.Any(y => y.Name == genreName));
            }
            return genre;
        }

        public IEnumerable<string> GetGenres()
        {
            var genres = GetAll();
            return genres.Select(x => x.Name).ToList();
        }

        public IEnumerable<string> GetGenresExcept(string genreName)
        {
            var genres = GetAll();
            var genreStrings = genres.Select(x => x.Name).ToList();
            genreStrings.Remove(genreName);
            return genreStrings;
        }

        public IEnumerable<string> GetLocalizedGenres()
        {
            var genres = GetAllGenres();
            return genres.Select(x => x.Name).ToList();
        }

        public IEnumerable<string> GetLocalizedGenresExcept(string genreName)
        {
            var genres = GetAll().ToList();

            genres.Remove(genres.Find(x => x.Name == genreName));

            LocalizeGenres(genres, LocalizationDirection.Out);

            return genres.Select(x => x.Name).ToList();
        }
    }
}
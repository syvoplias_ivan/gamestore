﻿using AutoMapper;
using GameStore.BLL.Abstract;
using GameStore.BLL.Localization;
using GameStore.Common;
using GameStore.Common.Helpers;
using GameStore.DAL.Abstract;
using GameStore.DAL.Entities;
using GameStore.DAL.UnitOfWork;
using GameStore.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GameStore.BLL.Service
{
    public class PlatformTypeService : BaseService<PlatformTypeDto, PlatformType>, IPlatformService
    {
        public PlatformTypeService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        protected override PlatformType PrepareForDelete(Func<PlatformType, bool> expression)
        {
            var res = _unitOfWork.Repository<PlatformType>().Get(expression);
            return res;
        }

        protected override PlatformType PrepareForUpdate(PlatformTypeDto item)
        {
            var updated = Mapper.Map<PlatformType>(item);
            var original = _unitOfWork.Repository<PlatformType>().Get(x => x.Id == item.Id && !x.IsDeleted);
            var gameIds = item.GameIds.ToList();

            original.Type = updated.Type;
            original.IsDeleted = updated.IsDeleted;

            List<Game> originalGames;
            if (original.Games != null)
            {
                originalGames = original.Games.ToList();
                originalGames.Clear();
            }
            else
            {
                originalGames = new List<Game>();
            }

            foreach (var game in updated.Games)
            {
                var newgame = _unitOfWork.Repository<Game>().Get(x => x.Key == game.Key && !x.IsDeleted);
                originalGames.Add(newgame);
            }
            original.Games = originalGames;

            return original;
        }

        protected override PlatformType PrepareForAdd(PlatformTypeDto item)
        {
            var platformType = Mapper.Map<PlatformType>(item);
            return platformType;
        }

        public IEnumerable<PlatformTypeDto> GetAllPlatforms()
        {
            return GetAll();
        }

        public void AddPlatformType(PlatformTypeDto platformTypeDto)
        {
            Add(platformTypeDto);
        }

        public void DeletePlatformType(PlatformTypeDto platformTypeDto)
        {
            Delete(x => !x.IsDeleted && x.Type == platformTypeDto.Type);
        }

        public PlatformTypeDto Get(string typeName)
        {
            return Get(x => !x.IsDeleted && x.Type == typeName.ConvertToEnum<TypeOfPlatform>());
        }
    }
}
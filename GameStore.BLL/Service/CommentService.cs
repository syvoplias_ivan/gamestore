﻿using AutoMapper;
using GameStore.BLL.Abstract;
using GameStore.BLL.Localization;
using GameStore.DAL.Abstract;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Authorizaion;
using GameStore.DAL.UnitOfWork;
using GameStore.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GameStore.BLL.Service
{
    public class CommentService : BaseService<CommentDto, Comment>, ICommentService
    {
        public CommentService(IUnitOfWork workUnit)
        {
            _unitOfWork = workUnit;
        }

        public void AddComment(CommentDto comment, string key)
        {
            comment.GameKey = key;
            Add(comment);
        }

        public IEnumerable<CommentDto> GetAllComments()
        {
            return GetAll();
        }

        public CommentDto GetComment(Guid id)
        {
            return Get(id);
        }

        public IEnumerable<CommentDto> GetCommentsPage(int pageNumber, int numberOnPage)
        {
            return GetPage(pageNumber, numberOnPage);
        }

        protected override Comment PrepareForDelete(Func<Comment, bool> expression)
        {
            var temp = _unitOfWork.Repository<Comment>().Get(expression);
            Comment comment = null;
            if (temp != null)
            {
                comment = temp;
            }
            return comment;
        }

        protected override Comment PrepareForAdd(CommentDto item)
        {
            string key = item.GameKey;
            var game = _unitOfWork.Repository<Game>().Get(x => x.Key == key && !x.IsDeleted);
            User user = _unitOfWork.Repository<User>().Get(x => x.NickName == item.User.NickName);

            var comment = Mapper.Map<Comment>(item);
            if (comment != null && game != null)
            {
                comment.GameId = game.Id;
                comment.Game = game;

                if (user != null && user.NickName != "anonym")
                {
                    comment.User = user;
                    comment.UserId = user.Id;
                }
                else
                {
                    comment.User = null;
                    comment.UserId = null;
                }

                if (game.Comments == null)
                {
                    game.Comments = new List<Comment>();
                }
                game.Comments.Add(comment);
            }
            return comment;
        }

        public IEnumerable<CommentDto> GetComments(string key)
        {
            return GetAllWithCondition(x => x.Game.Key == key && !x.IsDeleted);
        }

        public IEnumerable<CommentDto> GetAllCommentWithoutParents(string key)
        {
            return GetAllWithCondition(x => x.Game.Key == key && !x.IsDeleted && x.Parent == null);
        }

        public void DeleteComment(Guid id)
        {
            Delete(x => x.Id == id);
        }
    }
}
﻿using GameStore.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.BLL.Pipeline.OrderFilters
{
    public class OrderSelectFilter : ExpressionFilter<Order>
    {
        public OrderSelectFilter()
        {
            FilterBodyExpression = x => !x.IsDeleted && x.OrderDetails.Any(y => !y.IsDeleted);
        }
    }
}
﻿using GameStore.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.BLL.Pipeline.OrderFilters
{
    public class OrderBetweenDatesFilter : ExpressionFilter<Order>
    {
        private readonly DateTime _startDate;
        private readonly DateTime _endDate;

        public OrderBetweenDatesFilter(DateTime start, DateTime end)
        {
            if (start != default(DateTime) && end != default(DateTime) && start <= end)
            {
                _startDate = start;
                _endDate = end;
                FilterBodyExpression = x => x.OrderDate >= _startDate && x.OrderDate <= _endDate;
            }
            else
            {
                FilterBodyExpression = null;
            }
        }
    }
}
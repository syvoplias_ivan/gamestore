﻿using GameStore.BLL.Abstract;
using GameStore.DAL.Entities;
using GameStore.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.BLL.Pipeline.OrderFilters
{
    public class OrderPipelineConstructor
    {
        public static IPipeline<Order> ConstructPipeline(OrderHistoryFilterDto filter)
        {
            var pipeline = new Pipeline<Order>();

            pipeline.Register(new OrderSelectFilter());
            pipeline.Register(new OrderBetweenDatesFilter(filter.StartDate, filter.EndDate));
            pipeline.Register(new OrderSortFilter());
            pipeline.Register(new PaginationFilter(filter.NumberOnPage, filter.PageNumber));

            return pipeline;
        }
    }
}
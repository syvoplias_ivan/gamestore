﻿using GameStore.BLL.Abstract;
using GameStore.Common;
using GameStore.DAL.Entities;
using System;
using System.Linq.Expressions;

namespace GameStore.BLL.Pipeline.OrderFilters
{
    public class OrderSortFilter : IOrderFilter<Order>
    {
        private readonly Expression<Func<Order, object>> _order;

        public Expression<Func<Order, object>> Order => _order;

        public SortType SortType { get; set; }

        public OrderSortFilter()
        {
            var current = DateTime.UtcNow;
            _order = x => new { Date = current.Ticks - x.OrderDate.Ticks };
        }
    }
}
﻿using GameStore.BLL.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.BLL.Pipeline
{
    public class PaginationFilter : IPaginationFilter
    {
        private readonly int _countOnPage;
        private readonly int _pageNumber;

        public int CountOnPage => _countOnPage;

        public int PageNumber => _pageNumber;

        public PaginationFilter(string countOnPage, int pageNumber)
        {
            _pageNumber = pageNumber;

            int temp;
            if (int.TryParse(countOnPage, out temp))
            {
                _countOnPage = temp;
            }
        }
    }
}
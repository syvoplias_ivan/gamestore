﻿using GameStore.DAL.Entities;
using System.Collections.Generic;
using System.Linq;

namespace GameStore.BLL.Pipeline.GameFilters
{
    public class GameByPublisher : ExpressionFilter<Game>
    {
        private readonly IEnumerable<string> _publishers;

        public GameByPublisher(IEnumerable<string> publishers)
        {
            _publishers = publishers;
            if (_publishers != null && _publishers.Any())
            {
                FilterBodyExpression = x => _publishers.Contains(x.Publisher.CompanyName);
            }
            else
            {
                FilterBodyExpression = null;
            }
        }
    }
}
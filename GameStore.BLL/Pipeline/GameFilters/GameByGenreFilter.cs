﻿using GameStore.DAL.Entities;
using Spire.Pdf.General.Render.Font.OpenTypeFile;
using System.Collections.Generic;
using System.Linq;

namespace GameStore.BLL.Pipeline.GameFilters
{
    public class GameByGenreFilter : ExpressionFilter<Game>
    {
        private readonly IEnumerable<string> _genres;

        public GameByGenreFilter(IEnumerable<string> genres)
        {
            _genres = genres;
            if (_genres != null && _genres.Any())
            {
                FilterBodyExpression = x => x.Genres.Any(y => _genres.Contains(y.Name));
            }
            else
            {
                FilterBodyExpression = null;
            }
        }
    }
}
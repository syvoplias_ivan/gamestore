﻿using GameStore.Common;
using GameStore.Common.Helpers;
using GameStore.DAL.Entities;
using System.Collections.Generic;
using System.Linq;

namespace GameStore.BLL.Pipeline.GameFilters
{
    public class GameByPlatform : ExpressionFilter<Game>
    {
        private readonly IEnumerable<TypeOfPlatform> _platforms;

        public GameByPlatform(IEnumerable<string> platforms)
        {
            var temp = platforms;
            if (temp != null && temp.Any())
            {
                _platforms = platforms.Select(x => x.ConvertToEnum<TypeOfPlatform>()).ToList();
                FilterBodyExpression =
                    x => x.PlatformTypes.Any(y => _platforms.Contains(y.Type));
            }
            else
            {
                FilterBodyExpression = null;
            }
        }
    }
}
﻿using GameStore.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.BLL.Pipeline.GameFilters
{
    public class GameDefaultFilter : ExpressionFilter<Game>
    {
        public GameDefaultFilter()
        {
            FilterBodyExpression = x => !x.IsDeleted;
        }
    }
}
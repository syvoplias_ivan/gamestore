﻿using GameStore.DAL.Entities;
using System.Linq;

namespace GameStore.BLL.Pipeline.GameFilters
{
    public class GameByGameNameFilter : ExpressionFilter<Game>
    {
        private readonly string _namePart;

        public GameByGameNameFilter(string name)
        {
            _namePart = name;

            if (_namePart != null && _namePart.Length >= 3)
            {
                FilterBodyExpression = x => x.Name.Contains(_namePart);
            }
            else
            {
                FilterBodyExpression = null;
            }
        }
    }
}
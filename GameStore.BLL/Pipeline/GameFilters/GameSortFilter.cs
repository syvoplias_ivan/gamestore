﻿using GameStore.BLL.Abstract;
using GameStore.Common;
using GameStore.DAL.Entities;
using System;
using System.Linq.Expressions;

namespace GameStore.BLL.Pipeline.GameFilters
{
    public class GameSortFilter : IOrderFilter<Game>
    {
        private readonly SortType _sortType;

        private readonly Expression<Func<Game, object>> _order;

        public SortType SortType => _sortType;

        public Expression<Func<Game, object>> Order => _order;

        public GameSortFilter(SortType sortType)
        {
            _sortType = sortType;

            switch (_sortType)
            {
                case SortType.MostPopular:
                    _order = x => new { Visits = -x.VisitsCount };
                    break;

                case SortType.MostCommented:
                    _order = x => new { Comment = -x.Comments.Count };
                    break;

                case SortType.PriceAsc:
                    _order = x => new { PriceAsc = x.Price };
                    break;

                case SortType.PriceDesc:
                    _order = x => new { PriceDesc = -x.Price };
                    break;

                case SortType.New:
                    var current = DateTime.UtcNow;
                    _order = x => new { Date = current.Ticks - x.PublishDate.Ticks };
                    break;

                default:
                    _order = x => new { Id = x.Id };
                    break;
            }
        }
    }
}
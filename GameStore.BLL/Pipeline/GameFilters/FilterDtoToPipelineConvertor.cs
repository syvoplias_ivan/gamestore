﻿using GameStore.BLL.Abstract;
using GameStore.Common;
using GameStore.Common.Helpers;
using GameStore.DAL.Entities;
using GameStore.DTO;

namespace GameStore.BLL.Pipeline.GameFilters
{
    public class FilterDtoToPipelineConvertor
    {
        public static IPipeline<Game> Convert(FilterDto filter)
        {
            var pipeline = new Pipeline<Game>();

            pipeline.Register(new GameDefaultFilter());

            if (!string.IsNullOrEmpty(filter.Date))
            {
                pipeline.Register(new GameByDateFilter(filter.Date.ConvertToEnum<DateFilterType>()));
            }

            if (!string.IsNullOrEmpty(filter.GameName))
            {
                pipeline.Register(new GameByGameNameFilter(filter.GameName));
            }

            pipeline.Register(new GameByGenreFilter(filter.Genres));
            pipeline.Register(new GameByPlatform(filter.Platforms));
            pipeline.Register(new GameByPublisher(filter.Publishers));
            pipeline.Register(new GameByPriceRangeFilter(filter.PriceLow, filter.PriceHigh));

            if (!string.IsNullOrEmpty(filter.SortType))
            {
                pipeline.Register(new GameSortFilter(filter.SortType.ConvertToEnum<SortType>()));
            }
            else
            {
                pipeline.Register(new GameSortFilter(SortType.None));
            }

            if (!string.IsNullOrEmpty(filter.NumberOnPage) && filter.NumberOnPage != "All")
            {
                pipeline.Register(new PaginationFilter(filter.NumberOnPage, filter.PageNumber));
            }

            return pipeline;
        }
    }
}
﻿using GameStore.DAL.Entities;
using System;

namespace GameStore.BLL.Pipeline.GameFilters
{
    public class GameByPriceRangeFilter : ExpressionFilter<Game>
    {
        private const decimal LowDefault = decimal.MinusOne;
        private const decimal HighDefault = decimal.MinusOne;

        public GameByPriceRangeFilter(decimal? low, decimal? high)
        {
            var lowLimit = low ?? LowDefault;
            var highLimit = high ?? HighDefault;

            if (lowLimit != LowDefault && highLimit != HighDefault && lowLimit >= 0 && highLimit >= lowLimit)
            {
                FilterBodyExpression = x => x.Price >= low && x.Price <= highLimit;
            }
            else
            {
                FilterBodyExpression = null;
            }
        }
    }
}
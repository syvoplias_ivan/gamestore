﻿using GameStore.Common;
using GameStore.DAL.Entities;
using System;

namespace GameStore.BLL.Pipeline.GameFilters
{
    public class GameByDateFilter : ExpressionFilter<Game>
    {
        private readonly DateFilterType _date;

        public GameByDateFilter(DateFilterType date)
        {
            _date = date;

            switch (_date)
            {
                case DateFilterType.LastWeek:
                    var lastWeek = DateTime.UtcNow.AddDays(-7);
                    FilterBodyExpression = x => x.PublishDate >= lastWeek;
                    break;

                case DateFilterType.LastMonth:
                    var lastMonth = DateTime.UtcNow.AddMonths(-1);
                    FilterBodyExpression = x => x.PublishDate >= lastMonth;
                    break;

                case DateFilterType.LastYear:
                    var lastYear = DateTime.UtcNow.AddYears(-1);
                    FilterBodyExpression = x => x.PublishDate >= lastYear;
                    break;

                case DateFilterType.Last2Years:
                    var last2Year = DateTime.UtcNow.AddYears(-2);
                    FilterBodyExpression = x => x.PublishDate >= last2Year;
                    break;

                case DateFilterType.Last3Years:
                    var last3Year = DateTime.UtcNow.AddYears(-3);
                    FilterBodyExpression = x => x.PublishDate >= last3Year;
                    break;

                default:
                    FilterBodyExpression = null;
                    break;
            }
        }
    }
}
﻿using GameStore.BLL.Abstract;
using GameStore.BLL.ExpressionExtentions;
using System;
using System.Linq.Expressions;

namespace GameStore.BLL.Pipeline
{
    public class ExpressionFilter<T> : IExpressionFilter<T>
    {
        private Expression<Func<T, bool>> _filterBody;

        public Expression<Func<T, bool>> FilterBodyExpression
        {
            get { return _filterBody; }
            set { _filterBody = value; }
        }

        public IExpressionFilter<T> Execute(IExpressionFilter<T> filter)
        {
            if (filter.FilterBodyExpression != null)
            {
                if (_filterBody != null)
                {
                    _filterBody = _filterBody.And(filter.FilterBodyExpression);
                }
                else
                {
                    _filterBody = filter.FilterBodyExpression;
                }
            }
            return this;
        }
    }
}
﻿using GameStore.BLL.Abstract;
using System;

namespace GameStore.BLL.Pipeline
{
    public class Pipeline<T> : IPipeline<T>
    {
        private IExpressionFilter<T> _expression;
        private IOrderFilter<T> _orderFilter;
        private IPaginationFilter _paginationFilter;

        public IExpressionFilter<T> Filter => _expression;

        public IOrderFilter<T> OrderFilter => _orderFilter;

        public IPaginationFilter Pagination => _paginationFilter;

        public Pipeline()
        {
            _expression = new ExpressionFilter<T>();
        }

        public void Register(IExpressionFilter<T> filter)
        {
            _expression = _expression.Execute(filter);
        }

        public IPipeline<T> Process()
        {
            return this;
        }

        public void Register(IOrderFilter<T> orderFilter)
        {
            _orderFilter = orderFilter;
        }

        public void Register(IPaginationFilter paginationFilter)
        {
            _paginationFilter = paginationFilter;
        }
    }
}
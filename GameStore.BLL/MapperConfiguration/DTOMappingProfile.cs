﻿using AutoMapper;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Authorizaion;
using GameStore.DAL.Entities.Localization;
using GameStore.DTO;
using GameStore.DTO.Authentification;
using GameStore.DTO.Localization;
using System.Linq;

namespace GameStore.BLL.MapperConfiguration
{
    public class DtoMappingProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<Comment, CommentDto>()
                .ForSourceMember(x => x.Parent, opt => opt.Ignore())
                .ForMember(x => x.GameKey, opt => opt.MapFrom(y => y.Game.Key))
                .ForSourceMember(x => x.GameId, opt => opt.Ignore());

            CreateMap<Genre, GenreDto>()
                .ForSourceMember(x => x.Parent, opt => opt.Ignore())
                .ForMember(x => x.GameIds, opt => opt.MapFrom(y => y.Games.Select(z => z.Id).ToList()))
                .ForMember(x => x.ParentName, opt => opt.MapFrom(y => y.Parent.Name))
                .ForSourceMember(x => x.ParentId, opt => opt.Ignore());

            CreateMap<GenreDto, Genre>()
                .ForMember(x => x.Parent, opt => opt.MapFrom(y => new Genre() { Name = y.ParentName }))
                .ForMember(x => x.Games, opt => opt.MapFrom(opt2 => opt2.GameIds.Select(y => new Game { Id = y })))
                .ForMember(x => x.ParentId, opt => opt.Ignore())
                .ForSourceMember(x => x.ParentName, opt => opt.Ignore());

            CreateMap<CommentDto, Comment>()
                .ForMember(x => x.Parent, opt => opt.Ignore())
                .ForMember(x => x.Game, opt => opt.MapFrom(opt2 => new Game { Key = opt2.GameKey }))
                .ForMember(x => x.GameId, opt => opt.Ignore());

            CreateMap<PlatformType, PlatformTypeDto>()
                .ForMember(x => x.GameIds, opt => opt.MapFrom(opt2 => opt2.Games.Select(y => y.Id)));

            CreateMap<PlatformTypeDto, PlatformType>()
                .ForMember(x => x.Games, opt => opt.MapFrom(opt2 => opt2.GameIds.Select(y => new Game { Id = y })));

            CreateMap<OrderDetail, OrderDetailDto>()
                .ForSourceMember(x => x.Game, opt => opt.Ignore())
                .ForSourceMember(x => x.Order, opt => opt.Ignore())
                .ForMember(x => x.ProductName, opt => opt.MapFrom(y => y.Game.Name))
                .ForMember(x => x.ProductKey, opt => opt.MapFrom(y => y.Game.Key))
                .ForSourceMember(x => x.ProductId, opt => opt.Ignore());

            CreateMap<OrderDetailDto, OrderDetail>()
                .ForMember(x => x.ProductId, opt => opt.Ignore())
                .ForMember(x => x.Order, opt => opt.Ignore())
                .ForMember(x => x.Game, opt => opt.MapFrom(y => new Game() { Key = y.ProductKey }));

            CreateMap<Order, OrderDto>()
                .ForSourceMember(x => x.Basket, opt => opt.Ignore())
                .ForSourceMember(x => x.User, opt => opt.Ignore());

            CreateMap<OrderDto, Order>()
                .ForMember(x => x.Basket, opt => opt.Ignore())
                .ForMember(x => x.User, opt => opt.Ignore());

            CreateMap<Basket, BasketDto>()
                .ForSourceMember(x => x.User, opt => opt.Ignore());

            CreateMap<BasketDto, Basket>()
                .ForMember(x => x.User, opt => opt.Ignore());

            CreateMap<Game, GameDto>()
                .ForMember(x => x.Genres, opt => opt.MapFrom(opt2 => opt2.Genres))
                .ForMember(x => x.PlatformTypes, opt => opt.MapFrom(opt2 => opt2.PlatformTypes))
                .ForSourceMember(x => x.Publisher, opt => opt.Ignore())
                .ForMember(x => x.PublisherName, opt => opt.MapFrom(y => y.Publisher.CompanyName));

            CreateMap<GameDto, Game>()
                .ForMember(x => x.Genres, opt => opt.MapFrom(opt2 => opt2.Genres))
                .ForMember(x => x.PlatformTypes, opt => opt.MapFrom(opt2 => opt2.PlatformTypes))
                .ForMember(x => x.Publisher, opt => opt.MapFrom(y => new Publisher() { CompanyName = y.PublisherName }))
                .ForSourceMember(x => x.PublisherName, opt => opt.Ignore());

            CreateMap<Publisher, PublisherDto>()
                .ForMember(x => x.Name, opt => opt.MapFrom(y => y.CompanyName))
                .ForSourceMember(x => x.CompanyName, opt => opt.Ignore());

            CreateMap<PublisherDto, Publisher>()
                .ForMember(x => x.CompanyName, opt => opt.MapFrom(y => y.Name))
                .ForSourceMember(x => x.Name, opt => opt.Ignore());

            CreateMap<PublisherTranslate, PublisherTranslateDto>()
                .ForSourceMember(x => x.Publisher, opt => opt.Ignore());

            CreateMap<GameTranslate, GameTranslateDto>()
                .ForSourceMember(x => x.Game, opt => opt.Ignore());

            CreateMap<GenreTranslate, GenreTranslateDto>()
                .ForSourceMember(x => x.Genre, opt => opt.Ignore());

            CreateMap<PublisherTranslateDto, PublisherTranslate>()
                .ForMember(x => x.Publisher, opt => opt.Ignore());

            CreateMap<GameTranslateDto, GameTranslate>()
                .ForMember(x => x.Game, opt => opt.Ignore());

            CreateMap<GenreTranslateDto, GenreTranslate>()
                .ForMember(x => x.Genre, opt => opt.Ignore());

            CreateMap<User, UserDto>();
            CreateMap<UserDto, User>();

            CreateMap<Role, RoleDto>()
                .ForSourceMember(x => x.Users, opt => opt.Ignore());

            CreateMap<RoleDto, Role>()
                .ForMember(x => x.Users, opt => opt.Ignore());
        }
    }
}
﻿using GameStore.DAL.MongoDb.MongoMapper;

namespace GameStore.BLL.MongoMapConfiguration
{
    public class MongoMapperInitializer
    {
        public static void Initialize()
        {
            MongoMapper.ConfigureBsonMapper();
        }
    }
}
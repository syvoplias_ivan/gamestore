﻿using GameStore.BLL.Abstract;
using GameStore.BLL.Abstract.Auth;
using GameStore.BLL.Authentication;
using GameStore.BLL.Localization;
using GameStore.BLL.PaymentStrategy;
using GameStore.BLL.Service;
using GameStore.DAL.Abstract;
using GameStore.DAL.Context;
using GameStore.DAL.MongoDb;
using GameStore.DAL.MongoDb.MongoLog;
using GameStore.DAL.Repository;
using GameStore.DAL.UnitOfWork;
using GameStore.DTO;
using Ninject;

namespace GameStore.BLL.DI
{
    public class NinjectInitializer
    {
        public static void Initialize(IKernel kernel)
        {
            kernel.Bind<IGameStoreContext>().To<GameStoreContext>();
            kernel.Bind<IMongoNorthwindContext>().To<MongoContext>();
            kernel.Bind<IUnitOfWork>().To<UnitOfWork>();
            kernel.Bind<IMongoLogger>().To<MongoLogger>();
            kernel.Bind<IGameService>().To<GameService>();
            kernel.Bind<ICommentService>().To<CommentService>();
            kernel.Bind<IGenreService>().To<GenreService>();
            kernel.Bind<IPlatformService>().To<PlatformTypeService>();
            kernel.Bind<IPublisherService>().To<PublisherService>();
            kernel.Bind<IAuthenticationService>().To<AuthenticationService>();
            kernel.Bind<IOrderService>().To<OrderService>();
            kernel.Bind<IUserService>().To<UserService>();
            kernel.Bind<IBasketService>().To<BasketService>();
            kernel.Bind<IPaymentStrategy>().To<BankStrategy>();
            kernel.Bind<IPaymentStrategy>().To<VisaStrategy>();
            kernel.Bind<IPaymentStrategy>().To<TerminalStrategy>();
            kernel.Bind<IPaymentProvider>().To<PaymentProvider>();
            kernel.Bind<ILocalizator<GameDto>>().To<GameLocalizator>();
            kernel.Bind<ILocalizator<GenreDto>>().To<GenreLocalizator>();
            kernel.Bind<ILocalizator<PublisherDto>>().To<PublisherLocalizator>();
        }
    }
}
﻿using GameStore.App_LocalResources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GameStore.ViewModels.AuthViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "Login_NickName")]
        public string NickName { get; set; }

        [Required(ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "Login_Password")]
        public string Password { get; set; }

        public bool IsPersistent { get; set; }
    }
}
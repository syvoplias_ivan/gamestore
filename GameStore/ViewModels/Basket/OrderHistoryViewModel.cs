﻿using System.Collections.Generic;

namespace GameStore.ViewModels.Basket
{
    public class OrderHistoryViewModel
    {
        public HistoryListViewModel HistoryList { get; set; }

        public OrderFilterViewModel Filter { get; set; }

        public int FilteredCount { get; set; }

        public int NumberOnPage { get; set; }
    }
}
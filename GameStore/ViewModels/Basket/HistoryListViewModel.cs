﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameStore.ViewModels.Basket
{
    public class HistoryListViewModel
    {
        public IEnumerable<OrderViewModel> Orders { get; set; }
        public bool CanEdit { get; set; }
    }
}
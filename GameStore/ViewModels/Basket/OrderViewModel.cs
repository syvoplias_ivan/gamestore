﻿using GameStore.App_LocalResources;
using GameStore.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GameStore.ViewModels.Basket
{
    public class OrderViewModel
    {
        public Guid UserId { get; set; }

        [Display(Name = "OrderDate", ResourceType = typeof(GlobalRes))]
        public DateTime OrderDate { get; set; }

        public DateTime? ShippedDate { get; set; }

        public IEnumerable<OrderDetailViewModel> OrderDetails { get; set; }

        [Display(Name = "TypeOfPayment", ResourceType = typeof(GlobalRes))]
        public TypeOfPayment PaymentType { get; set; }

        public Guid BasketId { get; set; }

        [Display(Name = "SumOfOrder", ResourceType = typeof(GlobalRes))]
        public decimal Summ { get; set; }

        [Display(Name = "OrderStatus", ResourceType = typeof(GlobalRes))]
        public OrderStatus Status { get; set; }

        public Guid Id { get; set; }
    }
}
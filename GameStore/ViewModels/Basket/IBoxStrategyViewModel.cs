﻿using GameStore.App_LocalResources;
using System;
using System.ComponentModel.DataAnnotations;

namespace GameStore.ViewModels.Basket
{
    public class IBoxStrategyViewModel
    {
        [Display(Name = "AccountNumber", ResourceType = typeof(GlobalRes))]
        public Guid AccountNumber { get; set; }

        [Display(Name = "InvoiceNumber", ResourceType = typeof(GlobalRes))]
        public Guid InvoiceNumber { get; set; }

        [Display(Name = "SumOfOrder", ResourceType = typeof(GlobalRes))]
        public decimal Summ { get; set; }
    }
}
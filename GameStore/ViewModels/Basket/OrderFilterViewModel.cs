﻿using System;
using System.Collections.Generic;

namespace GameStore.ViewModels.Basket
{
    public class OrderFilterViewModel
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public string NumberOnPage { get; set; }

        public IEnumerable<ListItem<string>> NumbersOnPage { get; set; }

        public int PageNumber { get; set; }

        public bool CanEdit { get; set; }
    }
}
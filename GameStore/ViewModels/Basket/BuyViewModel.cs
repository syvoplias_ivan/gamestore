﻿using GameStore.App_LocalResources;
using System.ComponentModel.DataAnnotations;

namespace GameStore.ViewModels.Basket
{
    public class BuyViewModel
    {
        [Required(ErrorMessageResourceName = "BuyQuantityRequired", ErrorMessageResourceType = typeof(GlobalRes))]
        [Range(minimum: 1, maximum: 65500, ErrorMessageResourceName = "BuyQuantityRange", ErrorMessageResourceType = typeof(GlobalRes))]
        public short Quantity { get; set; }

        [Required]
        public string GameKey { get; set; }
    }
}
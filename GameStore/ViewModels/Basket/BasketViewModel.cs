﻿using System.Collections.Generic;

namespace GameStore.ViewModels.Basket
{
    public class BasketViewModel
    {
        public decimal Summ { get; set; }
        public IEnumerable<OrderDetailViewModel> OrderDetails { get; set; }
    }
}
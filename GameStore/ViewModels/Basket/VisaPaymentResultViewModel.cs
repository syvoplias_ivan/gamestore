﻿using GameStore.App_LocalResources;
using System;
using System.ComponentModel.DataAnnotations;

namespace GameStore.ViewModels.Basket
{
    public class VisaPaymentResultViewModel
    {
        public VisaViewModel Data { get; set; }

        [Display(Name = "OrderDate", ResourceType = typeof(GlobalRes))]
        public DateTime OrderDate { get; set; }

        [Display(Name = "SumOfOrder", ResourceType = typeof(GlobalRes))]
        public decimal OrderSumm { get; set; }
    }
}
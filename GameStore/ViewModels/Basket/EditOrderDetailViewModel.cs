﻿using GameStore.App_LocalResources;
using System;
using System.ComponentModel.DataAnnotations;

namespace GameStore.ViewModels.Basket
{
    public class EditOrderDetailViewModel
    {
        [Required(ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "OrderDetailProductRequired")]
        [RegularExpression(pattern: "^[a-zA-Zа-яА-Я0-9-:!? ]+$", ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "OrderDetailProductRegex")]
        [MaxLength(length: 60, ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "OrderDetailProductMaxLen")]
        [MinLength(length: 4, ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "OrderDetailProductMinLen")]
        [Display(Name = "ProductName", ResourceType = typeof(GlobalRes))]
        public string ProductName { get; set; }

        [Required(ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "OrderDetailQuantityRequired")]
        [Range(minimum: 1, maximum: 65500, ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "OrderDetailQuantityRange")]
        [Display(Name = "OrderDetailQuantity", ResourceType = typeof(GlobalRes))]
        public short Quantity { get; set; }

        [Required]
        public Guid Id { get; set; }
    }
}
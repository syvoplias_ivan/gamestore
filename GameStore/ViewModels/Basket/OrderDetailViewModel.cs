﻿using GameStore.App_LocalResources;
using System;
using System.ComponentModel.DataAnnotations;

namespace GameStore.ViewModels.Basket
{
    public class OrderDetailViewModel
    {
        [Display(Name = "ProductName", ResourceType = typeof(GlobalRes))]
        public string ProductName { get; set; }

        [Display(Name = "OrderDetailPrice", ResourceType = typeof(GlobalRes))]
        public decimal Price { get; set; }

        [Display(Name = "OrderDetailQuantity", ResourceType = typeof(GlobalRes))]
        public short Quantity { get; set; }

        [Display(Name = "OrderDetailDiscount", ResourceType = typeof(GlobalRes))]
        public float Discount { get; set; }

        [Display(Name = "OrderId", ResourceType = typeof(GlobalRes))]
        public Guid OrderId { get; set; }

        public Guid Id { get; set; }
    }
}
﻿using GameStore.App_LocalResources;
using GameStore.Attributes;
using System.ComponentModel.DataAnnotations;

namespace GameStore.ViewModels.Basket
{
    public class VisaViewModel
    {
        [Required(ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "VisaCartHoldersNameRequired")]
        [MinLength(length: 2, ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "VisaCartHoldersNameMinLen")]
        [MaxLength(length: 20, ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "VisaCartHoldersNameMaxlen")]
        [Display(Name = "CartHoldersName", ResourceType = typeof(GlobalRes))]
        public string CartHoldersName { get; set; }

        [Required(ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "VisaCardRequired")]
        [MaxLength(length: 16, ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "VisaCardMaxLen")]
        [MinLength(length: 16, ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "VisaCardMinLen")]
        [RegularExpression(@"^\d+$", ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "VisaCardRegex")]
        [ValidateCardNumber(ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "VisaCardValidate")]
        [Display(Name = "CardNumber", ResourceType = typeof(GlobalRes))]
        public string CardNumber { get; set; }

        [Required(ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "VisaDateRequired")]
        [RegularExpression(@"^(\d\d)/(\d\d\d\d)$", ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "VisaDateRegex")]
        [ValidateCardDate(ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "VisaDateValidate")]
        [Display(Name = "DateOfExpiry", ResourceType = typeof(GlobalRes))]
        public string DateOfExpiry { get; set; }

        [Required(ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "VisaCVVRequired")]
        [RegularExpression(@"^(\d\d\d)$", ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "VisaCVVRegex")]
        [MinLength(length: 3, ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "VisaCVVMinLen")]
        [Display(Name = "CVVCode", ResourceType = typeof(GlobalRes))]
        public string CVV2OrCVC2Code { get; set; }
    }
}
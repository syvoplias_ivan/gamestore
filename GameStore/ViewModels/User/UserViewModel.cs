﻿using GameStore.Common;
using GameStore.Common.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameStore.ViewModels.User
{
    public class UserViewModel
    {
        public Guid Id { get; set; }

        public bool IsDeleted { get; set; }

        public string NickName { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string FirstName { get; set; }

        public string Lastname { get; set; }

        public Gender Gender { get; set; }

        public string MiddleName { get; set; }

        public IEnumerable<RoleCode> Roles { get; set; }
    }
}
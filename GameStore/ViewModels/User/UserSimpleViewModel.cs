﻿using GameStore.Common;
using GameStore.Common.Auth;
using System.Collections.Generic;

namespace GameStore.ViewModels.User
{
    public class UserSimpleViewModel
    {
        public bool IsDeleted { get; set; }

        public string NickName { get; set; }

        public string Email { get; set; }

        public Gender Gender { get; set; }

        public IEnumerable<RoleCode> Roles { get; set; }
    }
}
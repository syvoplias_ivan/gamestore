﻿using GameStore.Common;
using GameStore.Common.Auth;
using System;
using System.Collections.Generic;

namespace GameStore.ViewModels.User
{
    public class UpdateUserViewModel
    {
        public Guid Id { get; set; }

        public bool IsDeleted { get; set; }

        public string NickName { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string FirstName { get; set; }

        public string Lastname { get; set; }

        public Gender Gender { get; set; }

        public string MiddleName { get; set; }

        public IEnumerable<ListItem<string>> Roles { get; set; }

        public IEnumerable<string> SelectedRoles { get; set; }
    }
}
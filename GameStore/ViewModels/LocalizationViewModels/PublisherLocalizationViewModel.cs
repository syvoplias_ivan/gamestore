﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameStore.ViewModels.LocalizationViewModels
{
    public class PublisherLocalizationViewModel
    {
        public string CompanyName { get; set; }

        public string Description { get; set; }

        public LanguageCode Language { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameStore.ViewModels.LocalizationViewModels
{
    public class GenreLocalizationViewModel
    {
        public string Name { get; set; }

        public LanguageCode Language { get; set; }
    }
}
﻿using GameStore.App_LocalResources;
using System;
using System.ComponentModel.DataAnnotations;

namespace GameStore.ViewModels.Comment
{
    public class CommentViewModel
    {
        [Required]
        public Guid IdGuid { get; set; }

        [Required]
        public int CommentId { get; set; }

        [Required(ErrorMessageResourceName = "CommentAuthorRequired", ErrorMessageResourceType = typeof(GlobalRes))]
        public string AuthorName { get; set; }

        [Required(ErrorMessageResourceName = "CommentBodyRequired", ErrorMessageResourceType = typeof(GlobalRes))]
        public string Body { get; set; }

        [Required(ErrorMessageResourceName = "CommentQuoteRequired", ErrorMessageResourceType = typeof(GlobalRes))]
        public string Quote { get; set; }

        [Required]
        public string AnswerId { get; set; }

        [Required(ErrorMessageResourceName = "CommentParentAuthorRequired", ErrorMessageResourceType = typeof(GlobalRes))]
        public string ParentCommentAuthorName { get; set; }

        [Required(ErrorMessageResourceName = "СommentDateRequired", ErrorMessageResourceType = typeof(GlobalRes))]
        public DateTime CommentDate { get; set; }

        public Guid UserId { get; set; }

        [Required]
        public int CommentLevel { get; set; }

        [Required]
        public string GameKey { get; set; }
    }
}
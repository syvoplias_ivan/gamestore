﻿using GameStore.App_LocalResources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GameStore.ViewModels.Comment
{
    public class BanViewModel
    {
        [Required(ErrorMessageResourceName = "BanDurationRequired", ErrorMessageResourceType = typeof(GlobalRes))]
        public string BanDuration { get; set; }

        public IEnumerable<ListItem<string>> BanDurations { get; set; }
        public ListItem<string> SelectedBan { get; set; }
    }
}
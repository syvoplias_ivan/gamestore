﻿using GameStore.App_LocalResources;
using System;
using System.ComponentModel.DataAnnotations;

namespace GameStore.ViewModels.Comment
{
    public class AddCommentViewModel
    {
        [Required(ErrorMessageResourceName = "CommentNameRequired", ErrorMessageResourceType = typeof(GlobalRes))]
        [MaxLength(length: 25, ErrorMessageResourceName = "CommentNameMaxLen", ErrorMessageResourceType = typeof(GlobalRes))]
        [MinLength(length: 2, ErrorMessageResourceName = "CommentNameMaxLen", ErrorMessageResourceType = typeof(GlobalRes))]
        public string Name { get; set; }

        [Required]
        [RegularExpression(pattern: @"^[a-zA-Zа-яА-Я0-9,\.\!\?@()\|\/\\\[\]&\-;\s]+$", ErrorMessageResourceName = "СommentBodyRegex", ErrorMessageResourceType = typeof(GlobalRes))]
        [MaxLength(length: 500, ErrorMessageResourceName = "CommentBodyMaxlen", ErrorMessageResourceType = typeof(GlobalRes))]
        public string Body { get; set; }

        public Guid UserId { get; set; }

        public string Quote { get; set; }

        public string GameKey { get; set; }

        public Guid? ParentId { get; set; }
    }
}
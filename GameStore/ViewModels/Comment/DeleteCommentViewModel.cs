﻿using System;

namespace GameStore.ViewModels.Comment
{
    public class DeleteCommentViewModel
    {
        public Guid Id { get; set; }

        public bool OkCancel { get; set; }

        public string GameKey { get; set; }
    }
}
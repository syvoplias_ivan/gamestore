﻿using System;
using System.Collections.Generic;

namespace GameStore.ViewModels.Comment
{
    public class FullCommentsViewModel
    {
        public IEnumerable<CommentViewModel> Comments { get; set; }

        public String GameKey { get; set; }

        public AddCommentViewModel AddCommentViewModel { get; set; }
    }
}
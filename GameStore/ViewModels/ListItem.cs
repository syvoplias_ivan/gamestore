﻿namespace GameStore.ViewModels
{
    public class ListItem<T>
    {
        public int Id { get; set; }
        public T Item { get; set; }
        public T Value { get; set; }
    }
}
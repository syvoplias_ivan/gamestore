﻿using GameStore.App_LocalResources;
using GameStore.ViewModels.LocalizationViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GameStore.ViewModels.Genre
{
    public class GenreFullViewModel
    {
        public Guid Id { get; set; }

        public bool IsDeleted { get; set; }

        [Display(ResourceType = typeof(GlobalRes), Name = "GenreNameDisplay")]
        public string Name { get; set; }

        [Display(ResourceType = typeof(GlobalRes), Name = "ParentGenreNameDisplay")]
        public string ParentName { get; set; }

        public string ObjectId { get; set; }

        public IEnumerable<GenreLocalizationViewModel> GenreLocalization { get; set; }
    }
}
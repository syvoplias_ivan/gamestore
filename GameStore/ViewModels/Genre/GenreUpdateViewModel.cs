﻿using GameStore.App_LocalResources;
using GameStore.Attributes;
using GameStore.ViewModels.LocalizationViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GameStore.ViewModels.Genre
{
    public class GenreUpdateViewModel
    {
        public Guid Id { get; set; }

        [Required]
        [MaxLength(50, ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "GenreNameMaxLen")]
        [MinLength(3, ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "GenreNameMinLen")]
        [RegularExpression(pattern: @"^[a-zA-Zа-яА-Яі0-9,\.\!\?/&\-\s]+$", ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "GenreNameRegex")]
        [NameVerify(maxDigitsCount: 3, minLettersCount: 3, ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "GenreNameNameVerify")]
        [Display(ResourceType = typeof(GlobalRes), Name = "GenreNameDisplay")]
        public string GenreName { get; set; }

        public string PreviousGenreName { get; set; }

        [RegularExpression(pattern: @"^[a-zA-Zа-яА-Яі0-9,\.\!\?&\-\s]+$", ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "GenreNameRegex")]
        [Display(ResourceType = typeof(GlobalRes), Name = "ParentGenreNameDisplay")]
        public string ParentGenreName { get; set; }

        public IEnumerable<ListItem<string>> Genres { get; set; }

        public IEnumerable<GenreLocalizationViewModel> GenreLocalization { get; set; }
    }
}
﻿using GameStore.App_LocalResources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GameStore.ViewModels.Genre
{
    public class GenreSimpleViewModel
    {
        [Display(ResourceType = typeof(GlobalRes), Name = "GenreNameDisplay")]
        public string GenreName { get; set; }

        public bool IsDeleted { get; set; }
    }
}
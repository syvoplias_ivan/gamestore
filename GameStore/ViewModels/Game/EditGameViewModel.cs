﻿using GameStore.App_LocalResources;
using GameStore.ViewModels.LocalizationViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GameStore.ViewModels.Game
{
    public class EditGameViewModel
    {
        [Required]
        public Guid Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "GameNameRequired")]
        [RegularExpression(pattern: "^[a-zA-Zа-яА-Я0-9-:!? ]+$", ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "GameNameRegex")]
        [MaxLength(length: 60, ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "GameNameMaxlen")]
        [MinLength(length: 4, ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "GameNameMinLen")]
        [Display(Name = "GameName", ResourceType = typeof(GlobalRes))]
        public string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "GameKeyRequired")]
        [RegularExpression(pattern: "^[a-zA-Z_0-9-]+$", ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "GameKeyRegex")]
        [MaxLength(length: 40, ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "GameKeyMaxLen")]
        [MinLength(length: 2, ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "GameKeyMinLen")]
        [Display(Name = "GameKey", ResourceType = typeof(GlobalRes))]
        public string Key { get; set; }

        [Required(ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "GameDescriptionRequired")]
        [MaxLength(length: 700, ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "GameDescriptionMaxLen")]
        [MinLength(length: 20, ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "GameDescriptionMinLen")]
        [Display(Name = "GameDescription", ResourceType = typeof(GlobalRes))]
        public string Description { get; set; }

        [Required(ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "GamePriceRequired")]
        [Range(minimum: 1, maximum: 1000, ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "GamePriceRange")]
        [DataType(DataType.Currency)]
        [Display(Name = "GamePrice", ResourceType = typeof(GlobalRes))]
        public decimal Price { get; set; }

        [Required(ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "GameUnitsInStrockRequired")]
        [Range(minimum: 1, maximum: 65500, ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "GameUnitsInStockRange")]
        [Display(Name = "GameUnitsInStock", ResourceType = typeof(GlobalRes))]
        public short UnitsInStock { get; set; }

        [Required]
        [Display(Name = "GameDiscontinued", ResourceType = typeof(GlobalRes))]
        public bool Discontinued { get; set; }

        [Required(ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "GamePublisherRequired")]
        [Display(Name = "PublisherName", ResourceType = typeof(GlobalRes))]
        public string PublisherName { get; set; }

        [Required(ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "GameReleaseDateRequired")]
        [DataType(DataType.Date)]
        [Display(Name = "GameRelease", ResourceType = typeof(GlobalRes))]
        public DateTime ReleaseDate { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [Display(Name = "GamePublish", ResourceType = typeof(GlobalRes))]
        public DateTime PublishDate { get; set; }

        [Required(ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "GameGenresRequired")]
        [Display(Name = "GameGenres", ResourceType = typeof(GlobalRes))]
        public IEnumerable<string> SelectedGenreIds { get; set; }

        public string ObjectId { get; set; }

        [Required(ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "GamePlatformsRequired")]
        [Display(Name = "GamePlatforms", ResourceType = typeof(GlobalRes))]
        public IEnumerable<string> SelectedPlatformTypeIds { get; set; }

        public IEnumerable<ListItem<string>> SelectedPlatforms { get; set; }

        public IEnumerable<ListItem<string>> SelectedGenres { get; set; }

        public IEnumerable<ListItem<string>> Genres { get; set; }

        public IEnumerable<ListItem<string>> PlatformTypes { get; set; }

        public IEnumerable<ListItem<string>> PublisherNames { get; set; }

        public IEnumerable<GameLocalizationViewModel> GameLocalization { get; set; }
    }
}
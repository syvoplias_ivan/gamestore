﻿using GameStore.App_LocalResources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GameStore.ViewModels.Game
{
    public class FilterViewModel
    {
        public IEnumerable<ListItem<string>> Genres { get; set; }

        [Required]
        public IEnumerable<int> SelectedGenres { get; set; }

        public IEnumerable<ListItem<string>> Platforms { get; set; }

        [Required]
        public IEnumerable<int> SelectedPlatforms { get; set; }

        public IEnumerable<ListItem<string>> Publishers { get; set; }

        [Required]
        public IEnumerable<int> SelectedPublishers { get; set; }

        public IEnumerable<ListItem<string>> NumbersOnPage { get; set; }

        public IEnumerable<int> SelectedNumberOnPage { get; set; }

        public IEnumerable<ListItem<string>> SelectedSortType { get; set; }

        public string SortType { get; set; }

        public IEnumerable<ListItem<string>> SortTypes { get; set; }

        public string Date { get; set; }

        public IEnumerable<ListItem<string>> DateFilterTypes { get; set; }

        public IEnumerable<int> SelectedFilterTypes { get; set; }

        [Range(0, 1000000, ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "FilterPriceLowRange")]
        public decimal? PriceLow { get; set; }

        [Range(0, 1000000, ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "FilterPriceHighRange")]
        public decimal? PriceHigh { get; set; }

        [Display(Name = "GameNameFilter", ResourceType = typeof(GlobalRes))]
        public string GameName { get; set; }

        [Display(Name = "NumberOnPageFilter", ResourceType = typeof(GlobalRes))]
        public string NumberOnPage { get; set; }

        public int PageNumber { get; set; }
    }
}
﻿using GameStore.App_LocalResources;
using GameStore.ViewModels.Comment;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GameStore.ViewModels.Game
{
    public class GameViewModel
    {
        public Guid Id { get; set; }

        [Required]
        [Display(Name = "GameName", ResourceType = typeof(GlobalRes))]
        public string Name { get; set; }

        [Required]
        [Display(Name = "GameKey", ResourceType = typeof(GlobalRes))]
        public string Key { get; set; }

        [Required]
        [Display(Name = "GamePrice", ResourceType = typeof(GlobalRes))]
        public decimal Price { get; set; }

        [Required]
        [Display(Name = "GameUnitsInStock", ResourceType = typeof(GlobalRes))]
        public short UnitsInStock { get; set; }

        [Required]
        [Display(Name = "GameDiscontinued", ResourceType = typeof(GlobalRes))]
        public bool Discontinued { get; set; }

        [Required]
        [Display(Name = "GameDescription", ResourceType = typeof(GlobalRes))]
        public string Description { get; set; }

        [Required]
        [Display(Name = "PublisherName", ResourceType = typeof(GlobalRes))]
        public string PublisherName { get; set; }

        [Required]
        [Display(Name = "GameRelease", ResourceType = typeof(GlobalRes))]
        public DateTime ReleaseDate { get; set; }

        [Required]
        [Display(Name = "GameRelease", ResourceType = typeof(GlobalRes))]
        public DateTime PublishDate { get; set; }

        [Required]
        [Display(Name = "GameGenres", ResourceType = typeof(GlobalRes))]
        public IEnumerable<string> Genres { get; set; }

        [Required]
        [Display(Name = "GamePlatforms", ResourceType = typeof(GlobalRes))]
        public IEnumerable<string> PlatformTypes { get; set; }

        [Required]
        public FullCommentsViewModel FullCommentsViewModel { get; set; }
    }
}
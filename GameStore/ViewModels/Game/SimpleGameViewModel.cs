﻿using GameStore.App_LocalResources;
using System;
using System.ComponentModel.DataAnnotations;

namespace GameStore.ViewModels.Game
{
    public class SimpleGameViewModel
    {
        [Required]
        [Display(Name = "GameKey", ResourceType = typeof(GlobalRes))]
        public string Key { get; set; }

        [Required]
        [Display(Name = "GameName", ResourceType = typeof(GlobalRes))]
        public string Name { get; set; }

        [Required]
        [Display(Name = "GameDescription", ResourceType = typeof(GlobalRes))]
        public string Description { get; set; }

        [Required]
        [Display(Name = "GamePrice", ResourceType = typeof(GlobalRes))]
        public decimal Price { get; set; }

        [Required]
        [Display(Name = "GameUnitsInStock", ResourceType = typeof(GlobalRes))]
        public short UnitsInStock { get; set; }

        [Required]
        [Display(Name = "GameDiscontinued", ResourceType = typeof(GlobalRes))]
        public bool Discontinued { get; set; }

        [Required]
        [Display(Name = "GameRelease", ResourceType = typeof(GlobalRes))]
        public DateTime ReleaseDate { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [Display(Name = "GamePublish", ResourceType = typeof(GlobalRes))]
        public DateTime PublishDate { get; set; }

        [Required]
        [Display(Name = "PublisherName", ResourceType = typeof(GlobalRes))]
        public string PublisherName { get; set; }
    }
}
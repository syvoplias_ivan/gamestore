﻿using System.Collections.Generic;

namespace GameStore.ViewModels.Game
{
    public class GamesViewModel
    {
        public int PageNumber { get; set; }

        public int NumberOnPage { get; set; }

        public int TotalGamesCount { get; set; }

        public IEnumerable<SimpleGameViewModel> Games { get; set; }

        public FilterViewModel Filter { get; set; }
    }
}
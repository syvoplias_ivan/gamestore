﻿using GameStore.App_LocalResources;
using GameStore.Attributes;
using GameStore.ViewModels.LocalizationViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GameStore.ViewModels.Publisher
{
    public class PublisherViewModel
    {
        [Required(ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "PublisherNameRequired")]
        [MaxLength(length: 30, ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "PublisherNameMaxLen")]
        [MinLength(length: 3, ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "PublisherNameMinLen")]
        [RegularExpression(pattern: "^[a-zA-Z0-9- ]+$", ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "PublisherNameRegex")]
        [NameVerify(maxDigitsCount: 3, minLettersCount: 5, ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "PublisherNameVerify")]
        [Display(Name = "PublisherName", ResourceType = typeof(GlobalRes))]
        public string Name { get; set; }

        [DataType(DataType.MultilineText)]
        [MaxLength(length: 800, ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "PublisherDescriptionMaxLen")]
        [RegularExpression(pattern: @"^[a-zA-Zа-яА-Я0-9,\.\!\?@()\|\/\\\[\]&\-;\s]+$", ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "PublisherDescriptionRegex")]
        [Display(Name = "PublisherDescription", ResourceType = typeof(GlobalRes))]
        public string Description { get; set; }

        [MaxLength(length: 200, ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "PublisherHomePageMaxLen")]
        [DataType(DataType.Url)]
        [RegularExpression(pattern: @"(((https?|ftp)://(-\.)?)|(www\.))([^\s/?\.#-]+\.?)+(/[^\s]*)?$", ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "PublisherHomePageRegex")]
        [Display(Name = "PublisherHomePage", ResourceType = typeof(GlobalRes))]
        public string HomePage { get; set; }

        public Guid Id { get; set; }

        public string ObjectId { get; set; }

        public IEnumerable<PublisherLocalizationViewModel> PublisherLocalization { get; set; }
    }
}
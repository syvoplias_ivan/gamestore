﻿var target = $('#comments-block')[0];

// create an observer instance
var observer = new MutationObserver(function (mutations) {
    mutations.forEach(function (mutation) {
        var newNodes = mutation.addedNodes; // DOM NodeList
        if (newNodes !== null) { // If there are new nodes added
           if ($(newNodes).toArray().find(x => x.id.indexOf("comments") !== -1) !== undefined) {
               $("#addcomment-answer").val('');
               $("#addcomment-quote").val('');
               $("#addcomment-body").val('');

               $(".btn-answer").on("click", function (e) {
                   $("#addcomment-answer").val('');
                   $("#addcomment-quote").val('');
                   var id = e.currentTarget.id;
                   var commentId = id.lastIndexOf("-");
                   commentId = +id.substr(commentId + 1);

                   var idGuid = "#idguid-" + commentId;
                   var inputGuid = $(idGuid);
                   inputGuid = inputGuid[0].innerText;

                   var elem = $("#addcomment-answer");

                   elem[0].setAttribute("value", inputGuid);
               });

               $(".btn-quote").on("click", function (e) {
                   $("#addcomment-answer").val('');
                   $("#addcomment-quote").val('');
                   var id = e.currentTarget.id;
                   var commentId = id.lastIndexOf("-");
                   commentId = +id.substr(commentId + 1);

                   var idGuid = "#idguid-" + commentId;
                   var inputGuid = $(idGuid);
                   inputGuid = inputGuid[0].innerText;

                   var elem = $("#addcomment-answer");

                   elem[0].setAttribute("value", inputGuid);

                   commentId = "#comment-body-" + commentId;

                   var text = $(commentId)[0].innerText;

                   var quote = $("#addcomment-quote");
                   quote[0].value = text;
               });

               $(".btn-delete-comment")
               .on("click",
                   function (e) {
                       var id = e.currentTarget.id;
                       var commentId = id.lastIndexOf("-");
                       commentId = +id.substr(commentId + 1);

                       var idGuid = "#idguid-" + commentId;
                       var inputGuid = $(idGuid);
                       inputGuid = inputGuid[0].innerText;

                       var idGameKey = "#comment-gameKey-" + commentId;
                       var inputGameKey = $(idGameKey);
                       inputGameKey = inputGameKey[0].value;

                       $("#deleteCommentGuid").val(inputGuid);
                       $("#deleteCommentGameKey").val(inputGameKey);

                       $("#deleteModal").modal('show');

                       $("#deleteModalCancel").on("click",
                           function (e) {
                               $("#deleteModal").modal('hide');
                               $('.modal-backdrop').hide();
                               $("#deleteCommentGuid").val("");
                               $("#deleteCommentGameKey").val("");
                           });

                       $("#deleteModalDelete")
                           .on("click",
                               function (e) {
                                   $("#deleteModal").modal('hide');
                                   $('.modal-backdrop').hide();

                                   var guidDelete = $("#deleteCommentGuid").val();
                                   var gamekeyDelete = $("#deleteCommentGameKey").val();

                                   $.ajax({
                                       method: "POST",
                                       url: "/comment/delete",
                                       data: { id: guidDelete, gameKey: gamekeyDelete }
                                   })
                                   .done(function (data) {
                                       $("#deleteCommentGuid").val("");
                                       $("#deleteCommentGameKey").val("");
                                       $("#comments")[0].outerHTML = data;
                                   }
                                   );
                               }
                           );
                   }
               );
           }
        }
    });
});

// configuration of the observer:
var config = { attributes: true, childList: true, subtree: true, characterData: true };

// pass in the target node, as well as the observer options
observer.observe(target, config);

$(".btn-answer").on("click", function (e) {
    $("#addcomment-answer").val('');
    $("#addcomment-quote").val('');
    var id = e.currentTarget.id;
    var commentId = id.lastIndexOf("-");
    commentId = +id.substr(commentId + 1);

    var idGuid = "#idguid-" + commentId;
    var inputGuid = $(idGuid);
    inputGuid = inputGuid[0].innerText;

    var elem = $("#addcomment-answer");

    elem[0].setAttribute("value", inputGuid);
});

$(".btn-quote").on("click", function (e) {
    $("#addcomment-answer").val('');
    $("#addcomment-quote").val('');
    var id = e.currentTarget.id;
    var commentId = id.lastIndexOf("-");
    commentId = +id.substr(commentId + 1);

    var idGuid = "#idguid-" + commentId;
    var inputGuid = $(idGuid);
    inputGuid = inputGuid[0].innerText;

    var elem = $("#addcomment-answer");

    elem[0].setAttribute("value", inputGuid);

    commentId = "#comment-body-" + commentId;

    var text = $(commentId)[0].innerText;

    var quote = $("#addcomment-quote");
    quote[0].value = text;
});

$(".btn-delete-comment")
            .on("click",
                function (e) {
                    var id = e.currentTarget.id;
                    var commentId = id.lastIndexOf("-");
                    commentId = +id.substr(commentId + 1);

                    var idGuid = "#idguid-" + commentId;
                    var inputGuid = $(idGuid);
                    inputGuid = inputGuid[0].innerText;

                    var idGameKey = "#comment-gameKey-" + commentId;
                    var inputGameKey = $(idGameKey);
                    inputGameKey = inputGameKey[0].value;

                    $("#deleteCommentGuid").val(inputGuid);
                    $("#deleteCommentGameKey").val(inputGameKey);

                    $("#deleteModal").modal('show');

                    $("#deleteModalCancel").on("click",
                        function (e) {
                            $("#deleteModal").modal('hide');
                            $('.modal-backdrop').hide();
                            $("#deleteCommentGuid").val("");
                            $("#deleteCommentGameKey").val("");
                        });

                    $("#deleteModalDelete")
                        .on("click",
                            function (e) {
                                $("#deleteModal").modal('hide');
                                $('.modal-backdrop').hide();

                                var guidDelete = $("#deleteCommentGuid").val();
                                var gamekeyDelete = $("#deleteCommentGameKey").val();

                                $.ajax({
                                    method: "POST",
                                    url: "/comment/delete",
                                    data: { id: guidDelete, gameKey: gamekeyDelete }
                                })
                                .done(function (data) {
                                    $("#deleteCommentGuid").val("");
                                    $("#deleteCommentGameKey").val("");
                                    $("#comments")[0].outerHTML = data;
                                }
                                );
                            }
                        );
                }
            );
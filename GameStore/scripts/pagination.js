﻿var serializedForm = $("#filter-form").serialize();

$("#filter-form").on("submit",
        function (event) {
            var currentvalues = $("#filter-form").serialize();
            if (currentvalues === serializedForm) {
                event.preventDefault();
            }
        });

$(".pagination-button")
    .on("click",
        function (e) {
            e.preventDefault();
            var i = +e.target.innerHTML;
            var pageNumber = $("#PageNumber")[0];
            pageNumber.value = i;

            $("#filter-form")[0].submit();
        });

$(".pagination-first-page")
    .on("click",
        function (event) {
            event.preventDefault();
            var currentPage = $("#PageNumber")[0];
            currentPage = currentPage.value;
            if (currentPage !== 1) {
                var page = $("#PageNumber")[0];
                page.value = 1;
                $("#filter-form")[0].submit();
            }
        });

$(".pagination-last-page")
    .on("click",
        function (event) {
            event.preventDefault();
            var currentPage = $("#PageNumber")[0];
            currentPage = currentPage.value;

            var gamecount = $("#TotalFilteredCount")[0].value;
            var countOnPage = $("#CountOnPage")[0].value;
            var allPagesCount = Math.ceil((+gamecount) / (+countOnPage));

            if (+currentPage !== allPagesCount) {
                var page = $("#PageNumber")[0];
                page.value = allPagesCount;
                $("#filter-form")[0].submit();
            }
        });

$(".pagination-prev-page")
    .on("click",
        function (event) {
            event.preventDefault();
            var currentPage = $("#PageNumber")[0];
            currentPage = currentPage.value;
            if (currentPage > 1) {
                var page = $("#PageNumber")[0];
                page.value = +currentPage - 1;
                $("#filter-form")[0].submit();
            }
        });

$(".pagination-next-page")
    .on("click",
        function (event) {
            event.preventDefault();
            var currentPage = $("#PageNumber")[0];
            currentPage = currentPage.value;

            var gamecount = $("#TotalFilteredCount")[0].value;
            var countOnPage = $("#CountOnPage")[0].value;
            var allPagesCount = Math.ceil((+gamecount) / (+countOnPage));

            if (currentPage < allPagesCount) {
                var page = $("#PageNumber")[0];
                page.value = +currentPage + 1;
                $("#filter-form")[0].submit();
            }
        });
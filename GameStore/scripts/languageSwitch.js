﻿$("#language-switch")
    .change(function() {
        var newLang = $("#language-switch option:selected")[0].value;

        var langStrings = [];
        var counter = 0;
        var langs = $("#language-switch option").each(function() {
            langStrings[counter] = $(this).val();
            counter++;
        });

        var href = window.location.href;
        var langStartIntUrl = parseInt(href.indexOf(window.location.origin) + window.location.origin.length);

        var defaultLang = langStrings[0];

        var restHref = href.substr(langStartIntUrl);

        var regex = /(\/[a-z]{2}\/)/;
        var currentLang = "";

        if (regex.test(restHref)) {
            restHref = restHref.substr(3);
        } else {
            currentLang = defaultLang;
        }

        if (currentLang !== newLang && langStrings.indexOf(newLang) !== -1) {

            if (defaultLang === newLang) {
                newLang = "";
                restHref = restHref.substr(1);
            }
            var newUrl = window.location.origin + "/" + newLang + restHref;
            window.location.href = newUrl;
        }
    });
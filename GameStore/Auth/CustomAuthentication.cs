﻿using GameStore.Auth.Interfaces;
using GameStore.BLL.Abstract;
using GameStore.BLL.Abstract.Auth;
using GameStore.BLL.Authentication;
using GameStore.DTO.Authentification;
using System;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace GameStore.Auth
{
    public class CustomAuthentication : IAuthentication
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        private readonly IAuthenticationService _authenticationService;

        private const string CookieName = "__AUTH_COOKIE";

        public CustomAuthentication(IAuthenticationService service)
        {
            _authenticationService = service;
        }

        public HttpContext HttpContext { get; set; }

        private void CreateCookie(string userName, bool isPersistent = false)
        {
            var ticket = new FormsAuthenticationTicket(
                  1,
                  userName,
                  DateTime.Now,
                  DateTime.Now.Add(FormsAuthentication.Timeout),
                  isPersistent,
                  string.Empty,
                  FormsAuthentication.FormsCookiePath);

            // Encrypt the ticket.
            var encTicket = FormsAuthentication.Encrypt(ticket);

            // Create the cookie.
            var authCookie = new HttpCookie(CookieName)
            {
                Value = encTicket,
                Expires = DateTime.Now.Add(FormsAuthentication.Timeout)
            };
            HttpContext.Response.Cookies.Set(authCookie);
        }

        #region IAuthentication Members

        public UserDto Login(string userName, string password, bool isPersistent)
        {
            var user = _authenticationService.Login(userName, password);
            if (user != null)
            {
                CreateCookie(userName);
            }
            return user;
        }

        public UserDto Login(string userName)
        {
            var user = _authenticationService.Login(userName);
            if (user != null)
            {
                CreateCookie(userName);
            }
            return user;
        }

        public void LogOut()
        {
            var httpCookie = HttpContext.Response.Cookies[CookieName];
            if (httpCookie != null)
            {
                httpCookie.Value = string.Empty;
            }
        }

        private IPrincipal _currentUser;

        public IPrincipal CurrentUser
        {
            get
            {
                if (_currentUser == null)
                {
                    try
                    {
                        HttpCookie authCookie = HttpContext.Request.Cookies.Get(CookieName);
                        if (authCookie != null && !string.IsNullOrEmpty(authCookie.Value))
                        {
                            var userService = DependencyResolver.Current.GetService<IUserService>();
                            var ticket = FormsAuthentication.Decrypt(authCookie.Value);
                            _currentUser = new UserProvider(ticket.Name, userService);
                        }
                        else
                        {
                            _currentUser = new UserProvider(null, null);
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error("Failed authentication: " + ex.Message);
                        _currentUser = new UserProvider(null, null);
                    }
                }
                return _currentUser;
            }
        }

        #endregion IAuthentication Members
    }
}
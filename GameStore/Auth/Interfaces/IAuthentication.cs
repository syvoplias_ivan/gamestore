﻿using GameStore.BLL.Abstract.Auth;
using GameStore.DTO.Authentification;
using System.Security.Principal;
using System.Web;

namespace GameStore.Auth.Interfaces
{
    public interface IAuthentication
    {
        HttpContext HttpContext { get; set; }

        UserDto Login(string login, string password, bool isPersistent);

        UserDto Login(string login);

        void LogOut();

        IPrincipal CurrentUser { get; }
    }
}
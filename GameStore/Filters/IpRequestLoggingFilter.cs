﻿using GameStore.BLL.Abstract;
using Ninject;
using System.Web.Mvc;

namespace GameStore.Filters
{
    public class IpRequestLoggingFilter : ActionFilterAttribute
    {
        [Inject]
        public ILoggerService Logger { private get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var action = filterContext.ActionDescriptor.ActionName;
            var controller = filterContext.ActionDescriptor.ControllerDescriptor.ControllerType.Name;
            var request = filterContext.RequestContext.ToString();
            var ip = filterContext.RequestContext.HttpContext.Request.UserHostAddress;

            Logger.Info(string.Format("Action {0} in class {1} started executing on request {2} from user with IP:{3}", action, controller, request, ip));
            base.OnActionExecuting(filterContext);
        }
    }
}
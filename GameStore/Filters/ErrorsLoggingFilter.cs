﻿using GameStore.BLL.Abstract;
using GameStore.Helpers;
using Ninject;
using System.Web.Mvc;

namespace GameStore.Filters
{
    public class ErrorsLoggingFilter : FilterAttribute, IExceptionFilter
    {
        [Inject]
        public ILoggerService Logger { private get; set; }

        public void OnException(ExceptionContext filterContext)
        {
            if (filterContext.HttpContext.Response.StatusCode == 404)
            {
                filterContext.Result = new RedirectResult("error/notfound");
            }
            var type = filterContext.Exception.GetType().Name;
            var method = filterContext.Exception.TargetSite.Name;
            var errorInClass = filterContext.Controller.GetType().Name;
            var requestInfo = filterContext.RequestContext.GetRequestInfoForError();

            Logger.Error(filterContext.Exception, string.Format("Exception of type {0} in method {1} of class {2} occured while attempting to do {3}", type, method, errorInClass, requestInfo));
        }
    }
}
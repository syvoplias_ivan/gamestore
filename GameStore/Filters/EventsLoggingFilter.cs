﻿using GameStore.BLL.Abstract;
using GameStore.Helpers;
using Ninject;
using System.Web.Mvc;

namespace GameStore.Filters
{
    public class EventsLoggingFilter : ActionFilterAttribute
    {
        [Inject]
        public ILoggerService Logger { private get; set; }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            Logger.Debug(string.Format("Action {0} in controller {1} executed on request {2}.",
                filterContext.ActionDescriptor.ActionName,
                filterContext.ActionDescriptor.ControllerDescriptor.ControllerName,
                filterContext.RequestContext.GetRequestShortInfo()));
            base.OnActionExecuted(filterContext);
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Logger.Debug(string.Format("Action {0} in controller {1} started executing on request {2}.",
                filterContext.ActionDescriptor.ActionName,
                filterContext.ActionDescriptor.ControllerDescriptor.ControllerName,
                filterContext.RequestContext.GetRequestShortInfo()));
            base.OnActionExecuting(filterContext);
        }
    }
}
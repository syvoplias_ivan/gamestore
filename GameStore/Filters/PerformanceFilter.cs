﻿using GameStore.BLL.Abstract;
using Ninject;
using System.Diagnostics;
using System.Web.Mvc;

namespace GameStore.Filters
{
    public class PerformanceFilter : ActionFilterAttribute
    {
        [Inject]
        public ILoggerService Logger { private get; set; }

        private Stopwatch _timeStopwatch;

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            _timeStopwatch.Stop();
            Logger.Info(string.Format("Action {0} executed with time {1} ms", filterContext.ActionDescriptor.ActionName, _timeStopwatch.ElapsedMilliseconds));
            base.OnActionExecuted(filterContext);
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            _timeStopwatch = new Stopwatch();
            base.OnActionExecuting(filterContext);
        }
    }
}
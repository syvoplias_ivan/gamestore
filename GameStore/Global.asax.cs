﻿using GameStore.App_Start;
using GameStore.BLL.MongoMapConfiguration;
using GameStore.MapperConfiguration;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace GameStore
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            HtmlHelper.ClientValidationEnabled = true;
            HtmlHelper.UnobtrusiveJavaScriptEnabled = true;

            AreaRegistration.RegisterAllAreas();
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            FilterConfig.RegisterFilters(GlobalFilters.Filters);
            AutoMapperWebConfiguration.Configure();
            MongoMapperInitializer.Initialize();
        }
    }
}
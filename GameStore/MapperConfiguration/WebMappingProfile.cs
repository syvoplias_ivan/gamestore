﻿using AutoMapper;
using GameStore.Common;
using GameStore.Common.Helpers;
using GameStore.DTO;
using GameStore.DTO.Authentification;
using GameStore.DTO.Localization;
using GameStore.Helpers.EnumExtentions;
using GameStore.ViewModels;
using GameStore.ViewModels.AuthViewModels;
using GameStore.ViewModels.Basket;
using GameStore.ViewModels.Comment;
using GameStore.ViewModels.Game;
using GameStore.ViewModels.Genre;
using GameStore.ViewModels.LocalizationViewModels;
using GameStore.ViewModels.Publisher;
using GameStore.ViewModels.User;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GameStore.MapperConfiguration
{
    public class WebMappingProfile : Profile
    {
        protected override void Configure()
        {
            #region DefaultMappings

            CreateMap<GameDto, SimpleGameViewModel>().ForSourceMember(x => x.Comments, opt => opt.Ignore())
                .ForSourceMember(x => x.Genres, opt => opt.Ignore())
                .ForSourceMember(x => x.PlatformTypes, opt => opt.Ignore())
                .ForSourceMember(x => x.Id, opt => opt.Ignore())
                .ForSourceMember(x => x.PublisherId, opt => opt.Ignore())
                .ForSourceMember(x => x.PublisherName, opt => opt.Ignore())
                .ForSourceMember(x => x.IsDeleted, opt => opt.Ignore())
                .ForSourceMember(x => x.VisitsCount, opt => opt.Ignore())
                .ForSourceMember(x => x.GameLocalization, opt => opt.Ignore())
                .ForSourceMember(x => x.ObjectId, opt => opt.Ignore());

            CreateMap<SimpleGameViewModel, GameDto>().ForMember(x => x.Comments, opt => opt.Ignore())
                .ForMember(x => x.Genres, opt => opt.Ignore())
                .ForMember(x => x.PlatformTypes, opt => opt.Ignore())
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(x => x.PublisherId, opt => opt.Ignore())
                .ForMember(x => x.PublisherName, opt => opt.Ignore())
                .ForMember(x => x.IsDeleted, opt => opt.UseValue(false))
                .ForMember(x => x.VisitsCount, opt => opt.Ignore())
                .ForMember(x => x.GameLocalization, opt => opt.Ignore())
                .ForMember(x => x.ObjectId, opt => opt.Ignore());

            CreateMap<GameDto, EditGameViewModel>()
                .ForSourceMember(x => x.Comments, opt => opt.Ignore())
                .ForMember(x => x.Genres, opt => opt.Ignore())
                .ForMember(x => x.PlatformTypes, opt => opt.Ignore())
                .ForMember(x => x.SelectedGenreIds, opt => opt.MapFrom(game => game.Genres.Select(z => z.Name)))
                .ForMember(x => x.SelectedPlatformTypeIds, opt => opt.MapFrom(
                    game =>
                        game.PlatformTypes.Select(
                            z => z.Type.GetLocalizedDescription())))
                .ForSourceMember(x => x.PublisherId, opt => opt.Ignore())
                .ForMember(x => x.PublisherNames, opt => opt.Ignore())
                .ForMember(x => x.SelectedPlatforms, opt => opt.Ignore())
                .ForMember(x => x.SelectedGenres, opt => opt.Ignore())
                .ForSourceMember(x => x.IsDeleted, opt => opt.Ignore())
                .ForSourceMember(x => x.VisitsCount, opt => opt.Ignore());

            CreateMap<EditGameViewModel, GameDto>()
                .ForMember(x => x.Comments, opt => opt.Ignore())
                .ForMember(x => x.Genres,
                    opt => opt.MapFrom(opt2 => opt2.SelectedGenreIds.Select(y => new GenreDto { Name = y })))
                .ForMember(x => x.PlatformTypes,
                    opt =>
                        opt.MapFrom(
                            opt2 =>
                                opt2.SelectedPlatformTypeIds.Select(
                                    y =>
                                        new PlatformTypeDto
                                        {
                                            Type = y.ConvertLocalizedToEnum<TypeOfPlatform>()
                                        })))
                .ForMember(x => x.PublisherId, opt => opt.Ignore())
                .ForSourceMember(x => x.PublisherNames, opt => opt.Ignore())
                .ForSourceMember(x => x.SelectedGenreIds, opt => opt.Ignore())
                .ForSourceMember(x => x.SelectedPlatformTypeIds, opt => opt.Ignore())
                .ForSourceMember(x => x.SelectedPlatforms, opt => opt.Ignore())
                .ForSourceMember(x => x.SelectedGenres, opt => opt.Ignore())
                .ForMember(x => x.IsDeleted, opt => opt.Ignore())
                .ForMember(x => x.VisitsCount, opt => opt.Ignore());

            CreateMap<CreateGameViewModel, GameDto>().ForMember(x => x.Comments, opt => opt.Ignore())
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(x => x.Genres,
                    opt => opt.MapFrom(opt2 => opt2.SelectedGenres.Select(y => new GenreDto { Name = y })))
                .ForMember(x => x.PlatformTypes,
                    opt =>
                        opt.MapFrom(
                            opt2 =>
                                opt2.SelectedPlatformTypes.Select(
                                    y =>
                                        new PlatformTypeDto
                                        {
                                            Type = y.ConvertLocalizedToEnum<TypeOfPlatform>()
                                        })))
                .ForMember(x => x.PublisherId, opt => opt.Ignore())
                .ForSourceMember(x => x.Genres, opt => opt.Ignore())
                .ForSourceMember(x => x.PlatformTypes, opt => opt.Ignore())
                .ForMember(x => x.IsDeleted, opt => opt.UseValue(false))
                .ForSourceMember(x => x.PublisherNames, opt => opt.Ignore())
                .ForMember(x => x.VisitsCount, opt => opt.Ignore())
                .ForMember(x => x.PublishDate, opt => opt.UseValue(DateTime.UtcNow))
                .ForMember(x => x.ObjectId, opt => opt.Ignore());

            CreateMap<GameDto, CreateGameViewModel>()
                .ForMember(x => x.SelectedGenres, opt => opt.Ignore())
                .ForMember(x => x.SelectedPlatformTypes, opt => opt.Ignore())
                .ForSourceMember(x => x.Comments, opt => opt.Ignore())
                .ForSourceMember(x => x.Genres, opt => opt.Ignore())
                .ForSourceMember(x => x.PlatformTypes, opt => opt.Ignore())
                .ForSourceMember(x => x.Id, opt => opt.Ignore())
                .ForSourceMember(x => x.PublisherId, opt => opt.Ignore())
                .ForMember(x => x.Genres, opt => opt.Ignore())
                .ForMember(x => x.PlatformTypes, opt => opt.Ignore())
                .ForSourceMember(x => x.IsDeleted, opt => opt.Ignore())
                .ForMember(x => x.PublisherNames, opt => opt.Ignore())
                .ForSourceMember(x => x.VisitsCount, opt => opt.Ignore())
                .ForSourceMember(x => x.PublishDate, opt => opt.Ignore())
                .ForSourceMember(x => x.ObjectId, opt => opt.Ignore());

            CreateMap<GameDto, GameViewModel>()
                .ForSourceMember(x => x.Comments, opt => opt.Ignore())
                .ForMember(x => x.Genres,
                    opt =>
                        opt.MapFrom(
                            opt2 => opt2.Genres.Select(y => y.Name)))
                .ForMember(x => x.PlatformTypes,
                    opt =>
                        opt.MapFrom(
                            opt2 => opt2.PlatformTypes.Select(y => y.Type.GetLocalizedDescription())))
                .ForSourceMember(x => x.PublisherId, opt => opt.Ignore())
                .ForSourceMember(x => x.IsDeleted, opt => opt.Ignore())
                .ForMember(x => x.FullCommentsViewModel, opt => opt.Ignore())
                .ForSourceMember(x => x.VisitsCount, opt => opt.Ignore())
                .ForSourceMember(x => x.GameLocalization, opt => opt.Ignore())
                .ForSourceMember(x => x.ObjectId, opt => opt.Ignore());

            CreateMap<GameViewModel, GameDto>()
                .ForMember(x => x.Comments, opt => opt.Ignore())
                .ForMember(x => x.Genres, opt => opt.MapFrom(opt2 => opt2.Genres.Select(y => new GenreDto { Name = y })))
                .ForMember(x => x.PlatformTypes,
                    opt =>
                        opt.MapFrom(
                            opt2 =>
                                opt2.PlatformTypes.Select(
                                    y =>
                                        new PlatformTypeDto
                                        {
                                            Type = y.ConvertLocalizedToEnum<TypeOfPlatform>()
                                        })))
                .ForMember(x => x.PublisherId, opt => opt.Ignore())
                .ForMember(x => x.IsDeleted, opt => opt.UseValue(false))
                .ForMember(x => x.VisitsCount, opt => opt.Ignore())
                .ForMember(x => x.GameLocalization, opt => opt.Ignore())
                .ForMember(x => x.ObjectId, opt => opt.Ignore());

            CreateMap<AddCommentViewModel, CommentDto>()
                .ForMember(x => x.ChildComments, x => x.Ignore())
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(x => x.IsDeleted, opt => opt.UseValue(false))
                .ForMember(x => x.Date, opt => opt.UseValue(DateTime.UtcNow))
                .ForMember(x => x.User, opt => opt.MapFrom(y => new UserDto() { NickName = y.Name }));

            CreateMap<CommentDto, AddCommentViewModel>()
                .ForSourceMember(x => x.ChildComments, x => x.Ignore())
                .ForSourceMember(x => x.Id, opt => opt.Ignore())
                .ForSourceMember(x => x.IsDeleted, opt => opt.Ignore())
                .ForSourceMember(x => x.Date, opt => opt.Ignore())
                .ForSourceMember(x => x.User, opt => opt.Ignore())
                .ForMember(x => x.Name, opt => opt.Ignore());

            CreateMap<CommentDto, CommentViewModel>()
                .ForSourceMember(x => x.IsDeleted, opt => opt.Ignore())
                .ForMember(x => x.CommentId, opt => opt.Ignore())
                .ForMember(x => x.CommentLevel, opt => opt.Ignore())
                .ForMember(x => x.IdGuid, opt => opt.MapFrom(y => y.Id))
                .ForMember(x => x.AuthorName, opt => opt.MapFrom(y => y.User.NickName))
                .ForMember(x => x.AnswerId, opt => opt.Ignore())
                .ForMember(x => x.ParentCommentAuthorName, opt => opt.Ignore())
                .ForMember(x => x.CommentDate, opt => opt.MapFrom(y => y.Date))
                .ForSourceMember(x => x.User, opt => opt.Ignore());

            CreateMap<CommentViewModel, CommentDto>()
                .ForMember(x => x.IsDeleted, opt => opt.Ignore())
                .ForSourceMember(x => x.CommentId, opt => opt.Ignore())
                .ForSourceMember(x => x.CommentLevel, opt => opt.Ignore())
                .ForMember(x => x.Id, opt => opt.MapFrom(y => y.IdGuid))
                .ForMember(x => x.User, opt => opt.MapFrom(y => new UserDto() { NickName = y.AuthorName }))
                .ForSourceMember(x => x.AnswerId, opt => opt.Ignore())
                .ForSourceMember(x => x.ParentCommentAuthorName, opt => opt.Ignore())
                .ForMember(x => x.Date, opt => opt.MapFrom(y => y.CommentDate))
                .ForMember(x => x.ChildComments, opt => opt.Ignore())
                .ForMember(x => x.ParentId, opt => opt.Ignore());

            CreateMap<PublisherViewModel, PublisherDto>()
                .ForMember(x => x.Games, opt => opt.Ignore())
                .ForMember(x => x.IsDeleted, opt => opt.UseValue(false));

            CreateMap<PublisherDto, PublisherViewModel>()
                .ForSourceMember(x => x.Games, opt => opt.Ignore())
                .ForSourceMember(x => x.IsDeleted, opt => opt.Ignore());

            CreateMap<OrderDetailDto, OrderDetailViewModel>()
                .ForSourceMember(x => x.ProductKey, opt => opt.Ignore())
                .ForSourceMember(x => x.IsDeleted, opt => opt.Ignore())
                .ForSourceMember(x => x.ObjectId, opt => opt.Ignore());

            CreateMap<OrderDetailViewModel, OrderDetailDto>()
                .ForMember(x => x.ProductKey, opt => opt.Ignore())
                .ForMember(x => x.IsDeleted, opt => opt.Ignore())
                .ForMember(x => x.ObjectId, opt => opt.Ignore());

            CreateMap<OrderDto, OrderViewModel>()
                .ForSourceMember(x => x.IsDeleted, opt => opt.Ignore())
                .ForSourceMember(x => x.ObjectId, opt => opt.Ignore());

            CreateMap<OrderViewModel, OrderDto>()
                .ForMember(x => x.IsDeleted, opt => opt.Ignore())
                .ForMember(x => x.ObjectId, opt => opt.Ignore());

            CreateMap<EditOrderDetailViewModel, OrderDetailDto>()
                .ForMember(x => x.Discount, opt => opt.Ignore())
                .ForMember(x => x.IsDeleted, opt => opt.Ignore())
                .ForMember(x => x.OrderId, opt => opt.Ignore())
                .ForMember(x => x.Price, opt => opt.Ignore())
                .ForMember(x => x.ProductKey, opt => opt.Ignore())
                .ForMember(x => x.ObjectId, opt => opt.Ignore());

            CreateMap<OrderDetailDto, EditOrderDetailViewModel>()
                .ForSourceMember(x => x.Discount, opt => opt.Ignore())
                .ForSourceMember(x => x.IsDeleted, opt => opt.Ignore())
                .ForSourceMember(x => x.OrderId, opt => opt.Ignore())
                .ForSourceMember(x => x.Price, opt => opt.Ignore())
                .ForSourceMember(x => x.ProductKey, opt => opt.Ignore())
                .ForSourceMember(x => x.ObjectId, opt => opt.Ignore());

            CreateMap<GameLocalizationViewModel, GameTranslateDto>()
                .ForMember(x => x.GameId, opt => opt.Ignore())
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(x => x.IsDeleted, opt => opt.Ignore());

            CreateMap<GameTranslateDto, GameLocalizationViewModel>()
                .ForSourceMember(x => x.GameId, opt => opt.Ignore())
                .ForSourceMember(x => x.Id, opt => opt.Ignore())
                .ForSourceMember(x => x.IsDeleted, opt => opt.Ignore());

            CreateMap<GenreLocalizationViewModel, GenreTranslateDto>()
                .ForMember(x => x.GenreId, opt => opt.Ignore())
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(x => x.IsDeleted, opt => opt.Ignore());

            CreateMap<GenreTranslateDto, GenreLocalizationViewModel>()
                .ForSourceMember(x => x.GenreId, opt => opt.Ignore())
                .ForSourceMember(x => x.Id, opt => opt.Ignore())
                .ForSourceMember(x => x.IsDeleted, opt => opt.Ignore());

            CreateMap<PublisherLocalizationViewModel, PublisherTranslateDto>()
                .ForMember(x => x.PublisherId, opt => opt.Ignore())
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(x => x.IsDeleted, opt => opt.Ignore());

            CreateMap<PublisherTranslateDto, PublisherLocalizationViewModel>()
                .ForSourceMember(x => x.PublisherId, opt => opt.Ignore())
                .ForSourceMember(x => x.Id, opt => opt.Ignore())
                .ForSourceMember(x => x.IsDeleted, opt => opt.Ignore());

            #endregion DefaultMappings

            #region User Mappings

            CreateMap<RoleDto, RoleCode>()
                .ConvertUsing(x => x.RoleCode);

            CreateMap<RoleCode, RoleDto>()
                .ConvertUsing(x => new RoleDto() { RoleCode = x });

            CreateMap<AddUserViewModel, UserDto>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(x => x.Roles, opt => opt.MapFrom(y => y.SelectedRoles.Select(z => new RoleDto() { RoleCode = z.ConvertToEnum<RoleCode>() })))
                .ForSourceMember(x => x.SelectedRoles, opt => opt.Ignore());

            CreateMap<UserDto, AddUserViewModel>()
                .ForSourceMember(x => x.Id, opt => opt.Ignore())
                .ForMember(x => x.Roles, opt => opt.Ignore())
                .ForMember(x => x.SelectedRoles, opt => opt.Ignore());

            CreateMap<UpdateUserViewModel, UserDto>()
                .ForMember(x => x.Roles, opt => opt.MapFrom(y => y.SelectedRoles.Select(z => new RoleDto() { RoleCode = z.ConvertToEnum<RoleCode>() })))
                .ForSourceMember(x => x.SelectedRoles, opt => opt.Ignore());

            CreateMap<UserDto, UpdateUserViewModel>()
                .ForMember(x => x.Roles, opt => opt.Ignore())
                .ForMember(x => x.SelectedRoles, opt => opt.Ignore());

            CreateMap<UserSimpleViewModel, UserDto>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(x => x.FirstName, opt => opt.Ignore())
                .ForMember(x => x.Lastname, opt => opt.Ignore())
                .ForMember(x => x.MiddleName, opt => opt.Ignore())
                .ForMember(x => x.Password, opt => opt.Ignore());

            CreateMap<UserDto, UserSimpleViewModel>()
                .ForSourceMember(x => x.Id, opt => opt.Ignore())
                .ForSourceMember(x => x.FirstName, opt => opt.Ignore())
                .ForSourceMember(x => x.Lastname, opt => opt.Ignore())
                .ForSourceMember(x => x.MiddleName, opt => opt.Ignore())
                .ForSourceMember(x => x.Password, opt => opt.Ignore());

            CreateMap<UserViewModel, UserDto>();
            CreateMap<UserDto, UserViewModel>();

            CreateMap<RegisterViewModel, UserDto>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(x => x.Roles,
                    opt => opt.UseValue(new List<RoleDto>() { new RoleDto() { RoleCode = RoleCode.User } }))
                .ForMember(x => x.IsDeleted, opt => opt.UseValue(false));

            CreateMap<UserDto, RegisterViewModel>()
                .ForSourceMember(x => x.Id, opt => opt.Ignore())
                .ForSourceMember(x => x.Roles, opt => opt.Ignore())
                .ForSourceMember(x => x.IsDeleted, opt => opt.Ignore());

            #endregion User Mappings

            #region Genre Mappings

            CreateMap<AddGenreViewModel, GenreDto>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForSourceMember(x => x.Genres, opt => opt.Ignore())
                .ForMember(x => x.Name, opt => opt.MapFrom(y => y.GenreName))
                .ForMember(x => x.GameIds, opt => opt.Ignore())
                .ForMember(x => x.ObjectId, opt => opt.Ignore())
                .ForMember(x => x.IsDeleted, opt => opt.UseValue(false));

            CreateMap<GenreDto, AddGenreViewModel>()
                .ForSourceMember(x => x.Id, opt => opt.Ignore())
                .ForMember(x => x.Genres, opt => opt.Ignore())
                .ForMember(x => x.GenreName, opt => opt.MapFrom(y => y.Name))
                .ForSourceMember(x => x.Name, opt => opt.Ignore())
                .ForSourceMember(x => x.GameIds, opt => opt.Ignore())
                .ForSourceMember(x => x.ObjectId, opt => opt.Ignore())
                .ForSourceMember(x => x.IsDeleted, opt => opt.Ignore());

            CreateMap<GenreFullViewModel, GenreDto>()
                .ForMember(x => x.GameIds, opt => opt.Ignore());
            CreateMap<GenreDto, GenreFullViewModel>()
                .ForSourceMember(x => x.GameIds, opt => opt.Ignore());

            CreateMap<GenreSimpleViewModel, GenreDto>()
                .ForMember(x => x.Name, opt => opt.MapFrom(y => y.GenreName))
                .ForMember(x => x.IsDeleted, opt => opt.MapFrom(y => y.IsDeleted))
                .ForAllOtherMembers(opt => opt.Ignore());

            CreateMap<GenreDto, GenreSimpleViewModel>()
                .ForMember(x => x.GenreName, opt => opt.MapFrom(y => y.Name))
                .ForMember(x => x.IsDeleted, opt => opt.MapFrom(y => y.IsDeleted))
                .ForAllOtherMembers(opt => opt.Ignore());

            CreateMap<GenreDto, GenreUpdateViewModel>()
                .ForMember(x => x.Genres, opt => opt.Ignore())
                .ForMember(x => x.GenreName, opt => opt.MapFrom(y => y.Name))
                .ForMember(x => x.PreviousGenreName, opt => opt.Ignore())
                .ForMember(x => x.ParentGenreName, opt => opt.MapFrom(y => y.ParentName))
                .ForMember(x => x.GenreLocalization, opt => opt.MapFrom(y => y.GenreLocalization))
                .ForAllOtherMembers(x => x.Ignore());

            CreateMap<GenreUpdateViewModel, GenreDto>()
                .ForSourceMember(x => x.Genres, opt => opt.Ignore())
                .ForMember(x => x.Name, opt => opt.MapFrom(y => y.GenreName))
                .ForSourceMember(x => x.PreviousGenreName, opt => opt.Ignore())
                .ForMember(x => x.ParentName, opt => opt.MapFrom(y => y.ParentGenreName))
                .ForMember(x => x.GenreLocalization, opt => opt.MapFrom(y => y.GenreLocalization))
                .ForAllOtherMembers(x => x.Ignore());

            #endregion Genre Mappings
        }
    }
}
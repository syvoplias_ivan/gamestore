﻿using AutoMapper;
using GameStore.BLL.MapperConfiguration;

namespace GameStore.MapperConfiguration
{
    public static class AutoMapperWebConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<DtoMappingProfile>();
                cfg.AddProfile<WebMappingProfile>();
            });
            Mapper.AssertConfigurationIsValid();
        }
    }
}
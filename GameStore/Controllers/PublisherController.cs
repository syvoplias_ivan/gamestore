﻿using AutoMapper;
using GameStore.BLL.Abstract;
using GameStore.Common.Helpers;
using GameStore.DTO;
using GameStore.Helpers;
using GameStore.Helpers.LocalizationHelpers;
using GameStore.ViewModels;
using GameStore.ViewModels.Publisher;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Web.Mvc;

namespace GameStore.Controllers
{
    public class PublisherController : BaseController
    {
        private IPublisherService _service;

        public PublisherController(IPublisherService service)
        {
            _service = service;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var publishers = _service.GetAllPublishers();
            if (publishers == null)
            {
                var pulishersView = new List<PublisherViewModel>();
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return View(pulishersView);
            }

            var publishersView = Mapper.Map<IEnumerable<PublisherViewModel>>(publishers);
            Response.StatusCode = (int)HttpStatusCode.OK;

            return View(publishersView);
        }

        [HttpGet]
        [Authorize(Roles = "Manager")]
        public ActionResult New()
        {
            ViewBag.DefaultLanguage = Thread.CurrentThread.LanguageFromThread();
            var model = new PublisherViewModel();
            model.PublisherLocalization = EntitiesLocalizatorHelper.GetLocalizationPublisher(model.PublisherLocalization);
            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "Manager")]
        public ActionResult New(PublisherViewModel model)
        {
            if (ModelState.IsValid && !_service.IsPublisherNameExists(model.Name))
            {
                var item = Mapper.Map<PublisherDto>(model);
                _service.AddPublisher(item);
                return RedirectToAction("Get", "Publisher", new { companyName = model.Name });
            }

            ViewBag.DefaultLanguage = Thread.CurrentThread.LanguageFromThread();
            model.PublisherLocalization = EntitiesLocalizatorHelper.GetLocalizationPublisher(model.PublisherLocalization);
            Response.StatusCode = (int)HttpStatusCode.BadRequest;

            return View(model);
        }

        [Authorize(Roles = "manager")]
        public ActionResult Update(string publisherName)
        {
            ViewBag.DefaultLanguage = Thread.CurrentThread.LanguageFromThread();
            var publisherDto = _service.GetPublisherByName(publisherName);

            if (publisherDto.Id == default(Guid) && !string.IsNullOrEmpty(publisherDto.ObjectId))
            {
                _service.AddPublisher(publisherDto);
                publisherDto = _service.GetPublisherByName(publisherName);
            }

            if (publisherDto != null)
            {
                var model = Mapper.Map<PublisherViewModel>(publisherDto);
                model.PublisherLocalization = EntitiesLocalizatorHelper.GetLocalizationPublisher(model.PublisherLocalization);

                Response.StatusCode = (int)HttpStatusCode.OK;
                return View(model);
            }
            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return RedirectToAction("Index", "Genre");
        }

        [HttpPost]
        [Authorize(Roles = "manager")]
        public ActionResult Update(PublisherViewModel model)
        {
            if (ModelState.IsValid)
            {
                var publisherDto = Mapper.Map<PublisherDto>(model);
                _service.UpdatePublisher(publisherDto);
                Response.StatusCode = (int)HttpStatusCode.OK;
                return RedirectToAction("Index");
            }
            ViewBag.DefaultLanguage = Thread.CurrentThread.LanguageFromThread();
            return View(model);
        }

        [Authorize(Roles = "manager")]
        public ActionResult Delete(string companyName)
        {
            if (companyName == null)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return RedirectToAction("Index");
            }

            var publisher = _service.GetPublisherByName(companyName);

            if (!string.IsNullOrEmpty(publisher.ObjectId) && publisher.Id == default(Guid))
            {
                _service.AddPublisher(publisher);
            }

            _service.DeletePublisher(companyName);
            Response.StatusCode = (int)HttpStatusCode.OK;
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Get(string companyName)
        {
            if (String.IsNullOrEmpty(companyName))
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
            }
            var publisher = _service.GetPublisherByName(companyName);

            if (publisher == null)
            {
                Response.StatusCode = (int)HttpStatusCode.NotFound;
            }

            var model = Mapper.Map<PublisherViewModel>(publisher);

            return View(model);
        }
    }
}
﻿using AutoMapper;
using GameStore.BLL.Abstract;
using GameStore.Common;
using GameStore.Common.Helpers;
using GameStore.DTO;
using GameStore.Helpers;
using GameStore.Helpers.EnumExtentions;
using GameStore.Helpers.LocalizationHelpers;
using GameStore.ViewModels;
using GameStore.ViewModels.Comment;
using GameStore.ViewModels.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Threading;
using System.Web.Mvc;
using System.Web.UI;

namespace GameStore.Controllers
{
    public class GameController : BaseController
    {
        private readonly IGameService _gameService;
        private readonly IGenreService _genreService;
        private readonly IPlatformService _platformService;
        private readonly IPublisherService _publisherService;

        public GameController(IGameService service, IPlatformService platform, IGenreService genre, IPublisherService publisher)
        {
            this._gameService = service;

            this._genreService = genre;

            this._platformService = platform;

            this._publisherService = publisher;
        }

        // GET: Game
        [HttpGet]
        [OutputCache(Duration = 60, VaryByParam = "none", Location = OutputCacheLocation.Client, NoStore = true)]
        public ActionResult Index(FilterViewModel filter = null, int[] genres = null, int[] publishers = null, int[] platforms = null, int? date = null)
        {
            var gamesModel = new GamesViewModel();

            if (filter.PriceLow.HasValue && filter.PriceLow.Value < 0)
            {
                ModelState.AddModelError("PriceLow", "Low bound of price should be greater zero");
            }

            if (filter.PriceHigh.HasValue && filter.PriceHigh.Value < 0)
            {
                ModelState.AddModelError("PriceHigh", "High bound of price should be greater zero");
            }

            if (filter.PriceLow.HasValue && filter.PriceHigh.HasValue && filter.PriceHigh.Value < filter.PriceLow.Value)
            {
                ModelState.AddModelError("PriceHigh", "High bound of price should be greater than low bound of price");
            }

            if (filter.NumberOnPage == null && genres == null && publishers == null && platforms == null)
            {
                filter = GetDefaultFilter();
            }
            else
            {
                filter = UpdateFilterDefaultValues(filter);
                filter.SelectedGenres = genres?.ToList();
                filter.SelectedPlatforms = platforms?.ToList();
                filter.SelectedPublishers = publishers?.ToList();
                filter.SelectedGenres = genres?.ToList();
                filter.Date = filter.DateFilterTypes.FirstOrDefault(x => x.Id == (date ?? 0)).Value;

                var dateFilterIndex = filter.DateFilterTypes.FirstOrDefault(x => x.Value == filter.Date)?.Id;

                if (date != null)
                {
                    filter.SelectedFilterTypes = new List<int>() { dateFilterIndex.Value };
                }
            }

            gamesModel.TotalGamesCount = _gameService.GetAllGamesCount();

            var filterDto = FilterViewModelToFilterDtoConvertor.Convert(filter, gamesModel.TotalGamesCount);

            gamesModel.TotalGamesCount = _gameService.GetCountOfFilteredGames(filterDto);

            filterDto = FilterViewModelToFilterDtoConvertor.Convert(filter, gamesModel.TotalGamesCount);

            var games =
                Mapper.Map<IEnumerable<GameDto>, IEnumerable<SimpleGameViewModel>>(
                    _gameService.GetFilteredGames(filterDto).ToList());
            if (games != null)
            {
                Response.StatusCode = (int)HttpStatusCode.OK;

                if (filter.NumberOnPage != null && filter.NumberOnPage != "All")
                {
                    gamesModel.NumberOnPage = int.Parse(filter.NumberOnPage);
                }
                else
                {
                    gamesModel.NumberOnPage = gamesModel.TotalGamesCount;
                }

                gamesModel.Games = games;
                gamesModel.Filter = filter;
                return View(gamesModel);
            }

            gamesModel.Filter = filter;
            gamesModel.Games = new List<SimpleGameViewModel>();

            Response.StatusCode = (int)HttpStatusCode.NotFound;
            return View(gamesModel);
        }

        [HttpGet]
        [Authorize(Roles = "Manager")]
        public ActionResult Update(string key)
        {
            ViewBag.DefaultLanguage = Thread.CurrentThread.LanguageFromThread();
            var game = _gameService.GetGame(key);
            var model = Mapper.Map<EditGameViewModel>(game);

            model.GameLocalization = EntitiesLocalizatorHelper.GetLocalizationGame(model.GameLocalization);

            var genres = _genreService.GetAllGenres().Select(x => x.Name);
            var platforms = _platformService.GetAllPlatforms().Select(x => x.Type.GetLocalizedDescription());
            var publishers = _publisherService.GetAllPublishers().Select(x => x.Name);

            var selGenres = new List<ListItem<string>>();
            var selPlatf = new List<ListItem<string>>();
            foreach (var item in model.SelectedGenreIds)
            {
                selGenres.Add(new ListItem<string>() { Item = item, Value = item });
            }

            foreach (var item in model.SelectedPlatformTypeIds)
            {
                selPlatf.Add(new ListItem<string>() { Item = item, Value = item });
            }
            model.SelectedPlatforms = selPlatf;
            model.SelectedGenres = selGenres;

            var i = 0;

            var genreList = new List<ListItem<string>>();
            foreach (var item in genres.ToList())
            {
                genreList.Add(new ListItem<string>() { Id = i++, Item = item, Value = item });
            }
            model.Genres = genreList;
            i = 0;

            var platformList = new List<ListItem<string>>();
            foreach (var item in platforms.ToList())
            {
                platformList.Add(new ListItem<string>() { Id = i++, Item = item, Value = item });
            }
            model.PlatformTypes = platformList;
            i = 0;

            var publisherNamesList = new List<ListItem<string>>();
            foreach (var item in publishers.ToList())
            {
                publisherNamesList.Add(new ListItem<string>() { Id = i++, Item = item, Value = item });
            }
            model.PublisherNames = publisherNamesList;

            ViewData["PublisherNameId"] = model.PublisherNames.First(x => x.Item == game.PublisherName).Id;

            foreach (var selectedGenre in model.SelectedGenres)
            {
                selectedGenre.Id = model.Genres.First(x => x.Item == selectedGenre.Item).Id;
            }

            foreach (var selectedPlatform in model.SelectedPlatforms)
            {
                selectedPlatform.Id = model.PlatformTypes.First(x => x.Item == selectedPlatform.Item).Id;
            }

            return View(model);
        }

        [HttpPost]
        [OutputCache(Duration = 60, Location = OutputCacheLocation.Client, NoStore = true)]
        public ActionResult GetGamesCount()
        {
            var count = _gameService.GetAllGamesCount();
            return Json(new { success = true, count = count });
        }

        [HttpPost]
        [Authorize(Roles = "Manager")]
        [OutputCache(Duration = 60, VaryByParam = "game", Location = OutputCacheLocation.Client, NoStore = true)]
        public ActionResult Update(EditGameViewModel game)
        {
            if (ModelState.IsValid)
            {
                var gameDTO = Mapper.Map<GameDto>(game);
                _gameService.UpdateGame(gameDTO);
                Response.StatusCode = (int)HttpStatusCode.OK;
                return RedirectToAction("Index");
            }
            Response.StatusCode = (int)HttpStatusCode.BadRequest;

            ViewBag.DefaultLanguage = Thread.CurrentThread.LanguageFromThread();

            var genres = _genreService.GetAllGenres().Select(x => x.Name);
            var platforms = _platformService.GetAllPlatforms().Select(x => x.Type.GetLocalizedDescription());
            var publishers = _publisherService.GetAllPublishers().Select(x => x.Name);

            var selGenres = new List<ListItem<string>>();
            var selPlatf = new List<ListItem<string>>();
            foreach (var item in game.SelectedGenreIds)
            {
                selGenres.Add(new ListItem<string>() { Item = item, Value = item });
            }

            foreach (var item in game.SelectedPlatformTypeIds)
            {
                selPlatf.Add(new ListItem<string>() { Item = item, Value = item });
            }
            game.SelectedPlatforms = selPlatf;
            game.SelectedGenres = selGenres;
            var i = 0;

            var genreList = new List<ListItem<string>>();
            foreach (var item in genres.ToList())
            {
                genreList.Add(new ListItem<string>() { Id = i++, Item = item, Value = item });
            }
            game.Genres = genreList;
            i = 0;

            var platformList = new List<ListItem<string>>();
            foreach (var item in platforms.ToList())
            {
                platformList.Add(new ListItem<string>() { Id = i++, Item = item, Value = item });
            }
            game.PlatformTypes = platformList;
            i = 0;

            var publisherNamesList = new List<ListItem<string>>();
            foreach (var item in publishers.ToList())
            {
                publisherNamesList.Add(new ListItem<string>() { Id = i++, Item = item, Value = item });
            }
            game.PublisherNames = publisherNamesList;

            ViewData["PublisherNameId"] = game.PublisherNames.First(x => x.Item == game.PublisherName).Id;

            foreach (var selectedGenre in game.SelectedGenres)
            {
                selectedGenre.Id = game.Genres.First(x => x.Item == selectedGenre.Item).Id;
            }

            foreach (var selectedPlatform in game.SelectedPlatforms)
            {
                selectedPlatform.Id = game.PlatformTypes.First(x => x.Item == selectedPlatform.Item).Id;
            }

            return View(game);
        }

        [HttpGet]
        [Authorize(Roles = "Manager")]
        public ActionResult New()
        {
            ViewBag.DefaultLanguage = Thread.CurrentThread.LanguageFromThread();
            var model = new CreateGameViewModel();

            model.GameLocalization = EntitiesLocalizatorHelper.GetLocalizationGame(model.GameLocalization);

            var genres = _genreService.GetAllGenres().Select(x => x.Name);
            var platforms = _platformService.GetAllPlatforms().Select(x => x.Type.GetLocalizedDescription());
            var publishers = _publisherService.GetAllPublishers().Select(x => x.Name);

            model.Genres = genres;
            model.PlatformTypes = platforms;
            model.PublisherNames = publishers;
            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "Manager")]
        [OutputCache(Duration = 60, VaryByParam = "game", Location = OutputCacheLocation.Client, NoStore = true)]
        public ActionResult New(CreateGameViewModel game)
        {
            if (ModelState.IsValid)
            {
                var samegame = _gameService.GetGame(game.Key);
                if (samegame == null)
                {
                    var gameDTO = Mapper.Map<GameDto>(game);
                    _gameService.AddGame(gameDTO);
                    Response.StatusCode = (int)HttpStatusCode.OK;
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("Key", "The key is not unique. Please select other key and try again.");
                }
            }
            Response.StatusCode = (int)HttpStatusCode.BadRequest;

            ViewBag.DefaultLanguage = Thread.CurrentThread.LanguageFromThread();

            game.PublisherNames = _publisherService.GetAllPublishers().Select(x => x.Name);
            game.Genres = _genreService.GetAllGenres().Select(x => x.Name);
            game.PlatformTypes = _platformService.GetAllPlatforms().Select(x => x.Type.GetLocalizedDescription());

            return View(game);
        }

        [HttpGet]
        [OutputCache(Duration = 60, VaryByParam = "key", Location = OutputCacheLocation.Client, NoStore = true)]
        public ActionResult Game(string key)
        {
            if (key == null)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(HttpStatusCode.BadRequest);
            }

            var game = _gameService.GetGame(key);

            if (!string.IsNullOrEmpty(game.ObjectId) && game.Id == default(Guid))
            {
                _gameService.AddGame(game);
                game = _gameService.GetGame(key);
            }

            game.VisitsCount++;
            _gameService.UpdateGame(game);

            game = _gameService.GetGame(key);

            var simple = Mapper.Map<GameViewModel>(game);
            var comments = new FullCommentsViewModel();
            comments.GameKey = game.Key;
            comments.Comments = CommentHelper.GetCommentViewFromDto(game.Comments.Where(x => !x.IsDeleted && x.ParentId == null));
            comments.AddCommentViewModel = new AddCommentViewModel() { GameKey = game.Key };
            simple.FullCommentsViewModel = comments;

            Response.StatusCode = (int)HttpStatusCode.OK;
            return View(simple);
        }

        [Authorize(Roles = "Manager")]
        public ActionResult Remove(String key)
        {
            if (key == null)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return RedirectToAction("Index");
            }

            var game = _gameService.GetGame(key);

            if (!string.IsNullOrEmpty(game.ObjectId) && game.Id == default(Guid))
            {
                _gameService.AddGame(game);
            }

            _gameService.DeleteGame(key);
            Response.StatusCode = (int)HttpStatusCode.OK;
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Download(string key)
        {
            if (key == null)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return RedirectToRoute("Games", new { param = "games" });
            }

            if (!System.IO.File.Exists("~/GameFiles/" + key + "/game_" + key + ".txt"))
            {
                Response.StatusCode = (int)HttpStatusCode.NotFound;
                return RedirectToRoute("Games", new { param = "games" });
            }

            if (Response != null)
            {
                Response.StatusCode = (int)HttpStatusCode.OK;
            }
            return File("~/GameFiles/" + key + ".txt", MediaTypeNames.Application.Octet);
        }

        private FilterViewModel GetDefaultFilter()
        {
            var filter = new FilterViewModel();
            int i = 0;

            var genres = _genreService.GetAllGenres()
                .Select(x => new ListItem<string>() { Id = i++, Item = x.Name, Value = x.Name }).ToList();
            i = 0;
            var publishers = _publisherService.GetAllPublishers()
                .Select(x => new ListItem<string>() { Id = i++, Item = x.Name, Value = x.Name }).ToList();
            i = 0;
            var platforms = _platformService.GetAllPlatforms()
                .Select(x => new ListItem<string>() { Id = i++, Item = x.Type.GetLocalizedDescription(), Value = x.Type.ConvertToString() }).ToList();
            i = 0;
            var sortFilter = SortType.MostCommented.GetAllValuesOfEnum()
                .Select(x => new ListItem<string>() { Id = i++, Item = x.GetLocalizedDescription(), Value = x.ConvertToString() }).ToList();
            i = 0;
            var dateFilter = DateFilterType.Last2Years.GetAllValuesOfEnum()
                .Select(x => new ListItem<string>() { Id = i++, Item = x.GetLocalizedDescription(), Value = x.ConvertToString() }).ToList();

            filter.Genres = genres;
            filter.DateFilterTypes = dateFilter;
            filter.SortType = SortType.New.ConvertToString();
            filter.SortTypes = sortFilter;
            filter.NumbersOnPage = PagesCounts.GetCountOnPage();
            filter.PageNumber = 1;
            filter.NumberOnPage = 3.ToString();
            filter.Platforms = platforms;
            filter.Publishers = publishers;

            return filter;
        }

        private FilterViewModel UpdateFilterDefaultValues(FilterViewModel filter)
        {
            int i = 0;

            var genres = _genreService.GetAllGenres()
                .Select(x => new ListItem<string>() { Id = i++, Item = x.Name, Value = x.Name }).ToList();
            i = 0;

            var publishers = _publisherService.GetAllPublishers()
                .Select(x => new ListItem<string>() { Id = i++, Item = x.Name, Value = x.Name }).ToList();
            i = 0;

            List<ListItem<string>> platforms = _platformService.GetAllPlatforms()
                .Select(x => new ListItem<string>() { Id = i++, Item = x.Type.GetLocalizedDescription(), Value = x.Type.ConvertToString() }).ToList();
            i = 0;

            var sortFilter = SortType.MostCommented.GetAllValuesOfEnum()
                .Select(x => new ListItem<string>() { Id = i++, Item = x.GetLocalizedDescription(), Value = x.ConvertToString() }).ToList();
            i = 0;

            var dateFilter = DateFilterType.Last2Years.GetAllValuesOfEnum()
                .Select(x => new ListItem<string>() { Id = i++, Item = x.GetLocalizedDescription(), Value = x.ConvertToString() }).ToList();

            filter.Genres = genres;
            filter.DateFilterTypes = dateFilter;
            filter.SortTypes = sortFilter;
            filter.SortType = filter.SortType.ConvertToEnum<SortType>().ConvertToString();
            filter.NumbersOnPage = PagesCounts.GetCountOnPage();
            if (filter.NumberOnPage == null) filter.NumberOnPage = 3.ToString();
            filter.Platforms = platforms;
            filter.Publishers = publishers;

            if (filter.PageNumber == 0)
            {
                filter.PageNumber = 1;
            }

            return filter;
        }
    }
}
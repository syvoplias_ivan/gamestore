﻿using GameStore.Auth.Interfaces;
using GameStore.BLL.Abstract.Auth;
using GameStore.DTO.Authentification;
using Ninject;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace GameStore.Controllers
{
    public class BaseController : Controller
    {
        private IAuthentication _authentication;

        [Inject]
        public IAuthentication Auth
        {
            get { return _authentication; }
            set
            {
                _authentication = value;
                _authentication.HttpContext = System.Web.HttpContext.Current;
            }
        }

        public UserDto CurrentUser
        {
            get
            {
                return ((IUserIdentity)Auth.CurrentUser.Identity).User;
            }
        }

        public string CurrentLangCode { get; protected set; }

        protected override void Initialize(RequestContext requestContext)
        {
            if (requestContext.RouteData.Values["lang"] != null && requestContext.RouteData.Values["lang"] as string != "null")
            {
                CurrentLangCode = requestContext.RouteData.Values["lang"] as string;

                var ci = new CultureInfo(CurrentLangCode);
                Thread.CurrentThread.CurrentUICulture = ci;
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(ci.Name);
            }

            base.Initialize(requestContext);
        }
    }
}
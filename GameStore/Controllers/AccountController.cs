﻿using AutoMapper;
using GameStore.App_LocalResources;
using GameStore.BLL.Abstract;
using GameStore.DTO.Authentification;
using GameStore.ViewModels.AuthViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace GameStore.Controllers
{
    public class AccountController : BaseController
    {
        private IUserService _service;

        public AccountController(IUserService userService)
        {
            _service = userService;
        }

        public ActionResult UserLogin()
        {
            return PartialView(CurrentUser);
        }

        public ActionResult Login()
        {
            return View(new LoginViewModel());
        }

        public ActionResult Register()
        {
            return View(new RegisterViewModel());
        }

        [HttpPost]
        public ActionResult Register(RegisterViewModel registerModel)
        {
            if (ModelState.IsValid)
            {
                if (!_service.IsUserNickNameExist(registerModel.NickName))
                {
                    var user = Mapper.Map<UserDto>(registerModel);
                    _service.CreateUser(user);
                    Response.StatusCode = (int)HttpStatusCode.OK;
                    return RedirectToAction("Index", "Game");
                }
                else
                {
                    ModelState.AddModelError("NickName", GlobalRes.NicknameExistsError);
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                }
            }
            return View(registerModel);
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel loginModel)
        {
            if (ModelState.IsValid)
            {
                var user = Auth.Login(loginModel.NickName, loginModel.Password, loginModel.IsPersistent);
                if (user != null)
                {
                    return RedirectToAction("Index", "Game");
                }
                ModelState["Password"].Errors.Add(GlobalRes.PasswordsAreNotEqualError);
            }
            return View(loginModel);
        }

        public ActionResult Logout()
        {
            Auth.LogOut();
            return RedirectToAction("Index", "Game");
        }
    }
}
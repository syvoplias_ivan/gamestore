﻿using AutoMapper;
using GameStore.BLL.Abstract;
using GameStore.DTO.Authentification;
using GameStore.Helpers;
using GameStore.Helpers.EnumExtentions;
using GameStore.ViewModels.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace GameStore.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class UserController : BaseController
    {
        private readonly IUserService _userService;

        public UserController(IUserService service)
        {
            _userService = service;
        }

        public ActionResult Index()
        {
            var users = _userService.GetAllUsers();
            if (users != null)
            {
                var models = Mapper.Map<IEnumerable<UserSimpleViewModel>>(users);
                Response.StatusCode = (int)HttpStatusCode.OK;
                return View(models);
            }

            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return RedirectToAction("Index", "Game");
        }

        [Authorize(Roles = "administrator")]
        public ActionResult Update(string nickName)
        {
            var user = _userService.GetUser(nickName);

            if (user != null)
            {
                var model = Mapper.Map<UpdateUserViewModel>(user);

                var roles = _userService.GetAllRoles();
                var localized = _userService.GetAllRoleCodes().Select(x => x.GetLocalizedDescription());
                model.Roles = ListStringToListListItemConverter.GetListItems(roles, localized);
                model.SelectedRoles = _userService.GetUsersRoles(user.Id);

                Response.StatusCode = (int)HttpStatusCode.OK;
                return View(model);
            }

            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return RedirectToAction("Index", "User");
        }

        [HttpPost]
        [Authorize(Roles = "administrator")]
        public ActionResult Update(UpdateUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userDto = Mapper.Map<UserDto>(model);
                _userService.UpdateUser(userDto);
                Response.StatusCode = (int)HttpStatusCode.OK;

                return RedirectToAction("Index");
            }

            var roles = _userService.GetAllRoles();
            var localized = _userService.GetAllRoleCodes().Select(x => x.GetLocalizedDescription());
            model.Roles = ListStringToListListItemConverter.GetListItems(roles, localized);

            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return View(model);
        }

        [Authorize(Roles = "administrator")]
        public ActionResult New()
        {
            var model = new AddUserViewModel();
            var roles = _userService.GetAllRoles();
            var localized = _userService.GetAllRoleCodes().Select(x => x.GetLocalizedDescription());
            model.Roles = ListStringToListListItemConverter.GetListItems(roles, localized);

            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "administrator")]
        public ActionResult New(AddUserViewModel model)
        {
            if (ModelState.IsValid && !_userService.IsUserNickNameExist(model.NickName))
            {
                var userDto = Mapper.Map<UserDto>(model);
                _userService.CreateUser(userDto);
                return RedirectToAction("Index");
            }

            var roles = _userService.GetAllRoles();
            var localized = _userService.GetAllRoleCodes().Select(x => x.GetLocalizedDescription());
            model.Roles = ListStringToListListItemConverter.GetListItems(roles, localized);
            return View(model);
        }

        public ActionResult Get(string nickName)
        {
            var user = _userService.GetUser(nickName);

            if (user != null)
            {
                var model = Mapper.Map<UserViewModel>(user);
                return View(model);
            }

            Response.StatusCode = (int)HttpStatusCode.NotFound;
            return RedirectToAction("Index", "User");
        }

        [HttpPost]
        [Authorize(Roles = "administrator")]
        public ActionResult Delete(string nickName)
        {
            try
            {
                _userService.DeleteUser(nickName);
            }
            catch (Exception exception)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
            }
            return RedirectToAction("Index", "User");
        }
    }
}
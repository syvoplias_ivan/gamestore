﻿using AutoMapper;
using GameStore.BLL.Abstract;
using GameStore.Common.Helpers;
using GameStore.DTO;
using GameStore.Helpers;
using GameStore.Helpers.LocalizationHelpers;
using GameStore.ViewModels;
using GameStore.ViewModels.Genre;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web.Mvc;

namespace GameStore.Controllers
{
    public class GenreController : BaseController
    {
        private IGenreService _genreService;

        public GenreController(IGenreService service)
        {
            _genreService = service;
        }

        public ActionResult Index()
        {
            var genres = _genreService.GetAllGenres();
            if (genres != null)
            {
                var model = Mapper.Map<IEnumerable<GenreSimpleViewModel>>(genres);
                Response.StatusCode = (int)HttpStatusCode.OK;
                return View(model);
            }

            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return RedirectToAction("Index", "Game");
        }

        [Authorize(Roles = "Manager")]
        public ActionResult New()
        {
            ViewBag.DefaultLanguage = Thread.CurrentThread.LanguageFromThread();
            var model = new AddGenreViewModel();
            model.Genres = ListStringToListListItemConverter.GetListItems(_genreService.GetGenres(), _genreService.GetLocalizedGenres());
            model.GenreLocalization = EntitiesLocalizatorHelper.GetLocalizationGenre(model.GenreLocalization);

            return View(model);
        }

        [Authorize(Roles = "Manager")]
        [HttpPost]
        public ActionResult New(AddGenreViewModel model)
        {
            if (ModelState.IsValid && !_genreService.IsGenreNameExists(model.GenreName))
            {
                var genreDto = Mapper.Map<GenreDto>(model);
                _genreService.CreateGenre(genreDto);
                Response.StatusCode = (int)HttpStatusCode.OK;
                return RedirectToAction("Index");
            }
            ViewBag.DefaultLanguage = Thread.CurrentThread.LanguageFromThread();
            model.Genres = ListStringToListListItemConverter.GetListItems(_genreService.GetGenres(), _genreService.GetLocalizedGenres());

            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return View(model);
        }

        [Authorize(Roles = "Manager")]
        public ActionResult Update(string genreName)
        {
            ViewBag.DefaultLanguage = Thread.CurrentThread.LanguageFromThread();
            var genre = _genreService.GetGenre(genreName);
            if (genre != null)
            {
                var model = Mapper.Map<GenreUpdateViewModel>(genre);
                model.Genres =
                    ListStringToListListItemConverter.GetListItems(_genreService.GetGenresExcept(genreName), _genreService.GetLocalizedGenresExcept(genreName));
                model.GenreLocalization = EntitiesLocalizatorHelper.GetLocalizationGenre(model.GenreLocalization);

                Response.StatusCode = (int)HttpStatusCode.OK;
                return View(model);
            }
            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return RedirectToAction("Index", "Genre");
        }

        [HttpPost]
        public ActionResult Update(GenreUpdateViewModel model)
        {
            if (ModelState.IsValid)
            {
                var genreDto = Mapper.Map<GenreDto>(model);
                _genreService.UpdateGenre(genreDto);
                Response.StatusCode = (int)HttpStatusCode.OK;
                return RedirectToAction("Index");
            }
            ViewBag.DefaultLanguage = Thread.CurrentThread.LanguageFromThread();
            model.Genres =
                ListStringToListListItemConverter.GetListItems(_genreService.GetGenresExcept(model.PreviousGenreName), _genreService.GetLocalizedGenresExcept(model.PreviousGenreName));
            return View(model);
        }

        [Authorize(Roles = "Manager")]
        public ActionResult Delete(string genreName)
        {
            if (genreName != null && _genreService.IsGenreNameExists(genreName))
            {
                _genreService.DeleteGenre(genreName);
                Response.StatusCode = (int)HttpStatusCode.OK;
                return RedirectToAction("Index", "Genre");
            }

            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return RedirectToAction("Index", "Genre");
        }

        public ActionResult Get(string genreName)
        {
            var genre = _genreService.GetGenre(genreName);
            if (genre != null)
            {
                var model = Mapper.Map<GenreFullViewModel>(genre);
                Response.StatusCode = (int)HttpStatusCode.OK;
                return View(model);
            }

            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return RedirectToAction("Index");
        }
    }
}
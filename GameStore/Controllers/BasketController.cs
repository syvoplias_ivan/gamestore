﻿using AutoMapper;
using GameStore.BLL.Abstract;
using GameStore.Common;
using GameStore.DTO;
using GameStore.Helpers;
using GameStore.ViewModels;
using GameStore.ViewModels.Basket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace GameStore.Controllers
{
    [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
    public class BasketController : BaseController
    {
        private readonly IBasketService _basketService;
        private readonly IGameService _gameService;
        private readonly IOrderService _orderService;
        private readonly IPaymentProvider _provider;

        public BasketController(IBasketService service, IGameService gameService, IOrderService orderService, IPaymentProvider provider)
        {
            _basketService = service;
            _gameService = gameService;
            _orderService = orderService;
            _provider = provider;
        }

        [HttpGet]
        [Authorize(Roles = "Manager")]
        public ActionResult Edit(int id)
        {
            Guid guid = CheckIfBasketExistAndCreateNewIfNot();

            var order = _basketService.GetOrderFromBasket(guid);

            if (order == null)
            {
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return RedirectToRoute("Basket", new { controller = "Basket", action = "Index" });
            }

            var orderDetail = order.OrderDetails.Where(x => !x.IsDeleted).ElementAt(id);

            if (orderDetail == null)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return RedirectToRoute("Basket", new { controller = "Basket", action = "Index" });
            }

            var result = Mapper.Map<EditOrderDetailViewModel>(orderDetail);

            return View(result);
        }

        [HttpPost]
        [Authorize(Roles = "Manager")]
        public ActionResult Edit(EditOrderDetailViewModel model)
        {
            Guid guid = CheckIfBasketExistAndCreateNewIfNot();

            var order = _basketService.GetOrderFromBasket(guid);

            if (order == null)
            {
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return RedirectToRoute("Basket", new { controller = "Basket", action = "Index" });
            }

            var orderDetail = order.OrderDetails.First(x => x.Id == model.Id);

            if (orderDetail == null)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return RedirectToRoute("Basket", new { controller = "Basket", action = "Index" });
            }

            var game = _gameService.GetGame(orderDetail.ProductKey);
            var newUnitsInStok = game.UnitsInStock - (model.Quantity - orderDetail.Quantity);

            if (newUnitsInStok >= model.Quantity)
            {
                orderDetail.Quantity = model.Quantity;
                _basketService.UpdateOrderDetail(orderDetail, order);
            }
            else
            {
                ModelState.AddModelError("Quantity", "Quantity are greater than actual count of games. Please input Quantity that is lower than" + game.UnitsInStock + "!");
                return View(model);
            }
            return RedirectToRoute("Basket", new { controller = "Basket", action = "Index" });
        }

        public ActionResult Delete(Guid id)
        {
            Guid guid = CheckIfBasketExistAndCreateNewIfNot();

            var order = _basketService.GetOrderFromBasket(guid);

            var orderDetail = order.OrderDetails.FirstOrDefault(x => x.Id == id && !x.IsDeleted);

            if (orderDetail != null)
            {
                _basketService.DeleteOrderDetail(orderDetail, order);
                order = _basketService.GetOrderFromBasket(guid);
            }

            IEnumerable<OrderDetailViewModel> orderDetails;

            if (order == null)
            {
                orderDetails = new List<OrderDetailViewModel>();
            }
            else
            {
                orderDetails = Mapper.Map<IEnumerable<OrderDetailViewModel>>(order.OrderDetails.Where(x => !x.IsDeleted));
            }

            return PartialView("_OrderDetailsPage", orderDetails);
        }

        [HttpGet]
        [Authorize(Roles = "administrator,manager")]
        public ActionResult History(OrderFilterViewModel filter = null)
        {
            var historyModel = new OrderHistoryViewModel();
            historyModel.HistoryList = new HistoryListViewModel();

            if (!string.IsNullOrEmpty(filter.NumberOnPage))
            {
                filter = UpdateFilterInfo(filter);
            }
            else
            {
                filter = GetDefaultFilter();
                filter.EndDate = DateTime.UtcNow.AddDays(-30);
                filter.StartDate = DateTime.UtcNow.AddYears(-50);
            }

            historyModel.FilteredCount = _basketService.GetOrdersCount();

            var filterDto = OrderFilterViewModelToOrderFilterDtoConvertor.Convertor(filter, historyModel.FilteredCount);

            historyModel.FilteredCount = _basketService.GetFilteredOrdersCount(filterDto);

            filterDto = OrderFilterViewModelToOrderFilterDtoConvertor.Convertor(filter, historyModel.FilteredCount);

            var orderDetailViewModels =
                Mapper.Map<IEnumerable<OrderDto>, IEnumerable<OrderViewModel>>(
                    _basketService.GetHistory(filterDto).ToList());
            if (orderDetailViewModels != null)
            {
                Response.StatusCode = (int)HttpStatusCode.OK;

                if (filter.NumberOnPage != null && filter.NumberOnPage != "All")
                {
                    historyModel.NumberOnPage = int.Parse(filter.NumberOnPage);
                }
                else
                {
                    historyModel.NumberOnPage = historyModel.FilteredCount;
                }

                historyModel.HistoryList.Orders = orderDetailViewModels;
                historyModel.Filter = filter;
                return View(historyModel);
            }

            historyModel.Filter = filter;
            historyModel.HistoryList.Orders = new List<OrderViewModel>();

            Response.StatusCode = (int)HttpStatusCode.NotFound;
            return View(historyModel);
        }

        [HttpGet]
        [Authorize(Roles = "manager")]
        public ActionResult Orders(OrderFilterViewModel filter = null)
        {
            var historyModel = new OrderHistoryViewModel();
            historyModel.HistoryList = new HistoryListViewModel();
            historyModel.HistoryList.CanEdit = true;

            if (!string.IsNullOrEmpty(filter.NumberOnPage))
            {
                filter = UpdateFilterInfo(filter);
            }
            else
            {
                filter = GetDefaultFilter();
            }

            if (filter.EndDate == default(DateTime))
            {
                filter.EndDate = DateTime.UtcNow;
            }

            if (filter.StartDate == default(DateTime))
            {
                filter.StartDate = DateTime.UtcNow.AddDays(-30);
            }

            filter.CanEdit = true;

            historyModel.FilteredCount = _basketService.GetOrdersCount();

            var filterDto = OrderFilterViewModelToOrderFilterDtoConvertor.Convertor(filter, historyModel.FilteredCount);

            historyModel.FilteredCount = _basketService.GetFilteredOrdersCount(filterDto);

            filterDto = OrderFilterViewModelToOrderFilterDtoConvertor.Convertor(filter, historyModel.FilteredCount);

            var orderDetailViewModels =
                Mapper.Map<IEnumerable<OrderDto>, IEnumerable<OrderViewModel>>(
                    _basketService.GetHistory(filterDto).ToList());
            if (orderDetailViewModels != null)
            {
                Response.StatusCode = (int)HttpStatusCode.OK;

                if (filter.NumberOnPage != null && filter.NumberOnPage != "All")
                {
                    historyModel.NumberOnPage = int.Parse(filter.NumberOnPage);
                }
                else
                {
                    historyModel.NumberOnPage = historyModel.FilteredCount;
                }

                historyModel.HistoryList.Orders = orderDetailViewModels;
                historyModel.Filter = filter;
                return View("History", historyModel);
            }

            historyModel.Filter = filter;
            historyModel.HistoryList.Orders = new List<OrderViewModel>();

            Response.StatusCode = (int)HttpStatusCode.NotFound;
            return View("History", historyModel);
        }

        [HttpGet]
        [Authorize(Roles = "manager")]
        public ActionResult ChangeStatus(Guid id)
        {
            var order = _orderService.GetOrder(id);
            if (order != null && order.Status == OrderStatus.Paid)
            {
                _orderService.UpdateOrderStatus(order.Id, OrderStatus.Shipped);
                Response.StatusCode = (int)HttpStatusCode.OK;
                return RedirectToAction("Index");
            }

            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return RedirectToAction("Index");
        }

        private OrderFilterViewModel GetDefaultFilter()
        {
            var filter = new OrderFilterViewModel();
            filter.NumbersOnPage = PagesCounts.GetCountOnPage();

            filter.NumberOnPage = 10.ToString();

            filter.PageNumber = 1;

            return filter;
        }

        private OrderFilterViewModel UpdateFilterInfo(OrderFilterViewModel filter)
        {
            filter.NumbersOnPage = PagesCounts.GetCountOnPage();

            if (string.IsNullOrEmpty(filter.NumberOnPage))
            {
                filter.NumberOnPage = 10.ToString();
            }

            return filter;
        }

        [HttpGet]
        public ActionResult Index()
        {
            Guid guid = CheckIfBasketExistAndCreateNewIfNot();

            var order = _basketService.GetOrderFromBasket(guid);
            IEnumerable<OrderDetailViewModel> orderDetails;

            if (order == null)
            {
                orderDetails = new List<OrderDetailViewModel>();
            }
            else
            {
                orderDetails = Mapper.Map<IEnumerable<OrderDetailViewModel>>(order.OrderDetails.Where(x => !x.IsDeleted));
            }

            var basket = _basketService.GetBasket(guid);

            var basketView = new BasketViewModel() { OrderDetails = orderDetails, Summ = basket.Summ };

            return View(basketView);
        }

        [HttpPost]
        public ActionResult VisaPayment(VisaViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("VisaPayment", model);
            }
            Guid guid = CheckIfBasketExistAndCreateNewIfNot();

            var basket = _basketService.GetBasket(guid);
            var resultModel = new VisaPaymentResultViewModel() { Data = model };
            resultModel.OrderDate = basket.Orders.Last().OrderDate;
            resultModel.OrderSumm = basket.Orders.Last().Summ;

            Response.StatusCode = (int)HttpStatusCode.OK;
            return View("VisaPaymentResult", resultModel);
        }

        [HttpGet]
        public ActionResult Pay(TypeOfPayment type)
        {
            Guid id = CheckIfBasketExistAndCreateNewIfNot();
            var order = _basketService.GetOrderFromBasket(id);

            if (order == null)
            {
                return RedirectToRoute("Basket", new { controller = "Basket", action = "Index" });
            }
            var result = _provider.GetPaymentStrategy(type).Pay(order);

            switch (type)
            {
                case TypeOfPayment.Bank:
                    order.PaymentType = TypeOfPayment.Bank;
                    _basketService.PayOrder(order);
                    order = _basketService.GetOrderFromBasket(id);
                    var path = result.GetResultValue<string>("filename");
                    return File(path, "application/pdf");

                case TypeOfPayment.IBox:
                    order.PaymentType = TypeOfPayment.IBox;
                    _basketService.PayOrder(order);
                    order = _basketService.GetOrderFromBasket(id);
                    var iboxView = new IBoxStrategyViewModel();
                    iboxView.AccountNumber = result.GetResultValue<Guid>("AccountNumber");
                    iboxView.InvoiceNumber = result.GetResultValue<Guid>("InvoiceNumber");
                    iboxView.Summ = result.GetResultValue<decimal>("Summ");
                    return View("IBoxPayment", iboxView);

                case TypeOfPayment.Visa:
                    order.PaymentType = TypeOfPayment.Visa;
                    _basketService.PayOrder(order);
                    order = _basketService.GetOrderFromBasket(id);
                    var visaView = new VisaViewModel();
                    return View("VisaPayment", visaView);
            }
            return RedirectToRoute("Basket", new { controller = "Basket", action = "Index" });
        }

        [HttpGet]
        public ActionResult Order()
        {
            Guid id = CheckIfBasketExistAndCreateNewIfNot();
            var order = _basketService.GetOrderFromBasket(id);
            OrderViewModel orderView;

            if (order == null)
            {
                orderView = new OrderViewModel();
                orderView.OrderDetails = new List<OrderDetailViewModel>();
                orderView.UserId = id;
                return RedirectToRoute("Basket", new { controller = "Basket", action = "Index" });
            }
            else
            {
                orderView = Mapper.Map<OrderViewModel>(order);
                orderView.OrderDetails = Mapper.Map<IEnumerable<OrderDetailViewModel>>(order.OrderDetails.Where(x => !x.IsDeleted));
            }
            return View(orderView);
        }

        [HttpGet]
        public ActionResult Buy(string key)
        {
            var buyModel = new BuyViewModel() { GameKey = key };
            return View(buyModel);
        }

        [HttpPost]
        public ActionResult Buy(BuyViewModel model)
        {
            if (string.IsNullOrEmpty(model.GameKey))
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return RedirectToRoute("Games", new { param = "games" });
            }

            Guid userGuid = CheckIfBasketExistAndCreateNewIfNot();
            var game = _gameService.GetGame(model.GameKey);

            if (!string.IsNullOrEmpty(game.ObjectId) && game.Id == default(Guid))
            {
                _gameService.AddGame(game);
            }

            if (game == null)
            {
                Response.StatusCode = (int)HttpStatusCode.NotFound;
                return RedirectToRoute("Games", new { param = "games" });
            }

            if (game.UnitsInStock >= model.Quantity)
            {
                var order = _basketService.GetOrderFromBasket(userGuid);
                if (order == null)
                {
                    order = new OrderDto();
                    order.UserId = userGuid;
                    order.IsDeleted = false;
                    order.Status = OrderStatus.NotPaid;
                    order.OrderDate = DateTime.UtcNow;
                    order.PaymentType = TypeOfPayment.NotSelected;
                    order.Summ = 0;
                    _basketService.AddOrderToBasket(order);

                    order = _basketService.GetOrderFromBasket(userGuid);
                }

                var orderDetail = new OrderDetailDto
                {
                    ProductName = game.Name,
                    ProductKey = game.Key,
                    Quantity = model.Quantity,
                    Discount = 0,
                    IsDeleted = false,
                    Price = game.Price
                };

                _basketService.AddOrderDetails(orderDetail, order);
            }
            else
            {
                ModelState.AddModelError("Quantity", "Quantity are greater than actual count of games. Please input Quantity that is lower than" + game.UnitsInStock + "!");
                return View(model);
            }

            Response.StatusCode = (int)HttpStatusCode.OK;
            return RedirectToRoute("Basket", new { controller = "Basket", action = "Index" });
        }

        [HttpPost]
        public ActionResult Clear()
        {
            var userId = CheckIfBasketExistAndCreateNewIfNot();
            if (_basketService.GetOrdersCount(userId) != 0)
            {
                _basketService.ClearBasket(userId);
            }

            var orderDetails = new List<OrderDetailViewModel>();
            return PartialView("_OrderDetailsPage", orderDetails);
        }

        /// <summary>
        /// Adds new basket and adds cookie with Guid to user.
        /// </summary>
        /// <returns>Guid id of user</returns>
        private Guid CheckIfBasketExistAndCreateNewIfNot()
        {
            Guid guid = default(Guid);

            if (CurrentUser != null && CurrentUser.NickName != "anonym")
            {
                guid = CurrentUser.Id;
            }
            else
            {
                throw new Exception("user is not authorized");
            }

            if (!_basketService.IfBasketExistsForCurrentUser(guid))
            {
                _basketService.CreateBasketForCurrentUser(guid);
            }

            return guid;
        }
    }
}
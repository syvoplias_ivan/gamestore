﻿using System.Web.Mvc;

namespace GameStore.Controllers
{
    public class ErrorController : BaseController
    {
        [HandleError]
        public ActionResult NotFound()
        {
            Response.StatusCode = 404;

            return View();
        }

        [HandleError]
        public ActionResult Error()
        {
            Response.StatusCode = 500;

            return View();
        }

        [HandleError]
        public ActionResult Unauthorized()
        {
            Response.StatusCode = 401;

            return View();
        }
    }
}
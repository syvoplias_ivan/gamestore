﻿using AutoMapper;
using GameStore.BLL.Abstract;
using GameStore.Common;
using GameStore.Common.Helpers;
using GameStore.DTO;
using GameStore.Helpers;
using GameStore.Helpers.EnumExtentions;
using GameStore.ViewModels;
using GameStore.ViewModels.Comment;
using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace GameStore.Controllers
{
    public class CommentController : BaseController
    {
        private readonly ICommentService _commentService;
        private readonly IGameService _gameService;

        public CommentController(ICommentService service, IGameService gameService)
        {
            this._commentService = service;
            this._gameService = gameService;
        }

        [HttpPost]
        public ActionResult NewComment(AddCommentViewModel comment)
        {
            if (ModelState.IsValid)
            {
                var commentDto = Mapper.Map<CommentDto>(comment);

                Response.StatusCode = (int)HttpStatusCode.OK;
                var full = new FullCommentsViewModel();

                var addcomment = new AddCommentViewModel() { GameKey = comment.GameKey };

                _commentService.AddComment(commentDto, comment.GameKey);

                var game = _gameService.GetGame(comment.GameKey);

                full.AddCommentViewModel = addcomment;
                full.GameKey = comment.GameKey;
                full.Comments = CommentHelper.GetCommentViewFromDto(game.Comments.Where(x => !x.IsDeleted && x.ParentId == null));
                return PartialView("_Comments", full);
            }

            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            var fullCommentsViewModel = new FullCommentsViewModel();
            fullCommentsViewModel.AddCommentViewModel = comment;
            fullCommentsViewModel.GameKey = comment.GameKey;
            fullCommentsViewModel.Comments = CommentHelper.GetCommentViewFromDto(_gameService.GetGame(comment.GameKey).Comments.Where(x => !x.IsDeleted && x.ParentId == null));
            return PartialView("_Comments", fullCommentsViewModel);
        }

        [HttpPost]
        [Authorize(Roles = "Manager,Moderator")]
        public ActionResult Delete(Guid id, string gameKey)
        {
            var comment = _commentService.GetComment(id);
            if (comment == null || string.IsNullOrEmpty(gameKey))
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                throw new Exception($"Bad request for deleting comment occured id = {id} and gameKey = {gameKey}");
            }

            _commentService.DeleteComment(id);
            var game = _gameService.GetGame(comment.GameKey);

            Response.StatusCode = (int)HttpStatusCode.OK;
            var full = new FullCommentsViewModel();

            var addcomment = new AddCommentViewModel() { GameKey = game.Key };

            full.AddCommentViewModel = addcomment;
            full.GameKey = game.Key;
            full.Comments = CommentHelper.GetCommentViewFromDto(_gameService.GetGame(game.Key).Comments.Where(x => !x.IsDeleted && x.ParentId == null));
            return PartialView("_Comments", full);
        }

        [HttpGet]
        [Authorize(Roles = "Manager,Moderator")]
        public ActionResult Ban(Guid id)
        {
            var newId = 0;
            var banDurations = BanDuration.Day.GetAllValuesOfEnum<BanDuration>().Select(x => new ListItem<string>() { Id = newId++, Item = x.GetLocalizedDescription(), Value = x.ConvertToString() });
            var banView = new BanViewModel() { BanDurations = banDurations };
            return View(banView);
        }

        [HttpPost]
        [Authorize(Roles = "Manager,Moderator")]
        public ActionResult Ban(BanViewModel ban)
        {
            if (ModelState.IsValid)
            {
                return View("BanSuccess");
            }
            return View(ban);
        }

        [HttpGet]
        public ActionResult Comments(String key)
        {
            if (key == null)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(HttpStatusCode.BadRequest);
            }

            var comments = _commentService.GetComments(key);
            if (comments == null)
            {
                Response.StatusCode = (int)HttpStatusCode.NotFound;
                return Json(HttpStatusCode.NotFound);
            }

            Response.StatusCode = (int)HttpStatusCode.OK;
            return Json(comments, JsonRequestBehavior.AllowGet);
        }
    }
}
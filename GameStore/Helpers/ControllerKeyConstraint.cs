﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace GameStore.Helpers
{
    public class ControllerKeyConstraint : IRouteConstraint
    {
        private readonly Dictionary<String, String> _controllerMethod;
        private readonly string[] _restrictedKeys;

        public ControllerKeyConstraint(Dictionary<String, String> constraint, string[] restricted)
        {
            _controllerMethod = constraint;
            _restrictedKeys = restricted;
        }

        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            var controller = values[parameterName]?.ToString().ToLower();
            var key = values["key"]?.ToString().ToLower();

            if (_controllerMethod.ContainsKey(controller) && !string.IsNullOrEmpty(key) && !_restrictedKeys.Contains(key))
            {
                values["action"] = _controllerMethod[controller];
                return true;
            }
            return false;
        }
    }
}
﻿using System.Linq;
using System.Web;
using System.Web.Routing;

namespace GameStore.Helpers
{
    public class NotEqual : IRouteConstraint
    {
        private string[] _restricted;

        public NotEqual(params string[] constaint)
        {
            _restricted = constaint;
        }

        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            var value = values[parameterName].ToString();

            if (string.IsNullOrEmpty(value)) return false;

            var valueLower = value.ToLower();

            if (_restricted.Contains(valueLower)) return false;

            return true;
        }
    }
}
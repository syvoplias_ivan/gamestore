﻿using GameStore.App_LocalResources;
using GameStore.Common.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Web;

namespace GameStore.Helpers.EnumExtentions
{
    public static class WebEnumExtentions
    {
        public static string GetLocalizedDescription(this Enum en)
        {
            Type type = en.GetType();

            MemberInfo[] memInfo = type.GetMember(en.ToString());

            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attrs != null && attrs.Length > 0)
                {
                    var resource = new ResourceManager(typeof(GlobalRes));
                    var description = ((DescriptionAttribute)attrs[0]).Description;
                    return resource.GetString(description);
                }
            }

            return en.ToString();
        }

        public static T ConvertLocalizedToEnum<T>(this string value) where T : struct, IComparable, IFormattable, IConvertible
        {
            if (!typeof(T).IsEnum) { throw new ArgumentException("T must be an enumerated type"); }
            T result = default(T);
            var values = typeof(T).GetEnumValues();

            foreach (var item in values)
            {
                if (((Enum)item).GetLocalizedDescription() == value)
                {
                    result = (T)item;
                    break;
                }
            }
            return result;
        }
    }
}
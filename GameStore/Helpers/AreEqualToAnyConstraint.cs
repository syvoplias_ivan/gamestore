﻿using System;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace GameStore.Helpers
{
    public class AreEqualToAnyConstraint : IRouteConstraint
    {
        private string[] _allowed;

        public AreEqualToAnyConstraint(params string[] constaint)
        {
            _allowed = constaint;
        }

        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            var value = values[parameterName].ToString().ToLower();
            var valueLower = value.ToLower();

            if (string.IsNullOrEmpty(value) && !_allowed.Contains(string.Empty)) return false;

            if (_allowed.Contains(valueLower)) return true;

            return false;
        }
    }
}
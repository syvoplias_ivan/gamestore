﻿using GameStore.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebGrease.Css.Extensions;

namespace GameStore.Helpers
{
    public class ListStringToListListItemConverter
    {
        public static IEnumerable<ListItem<string>> GetListItems(IEnumerable<string> strings)
        {
            var stringsList = strings.ToList();
            var i = 0;
            var genresSelectList = new List<ListItem<string>>();
            stringsList.ForEach(x => genresSelectList.Add(new ListItem<string>() { Id = i++, Item = x, Value = x }));
            return genresSelectList;
        }

        public static IEnumerable<ListItem<string>> GetListItems(IEnumerable<string> strings,
            IEnumerable<string> localizedStrings)
        {
            var stringsList = strings.ToList();
            var localizedList = localizedStrings.ToList();
            var genresSelectList = new List<ListItem<string>>();

            if (stringsList.Count == localizedList.Count)
            {
                for (int j = 0; j < stringsList.Count; j++)
                {
                    genresSelectList.Add(new ListItem<string>()
                    {
                        Id = j,
                        Item = localizedList[j],
                        Value = stringsList[j]
                    });
                }
            }
            else
            {
                throw new ArgumentException("values must be equals in length");
            }

            return genresSelectList;
        }
    }
}
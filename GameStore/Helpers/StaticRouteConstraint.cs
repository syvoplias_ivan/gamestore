﻿using System.Collections.Generic;
using System.Web;
using System.Web.Routing;

namespace GameStore.Helpers
{
    public class StaticRouteConstraint : IRouteConstraint
    {
        private readonly Dictionary<string, List<string>> _routes;

        public StaticRouteConstraint(Dictionary<string, List<string>> routes)
        {
            _routes = routes;
        }

        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values,
            RouteDirection routeDirection)
        {
            var value = values[parameterName]?.ToString().ToLower();
            if (!string.IsNullOrEmpty(value) && _routes.ContainsKey(value))
            {
                values["controller"] = _routes[value][0];
                values["action"] = _routes[value][1];
                return true;
            }
            return false;
        }
    }
}
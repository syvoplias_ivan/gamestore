﻿using AutoMapper;
using GameStore.DTO;
using GameStore.ViewModels;
using GameStore.ViewModels.Comment;
using System.Collections.Generic;
using System.Linq;

namespace GameStore.Helpers
{
    public class CommentHelper
    {
        public static IEnumerable<CommentViewModel> GetCommentViewFromDto(IEnumerable<CommentDto> comments)
        {
            int id = 0, level = 1;
            var list = new List<CommentViewModel>();
            for (int i = 0; i < comments.Count(); i++)
            {
                var dest = comments.ElementAt(i);
                var comment = Mapper.Map<CommentViewModel>(dest);
                comment.CommentId = id;
                comment.CommentLevel = level;
                if (comment.AuthorName == null)
                {
                    comment.AuthorName = "anonym";
                }
                list.Add(comment);
                level++;
                id++;
                if (dest.ChildComments.Any())
                {
                    GetCommentsFromDtoSource(dest.ChildComments.Where(x => !x.IsDeleted), list, ref id, ref level, comment.AuthorName, "comment-" + (id - 1));
                }
                level = 1;
            }
            return list;
        }

        private static void GetCommentsFromDtoSource(IEnumerable<CommentDto> source, List<CommentViewModel> dest, ref int id, ref int level,
            string parentName, string answerId)
        {
            var templevel = level;
            foreach (var item in source)
            {
                var temp = Mapper.Map<CommentViewModel>(item);
                temp.CommentId = id;
                temp.AnswerId = answerId;
                temp.CommentLevel = level;
                if (temp.AuthorName == null)
                {
                    temp.AuthorName = "anonym";
                }
                temp.ParentCommentAuthorName = parentName;
                dest.Add(temp);
                id++;
                if (item.ChildComments.Any())
                {
                    level = (level + 1) < 3 ? level + 1 : 3;
                    GetCommentsFromDtoSource(item.ChildComments.Where(x => !x.IsDeleted), dest, ref id, ref level, temp.AuthorName, "comment-" + id);
                }
                level = templevel;
            }
        }
    }
}
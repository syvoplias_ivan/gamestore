﻿using System.Text;
using System.Web.Routing;

namespace GameStore.Helpers
{
    public static class RequestLogInfoHelper
    {
        public static string GetRequestInfoForError(this RequestContext context)
        {
            var userName = context.HttpContext.User.Identity.Name;
            var ip = context.HttpContext.Request.UserHostAddress;
            var httpMethod = context.HttpContext.Request.HttpMethod;
            var url = context.HttpContext.Request.Url;
            var sb = new StringBuilder();

            if (context.RouteData.DataTokens.Count != 0)
            {
                sb.Append("Route Data: \n");
                foreach (var t in context.RouteData.DataTokens.Keys)
                {
                    if (context.RouteData.DataTokens[t] is string)
                    {
                        string temp = (string)context.RouteData.DataTokens[t];
                        sb.Append(string.Format("[ {0} ] = [ {1} ]\n", t, temp));
                    }
                }
                sb.Append("\n");
            }

            if (context.HttpContext.Request.QueryString.Count != 0)
            {
                sb.Append("Query data:\n");

                foreach (string queryStringKey in context.HttpContext.Request.QueryString.Keys)
                {
                    var temp = context.HttpContext.Request.QueryString.Get(queryStringKey);
                    sb.Append(string.Format("[ {0} ] = [ {1} ]\n", queryStringKey, temp));
                }
                sb.Append("\n");
            }

            if (context.HttpContext.Request.Form.Count != 0)
            {
                sb.Append("Form data:\n");
                foreach (var key in context.HttpContext.Request.Form.AllKeys)
                {
                    sb.Append(string.Format("[ {0} ] = [ {1} ]\n", key, context.HttpContext.Request.Form[key]));
                }
                sb.Append("\n");
            }

            if (context.HttpContext.Request.Headers.Count != 0)
            {
                sb.Append("Headers: \n");
                foreach (string key in context.HttpContext.Request.Headers.AllKeys)
                {
                    sb.Append(string.Format("{0}: {1}", key, context.HttpContext.Request.Headers[key]));
                }
                sb.Append("\n");
            }

            var resultString = string.Format("Username:{0}, Url: {1}, Ip: {2}, HttpMethod: {3}, \n Request Info: {4}", userName, url, ip, httpMethod, sb.ToString());

            return resultString;
        }

        public static string GetRequestShortInfo(this RequestContext context)
        {
            var userName = context.HttpContext.User.Identity.Name;
            var ip = context.HttpContext.Request.UserHostAddress;
            var httpMethod = context.HttpContext.Request.HttpMethod;
            var url = context.HttpContext.Request.Url;
            var resultString = string.Format("Username:{0}, Url: {1}, Ip: {2}, HttpMethod: {3}", userName, url, ip, httpMethod);
            return resultString;
        }
    }
}
﻿using GameStore.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace GameStore.Helpers
{
    public static class DropDownHelper
    {
        public static MvcHtmlString CustomDropDownList<T>(this HtmlHelper helper, string name, string id, IEnumerable<ListItem<T>> list, IEnumerable<ListItem<T>> selected = null, object htmlAttributes = null)
        {
            var select = new TagBuilder("select");
            if (htmlAttributes != null)
            {
                if (htmlAttributes.GetType().GetProperty("class") != null)
                {
                    var cssClass = (string)htmlAttributes.GetType().GetProperty("class").GetValue(htmlAttributes);
                    select.AddCssClass(cssClass);
                }

                if (htmlAttributes.GetType().GetProperty("multiple") != null)
                {
                    var multiple = (string)htmlAttributes.GetType().GetProperty("multiple").GetValue(htmlAttributes);
                    select.Attributes["multiple"] = multiple;
                }
            }

            select.Attributes["name"] = name;
            select.Attributes["id"] = id;

            foreach (var item in list)
            {
                var option = new TagBuilder("option");
                option.Attributes["value"] = item.Value.ToString();
                option.SetInnerText(item.Item as string);

                bool isSelected = false;
                if (selected != null && selected.Any())
                {
                    var tempList = selected.ToList();
                    isSelected = tempList.FirstOrDefault(x => x.Id == item.Id) != null;
                }

                if (isSelected)
                {
                    option.Attributes["selected"] = "true";
                }
                select.InnerHtml += option.ToString();
            }

            return new MvcHtmlString(select.ToString());
        }

        public static MvcHtmlString CustomDropDownList<T>(this HtmlHelper helper, string name, string id, IEnumerable<ListItem<T>> list, IEnumerable<int> selected = null, object htmlAttributes = null)
        {
            var listSelected = selected?.Select(x => new ListItem<T>() { Id = x });
            return CustomDropDownList(helper, name, id, list, listSelected, htmlAttributes);
        }

        public static MvcHtmlString CustomDropDownList<T>(this HtmlHelper helper, string name, string id, IEnumerable<ListItem<T>> list, IEnumerable<string> selected = null, object htmlAttributes = null)
        {
            var listSelectedIds = new List<ListItem<T>>();
            if (selected != null)
            {
                foreach (var item in selected)
                {
                    var temp = list.FirstOrDefault(x => x.Value as string == item);
                    if (temp != null)
                    {
                        listSelectedIds.Add(temp);
                    }
                }
            }
            return CustomDropDownList(helper, name, id, list, listSelectedIds, htmlAttributes);
        }

        public static MvcHtmlString CustomDropDownList<T>(this HtmlHelper helper, string name, string id,
            IEnumerable<ListItem<T>> list, string selected = null, object htmlAttributes = null)
        {
            var selectedList = new List<ListItem<T>>();
            if (selected != null)
            {
                selectedList.Add(list.FirstOrDefault(x => x.Value as string == selected));
            }

            return CustomDropDownList(helper, name, id, list, selectedList, htmlAttributes);
        }
    }
}
﻿using GameStore.App_LocalResources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GameStore.Helpers
{
    public static class PaginationHelper
    {
        public static MvcHtmlString CustomPagination(this HtmlHelper helper, int totalItems, int currentPage, int countOnPage)
        {
            int step = 3;
            int standartPages = 6;
            int pagesCount = 0;

            string className = "pagination-button";

            if (countOnPage >= totalItems)
            {
                countOnPage = totalItems;
                pagesCount = 1;
            }
            else
            {
                pagesCount = (int)Math.Ceiling((double)totalItems / countOnPage);
            }

            var ul = new TagBuilder("ul");
            ul.AddCssClass("pagination");

            var liFirstPage = new TagBuilder("li");
            liFirstPage.AddCssClass("pagination-first-page");
            var linkFirst = new TagBuilder("a");
            linkFirst.Attributes["href"] = "#";
            linkFirst.SetInnerText(GlobalRes.PagianationFirst);
            liFirstPage.InnerHtml += linkFirst.ToString();

            var liPrev = new TagBuilder("li");
            liPrev.AddCssClass("pagination-prev-page");
            var linkPrev = new TagBuilder("a");
            linkPrev.Attributes["href"] = "#";
            linkPrev.SetInnerText("<<");
            liPrev.InnerHtml += linkPrev.ToString();

            ul.InnerHtml += liFirstPage.ToString() + liPrev.ToString();

            int start = (currentPage > 3) ? currentPage - 3 : 1;

            if (pagesCount - start >= standartPages)
            {
                pagesCount = start + standartPages;
            }

            for (int i = start; i <= pagesCount; i++)
            {
                var li = new TagBuilder("li");
                var a = new TagBuilder("a");
                a.Attributes["href"] = "#";

                if (i == currentPage)
                {
                    li.AddCssClass("active " + className);
                }
                else
                {
                    li.AddCssClass(className);
                }

                a.SetInnerText(i.ToString());
                li.InnerHtml += a.ToString();
                ul.InnerHtml += li.ToString();
            }

            var liLast = new TagBuilder("li");
            liLast.AddCssClass("pagination-last-page");
            var linkLast = new TagBuilder("a");
            linkLast.Attributes["href"] = "#";
            linkLast.SetInnerText(GlobalRes.PaginationLast);
            liLast.InnerHtml += linkLast.ToString();

            var liNext = new TagBuilder("li");
            liNext.AddCssClass("pagination-next-page");
            var linkNext = new TagBuilder("a");
            linkNext.Attributes["href"] = "#";
            linkNext.SetInnerText(">>");
            liNext.InnerHtml += linkNext.ToString();

            ul.InnerHtml += liNext.ToString() + liLast.ToString();

            return new MvcHtmlString(ul.ToString());
        }
    }
}
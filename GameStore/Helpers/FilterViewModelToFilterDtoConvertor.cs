﻿using GameStore.Common;
using GameStore.Common.Helpers;
using GameStore.DTO;
using GameStore.Helpers.EnumExtentions;
using GameStore.ViewModels;
using GameStore.ViewModels.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameStore.Helpers
{
    public class FilterViewModelToFilterDtoConvertor
    {
        public static FilterDto Convert(FilterViewModel model, int totalGames)
        {
            var filter = new FilterDto();
            IEnumerable<string> publishers, platforms, genres;

            if (model.SelectedPublishers != null)
            {
                publishers = model.Publishers.Where(x => model.SelectedPublishers.Contains(x.Id))
                    .Select(x => x.Item).ToList();
            }
            else
            {
                publishers = model.Publishers.Select(x => x.Item).ToList();
            }

            if (model.SelectedPlatforms != null)
            {
                platforms = model.Platforms.Where(x => model.SelectedPlatforms.Contains(x.Id))
                    .Select(x => x.Item.ConvertLocalizedToEnum<TypeOfPlatform>().ConvertToString()).ToList();
            }
            else
            {
                platforms = model.Platforms.Select(x => x.Item.ConvertLocalizedToEnum<TypeOfPlatform>().ConvertToString()).ToList();
            }

            if (model.SelectedGenres != null)
            {
                genres = model.Genres.Where(x => model.SelectedGenres.Contains(x.Id))
                    .Select(x => x.Item).ToList();
            }
            else
            {
                genres = model.Genres.Select(x => x.Item).ToList();
            }

            filter.SortType = model.SortType.ConvertToEnum<SortType>().ConvertToString();
            filter.Date = model.Date;
            filter.PriceLow = model.PriceLow;
            filter.PriceHigh = model.PriceHigh;
            filter.PageNumber = model.PageNumber;

            if (model.NumberOnPage != "All")
            {
                filter.NumberOnPage = (int.Parse(model.NumberOnPage) > totalGames)
                    ? totalGames.ToString()
                    : model.NumberOnPage;

                var numberPage = int.Parse(filter.NumberOnPage);

                if (numberPage * filter.PageNumber > totalGames)
                {
                    filter.PageNumber = (int)Math.Ceiling((double)totalGames / numberPage);
                    model.PageNumber = filter.PageNumber;
                }
            }
            else
            {
                filter.NumberOnPage = model.NumberOnPage;
                filter.PageNumber = 1;
                model.PageNumber = filter.PageNumber;
            }

            filter.GameName = model.GameName;
            filter.Genres = genres;
            filter.Platforms = platforms;
            filter.Publishers = publishers;

            return filter;
        }
    }
}
﻿using GameStore.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameStore.Helpers
{
    public class PagesCounts
    {
        public static IEnumerable<ListItem<string>> GetCountOnPage()
        {
            return new List<ListItem<string>>()
            {
                new ListItem<string>() {Id = 0, Item = "10", Value = "10"},
                new ListItem<string>() {Id = 1, Item = "20", Value = "20"},
                new ListItem<string>() {Id = 2, Item = "50", Value = "50"},
                new ListItem<string>() {Id = 3, Item = "100", Value = "100"},
                new ListItem<string>() {Id = 4, Item = "All", Value = "All"},
                new ListItem<string>() {Id = 5, Item = "3", Value = "3"}
            };
        }
    }
}
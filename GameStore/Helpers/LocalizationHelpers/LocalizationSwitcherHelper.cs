﻿using GameStore.BLL.Localization;
using GameStore.Common.Helpers;
using GameStore.Helpers.EnumExtentions;
using System.Linq;
using System.Threading;
using System.Web.Mvc;

namespace GameStore.Helpers.LocalizationHelpers
{
    public static class LocalizationSwitcherHelper
    {
        public static MvcHtmlString LanguageDropdown(this HtmlHelper helper)
        {
            var dropdown = new TagBuilder("div");

            dropdown.AddCssClass("form-group");

            var selectList = new TagBuilder("select");
            selectList.AddCssClass("form-control");
            selectList.Attributes["id"] = "language-switch";

            var languageEnumValues = LanguageCode.En.GetAllValuesOfEnum().ToList();
            var languages = LanguageCode.En.GetAllValuesOfEnum().Select(x => x.GetLocalizedDescription());

            var i = 0;
            foreach (var language in languages)
            {
                var option = new TagBuilder("option");
                option.SetInnerText(language);
                option.Attributes["value"] = languageEnumValues[i].ToString().ToLower();

                if (Thread.CurrentThread.LanguageFromThread() == languageEnumValues[i])
                {
                    option.Attributes["selected"] = "true";
                }

                selectList.InnerHtml += option.ToString();

                i++;
            }

            dropdown.InnerHtml += selectList.ToString();

            return new MvcHtmlString(dropdown.ToString());
        }
    }
}
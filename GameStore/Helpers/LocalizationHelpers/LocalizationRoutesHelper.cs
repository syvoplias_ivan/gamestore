﻿using Microsoft.Ajax.Utilities;
using System;
using System.Collections;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Mvc.Routing;
using System.Web.Routing;

namespace GameStore.Helpers.LocalizationHelpers
{
    public static class LocalizationRoutesHelper
    {
        public static Route MapRouteWithName(this RouteCollection routes, string name, string url, object defaults, object constraints)
        {
            Route route = routes.MapRoute(name, url, defaults, constraints);
            route.DataTokens = new RouteValueDictionary();
            route.DataTokens.Add("RouteName", name);

            return route;
        }

        public static void MapLocalizedMvcAttributeRoutes(this RouteCollection routes, string urlPrefix, object defaults, object constraints)
        {
            MapLocalizedMvcAttributeRoutes(routes, urlPrefix, new RouteValueDictionary(defaults), new RouteValueDictionary(constraints));
        }

        public static void MapLocalizedMvcAttributeRoutes(this RouteCollection routes, string urlPrefix, RouteValueDictionary defaults, RouteValueDictionary constraints)
        {
            var routeCollectionRouteType = typeof(Route);

            var localizedRouteTable = new RouteCollection();

            // Get a copy of the attribute routes
            routes.CopyItemsTo(localizedRouteTable);
            routes.Clear();

            foreach (var routeBase in localizedRouteTable)
            {
                if (routeBase.GetType().Equals(routeCollectionRouteType))
                {
                    var route = routeBase as Route;

                    var routeName = route.DataTokens["RouteName"].ToString();

                    // Create the localized route
                    var localizedRoute = CreateLocalizedRoute(route, urlPrefix, constraints);
                    routes.Add(routeName + "_Localized", localizedRoute);

                    // Add the default link generation route
                    //FIX: needed for default culture on normal attribute route
                    var newDefaults = new RouteValueDictionary(defaults);

                    var routeDefaultsList = route.Defaults.ToList();

                    foreach (var keyValuePair in routeDefaultsList)
                    {
                        if (!newDefaults.ContainsKey(keyValuePair.Key))
                        {
                            newDefaults.Add(keyValuePair.Key, keyValuePair.Value);
                        }
                    }
                    var routeWithNewDefaults = new Route(route.Url, newDefaults, route.Constraints, route.DataTokens, route.RouteHandler);
                    routes.Add(routeName, routeWithNewDefaults);
                }
            }
        }

        private static Route CreateLocalizedRoute(Route route, string urlPrefix, RouteValueDictionary constraints)
        {
            // Add the URL prefix
            var routeUrl = urlPrefix + route.Url;

            // Combine the constraints
            var routeConstraints = new RouteValueDictionary(constraints);
            foreach (var constraint in route.Constraints)
            {
                routeConstraints.Add(constraint.Key, constraint.Value);
            }

            return new Route(routeUrl, route.Defaults, routeConstraints, route.DataTokens, route.RouteHandler);
        }
    }
}
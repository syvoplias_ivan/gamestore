﻿using GameStore.BLL.Localization;
using GameStore.Common.Helpers;
using GameStore.DTO;
using GameStore.DTO.Localization;
using GameStore.ViewModels.LocalizationViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;

namespace GameStore.Helpers.LocalizationHelpers
{
    public class EntitiesLocalizatorHelper
    {
        public static ICollection<GameLocalizationViewModel> GetLocalizationGame(IEnumerable<GameLocalizationViewModel> localizations)
        {
            ICollection<GameLocalizationViewModel> result;

            var currentLanguage = Thread.CurrentThread.LanguageFromThread();
            var languages = LanguageCode.En.GetAllValuesOfEnum().ToList();

            languages.Remove(currentLanguage);

            result = localizations?.ToList();

            if (result == null || !result.Any())
            {
                var newLocalizations = new List<GameLocalizationViewModel>();

                foreach (var language in languages)
                {
                    newLocalizations.Add(new GameLocalizationViewModel()
                    {
                        Language = language
                    });
                }

                result = newLocalizations;
            }
            else
            {
                var currentLanguageLoc = result.FirstOrDefault(x => x.Language == currentLanguage);
                string defaultName = "", defaultDescription = "";
                if (currentLanguageLoc != null)
                {
                    defaultName = currentLanguageLoc.Name;
                    defaultDescription = currentLanguageLoc.Description;
                    result.Remove(currentLanguageLoc);
                }

                foreach (var languageCode in languages)
                {
                    var locModel = result.FirstOrDefault(x => x.Language == languageCode);
                    if (locModel == null)
                    {
                        result.Add(new GameLocalizationViewModel()
                        {
                            Name = defaultName,
                            Description = defaultDescription,
                            Language = languageCode
                        });
                    }
                }
            }

            return result;
        }

        public static ICollection<GenreLocalizationViewModel> GetLocalizationGenre(
            IEnumerable<GenreLocalizationViewModel> localizations)
        {
            ICollection<GenreLocalizationViewModel> result;

            var currentLanguage = Thread.CurrentThread.LanguageFromThread();
            var languages = LanguageCode.En.GetAllValuesOfEnum().ToList();

            languages.Remove(currentLanguage);

            result = localizations?.ToList();

            if (result == null || !result.Any())
            {
                var newLocalizations = new List<GenreLocalizationViewModel>();

                foreach (var language in languages)
                {
                    newLocalizations.Add(new GenreLocalizationViewModel()
                    {
                        Language = language
                    });
                }

                result = newLocalizations;
            }
            else
            {
                var defaultName = "";
                var currentLanguageLoc = result.FirstOrDefault(x => x.Language == currentLanguage);
                if (currentLanguageLoc != null)
                {
                    defaultName = currentLanguageLoc.Name;
                    result.Remove(currentLanguageLoc);
                }

                foreach (var languageCode in languages)
                {
                    var locModel = result.FirstOrDefault(x => x.Language == languageCode);
                    if (locModel == null)
                    {
                        result.Add(new GenreLocalizationViewModel()
                        {
                            Name = defaultName,
                            Language = languageCode
                        });
                    }
                }
            }

            return result;
        }

        public static ICollection<PublisherLocalizationViewModel> GetLocalizationPublisher(
            IEnumerable<PublisherLocalizationViewModel> localizations)
        {
            ICollection<PublisherLocalizationViewModel> result;

            var currentLanguage = Thread.CurrentThread.LanguageFromThread();
            var languages = LanguageCode.En.GetAllValuesOfEnum().ToList();

            languages.Remove(currentLanguage);

            result = localizations?.ToList();

            if (result == null || !result.Any())
            {
                var newLocalizations = new List<PublisherLocalizationViewModel>();

                foreach (var language in languages)
                {
                    newLocalizations.Add(new PublisherLocalizationViewModel()
                    {
                        Language = language
                    });
                }

                result = newLocalizations;
            }
            else
            {
                string defCompanyName = "", defDescr = "";
                var currentLanguageLoc = result.FirstOrDefault(x => x.Language == currentLanguage);
                if (currentLanguageLoc != null)
                {
                    defCompanyName = currentLanguageLoc.CompanyName;
                    defDescr = currentLanguageLoc.Description;
                    result.Remove(currentLanguageLoc);
                }

                foreach (var languageCode in languages)
                {
                    var locModel = result.FirstOrDefault(x => x.Language == languageCode);
                    if (locModel == null)
                    {
                        result.Add(new PublisherLocalizationViewModel()
                        {
                            CompanyName = defCompanyName,
                            Description = defDescr,
                            Language = languageCode
                        });
                    }
                }
            }

            return result;
        }
    }
}
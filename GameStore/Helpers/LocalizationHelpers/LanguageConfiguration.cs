﻿using GameStore.Common;
using GameStore.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace GameStore.Helpers.LocalizationHelpers
{
    public class LanguageConfiguration : ILanguageConfiguration
    {
        public LanguageCode DefaultLanguage
        {
            get
            {
                return ConfigurationManager.AppSettings["DefaultLanguageCode"].ConvertToEnum<LanguageCode>();
            }
        }
    }
}
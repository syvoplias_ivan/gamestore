﻿using System.Linq;
using System.Web;
using System.Web.Routing;

namespace GameStore.Helpers.LocalizationHelpers
{
    public class CultureConstraint : IRouteConstraint
    {
        private readonly string defaultCulture;
        private string[] _allowed;

        public CultureConstraint(string defaultCulture, params string[] constaint)
        {
            this.defaultCulture = defaultCulture;
            _allowed = constaint;
        }

        public bool Match(
            HttpContextBase httpContext,
            Route route,
            string parameterName,
            RouteValueDictionary values,
            RouteDirection routeDirection)
        {
            if (routeDirection == RouteDirection.UrlGeneration &&
                this.defaultCulture.Equals(values[parameterName]))
            {
                return false;
            }
            else
            {
                var value = values[parameterName].ToString().ToLower();
                var valueLower = value.ToLower();

                if (string.IsNullOrEmpty(value) && !_allowed.Contains(string.Empty)) return false;

                if (_allowed.Contains(valueLower)) return true;

                return false;
            }
        }
    }
}
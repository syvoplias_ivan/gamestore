﻿using GameStore.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace GameStore.Helpers
{
    public static class CheckBoxGroupHelper
    {
        public static MvcHtmlString CustomCheckboxGroup<T>(this HtmlHelper helper, string groupName, string name, string id, IEnumerable<ListItem<T>> list, IEnumerable<int> selected = null)
        {
            var group = new TagBuilder("fieldset");
            group.AddCssClass("form-group row");

            var label = new TagBuilder("label");
            label.AddCssClass("col-md-12");
            label.SetInnerText(groupName);
            group.InnerHtml += label.ToString();

            foreach (var item in list)
            {
                var divCheck = new TagBuilder("div");
                divCheck.AddCssClass("form-check");

                var labelCheck = new TagBuilder("label");
                labelCheck.AddCssClass("form-check-label");

                var checkbox = new TagBuilder("input");
                checkbox.Attributes["type"] = "checkbox";
                checkbox.AddCssClass("form-check-input");
                checkbox.Attributes["value"] = item.Id.ToString();
                checkbox.Attributes["name"] = name;
                checkbox.Attributes["id"] = id;

                if (selected != null && selected.Any() && selected.Contains(item.Id))
                {
                    checkbox.Attributes.Add("checked", "checked");
                }

                labelCheck.InnerHtml += checkbox.ToString();
                labelCheck.InnerHtml += item.Item as string;

                divCheck.InnerHtml += labelCheck.ToString();
                group.InnerHtml += divCheck.ToString();
            }

            return new MvcHtmlString(group.ToString());
        }
    }
}
﻿using GameStore.DTO;
using GameStore.ViewModels;
using GameStore.ViewModels.Basket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameStore.Helpers
{
    public class OrderFilterViewModelToOrderFilterDtoConvertor
    {
        public static OrderHistoryFilterDto Convertor(OrderFilterViewModel filterViewModel, int totalFilteredCount)
        {
            var filter = new OrderHistoryFilterDto();
            filter.EndDate = filterViewModel.EndDate;
            filter.StartDate = filterViewModel.StartDate;

            filter.PageNumber = filterViewModel.PageNumber;
            filter.NumberOnPage = filterViewModel.NumberOnPage;

            if (filterViewModel.NumberOnPage != "All")
            {
                filter.NumberOnPage = (int.Parse(filterViewModel.NumberOnPage) > totalFilteredCount)
                    ? totalFilteredCount.ToString()
                    : filterViewModel.NumberOnPage;
            }
            else
            {
                filter.NumberOnPage = totalFilteredCount.ToString();
                filter.PageNumber = 1;
            }

            return filter;
        }
    }
}
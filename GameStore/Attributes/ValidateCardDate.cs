﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GameStore.Attributes
{
    public sealed class ValidateCardDate : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            bool result = false;
            if (value != null)
            {
                var date = value.ToString().Trim();
                var parts = date.Split('/');
                if (parts.Length == 2)
                {
                    var month = parts[0];
                    var year = parts[1];

                    int monthInt, yearInt;

                    bool getMonth = int.TryParse(month, out monthInt);
                    bool getYear = int.TryParse(year, out yearInt);

                    if (getYear && getMonth)
                    {
                        var dateCurrent = DateTime.UtcNow;
                        if (yearInt >= dateCurrent.Year && yearInt <= dateCurrent.Year + 10)
                        {
                            if (yearInt == dateCurrent.Year)
                            {
                                if (monthInt >= dateCurrent.Month && monthInt <= 12)
                                {
                                    result = true;
                                }
                            }
                            else if (monthInt > 0 && monthInt <= 12)
                            {
                                result = true;
                            }
                        }
                    }
                }
            }
            return result;
        }
    }
}
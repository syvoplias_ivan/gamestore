﻿using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace GameStore.Attributes
{
    public class NameVerifyAttribute : ValidationAttribute
    {
        private int _allowedDigitsCount;
        private int _minLettersCount;

        public NameVerifyAttribute(int minLettersCount, int maxDigitsCount)
        {
            _minLettersCount = minLettersCount;
            _allowedDigitsCount = maxDigitsCount;
        }

        public override bool IsValid(object value)
        {
            bool result = false;

            string stringValue = value as string;

            if (stringValue != null)
            {
                var digitsCount = stringValue.Count(x => char.IsDigit(x));

                var lettersCount = stringValue.Count(x => char.IsLetter(x));

                if (digitsCount <= _allowedDigitsCount && lettersCount >= _minLettersCount)
                {
                    result = true;
                }
            }

            return result;
        }
    }
}
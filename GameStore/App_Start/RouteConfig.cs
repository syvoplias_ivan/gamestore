﻿using GameStore.Common.Helpers;
using GameStore.Helpers;
using GameStore.Helpers.LocalizationHelpers;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;

namespace GameStore
{
    public class RouteConfig
    {
        private static CultureConstraint langPattern = new CultureConstraint("en", LanguageCode.En.GetAllValuesOfEnum().Select(x => x.ToString().ToLower()).ToArray());

        public static void RegisterRoutes(RouteCollection routes)
        {
            //routes.LowercaseUrls = true;
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRouteWithName("Games",
                "games",
                new { controller = "game", action = "index", lang = "en" },
                new { controller = new AreEqualToAnyConstraint("game"), action = new AreEqualToAnyConstraint("index") }
            );

            routes.MapRouteWithName("Orders",
                "orders",
                new { controller = "basket", action = "orders", lang = "en" },
                new { controller = new AreEqualToAnyConstraint("basket"), action = new AreEqualToAnyConstraint("orders") }
            );

            routes.MapRouteWithName("Comments",
                "{controller}/{action}",
                new { controller = "comment", lang = "en" },
                new { controller = new AreEqualToAnyConstraint("comment"), action = new AreEqualToAnyConstraint("delete", "ban") }
                );

            routes.MapRouteWithName("GamesNew",
                "games/{action}",
                new { controller = "game", lang = "en" },
                new { controller = new AreEqualToAnyConstraint("game"), action = new AreEqualToAnyConstraint("new") }
            );

            routes.MapRouteWithName("Gamemain",
                "{controller}/{action}",
                new { controller = "game", action = "index", lang = "en" },
                new { controller = new AreEqualToAnyConstraint("game"), action = new AreEqualToAnyConstraint("index") }
            );

            routes.MapRouteWithName("Order",
                "order/{action}",
                new { controller = "basket", action = "order", lang = "en" },
                new { controller = new AreEqualToAnyConstraint("basket"), action = new AreEqualToAnyConstraint("pay", "edit", "delete", "visapayment", "order", "history") }
            );

            routes.MapRouteWithName("PublisherGet",
                "{controller}/{companyName}",
                new { controller = "publisher", action = "get", lang = "en" },
                new { controller = new AreEqualToAnyConstraint("publisher"), companyName = new NotEqual("new", "index", "update", "delete", "") }
            );

            routes.MapRouteWithName("Publisher",
                "publisher/{action}",
                new { controller = "publisher", action = "index", lang = "en" },
                new { controller = new AreEqualToAnyConstraint("publisher"), action = new AreEqualToAnyConstraint("new", "index", "update", "delete", "") }
            );

            routes.MapRouteWithName("GameActions",
               "{controller}/{action}",
               new { controller = "game", action = "index", lang = "en" },
               new { controller = new AreEqualToAnyConstraint("game"), action = new AreEqualToAnyConstraint("update", "remove", "new", "download") }
           );

            routes.MapRouteWithName("GamesKey",
                "{controller}/{key}",
                new { controller = "game", action = "game", lang = "en" },
                new { controller = new AreEqualToAnyConstraint("game"), action = "game", key = new NotEqual("game", "new", "getgamescount", "index") }
            );

            routes.MapRouteWithName("GameComment",
                "game/{key}/newcomment",
                new { controller = "comment", action = "newcomment", lang = "en" },
                new { controller = new AreEqualToAnyConstraint("comment"), action = new AreEqualToAnyConstraint("newcomment"), key = new NotEqual("game", "new", "getgamescount") }
            );

            routes.MapRouteWithName("GameBuy",
                "game/{key}/buy",
                new { controller = "basket", action = "buy", lang = "en" },
                new { controller = "basket", action = "buy", key = new NotEqual("game", "new", "getgamescount") }
            );

            routes.MapRouteWithName("Basket",
                "basket/{action}",
                new { controller = "basket", action = "index", lang = "en" },
                new { controller = new AreEqualToAnyConstraint("basket"), action = new AreEqualToAnyConstraint("index", "clear"), }
            );

            routes.MapRouteWithName("",
                "{controller}/{action}/{id}",
                new
                {
                    controller = "Game",
                    action = "Index",
                    id = "",
                    lang = "en"
                },
                null
            );

            routes.MapLocalizedMvcAttributeRoutes(
                urlPrefix: "{lang}/",
                defaults: new { lang = "en" },
                constraints: new { lang = langPattern }
                );
        }
    }
}
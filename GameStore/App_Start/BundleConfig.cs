﻿using System.Web.Optimization;

namespace GameStore.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.IgnoreList.Clear();

            bundles.Add(new ScriptBundle("~/bundles/jquery")
                .Include("~/Scripts/jquery-{version}.js",
                "~/Scripts/jquery-migrate-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui")
                .Include("~/Scripts/jquery-ui-1.12.1",
                "~/Scripts/jquery-ui-1.12.1.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval")
                .Include("~/Scripts/jquery.unobtrusive*",
                "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/datepicker")
                .Include("~/Scripts/jquery-ui-1.12.1.min.js").Include("~/Scripts/datepicker.js"));

            bundles.Add(new StyleBundle("~/styles/jqueryui")
                    .IncludeDirectory("~/Content/themes/base", "*.css"));
        }
    }
}
using GameStore.Auth;
using GameStore.Auth.Interfaces;
using GameStore.BLL.Abstract;
using GameStore.BLL.DI;
using GameStore.BLL.Service;
using GameStore.Common;
using GameStore.Filters;
using GameStore.Helpers.LocalizationHelpers;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Web.Common;
using Ninject.Web.Mvc.FilterBindingSyntax;
using System;
using System.Web;
using System.Web.Mvc;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(GameStore.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(GameStore.App_Start.NinjectWebCommon), "Stop")]

namespace GameStore.App_Start
{
    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            NinjectInitializer.Initialize(kernel);
            kernel.Bind<ILanguageConfiguration>().To<LanguageConfiguration>();
            kernel.Bind<IAuthentication>().To<CustomAuthentication>();
            kernel.Bind<ILoggerService>()
                .ToMethod(
                    p => { return LoggerService.GetLoggingService(p.Request.Target.Member.DeclaringType.ToString()); });
            kernel.BindFilter<IpRequestLoggingFilter>(FilterScope.Global, 0);
            kernel.BindFilter<ErrorsLoggingFilter>(FilterScope.Global, 0);
            kernel.BindFilter<EventsLoggingFilter>(FilterScope.Global, 0);
            kernel.BindFilter<PerformanceFilter>(FilterScope.Global, 0);
        }
    }
}
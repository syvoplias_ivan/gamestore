﻿using GameStore.Filters;
using System.Web.Mvc;

namespace GameStore.App_Start
{
    public class FilterConfig
    {
        public static void RegisterFilters(GlobalFilterCollection filters)
        {
            filters.Add(new ErrorsLoggingFilter());
            filters.Add(new IpRequestLoggingFilter());
            filters.Add(new PerformanceFilter());
            filters.Add(new EventsLoggingFilter());
        }
    }
}
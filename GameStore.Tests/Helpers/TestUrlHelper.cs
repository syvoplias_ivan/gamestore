﻿using GameStore.Tests.Stubs;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace GameStore.Tests.Helpers
{
    public class TestUrlHelper
    {
        public static UrlHelper GetUrlHelper(string appPath = "/", RouteCollection routes = null)
        {
            if (routes == null)
            {
                routes = new RouteCollection();
                RouteConfig.RegisterRoutes(routes);
            }

            HttpContextBase httpContext = new StubHttpContext();
            var routeData = new RouteData();
            routeData.Values.Add("controller", "defaultcontroller");
            routeData.Values.Add("action", "defaultaction");
            var requestContext = new RequestContext(httpContext, routeData);
            var helper = new UrlHelper(requestContext, routes);
            return helper;
        }
    }
}
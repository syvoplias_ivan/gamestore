﻿using GameStore.Common;
using GameStore.Common.Helpers;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Localization;
using System;
using System.Collections.Generic;

namespace GameStore.Tests.Helpers
{
    public class GenerateGames
    {
        public static List<Game> GenerateWithGenre(string genreName)
        {
            var genres = new List<string> { "action", "shooter", "RPG", "arcade", "other", genreName };
            var guids = new List<Guid>
            {
                Guid.NewGuid(),
                Guid.NewGuid(),
                Guid.NewGuid(),
                Guid.NewGuid(),
                Guid.NewGuid(),
                Guid.NewGuid()
            };

            var list = new List<Game>();
            var j = 0;
            for (var i = 0; i < 5; i++)
            {
                var one = guids[i];
                var two = guids[(i + 1) % 6];
                var three = guids[(i + 2) % 6];
                var game = new Game
                {
                    Genres = new List<Genre>
                    {
                        new Genre {Id = one, Name = genres[i]},
                        new Genre {Id = two, Name = genres[(i + 1)%6]},
                        new Genre {Id = three, Name = genres[(i + 2)%6]}
                    },
                    Id = Guid.NewGuid(),
                    Name = "game" + i + 1,
                    Description = "game" + i + 1,
                    Key = "game-" + (i + 1)
                };
                list.Add(game);
            }
            return list;
        }

        public static List<Game> GenerateWithPlatformType(TypeOfPlatform type, List<Game> games)
        {
            var platforms = new List<TypeOfPlatform>
            {
                TypeOfPlatform.Browser,
                TypeOfPlatform.Console,
                TypeOfPlatform.Desktop,
                TypeOfPlatform.Mobile,
                type
            };
            var guids = new List<Guid>
            {
                Guid.NewGuid(),
                Guid.NewGuid(),
                Guid.NewGuid(),
                Guid.NewGuid(),
                Guid.NewGuid(),
                Guid.NewGuid()
            };
            var list = games;
            var j = 0;
            for (var i = 0; i < 5; i++)
            {
                var one = guids[i];
                var two = guids[(i + 1) % 6];
                var three = guids[(i + 2) % 6];
                var game = new Game
                {
                    Genres = games[i].Genres,
                    PlatformTypes = new List<PlatformType>
                    {
                        new PlatformType {Id = one, Type = platforms[i]},
                        new PlatformType {Id = two, Type = platforms[(i + 1)%5]},
                        new PlatformType {Id = three, Type = platforms[(i + 2)%5]}
                    },
                    Id = Guid.NewGuid(),
                    Name = "game" + i + 1,
                    Description = "game" + i + 1,
                    Key = "game-" + (i + 1)
                };
                list[i] = game;
            }
            return list;
        }

        public static List<Game> GenerateWithPublishers(List<string> publishersList, List<Game> source)
        {
            var random = new Random();
            foreach (var game in source)
            {
                var index = random.Next(0, publishersList.Count);
                game.Publisher = new Publisher() { CompanyName = publishersList[index] };
            }
            return source;
        }

        public static List<Game> UpdateEverySecondWithValues(List<string> genres, List<string> publishers,
            List<string> platforms, DateTime date, decimal price, List<Game> source)
        {
            var rnd = new Random();
            foreach (var game in source)
            {
                var genreIdx = rnd.Next(0, genres.Count);
                game.Genres = new List<Genre>() { new Genre() { Name = genres[genreIdx] } };

                var publIdx = rnd.Next(0, publishers.Count);
                game.Publisher = new Publisher() { CompanyName = publishers[publIdx] };

                var rndDays = rnd.Next(0, 5);
                game.PublishDate = date.AddDays(rndDays * -1);

                var platformIdx = rnd.Next(0, platforms.Count);
                var type = platforms[platformIdx].ConvertToEnum<TypeOfPlatform>();
                game.PlatformTypes = new List<PlatformType>() { new PlatformType() { Type = type } };

                var priceRnd = price + rnd.Next(0, 10);
                game.Price = priceRnd;
            }
            return source;
        }
    }
}
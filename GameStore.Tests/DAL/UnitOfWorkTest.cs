﻿using GameStore.DAL.Abstract;
using GameStore.DAL.Context;
using GameStore.DAL.Entities;
using GameStore.DAL.UnitOfWork;
using Moq;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace GameStore.Tests.DAL
{
    [TestFixture]
    public class UnitOfWorkTest
    {
        private Mock<IGameStoreContext> _mockContext;
        private Mock<IMongoNorthwindContext> _mockNorthwindContext;
        private Mock<IMongoLogger> _mongoLogger;
        private UnitOfWork _sut;

        [SetUp]
        public void SetUp()
        {
            _mockContext = new Mock<IGameStoreContext>();
            _mockNorthwindContext = new Mock<IMongoNorthwindContext>();
            _mongoLogger = new Mock<IMongoLogger>();
            _sut = new UnitOfWork(_mockContext.Object, _mockNorthwindContext.Object, _mongoLogger.Object);
        }

        [Test]
        public void SaveChanges_VerifySavingChanges_SaveChangesVerified()
        {
            _mockContext.Setup(x => x.SaveChanges()).Verifiable();
            _sut.SaveChanges();
            Mock.Verify(_mockContext);
        }

        [Test]
        public void RepositoryAsync_ReturnsRepositoryGame_NotNull()
        {
            Assert.IsNotNull(_sut.Repository<Game>());
        }

        [Test]
        public void Repository_ReturnRepositoryGame_IsInstanceOfRepositoryGame()
        {
            Assert.IsInstanceOf<IRepository<Game>>(_sut.Repository<Game>());
        }

        [Test]
        public void RepositoryAsync_RepositoryExists_IsInstanceOfRepositoryGame()
        {
            _sut.Repository<Game>();
            Assert.IsInstanceOf<IRepository<Game>>(_sut.Repository<Game>());
        }

        [Test]
        public void Dispose_DisposeContext_ExceptionNotThrown()
        {
            Assert.That(() => _sut.Dispose(), Throws.Nothing);
        }
    }
}
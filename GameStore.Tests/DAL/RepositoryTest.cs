﻿using GameStore.DAL.Abstract;
using GameStore.DAL.Context;
using GameStore.DAL.Entities;
using GameStore.DAL.Repository;
using GameStore.Tests.Helpers;
using GameStore.Tests.Stubs;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace GameStore.Tests.DAL
{
    public class RepositoryTest
    {
        private List<Game> _fakeGames;
        private Mock<IGameStoreContext> _mockContext;
        private Mock<IMongoLogger> _loggerMock;
        private Repository<Game> _sut;

        [SetUp]
        public void SetUp()
        {
            _fakeGames = GenerateGames.GenerateWithGenre("sport");
            var set = new FakeDbSet<Game>();
            _fakeGames.ForEach(x => set.Add(x));
            _mockContext = new Mock<IGameStoreContext>();
            _loggerMock = new Mock<IMongoLogger>();
            _mockContext.Setup(x => x.Set<Game>()).Returns(set);
            _sut = new Repository<Game>(_mockContext.Object, _loggerMock.Object);
        }

        [Test]
        public void Find_KeyIsNull_ReturnsNull()
        {
            //Assert.IsNull(_sut.Find(null));
        }

        [Test]
        public void Delete_EntityIsNull_ThrowsException()
        {
            Assert.Throws(typeof(NullReferenceException), () => _sut.Delete(null));
        }

        [Test]
        public void Insert_InsertElement_CollectionCountChanges()
        {
            var game = new Game { Id = Guid.NewGuid(), Key = "game_17", Name = "game_17" };
            var start = _sut.Enumerable().Count();
            _sut.Insert(game);
            var countEnd = _sut.Enumerable().Count();
            Assert.AreNotEqual(start, countEnd);
        }

        [Test]
        public void InsertRange_InsertElements_CollectionCountChanges()
        {
            var games = new List<Game>()
            {
                new Game {Id = Guid.NewGuid(), Key = "game_17", Name = "game_17"},
                new Game {Id = Guid.NewGuid(), Key = "game_18", Name = "game_18"}
            };
            var start = _sut.Enumerable().Count();
            _sut.InsertRange(games);
            var countEnd = _sut.Enumerable().Count();
            Assert.AreNotEqual(start, countEnd);
        }

        [Test]
        public void Delete_NewEntity_NotThrowsException()
        {
            Assert.DoesNotThrow(() => _sut.Delete(new Game { Id = Guid.NewGuid() }));
        }

        [Test]
        public void Update_UpdateExistingItem_ItemValueChanges()
        {
            //var game = new Game { Id = _fakeGames[0].Id, Name = "WarCraft", Key = "warcraft", Description = "any" };
            //var stGame = _sut.Find(_fakeGames[0].Id);
            //_sut.Update(game);
            //Assert.AreNotEqual(stGame, _sut.Find(1));
        }

        [Test]
        public void Enumerable_ElementsExist_ReturnCollection()
        {
            Assert.AreEqual(_fakeGames, _sut.Enumerable());
        }

        [Test]
        public void Select_KeyIsNull_ReturnsAllCollection()
        {
            var games = _sut.Select().ToList();
            Assert.AreEqual(_fakeGames, games);
        }

        [Test]
        public void SelectAll_WithPage_ReturnsCollection()
        {
            var page = 1;
            var pageSize = 2;
            var games = _sut.Select(page: page, pageSize: pageSize);
            Assert.AreEqual(2, games.Count());
        }

        [Test]
        public void Count_EmptyGames_ReturnsZero()
        {
            //Arrange
            _sut.Select(filter: x => !x.IsDeleted).ToList().ForEach(x => _sut.Delete(x));

            //Act & Assert
            Assert.AreEqual(_sut.Count(), 0);
        }

        [Test]
        public void Count_GamesExist_ReturnsCountOfGames()
        {
            //Act & Assert
            Assert.AreEqual(_sut.Count(), 5);
        }

        [Test]
        public void FilteredCount_EmptyGames_ReturnsZero()
        {
            //Arrange
            _sut.Select(filter: x => !x.IsDeleted).ToList().ForEach(x => _sut.Delete(x));

            //Act & Assert
            Assert.AreEqual(_sut.FilteredCount(x => !x.IsDeleted), 0);
        }

        [Test]
        public void FilteredCount_GamesExist_ReturnsCountOfGames()
        {
            //Act & Assert
            Assert.AreEqual(_sut.FilteredCount(x => !x.IsDeleted), 5);
        }
    }
}
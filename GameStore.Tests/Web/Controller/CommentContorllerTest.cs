﻿using GameStore.BLL.Abstract;
using GameStore.Controllers;
using GameStore.DAL.Entities.Authorizaion;
using GameStore.DTO;
using GameStore.DTO.Authentification;
using GameStore.MapperConfiguration;
using GameStore.Tests.Stubs;
using GameStore.ViewModels;
using GameStore.ViewModels.Comment;
using Moq;
using NUnit.Framework;
using NUnit.Framework.Internal;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;

namespace GameStore.Tests.Web.Controller
{
    [TestFixture]
    public class CommentContorllerTest
    {
        private Mock<ICommentService> _mockCommentService;
        private Mock<IGameService> _mockGameService;
        private CommentController _sut;
        private List<CommentDto> _fakeComments;

        [SetUp]
        public void SetUp()
        {
            AutoMapperWebConfiguration.Configure();
            _fakeComments = new List<CommentDto>();
            var guid = Guid.NewGuid();
            for (int i = 0; i < 10; i++)
            {
                _fakeComments.Add(new CommentDto()
                {
                    Id = Guid.NewGuid(),
                    Body = "New comment " + i,
                    User = new UserDto() { NickName = "User" + (i + 100) },
                    GameKey = ""
                });
            }

            _mockCommentService = new Mock<ICommentService>();
            _mockGameService = new Mock<IGameService>();
            _sut = new CommentController(_mockCommentService.Object, _mockGameService.Object);
        }

        [Test]
        public void AddComment_ValidComment_ReturnsSuccess()
        {
            //Arrange
            _mockCommentService.Setup(x => x.AddComment(It.IsAny<CommentDto>(), It.IsAny<String>()));
            _mockGameService.Setup(x => x.GetGame(It.IsAny<string>())).Returns(new GameDto() { Comments = new List<CommentDto>() });
            _sut.ControllerContext = new ControllerContext(new StubHttpContext(), new System.Web.Routing.RouteData(), _sut);

            //Act
            _sut.NewComment(new AddCommentViewModel() { Body = "game_1", Name = "John", GameKey = "" });

            //Assert
            Assert.IsTrue(_sut.Response.StatusCode == (int)HttpStatusCode.OK);
        }

        [Test]
        public void AddComment_InvalidComment_ReturnsBadRequest()
        {
            //Arrange
            _mockCommentService.Setup(x => x.AddComment(It.IsAny<CommentDto>(), It.IsAny<String>()));
            _mockGameService.Setup(x => x.GetGame(It.IsAny<string>())).Returns(new GameDto() { Comments = new List<CommentDto>() });
            _sut.ModelState.AddModelError("test", "test");
            _sut.ControllerContext = new ControllerContext(new StubHttpContext(), new System.Web.Routing.RouteData(), _sut);

            //Act
            _sut.NewComment(new AddCommentViewModel() { Body = "game_1", GameKey = "" });

            ////Assert
            Assert.IsTrue(_sut.Response.StatusCode == (int)HttpStatusCode.BadRequest);
        }

        [Test]
        public void Comments_GetCommentsWithInvalidKey_ReturnsError()
        {
            //Arrange
            _mockCommentService.Setup(x => x.GetComments(It.IsAny<String>()));
            _sut.ControllerContext = new ControllerContext(new StubHttpContext(), new System.Web.Routing.RouteData(), _sut);

            //Act
            _sut.Comments(null); ;

            //Assert
            Assert.IsTrue(_sut.Response.StatusCode == (int)HttpStatusCode.BadRequest);
        }

        [Test]
        public void Comments_GetCommentsWithValidKey_ReturnsSuccess()
        {
            //Arrange
            _mockCommentService.Setup(x => x.GetComments(It.IsAny<String>())).Returns(_fakeComments);
            _sut.ControllerContext = new ControllerContext(new StubHttpContext(), new System.Web.Routing.RouteData(), _sut);

            //Act
            _sut.Comments("game_1");

            //Assert
            Assert.IsTrue(_sut.Response.StatusCode == (int)HttpStatusCode.OK);
        }
    }
}
﻿using AutoMapper;
using GameStore.BLL.Abstract;
using GameStore.Common;
using GameStore.Controllers;
using GameStore.DAL.Entities;
using GameStore.DTO;
using GameStore.MapperConfiguration;
using GameStore.Tests.Helpers;
using GameStore.Tests.Stubs;
using GameStore.ViewModels;
using GameStore.ViewModels.Game;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Net;
using System.Web.Mvc;

namespace GameStore.Tests.Web.Controller
{
    [TestFixture]
    public class GameControllerTest
    {
        private GameController _sut;
        private Mock<IGameService> _mockGameService;
        private Mock<IPlatformService> _mockPlatformService;
        private Mock<IGenreService> _mockGenreService;
        private Mock<IPublisherService> _mockPublisherService;
        private List<Game> _fakeGames;

        [SetUp]
        public void SetUp()
        {
            _fakeGames = GenerateGames.GenerateWithGenre("sport");
            _fakeGames = GenerateGames.GenerateWithPlatformType(TypeOfPlatform.Desktop, _fakeGames);
            _mockGameService = new Mock<IGameService>();
            _mockPlatformService = new Mock<IPlatformService>();
            _mockGenreService = new Mock<IGenreService>();
            _mockPublisherService = new Mock<IPublisherService>();
            _sut = new GameController(_mockGameService.Object, _mockPlatformService.Object, _mockGenreService.Object, _mockPublisherService.Object);
            AutoMapperWebConfiguration.Configure();
        }

        [Test]
        public void Index_ReturnGames_ReturnGamesList()
        {
            //Arrange
            _mockGameService.Setup(x => x.GetAllGames()).Returns(Mapper.Map<List<GameDto>>(_fakeGames));
            _sut.ControllerContext = new ControllerContext(new StubHttpContext(), new System.Web.Routing.RouteData(), _sut);

            //Act
            _sut.Index(new FilterViewModel());

            //Assert
            Assert.IsTrue(_sut.Response.StatusCode == (int)HttpStatusCode.OK);
        }

        [Test]
        public void Update_UpdateNotExistingGame_ReturnsError()
        {
            //Arrange
            _mockGameService.Setup(x => x.UpdateGame(It.IsAny<GameDto>())).Throws(new InvalidOperationException());

            //Assert
            Assert.That(() => _sut.Update(new EditGameViewModel() { Description = "game_52", Id = Guid.NewGuid(), Key = "game_52" }),
                Throws.InvalidOperationException);
        }

        [Test]
        public void New_NotValidGame_ReturnsError()
        {
            //Arrange
            _mockGameService.Setup(x => x.AddGame(It.IsAny<GameDto>()));
            _sut.ModelState.AddModelError("Name", new ModelValidationException("Name is null"));
            _sut.ControllerContext = new ControllerContext(new StubHttpContext(), new System.Web.Routing.RouteData(), _sut);

            //Act
            var res = _sut.New(new CreateGameViewModel() { Key = "game_222", Name = null, Description = null, SelectedGenres = new List<string>() { "Arcada", "RPG" }, SelectedPlatformTypes = new List<string>() { "mobile", "desktop" } });

            //Assert
            Assert.IsTrue(_sut.Response.StatusCode == (int)HttpStatusCode.BadRequest);
        }

        [Test]
        public void Game_InvalidGameKey_ReturnError()
        {
            //Arrange
            _mockGameService.Setup(x => x.GetGame(It.IsAny<string>())).Throws<InvalidOperationException>();

            //Assert
            Assert.Throws<InvalidOperationException>(() => _sut.Game("game-11"));
        }

        [Test]
        public void Game_ValidGameKey_ReturnGame()
        {
            //Arrange
            _mockGameService.Setup(x => x.GetGame(It.IsAny<string>())).Returns((string key) => Mapper.Map<GameDto>(_fakeGames.Find(x => x.Key == key)));
            _sut.ControllerContext = new ControllerContext(new StubHttpContext(), new System.Web.Routing.RouteData(), _sut);

            //Act
            _sut.Game("game-1");

            //Assert
            Assert.IsNotNull(_sut.Response.StatusCode == (int)HttpStatusCode.OK);
        }

        [Test]
        public void Remove_GameInvalidKey_ThrowError()
        {
            //Arrange
            _mockGameService.Setup(x => x.GetGame(It.IsAny<string>())).Returns(new GameDto());
            _mockGameService.Setup(x => x.DeleteGame(It.IsAny<string>())).Throws<InvalidOperationException>();

            //Assert
            Assert.That(() => _sut.Remove("game_11"), Throws.InvalidOperationException);
        }

        [Test]
        public void Remove_DeleteValidKey_RemovesKey()
        {
            //Arrange
            _mockGameService.Setup(x => x.GetGame(It.IsAny<string>())).Returns(new GameDto());
            _mockGameService.Setup(x => x.DeleteGame(It.IsAny<string>()));
            _sut.ControllerContext = new ControllerContext(new StubHttpContext(), new System.Web.Routing.RouteData(), _sut);

            //Act
            _sut.Remove("game_1");

            //Assert
            Assert.IsTrue(_sut.Response.StatusCode == (int)HttpStatusCode.OK);
        }

        [Test]
        public void New_AddValidModel_ReturnSuccess()
        {
            //Arrange
            _mockGameService.Setup(x => x.AddGame(It.IsAny<GameDto>()));
            _sut.ControllerContext = new ControllerContext(new StubHttpContext(), new System.Web.Routing.RouteData(), _sut);

            //Act
            var res = _sut.New(new CreateGameViewModel() { Key = "game_11", Name = "any", Description = "any" });

            //Assert
            Assert.IsTrue(_sut.Response.StatusCode == (int)HttpStatusCode.OK);
        }

        [Test]
        public void New_AddModelWithExistingKey_ReturnError()
        {
            //Arrange
            _mockGameService.Setup(x => x.AddGame(It.IsAny<GameDto>())).Throws<InvalidOperationException>();

            //Assert
            Assert.That(() => _sut.New(new CreateGameViewModel() { Key = "game_1", Name = "any", Description = "any" }),
                Throws.InvalidOperationException);
        }

        [Test]
        public void Update_ValidViewModel_ReturnsSuccess()
        {
            // Arrange
            var tempEdit = Mapper.Map<EditGameViewModel>(Mapper.Map<GameDto>(_fakeGames[0]));
            _mockGameService.Setup(x => x.UpdateGame(It.IsAny<GameDto>()));
            _mockGameService.Setup(x => x.GetGame(It.IsAny<Guid>())).Returns(Mapper.Map<GameDto>(_fakeGames[0]));
            _sut.ControllerContext = new ControllerContext(new StubHttpContext(), new System.Web.Routing.RouteData(), _sut);

            var game = new EditGameViewModel()
            {
                Id = _fakeGames[0].Id,
                Key = "game_222",
                Description = "game_222",
                SelectedGenreIds = new List<string>()
                {
                    "Arcada",
                    "MMORPG"
                },
                SelectedPlatformTypeIds = new List<string>()
                {
                    "mobile", "desktop"
                }
            };

            //Act
            var t = Mapper.Map<GameDto>(game);
            _fakeGames[0] = Mapper.Map<Game>(t);
            _sut.Update(game);

            //Assert
            Assert.IsTrue(_sut.Response.StatusCode == (int)HttpStatusCode.OK);
        }

        [Test]
        public void Index_DbIsEmpty_ReturnError()
        {
            //Arrange
            _mockGameService.Setup(x => x.GetFilteredGames(It.IsAny<FilterDto>())).Throws<ArgumentNullException>();

            //Assert
            Assert.That(() => _sut.Index(new FilterViewModel()), Throws.ArgumentNullException);
        }
    }
}
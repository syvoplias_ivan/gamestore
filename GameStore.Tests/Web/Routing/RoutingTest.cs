﻿using GameStore.Tests.Stubs;
using NUnit.Framework;
using System.Web.Routing;

namespace GameStore.Tests.Web.Routing
{
    [TestFixture]
    public class RoutingTest
    {
        [SetUp]
        public void SetUp()
        {
            _routes = new RouteCollection();
            RouteConfig.RegisterRoutes(_routes);
        }

        private RouteCollection _routes;

        [Test]
        public void RouteConfig_GameComments_NoSuchAction()
        {
            //Arrange
            var context = new StubHttpContext(requestUrl: "~/game/comments");

            //Act
            var routeData = _routes.GetRouteData(context);

            //Assert
            Assert.AreNotEqual("comments", routeData.Values["action"]);
        }

        [Test]
        public void RouteConfig_DefaultPage_GameIndex()
        {
            //Arrange
            var context = new StubHttpContext(requestUrl: "~/");

            //Act
            var routeData = _routes.GetRouteData(context);

            //Assert
            Assert.IsNotNull(routeData);
            Assert.AreEqual("game", routeData.Values["controller"]);
            Assert.AreEqual("index", routeData.Values["action"]);
        }

        [Test]
        public void RouteConfig_GameKeyAction_ValidUrl()
        {
            //Arrange
            var context = new StubHttpContext(requestUrl: "~/game/game_1/buy");

            //Act
            var routeData = _routes.GetRouteData(context);

            //Assert
            Assert.IsNotNull(routeData);
            Assert.AreEqual("basket", routeData.Values["controller"]);
            Assert.AreEqual("buy", routeData.Values["action"]);
            Assert.AreEqual("game_1", routeData.Values["key"]);
        }

        [Test]
        public void RouteConfig_GameKeyRoute_GetAction()
        {
            //Arrange
            var context = new StubHttpContext(requestUrl: "~/game/game_1");

            //Act
            var routeData = _routes.GetRouteData(context);

            //Assert
            Assert.IsNotNull(routeData);
            Assert.AreEqual("game", routeData.Values["controller"]);
            Assert.AreEqual("game_1", routeData.Values["key"]);
            Assert.AreNotEqual("", routeData.Values["action"]);
        }

        [Test]
        public void RouteConfig_Games_GamesStart()
        {
            //Arrange
            var context = new StubHttpContext(requestUrl: "~/games");

            //Act
            var routeData = _routes.GetRouteData(context);

            //Assert
            Assert.IsNotNull(routeData);
            Assert.AreEqual("game", routeData.Values["controller"]);
            Assert.AreEqual("index", routeData.Values["action"]);
        }

        [Test]
        public void RouteConfig_Publisher_StandartRoute()
        {
            //Arrange
            var context = new StubHttpContext(requestUrl: "~/publisher");

            //Act
            var routeData = _routes.GetRouteData(context);

            //Assert
            Assert.IsNotNull(routeData);
            Assert.AreEqual("publisher", routeData.Values["controller"]);
            Assert.AreEqual("index", routeData.Values["action"]);
        }
    }
}
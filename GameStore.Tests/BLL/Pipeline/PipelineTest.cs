﻿using GameStore.BLL.Abstract;
using GameStore.BLL.Pipeline;
using GameStore.BLL.Pipeline.GameFilters;
using GameStore.BLL.Pipeline.OrderFilters;
using GameStore.Common;
using GameStore.DAL.Entities;
using GameStore.DTO;
using GameStore.Tests.Helpers;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace GameStore.Tests.BLL.Pipeline
{
    public class PipelineTest
    {
        private List<Game> _fakeGames;
        private Pipeline<Game> _sut;

        [SetUp]
        public void SetUp()
        {
            _sut = new Pipeline<Game>();
            _fakeGames = GenerateGames.GenerateWithGenre("action");
        }

        [Test]
        public void Pipeline_RegisterNullFilter_FilterFieldIsNull()
        {
            //Arrange
            IExpressionFilter<Game> filter = new GameDefaultFilter();
            filter.FilterBodyExpression = null;

            //Act & Assert
            Assert.DoesNotThrow(() => _sut.Register(filter));
        }

        [Test]
        public void Pipeline_RegisterFilter_FilterFieldIsNotNull()
        {
            //Arrange
            IExpressionFilter<Game> filter = new GameDefaultFilter();

            //Act
            _sut.Register(filter);
            var pipelineFilter = _sut.Filter;

            //Assert
            Assert.IsNotNull(pipelineFilter);
        }

        [Test]
        public void Pipeline_AddOrderFilter_OrderFilterIsNotNull()
        {
            //Arrange
            var orderFilter = new GameSortFilter(SortType.New);
            var current = DateTime.UtcNow;
            var orderedGames = _fakeGames.OrderBy(x => x.PublishDate.CompareTo(current) * -1).ToList();

            //Act
            _sut.Register(orderFilter);
            var pipelineResult = _fakeGames.OrderBy(_sut.OrderFilter.Order.Compile(), new EntitiesOrderComparer()).ToList();

            //Assert
            for (int i = 0; i < pipelineResult.Count(); i++)
            {
                var game1 = pipelineResult[i];
                var game2 = orderedGames[i];
                Assert.AreEqual(game2.Id, game1.Id);
            }
        }

        [Test]
        public void Pipeline_PipelineFromFilterDto_ReturnsFilteredGame()
        {
            //Arrange
            var filterDto = new FilterDto()
            {
                SortType = "New",
                Date = "Last 2 years",
                GameName = "game",
                Genres = new List<string>() { "action", "strategy", "arcade" },
                Platforms = new List<string>() { "Desktop", "Console", "Mobile" },
                Publishers = new List<string>() { "Activision" },
                NumberOnPage = "2",
                PageNumber = 1,
                PriceHigh = 50,
                PriceLow = 3
            };

            _fakeGames = GenerateGames.UpdateEverySecondWithValues(filterDto.Genres.ToList(),
                filterDto.Publishers.ToList(), filterDto.Platforms.ToList(), DateTime.UtcNow, 7, _fakeGames);
            var pipeline = FilterDtoToPipelineConvertor.Convert(filterDto);

            //Act
            var games = _fakeGames.Where(pipeline.Filter.FilterBodyExpression.Compile())
                .OrderBy(pipeline.OrderFilter.Order.Compile(), new EntitiesOrderComparer())
                .Skip((pipeline.Pagination.PageNumber - 1) * pipeline.Pagination.CountOnPage)
                .Take(pipeline.Pagination.CountOnPage);

            //Assert
            Assert.IsNotEmpty(games);
        }
    }
}
﻿using GameStore.BLL.Abstract;
using GameStore.BLL.Pipeline;
using GameStore.BLL.Pipeline.GameFilters;
using GameStore.Common;
using GameStore.DAL.Entities;
using GameStore.DTO;
using GameStore.Tests.Helpers;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.Tests.BLL.Pipeline
{
    public class PipelineGameFiltersTest
    {
        private List<Game> _fakeGames;
        private ExpressionFilter<Game> _sut;

        [SetUp]
        public void SetUp()
        {
            _fakeGames = GenerateGames.GenerateWithGenre("action");
            _fakeGames.Add(
                new Game()
                {
                    Description = "game_20",
                    Id = Guid.NewGuid(),
                    IsDeleted = true,
                    Key = "game_20",
                    Name = "game_20",
                    Price = 50,
                    PlatformTypes = new List<PlatformType>() { new PlatformType() { Type = TypeOfPlatform.Desktop } },
                    Genres = new List<Genre>() { new Genre() { Name = "Arcada" } }
                }
                );
        }

        [Test]
        public void DefaultFilter_DefaultFilter_CountOfGamesChanged()
        {
            //Arrange
            _sut = new GameDefaultFilter();

            //Act
            var countOfGames = _fakeGames.Count;
            var filteredGames = _fakeGames.Where(_sut.FilterBodyExpression.Compile());
            var filteredCount = filteredGames.Count();

            //Assert
            Assert.AreNotEqual(countOfGames, filteredCount);
        }

        [Test]
        public void FilterDtoToPipelineConvertor_FilterByDate_PipelineNotNull()
        {
            //Arrange
            var filterDto = new FilterDto() { GameName = "game_20", Genres = null, Platforms = null, Publishers = null };

            //Act
            var pipeline = FilterDtoToPipelineConvertor.Convert(filterDto);

            //Assert
            Assert.IsNotNull(pipeline);
        }

        [Test]
        public void GameNameFilter_FilterByName_ReturnsSpecifiedGame()
        {
            //Arrange
            var filter = new GameByGameNameFilter("game_20");

            //Act
            var games = _fakeGames.Where(filter.FilterBodyExpression.Compile()).ToList();

            //Assert
            Assert.AreEqual("game_20", games[0].Name);
        }

        [Test]
        public void GameByPlatformFilter_FilterByPlatform_ReturnsGameWithSpecifiedPlatform()
        {
            //Arrange
            var games = GenerateGames.GenerateWithPlatformType(TypeOfPlatform.Console, _fakeGames);
            games.Add(
                new Game()
                {
                    Id = Guid.NewGuid(),
                    IsDeleted = false,
                    Description = "game_21",
                    Name = "game_21",
                    Key = "game_21",
                    Price = 50,
                    PlatformTypes = new List<PlatformType>() { new PlatformType() { Type = TypeOfPlatform.Mobile } }
                }
            );
            var filter = new GameByPlatform(new List<string>() { "Console" });

            //Act
            var countBefore = games.Count;
            var gamesNew = games.Where(filter.FilterBodyExpression.Compile());
            var afterCount = gamesNew.Count();

            //Assert
            Assert.AreNotEqual(countBefore, afterCount);
        }

        [Test]
        public void GameByGenreFilter_FilterByGenre_ReturnsGamesWithAnyOfSpecifiedGenres()
        {
            //Arrange
            var filters = new GameByGenreFilter(new List<string>() { "Action", "Strategy", "Arcada" });

            //Act
            var countBefore = _fakeGames.Count;
            var newGames = _fakeGames.Where(filters.FilterBodyExpression.Compile());
            var countAfter = newGames.Count();

            //Assert
            Assert.AreNotEqual(countBefore, countAfter);
        }

        [Test]
        public void GameByPublisher_FilterByPublisher_ReturnsGamesWithAnyOfSpecifiedPublisher()
        {
            //Arrange
            _fakeGames = GenerateGames.GenerateWithPublishers(new List<string>() { "Activision", "Bethesda", "Valve" },
                _fakeGames);
            _fakeGames.Add(new Game()
            {
                Id = Guid.NewGuid(),
                IsDeleted = false,
                Genres = new List<Genre>() { new Genre() { Name = "Action" } },
                PlatformTypes = new List<PlatformType>() { new PlatformType() { Type = TypeOfPlatform.Desktop } },
                Name = "game_22",
                Description = "game_22",
                Publisher = new Publisher() { CompanyName = "Activision" }
            });
            var filter = new GameByPublisher(new List<string>() { "Activision" });

            //Act
            var countBefore = _fakeGames.Count;
            var newGames = _fakeGames.Where(filter.FilterBodyExpression.Compile());
            var countAfter = newGames.Count();

            //Assert
            Assert.AreNotEqual(countBefore, countAfter);
        }

        [Test]
        public void GameByDateFilter_FilterDateRange_ReturnsSingleGameWithDate()
        {
            //Arrange
            var date = DateTime.UtcNow.AddDays(-1);
            var filter = new GameByDateFilter(DateFilterType.LastWeek);
            _fakeGames.Add(new Game()
            {
                Key = "game_222",
                PublishDate = date
            });

            //Act
            var games = _fakeGames.Where(filter.FilterBodyExpression.Compile()).ToList();

            //Assert
            Assert.AreEqual("game_222", games[0].Key);
        }

        [Test]
        public void GameByGameName_EmptyName_CreatesNullFilter()
        {
            //Arrange
            var filter = new GameByGameNameFilter(string.Empty);

            //Act & Assert
            Assert.IsNull(filter.FilterBodyExpression);
        }
    }
}
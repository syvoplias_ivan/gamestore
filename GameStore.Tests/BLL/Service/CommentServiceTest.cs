﻿using GameStore.BLL.Service;
using GameStore.DAL.Abstract;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Authorizaion;
using GameStore.DAL.UnitOfWork;
using GameStore.DTO;
using GameStore.DTO.Authentification;
using GameStore.MapperConfiguration;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace GameStore.Tests.BLL.Service
{
    [TestFixture]
    public class CommentServiceTest
    {
        private Mock<IUnitOfWork> _mockUnitOfWork;
        private CommentService _sut;
        private List<Comment> _fakeComments;

        [SetUp]
        public void SetUp()
        {
            AutoMapperWebConfiguration.Configure();
            _mockUnitOfWork = new Mock<IUnitOfWork>();
            _sut = new CommentService(_mockUnitOfWork.Object);

            var games = new List<Game>
            {
                new Game {Key = "game_1", Id = new Guid()},
                new Game {Key = "game_2", Id = new Guid()}
            };

            _fakeComments = new List<Comment>
            {
                new Comment {Id = new Guid(), Body = "Hi", User = new User() {NickName = "fsdfsdfds"}, Game = games[0]},
                new Comment {Id = new Guid(), User = new User() {NickName = "new comment"}, Body = "Hello!", Game = games[1]},
                new Comment {Id = new Guid(), User = new User() {NickName = "another"}, Body = "Hello 2!", Game = games[0]}
            };
        }

        [Test]
        public void AddComment_CommentIsNull_Exception()
        {
            _mockUnitOfWork.Setup(x => x.Repository<Comment>().Insert(It.IsAny<Comment>()))
                .Throws(new NullReferenceException());

            Assert.That(() => { _sut.AddComment(null, String.Empty); },
                Throws.Exception.TypeOf<NullReferenceException>());
        }

        [Test]
        public void AddComment_CommentAlreadyExists_Exeption()
        {
            var comment = new CommentDto() { Id = _fakeComments[2].Id, User = new UserDto() { NickName = "another" }, Body = "Hello 2!" };
            _mockUnitOfWork.Setup(x => x.Repository<Comment>().Insert(It.IsAny<Comment>())).Throws<InvalidOperationException>();
            _mockUnitOfWork.Setup(x => x.Repository<Game>().Select(It.IsAny<Expression<Func<Game, bool>>>(), null, null, null)).Returns(new List<Game>() { new Game() { Id = _fakeComments[2].Id } }.AsQueryable());
            _mockUnitOfWork.Setup(x => x.Repository<User>().Get(It.IsAny<Func<User, bool>>())).Returns(() => null);
            Assert.That(() =>
            {
                _sut.AddComment(comment, String.Empty);
            },
            Throws.Exception.TypeOf<InvalidOperationException>());
        }

        [Test]
        public void GetAll_RepositoryIsEmpty_ReturnsEmptyList()
        {
            _mockUnitOfWork.Setup(x => x.Repository<Comment>().Enumerable()).Returns(new List<Comment>());
            Assert.IsEmpty(_sut.GetAllComments());
        }

        [Test]
        public void GetComment_IdIsNegative_ReturnsNull()
        {
            _mockUnitOfWork.Setup(x => x.Repository<Comment>().Get(It.IsAny<Func<Comment, bool>>()))
                .Returns((Comment)null);
            Assert.That(_sut.GetComment(Guid.Empty), Is.Null);
        }

        [Test]
        public void GetComments_KeyDoNotExist_ReturnEmpty()
        {
            _mockUnitOfWork.Setup(x => x.Repository<Comment>().Select(It.IsAny<Expression<Func<Comment, bool>>>(), null, null, null)).Returns(_fakeComments.Where(x => x.Game.Key == "game_3").ToList());
            Assert.That(_sut.GetComments("game_3"), Is.Null);
        }

        [Test]
        public void AddComment_UnableToSaveChanges_ThrowsAccessViolationException()
        {
            _mockUnitOfWork.Setup(x => x.Repository<Comment>().Insert(It.IsAny<Comment>()));
            _mockUnitOfWork.Setup(x => x.Repository<Game>().Get(It.IsAny<Func<Game, bool>>())).Returns(new Game() { Id = new Guid() });
            _mockUnitOfWork.Setup(x => x.Repository<User>().Get(It.IsAny<Func<User, bool>>())).Returns(() => null);
            _mockUnitOfWork.Setup(x => x.SaveChanges()).Throws<AccessViolationException>();
            Assert.That(() => _sut.AddComment(new CommentDto(), String.Empty), Throws.TypeOf<AccessViolationException>());
        }

        [Test]
        public void GetComment_IdIsRight_ReturnComment()
        {
            _mockUnitOfWork.Setup(
                    x => x.Repository<Comment>().Select(null, null, It.IsAny<int?>(), It.IsAny<int?>()))
                .Returns(_fakeComments.Skip(1).AsQueryable());
            Assert.That(() => _sut.GetCommentsPage(1, 2).Count(), Is.EqualTo(2));
        }

        [Test]
        public void GetPage_RepositoryIsEmpty_ReturnNull()
        {
            _mockUnitOfWork.Setup(
                    x => x.Repository<Comment>().Select(null, null, It.IsAny<int?>(), It.IsAny<int?>()))
                .Returns(_fakeComments.Skip(1).AsQueryable());
            Assert.That(() => _sut.GetCommentsPage(1, 2).ToList().Count, Is.EqualTo(2));
        }
    }
}
﻿using AutoMapper;
using GameStore.BLL.Abstract;
using GameStore.BLL.Service;
using GameStore.Common;
using GameStore.DAL.Abstract;
using GameStore.DAL.Entities;
using GameStore.DAL.UnitOfWork;
using GameStore.DTO;
using GameStore.MapperConfiguration;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GameStore.Tests.BLL.Service
{
    [TestFixture]
    public class BasketServiceTest
    {
        private Mock<IUnitOfWork> _unitOfWorkMock;
        private Basket _basket;
        private Order _fakeOrder;
        private Guid _fakeCustomer;
        private Game _fakeGame;
        private List<OrderDetail> _fakeOrderDetails;

        private IBasketService _sut;

        [SetUp]
        public void SetUp()
        {
            var orderId = Guid.NewGuid();

            _fakeGame = new Game()
            {
                Id = Guid.NewGuid(),
                Description = "Test description",
                Discontinued = false,
                IsDeleted = false,
                Publisher = new Publisher() { Id = _fakeCustomer, CompanyName = "Ababagalamaga" },
                Name = "Test",
                Key = "test",
                UnitsInStock = 15000,
                Price = 120,
                PublishDate = DateTime.UtcNow,
                ReleaseDate = DateTime.UtcNow,
                VisitsCount = 20,
                PublisherId = _fakeCustomer
            };

            _fakeOrderDetails = new List<OrderDetail>()
            {
                new OrderDetail() {Game = _fakeGame, Id = Guid.NewGuid(), IsDeleted = false, Price = 20, ProductId = _fakeCustomer, Quantity = 15, OrderId = orderId}
            };

            _fakeCustomer = Guid.NewGuid();
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            AutoMapperWebConfiguration.Configure();
            _sut = new BasketService(_unitOfWorkMock.Object);
            _basket = new Basket();
            _basket.Id = Guid.NewGuid();
            _fakeOrder = new Order()
            {
                BasketId = _basket.Id,
                Basket = _basket,
                UserId = _fakeCustomer,
                IsDeleted = false,
                Status = OrderStatus.NotPaid,
                Id = Guid.NewGuid(),
                OrderDate = DateTime.UtcNow,
                OrderDetails = _fakeOrderDetails
            };

            _basket.Orders = new List<Order>() { _fakeOrder };
            _fakeOrderDetails[0].Order = _fakeOrder;
            _fakeOrderDetails[0].OrderId = _fakeOrder.Id;
        }

        [Test]
        public void CreateBasket_BasketAdds_BasketCreatesForCurrentUser()
        {
            //Arrange
            _unitOfWorkMock.Setup(x => x.Repository<Basket>().Insert(It.IsAny<Basket>()));

            //Act & Assert
            Assert.DoesNotThrow(() => _sut.CreateBasketForCurrentUser(Guid.NewGuid()));
        }

        [Test]
        public void ClearBasket_AllOrdersInBasketIsDeletedSetTrue_AllOrdersIsDeleted()
        {
            //Arrange
            _unitOfWorkMock.Setup(x => x.Repository<Basket>().Update(It.IsAny<Basket>()));
            _unitOfWorkMock.Setup(x => x.Repository<Basket>().Get(It.IsAny<Func<Basket, bool>>())).Returns(_basket);

            //Act
            _sut.ClearBasket(_fakeCustomer);

            var orders = _basket.Orders.Where(x => !x.IsDeleted);

            //Assert
            Assert.IsTrue(!orders.Any());
        }

        [Test]
        public void GetBasket_ValidUserId_ReturnBasketForUser()
        {
            //Arrange
            _unitOfWorkMock.Setup(x => x.Repository<Basket>().Get(It.IsAny<Func<Basket, bool>>())).Returns(_basket);

            //Act
            var basket = _sut.GetBasket(_fakeCustomer);

            //Assert
            Assert.IsTrue(basket.Id == _basket.Id);
        }

        [Test]
        public void GetOrdersCount_ValidCustomerId_ReturnsOrdersCount()
        {
            //Arrange
            _unitOfWorkMock.Setup(x => x.Repository<Basket>().Get(It.IsAny<Func<Basket, bool>>())).Returns(_basket);

            //Act
            var ordersCount = _sut.GetOrdersCount(_fakeCustomer);

            //Assert
            Assert.AreEqual(1, ordersCount);
        }

        [Test]
        public void PayOrder_ValidOrder_OrderSetIsPaidTrue()
        {
            //Arrange
            _unitOfWorkMock.Setup(x => x.Repository<Basket>().Get(It.IsAny<Func<Basket, bool>>())).Returns(_basket);
            var orderDto = Mapper.Map<OrderDto>(_basket.Orders.ElementAt(0));
            orderDto.PaymentType = TypeOfPayment.Bank;

            //Act
            _sut.PayOrder(orderDto);

            //Assert
            Assert.IsTrue(_fakeOrder.Status == OrderStatus.Paid);
        }

        [Test]
        public void GetOrderFromBasket_ValidUserId_ReturnsOrder()
        {
            //Arrange
            _unitOfWorkMock.Setup(x => x.Repository<Order>().Get(It.IsAny<Func<Order, bool>>())).Returns(_fakeOrder);

            //Act
            var order = _sut.GetOrderFromBasket(_fakeCustomer);

            //Assert
            Assert.AreEqual(_fakeOrder.Id, order.Id);
        }

        [Test]
        public void UpdateOrderDetail_ValidOrderDetailAndOrderPassed_OrderDetailIsUpdated()
        {
            //Arrange
            _unitOfWorkMock.Setup(x => x.Repository<Basket>().Update(It.IsAny<Basket>()));
            _unitOfWorkMock.Setup(x => x.Repository<Basket>().Get(It.IsAny<Func<Basket, bool>>())).Returns(_basket);
            var orderDetail = Mapper.Map<OrderDetailDto>(_fakeOrderDetails[0]);
            orderDetail.Quantity = 150;
            var orderDto = Mapper.Map<OrderDto>(_fakeOrder);

            //Act
            _sut.UpdateOrderDetail(orderDetail, orderDto);

            //Assert
            Assert.AreEqual(150, _fakeOrderDetails[0].Quantity);
        }

        [Test]
        public void IfBasketExistsForCurrentUser_UserIdIsNotValid_ReturnsFalse()
        {
            //Arrange
            _unitOfWorkMock.Setup(x => x.Repository<Basket>().Get(It.IsAny<Func<Basket, bool>>())).Returns(() => null);

            //Act & Assert
            Assert.IsFalse(_sut.IfBasketExistsForCurrentUser(Guid.NewGuid()));
        }

        [Test]
        public void IfBasketExistsForCurrentUser_UserIdIsValid_ReturnsTrue()
        {
            //Arrange
            _unitOfWorkMock.Setup(x => x.Repository<Basket>().Get(It.IsAny<Func<Basket, bool>>())).Returns(_basket);

            //Act & Assert
            Assert.IsTrue(_sut.IfBasketExistsForCurrentUser(_fakeCustomer));
        }
    }
}
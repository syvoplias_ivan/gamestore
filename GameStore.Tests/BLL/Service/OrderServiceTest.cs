﻿using GameStore.BLL.Abstract;
using GameStore.BLL.Service;
using GameStore.Common;
using GameStore.DAL.Abstract;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Authorizaion;
using GameStore.DTO;
using GameStore.MapperConfiguration;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.Tests.BLL.Service
{
    [TestFixture]
    public class OrderServiceTest
    {
        private Mock<IUnitOfWork> _unitOfWorkMock;
        private List<Order> _orders;
        private IOrderService _sut;

        [SetUp]
        public void SetUp()
        {
            AutoMapperWebConfiguration.Configure();
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _sut = new OrderService(_unitOfWorkMock.Object);
            _orders = new List<Order>();
        }

        [Test]
        public void AddOrder_ProperOrder_OrderInserted()
        {
            //Arrange
            _unitOfWorkMock.Setup(x => x.Repository<User>().Get(It.IsAny<Func<User, bool>>())).Returns(() => null);
            _unitOfWorkMock.Setup(x => x.Repository<Order>().Insert(It.IsAny<Order>()))
                .Callback((Order order) => _orders.Add(order));

            //Act
            var newOrder = new OrderDto() { Id = Guid.NewGuid(), Status = OrderStatus.NotPaid };
            _sut.AddOrder(newOrder);

            //Assert
            Assert.IsNotEmpty(_orders);
        }

        [Test]
        public void AddOrder_NullOrder_ThrowsNullReferenceException()
        {
            //Arrange
            _unitOfWorkMock.Setup(x => x.Repository<User>().Get(It.IsAny<Func<User, bool>>())).Returns(() => null);
            _unitOfWorkMock.Setup(x => x.Repository<Order>().Insert(It.IsAny<Order>()));
            //Act & Assert
            Assert.Throws<NullReferenceException>(() => _sut.AddOrder(null));
        }

        [Test]
        public void DeleteOrder_NullOrder_ThrowsNullReferenceException()
        {
            //Arrange, Act & Assert
            Assert.Throws<NullReferenceException>(() => _sut.DeleteOrder(null));
        }

        [Test]
        public void DeleteOrder_DefaultGuid_ThrowsNullReferenceException()
        {
            //Arrange
            _unitOfWorkMock.Setup(x => x.Repository<Order>().Get(It.IsAny<Func<Order, bool>>())).Throws<NullReferenceException>();

            //Act & Assert
            Assert.Throws<NullReferenceException>(() => _sut.DeleteOrder(new Guid()));
        }

        [Test]
        public void GetAllOrders_OrdersIsEmpty_RetursEmptyList()
        {
            //Arrange
            _unitOfWorkMock.Setup(x => x.Repository<Order>().Enumerable()).Returns(_orders);

            //Act & Assert
            Assert.IsEmpty(_sut.GetAllOrders());
        }

        [Test]
        public void UpdateOrderStatus_OrderStatusPaid_UpdatesStatus()
        {
            //Arrange
            var order = new Order() { Id = Guid.NewGuid(), IsDeleted = false, Status = OrderStatus.Paid };
            _orders.Add(order);
            var guid = order.Id;

            _unitOfWorkMock.Setup(x => x.Repository<Order>().Get(It.IsAny<Func<Order, bool>>()))
                .Returns((Func<Order, bool> expression) => _orders.First(expression));
            _unitOfWorkMock.Setup(x => x.Repository<Order>().Update(It.IsAny<Order>())).Callback((Order order1) =>
            {
                _orders.Remove(order1);
                _orders.Add(order1);
            });

            //Act
            _sut.UpdateOrderStatus(guid, OrderStatus.Shipped);

            //Assert
            Assert.AreEqual(OrderStatus.Shipped, _orders[0].Status);
        }
    }
}
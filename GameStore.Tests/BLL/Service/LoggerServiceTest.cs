﻿using System;
using GameStore.BLL.Service;
using NUnit.Framework;

namespace GameStore.Tests.BLL.Service
{
    [TestFixture]
    public class LoggerServiceTest
    {
        [SetUp]
        public void SetUp()
        {
            _sut = (LoggerService)LoggerService.GetLoggingService(typeof(LoggerService).FullName);
            _sut.SetTest(true);
        }

        private LoggerService _sut;

        [Test]
        public void Debug_ExeptionNotNull_WriteException()
        {
            Assert.DoesNotThrow(() => _sut.Debug(new InvalidCastException("This is dummy exception")));
        }

        [Test]
        public void Debug_ExeptionWrite_WritesException()
        {
            Assert.DoesNotThrow(() => _sut.Debug(null));
        }

        [Test]
        public void Error_ExeptionWrite_WritesException()
        {
            Assert.DoesNotThrow(() => _sut.Error(null));
        }

        [Test]
        public void Fatal_ExeptionWrite_WritesException()
        {
            Assert.DoesNotThrow(() => _sut.Fatal(null));
        }

        [Test]
        public void GetLoggingService_LogNameIsNull_NullReferenceException()
        {
            Assert.That(() => LoggerService.GetLoggingService(null), Throws.Exception.TypeOf<NullReferenceException>());
        }

        [Test]
        public void Info_ExeptionWrite_WritesException()
        {
            Assert.DoesNotThrow(() => _sut.Info(null));
        }

        [Test]
        public void Trace_ExeptionWrite_WritesException()
        {
            Assert.DoesNotThrow(() => _sut.Trace(null));
        }

        [Test]
        public void Warn_ExeptionWrite_WritesException()
        {
            Assert.DoesNotThrow(() => _sut.Warn(null));
        }
    }
}
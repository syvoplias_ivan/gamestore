﻿using AutoMapper;
using GameStore.BLL.Abstract;
using GameStore.BLL.Service;
using GameStore.Common;
using GameStore.DAL.Abstract;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Localization;
using GameStore.DTO;
using GameStore.MapperConfiguration;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.Tests.BLL.Service
{
    [TestFixture]
    public class GenreServiceTest
    {
        private Mock<IUnitOfWork> _unitOfWorkMock;
        private Mock<ILanguageConfiguration> _configurationMock;
        private Mock<ILocalizator<GenreDto>> _genreLocalizatorMock;
        private List<Genre> _genres;
        private IGenreService _sut;

        [SetUp]
        public void SetUp()
        {
            AutoMapperWebConfiguration.Configure();
            _genres = new List<Genre>();

            _configurationMock = new Mock<ILanguageConfiguration>();
            _configurationMock.Setup(x => x.DefaultLanguage).Returns(LanguageCode.En);

            _genreLocalizatorMock = new Mock<ILocalizator<GenreDto>>();

            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _sut = new GenreService(_unitOfWorkMock.Object, _configurationMock.Object, _genreLocalizatorMock.Object);
        }

        [Test]
        public void GetAllGenres_GenresEmpty_ReturnEmptyList()
        {
            //Arrange
            _unitOfWorkMock.Setup(x => x.Repository<Genre>().Enumerable()).Returns(_genres);

            //Act & Assert
            Assert.IsEmpty(_sut.GetAllGenres());
        }

        [Test]
        public void GetAllGenres_GenresNotEmpty_ReturnGenres()
        {
            //Arrange
            _unitOfWorkMock.Setup(x => x.Repository<Genre>().Enumerable()).Returns(_genres);
            _genres.Add(new Genre());

            //Act & Assert
            Assert.IsNotEmpty(_sut.GetAllGenres());
        }

        [Test]
        public void CreateGenre_NullGenre_ThrowsNullReferenceException()
        {
            //Arrange
            _unitOfWorkMock.Setup(x => x.Repository<Genre>().Insert(It.IsAny<Genre>()));

            //Act & Assert
            Assert.Throws<NullReferenceException>(() => _sut.CreateGenre(null));
        }

        [Test]
        public void CreateGenre_GenreWithParent_GenreAdds()
        {
            //Arrange
            _unitOfWorkMock.Setup(x => x.Repository<Genre>().Get(It.IsAny<Func<Genre, bool>>()))
                .Returns(new Genre() { Name = "New genre" });
            _unitOfWorkMock.Setup(x => x.Repository<Genre>().Insert(It.IsAny<Genre>()))
                .Callback((Genre genre) => _genres.Add(genre));

            //Act
            var newGenre = new GenreDto()
            {
                ParentName = "Genre",
                Name = "New awesome genre"
            };
            _sut.CreateGenre(newGenre);
            var added = _genres[0];

            //Assert
            Assert.AreEqual("New awesome genre", added.Name);
        }

        [Test]
        public void UpdateGenre_ProperGenre_UpdateGenre()
        {
            //Arrange
            var guid = Guid.NewGuid();
            var genre = new Genre()
            {
                Name = "New genre 25",
                Id = guid,
                GenreLocalization = new List<GenreTranslate>(),
                IsDeleted = false
            };
            _genres.Add(genre);
            _unitOfWorkMock.Setup(x => x.Repository<Genre>().Get(It.IsAny<Func<Genre, bool>>())).Returns((Func<Genre, bool> expr) => _genres.FirstOrDefault(expr));
            _unitOfWorkMock.Setup(x => x.Repository<Genre>().Update(It.IsAny<Genre>())).Callback((Genre updated) =>
            {
                _genres.Remove(_genres.Find(x => x.Id == updated.Id));
                _genres.Add(updated);
            });

            //Act
            var genreDto = Mapper.Map<GenreDto>(genre);
            genreDto.Name = "New name 255";
            _sut.UpdateGenre(genreDto);

            //Assert
            Assert.AreEqual("New name 255", _genres[0].Name);
        }

        [Test]
        public void DeleteGenre_NullGenre_ThrowsNullReferenceException()
        {
            //Arrange
            _unitOfWorkMock.Setup(x => x.Repository<Genre>().Delete(It.IsAny<Genre>())).Throws<NullReferenceException>();

            //Act
            GenreDto genre = null;

            //Assert
            Assert.Throws<NullReferenceException>(() => _sut.DeleteGenre(genre));
        }

        [Test]
        public void IsGenreNameExists_NullGenre_ReturnsFalse()
        {
            //Arrange
            _unitOfWorkMock.Setup(x => x.Repository<Genre>().Get(It.IsAny<Func<Genre, bool>>())).Returns(() => null);

            //Act & Assert
            Assert.IsFalse(_sut.IsGenreNameExists(null));
        }

        [Test]
        public void GetGenre_NullGenreName_ThrowsNullReferenceException()
        {
            //Arrange, Act & Assert
            Assert.Throws<NullReferenceException>(() => _sut.GetGenre(null));
        }

        [Test]
        public void DeleteGenre_DefaultGuid_ThrowsNullReferenceException()
        {
            //Arrange
            _unitOfWorkMock.Setup(x => x.Repository<Genre>().Delete(It.IsAny<Genre>())).Throws<NullReferenceException>();

            //Act
            Guid id = Guid.Empty;

            //Assert
            Assert.Throws<NullReferenceException>(() => _sut.DeleteGenre(id));
        }

        [Test]
        public void DeleteGenre_EmptyString_ThrowsNullReferenceException()
        {
            //Arrange
            _unitOfWorkMock.Setup(x => x.Repository<Genre>().Delete(It.IsAny<Genre>())).Throws<NullReferenceException>();

            //Act
            string genre = "";

            //Assert
            Assert.Throws<NullReferenceException>(() => _sut.DeleteGenre(genre));
        }
    }
}
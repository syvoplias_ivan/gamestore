﻿using AutoMapper;
using GameStore.BLL.Abstract;
using GameStore.BLL.Service;
using GameStore.Common;
using GameStore.DAL.Abstract;
using GameStore.DAL.Entities;
using GameStore.DAL.UnitOfWork;
using GameStore.DTO;
using GameStore.MapperConfiguration;
using GameStore.Tests.Helpers;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace GameStore.Tests.BLL.Service
{
    [TestFixture]
    public class GameServiceTest
    {
        private Mock<IUnitOfWork> _mockUnitOfWork;
        private Mock<ILocalizator<GameDto>> _gameLocalizator;
        private Mock<ILocalizator<GenreDto>> _genreLocalizator;
        private Mock<ILanguageConfiguration> _defaultLanguage;
        private IGameService _sut;
        private List<PlatformType> fakeTypes;
        private List<Genre> fakeGenres;
        private List<Game> fakeGames;

        [SetUp]
        public void TestInitilize()
        {
            AutoMapperWebConfiguration.Configure();

            _gameLocalizator = new Mock<ILocalizator<GameDto>>();
            _genreLocalizator = new Mock<ILocalizator<GenreDto>>();
            _defaultLanguage = new Mock<ILanguageConfiguration>();
            _defaultLanguage.Setup(x => x.DefaultLanguage).Returns(LanguageCode.En);

            _gameLocalizator.Setup(
                x =>
                    x.Localize(It.IsAny<GameDto>(), It.IsAny<LanguageCode>(), It.IsAny<LanguageCode>(), It.IsAny<LocalizationDirection>(),
                            It.IsAny<bool>()));

            _genreLocalizator.Setup(
                x =>
                    x.Localize(It.IsAny<GenreDto>(), It.IsAny<LanguageCode>(), It.IsAny<LanguageCode>(), It.IsAny<LocalizationDirection>(),
                            It.IsAny<bool>()));

            fakeTypes = new List<PlatformType>()
            {
                new PlatformType() {Id = Guid.NewGuid(), Type = TypeOfPlatform.Desktop},
                new PlatformType() {Id = Guid.NewGuid(), Type = TypeOfPlatform.Console},
                new PlatformType() {Id = Guid.NewGuid(), Type = TypeOfPlatform.Browser},
                new PlatformType() {Id = Guid.NewGuid(), Type = TypeOfPlatform.Mobile}
            };

            fakeGenres = new List<Genre>()
            {
                new Genre() {Id = Guid.NewGuid(), Name = "Arcada", ParentId = Guid.NewGuid()},
                new Genre() {Id = Guid.NewGuid(), Name = "Race", ParentId = Guid.NewGuid()},
                new Genre() {Id = Guid.NewGuid(), Name = "RPG", ParentId = Guid.NewGuid()},
                new Genre() {Id = Guid.NewGuid(), Name = "MMORPG", ParentId = Guid.NewGuid()}
            };

            fakeGames = new List<Game>()
            {
                new Game() {Id = Guid.NewGuid(), Key = "game_10", Genres = new List<Genre>() {fakeGenres[0],fakeGenres[3]},
                    PlatformTypes = new List<PlatformType>() {fakeTypes[0], fakeTypes[1]} },
                new Game()
                {
                    Id = Guid.NewGuid(), Key = "game_11", Genres = new List<Genre>() {fakeGenres[1],fakeGenres[3]},
                    PlatformTypes = new List<PlatformType>() {fakeTypes[2], fakeTypes[1]}
                },
                new Game()
                {
                    Id = Guid.NewGuid(), Key = "game_12", Genres = new List<Genre>() {fakeGenres[2],fakeGenres[3]},
                    PlatformTypes = new List<PlatformType>() {fakeTypes[3], fakeTypes[2]}
                }
            };
            _mockUnitOfWork = new Mock<IUnitOfWork>();
            _sut = new GameService(_mockUnitOfWork.Object, _defaultLanguage.Object, _gameLocalizator.Object, _genreLocalizator.Object);
        }

        [Test]
        public void AddGame_ContextGamesCountChanges_GameAddsToCollection()
        {
            var game = new GameDto() { Description = "Crysis 3", Id = Guid.NewGuid(), Key = "crysis-3", Name = "Crysis 3", PlatformTypes = new List<PlatformTypeDto>(), Genres = new List<GenreDto>() };

            _mockUnitOfWork.Setup(x => x.Repository<Game>().Enumerable()).Returns(() =>
            {
                fakeGames.Add(Mapper.Map<Game>(game));
                return fakeGames;
            });
            _mockUnitOfWork.Setup(x => x.Repository<Publisher>().Get(It.IsAny<Func<Publisher, bool>>())).Returns(new Publisher());
            _mockUnitOfWork.Setup(x => x.Repository<Genre>().Get(It.IsAny<Func<Genre, bool>>())).Returns(new Genre());
            _mockUnitOfWork.Setup(x => x.Repository<PlatformType>().Get(It.IsAny<Func<PlatformType, bool>>())).Returns(new PlatformType());

            _sut.AddGame(game);
            Assert.AreEqual(4, _sut.GetAllGames().Count());
        }

        [Test]
        public void DeleteGame_ContextGamesCountReducesByOne_GameRemovedFromCollection()
        {
            var game = fakeGames[0];
            int count = fakeGames.Count;
            _mockUnitOfWork.Setup(x => x.Repository<Game>().Get(It.IsAny<Func<Game, bool>>())).Returns(
                fakeGames.Find(x => x.Id == fakeGames[0].Id)
                );
            _mockUnitOfWork.Setup(x => x.Repository<Game>().Delete(It.IsAny<Game>())).Callback(() =>
            {
                fakeGames.Remove(game);
            });

            _sut.DeleteGame(game.Id);
            Assert.AreNotEqual(count, fakeGames.Count);
        }

        [Test]
        public void UpdateGame_GameChanges_GameChangedInCollection()
        {
            var game = new GameDto() { Name = "Fallout 3", Description = "Fallout 3", Key = "game_10" };

            _mockUnitOfWork.Setup(x => x.Repository<Game>().Select(It.IsAny<Expression<Func<Game, bool>>>(), null, null, null))
                .Returns((Expression<Func<Game, bool>> predicate, Expression<Func<Game, object>> orderBy,
                int? page, int? pageSize) =>
                {
                    return fakeGames.AsQueryable().Where(predicate).AsQueryable();
                });

            _mockUnitOfWork.Setup(x => x.Repository<Game>().Delete(It.IsAny<Game>()));
            _mockUnitOfWork.Setup(x => x.Repository<Game>().Update(It.IsAny<Game>())).Verifiable();
            _sut.UpdateGame(game);
            Mock.Verify(_mockUnitOfWork);
        }

        [Test]
        public void GetAll_ReturnEmptyList_ReturnEmptyListOfGame()
        {
            _mockUnitOfWork.Setup(x => x.Repository<Game>().Enumerable()).Returns(new List<Game>());

            Assert.IsEmpty(_sut.GetAllGames());
        }

        [Test]
        public void GetGameByGenre_GetGamesWithGenre_ReturnGameWithSpecifiedGenre()
        {
            var games = GenerateGames.GenerateWithGenre("sport");

            _mockUnitOfWork.Setup(x => x.Repository<Game>().Select(It.IsAny<Expression<Func<Game, bool>>>(), null, null, null)).Returns(games.AsQueryable());
            var list = _sut.GetGameByGenre(fakeGames[2].Genres.ToList()[0].Name);

            Assert.IsNotEmpty(list);
        }

        [Test]
        public void GetGameByPlatformType_GetGamesWithPlatform_ReturnGameWithSpecifiedPlatformType()
        {
            var games = GenerateGames.GenerateWithGenre("sport");
            games = GenerateGames.GenerateWithPlatformType(TypeOfPlatform.Mobile, games);

            _mockUnitOfWork.Setup(x => x.Repository<Game>().Select(It.IsAny<Expression<Func<Game, bool>>>(), null, null, null)).Returns(games.AsQueryable());
            var list = _sut.GetGameByPlatformTypes(fakeGames[0].Genres.ToList()[0].Name, fakeGames[0].Genres.ToList()[1].Name);

            Assert.IsNotEmpty(list);
        }

        [Test]
        public void GetGame_KeyDoNotExists_ThrowsInvalidOperationExeption()
        {
            _mockUnitOfWork.Setup(x => x.Repository<Game>().Delete(It.IsAny<Game>()))
                .Throws<InvalidOperationException>();
            Assert.That(() => _sut.DeleteGame("csssss"), Throws.Exception.TypeOf<InvalidOperationException>());
        }

        [Test]
        public void UpdateGame_GameWithGenresAndPlatformTypes_AddsGame()
        {
            var gamestart = fakeGames[0];
            var game = new Game()
            {
                Key = "game_10",
                Genres = new List<Genre>() { fakeGenres[0], fakeGenres[3] },
                PlatformTypes = new List<PlatformType>() { fakeTypes[1], fakeTypes[0] }
            };

            _mockUnitOfWork.Setup(x => x.Repository<Game>().Select(It.IsAny<Expression<Func<Game, bool>>>(), null, null, null))
                .Returns((Expression<Func<Game, bool>> predicate, Expression<Func<Game, object>> orderBy,
                int? page, int? pageSize) =>
                {
                    return fakeGames.AsQueryable().Where(predicate).AsQueryable();
                });
            _mockUnitOfWork.Setup(x => x.Repository<Genre>().Select(It.IsAny<Expression<Func<Genre, bool>>>(), null, null, null))
                .Returns((Expression<Func<Genre, bool>> predicate, Expression<Func<Genre, object>> orderBy,
                int? page, int? pageSize) =>
                {
                    return fakeGenres.AsQueryable().Where(predicate).AsQueryable();
                });
            _mockUnitOfWork.Setup(x => x.Repository<PlatformType>().Select(It.IsAny<Expression<Func<PlatformType, bool>>>(), null, null, null))
                .Returns((Expression<Func<PlatformType, bool>> predicate, Expression<Func<PlatformType, object>> orderBy,
                  int? page, int? pageSize) =>
                {
                    return fakeTypes.AsQueryable().Where(predicate).AsQueryable();
                });
            _mockUnitOfWork.Setup(x => x.Repository<Game>().Update(It.IsAny<Game>())).Verifiable();

            _sut.UpdateGame(Mapper.Map<GameDto>(game));
            Mock.Verify(_mockUnitOfWork);
        }

        [Test]
        public void GetGame_IdIsNegative_ThrowsInvalidOperationException()
        {
            _mockUnitOfWork.Setup(x => x.Repository<Game>().Get(It.IsAny<Func<Game, bool>>())).Returns((Func<Game, bool> filter) =>
            {
                return fakeGames.First(filter);
            });

            Assert.That(() => _sut.GetGame(Guid.Empty), Throws.InvalidOperationException);
        }

        [Test]
        public void DeleteGame_SaveChanges_ThrowsException()
        {
            _mockUnitOfWork.Setup(x => x.Repository<Game>().Enumerable()).Returns(new List<Game>() { new Game() { Key = "key" } });
            _mockUnitOfWork.Setup(x => x.Repository<Game>().Select(It.IsAny<Expression<Func<Game, bool>>>(), null, null, null)).Returns(new List<Game>() { fakeGames[1] });
            _mockUnitOfWork.Setup(x => x.Repository<Game>().Delete(It.IsAny<Game>()));
            _mockUnitOfWork.Setup(x => x.SaveChanges()).Throws<InvalidOperationException>();

            Assert.That(() => _sut.DeleteGame("key"), Throws.Exception.TypeOf<InvalidOperationException>());
        }

        [Test]
        public void GetPage_InvalidParamValues_ReturnNull()
        {
            _mockUnitOfWork.Setup(x => x.Repository<Game>().Select(null, null, It.IsAny<int?>(), It.IsAny<int?>()))
                .Returns((Expression<Func<Game, bool>> filter,
            Expression<Func<Game, object>> orderBy,
            int? page,
            int? pageSize) => new List<Game>().AsQueryable());
            Assert.IsNull(_sut.GetGamesPage(-2, -5));
        }

        [Test]
        public void GetGame_KeyIsEmptyString_ReturnInvalidOperationException()
        {
            _mockUnitOfWork.Setup(x => x.Repository<Game>().Get(It.IsAny<Func<Game, bool>>()))
                .Throws<InvalidOperationException>();
            Assert.That(() => _sut.GetGame(""), Throws.TypeOf<InvalidOperationException>());
        }
    }
}
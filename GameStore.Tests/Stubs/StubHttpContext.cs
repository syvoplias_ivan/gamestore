﻿using System.Web;

namespace GameStore.Tests.Stubs
{
    public class StubHttpContext : HttpContextBase
    {
        private readonly StubHttpRequest _request;
        private readonly StubHttpResponse _response;

        public StubHttpContext(string appPath = "/",
            string requestUrl = "~/")
        {
            _request = new StubHttpRequest(appPath, requestUrl);
            _response = new StubHttpResponse();
        }

        public override HttpRequestBase Request
        {
            get { return _request; }
        }

        public override HttpResponseBase Response
        {
            get { return _response; }
        }
    }
}
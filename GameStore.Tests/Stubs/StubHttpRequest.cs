﻿using System.Collections.Specialized;
using System.Web;

namespace GameStore.Tests.Stubs
{
    public class StubHttpRequest : HttpRequestBase
    {
        public StubHttpRequest(string appPath, string requestUrl)
        {
            ApplicationPath = appPath;
            AppRelativeCurrentExecutionFilePath = requestUrl;
        }

        public override string ApplicationPath { get; }

        public override string AppRelativeCurrentExecutionFilePath { get; }

        public override string PathInfo
        {
            get { return ""; }
        }

        public override NameValueCollection ServerVariables
        {
            get { return new NameValueCollection(); }
        }
    }
}
﻿using System.Web;

namespace GameStore.Tests.Stubs
{
    public class StubHttpResponse : HttpResponseBase
    {
        public override int StatusCode { get; set; }

        public override string ApplyAppPathModifier(string virtualPath)
        {
            return virtualPath;
        }
    }
}
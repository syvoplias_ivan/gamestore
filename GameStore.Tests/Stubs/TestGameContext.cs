﻿using GameStore.Common;
using GameStore.DAL.Abstract;
using GameStore.DAL.Context;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Authorizaion;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading;
using System.Threading.Tasks;

namespace GameStore.Tests.Stubs
{
    public class TestGameContext : IGameStoreContext
    {
        private readonly IDbSet<Comment> _commentsItemsTable;
        private readonly IDbSet<Game> _gameItemsTable;
        private readonly IDbSet<Genre> _genreItemsTable;
        private readonly IDbSet<PlatformType> _platformItemsTable;

        public TestGameContext()
        {
            _gameItemsTable = new FakeDbSet<Game>();
            _platformItemsTable = new FakeDbSet<PlatformType>();
            _commentsItemsTable = new FakeDbSet<Comment>();
            _genreItemsTable = new FakeDbSet<Genre>();

            _platformItemsTable.Add(new PlatformType
            {
                Id = Guid.NewGuid(),
                Type = TypeOfPlatform.Mobile
            });

            _commentsItemsTable.Add(new Comment
            {
                Id = Guid.NewGuid(),
                User = new User() { NickName = "John" },
                Body = "Hi, All!!"
            });

            _genreItemsTable.Add(new Genre
            {
                Id = Guid.NewGuid(),
                Name = "Arcade"
            });
            _genreItemsTable.Add(new Genre
            {
                Id = Guid.NewGuid(),
                Name = "Action"
            });

            _gameItemsTable.Add(new Game
            {
                Description = "Counter-Strike 1.6",
                Id = Guid.NewGuid(),
                Key = "counter-strike",
                Name = "Counter-Strike 1.6"
            });
            _gameItemsTable.Add(new Game
            {
                Description = "CS Global Offensive",
                Id = Guid.NewGuid(),
                Key = "cs-global-offencive",
                Name = "CS Global Offensive"
            });
        }

        public TestGameContext(bool flag)
        {
            _gameItemsTable = new FakeDbSet<Game>();
            _platformItemsTable = new FakeDbSet<PlatformType>();
            _commentsItemsTable = new FakeDbSet<Comment>();
            _genreItemsTable = new FakeDbSet<Genre>();
        }

        public int SaveChanges()
        {
            return 0;
        }

        public IDbSet<T> Set<T>() where T : class, IEntity
        {
            var t = typeof(T);
            var name = t.Name;
            switch (name)
            {
                case "Game":
                    return (IDbSet<T>)_gameItemsTable;

                case "Genre":
                    return (IDbSet<T>)_genreItemsTable;

                case "PlatformType":
                    return (IDbSet<T>)_commentsItemsTable;

                case "Comment":
                    return (IDbSet<T>)_commentsItemsTable;
            }
            return null;
        }

        public void SetModified<T>(object entity)
        {
        }

        public void SetAdded<T>(object entity)
        {
        }

        public void SetDeleted<T>(object entity)
        {
        }

        public void SetDetached<T>(object entity)
        {
        }

        public void SetUnchanged<T>(object entity)
        {
        }

        public EntityState GetEntityState<T>(object entity) where T : class, IEntity
        {
            return EntityState.Unchanged;
        }

        public Task<int> SaveChangesAsync()
        {
            return new Task<int>(() => 0);
        }

        public Task<int> SaveChangesAsync(CancellationToken token)
        {
            return new Task<int>(() => 0);
        }

        public void SetData<T>(List<T> items) where T : class, IEntity
        {
            var t = typeof(T);
            var name = t.Name;
            switch (name)
            {
                case "Game":
                    items.ForEach(x =>
                    {
                        var temp = x as Game;
                        _gameItemsTable.Add(temp);
                    });
                    break;

                case "Genre":
                    items.ForEach(x =>
                    {
                        var temp = x as Genre;
                        _genreItemsTable.Add(temp);
                    });
                    break;

                case "PlatformType":
                    items.ForEach(x =>
                    {
                        var temp = x as PlatformType;
                        _platformItemsTable.Add(temp);
                    });
                    break;

                case "Comment":
                    items.ForEach(x =>
                    {
                        var temp = x as Comment;
                        _commentsItemsTable.Add(temp);
                    });
                    break;
            }
        }

        #region IDisposable Support

        private bool _disposedValue; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                }

                _disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion IDisposable Support
    }
}
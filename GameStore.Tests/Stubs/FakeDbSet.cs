﻿using GameStore.DAL.Abstract;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;

namespace GameStore.Tests.Stubs
{
    public class FakeDbSet<T> : IDbSet<T> where T : class, IEntity
    {
        private readonly IQueryable _query;

        public FakeDbSet()
        {
            Local = new ObservableCollection<T>();
            _query = Local.AsQueryable();
        }

        public virtual T Find(params object[] keyValues)
        {
            if (keyValues != null && keyValues.Length == 1 && keyValues[0].GetType().Name == "Guid")
            {
                var id = (Guid)keyValues[0];
                var res = Local.First(x => x.Id == id);
                return res;
            }
            return null;
        }

        public T Add(T item)
        {
            Local.Add(item);
            return item;
        }

        public T Remove(T item)
        {
            Local.Remove(item);
            return item;
        }

        public T Attach(T item)
        {
            for (var i = 0; i < Local.Count; i++)
            {
                if (Local[0].Id == item.Id)
                {
                    Local[i] = item;
                    return item;
                }
            }
            Local.Add(item);
            return item;
        }

        public T Create()
        {
            return Activator.CreateInstance<T>();
        }

        public TDerivedEntity Create<TDerivedEntity>() where TDerivedEntity : class, T
        {
            return Activator.CreateInstance<TDerivedEntity>();
        }

        public ObservableCollection<T> Local { get; }

        Type IQueryable.ElementType
        {
            get { return _query.ElementType; }
        }

        System.Linq.Expressions.Expression IQueryable.Expression
        {
            get { return _query.Expression; }
        }

        IQueryProvider IQueryable.Provider
        {
            get { return _query.Provider; }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return Local.GetEnumerator();
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return Local.GetEnumerator();
        }

        public T Detach(T item)
        {
            Local.Remove(item);
            return item;
        }
    }
}
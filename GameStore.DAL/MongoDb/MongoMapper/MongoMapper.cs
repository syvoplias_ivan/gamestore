﻿using GameStore.Common;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Localization;
using GameStore.DAL.MongoDb.EntitySerializers;
using GameStore.DAL.MongoDb.EntitySerializers.LocalizationSerializers;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.DAL.MongoDb.MongoMapper
{
    public class MongoMapper
    {
        public static void ConfigureBsonMapper()
        {
            BsonClassMap.RegisterClassMap<BaseEntity>(
                cm =>
                {
                    cm.MapMember(x => x.IsDeleted).SetIgnoreIfDefault(true);
                }
                );

            BsonClassMap.RegisterClassMap<Genre>(
                cm =>
                {
                    cm.SetIgnoreExtraElements(true);
                    cm.MapMember(x => x.Name).SetElementName("CategoryName");
                    cm.MapMember(x => x.Parent).ApplyDefaultValue(null);
                    cm.MapMember(x => x.ParentId).ApplyDefaultValue(null);
                    cm.MapMember(x => x.GenreLocalization).SetElementName("CategoryID")
                    .SetSerializer(new GenreLocalizationSerializer(new BsonClassMap(typeof(ICollection<GenreTranslate>))));
                });

            BsonClassMap.RegisterClassMap<Publisher>(
                cm =>
                {
                    cm.SetIgnoreExtraElements(true);
                    cm.MapMember(x => x.Games).ApplyDefaultValue(null);
                    cm.MapMember(x => x.CompanyName).SetElementName("CompanyName");
                    cm.MapMember(x => x.HomePage).SetElementName("HomePage");
                    cm.MapMember(x => x.Description).SetElementName("ContactName");
                    cm.MapMember(x => x.PublisherLocalization).SetElementName("SupplierID")
                    .SetSerializer(new PublisherLocalizationSerializer(new BsonClassMap(typeof(ICollection<PublisherTranslate>))));
                });

            BsonClassMap.RegisterClassMap<Order>(
                cm =>
                {
                    cm.SetIgnoreExtraElements(true);
                    cm.MapMember(x => x.PaymentType).SetElementName("ShipVia");
                    cm.MapMember(x => x.OrderDetails).SetElementName("OrderID").SetSerializer(new OrderDetailFromOrderSerializer(new BsonClassMap(typeof(ICollection<OrderDetail>))));
                    cm.MapMember(x => x.Basket).ApplyDefaultValue(null);
                    cm.MapMember(x => x.Status).SetDefaultValue(OrderStatus.Paid);
                    cm.MapMember(x => x.OrderDate).SetElementName("OrderDate").SetSerializer(new CustomDateTimeSerializer(new BsonClassMap(typeof(DateTime)))).SetIgnoreIfDefault(true);
                });

            BsonClassMap.RegisterClassMap<OrderDetail>(
                cm =>
                {
                    cm.SetIgnoreExtraElements(true);
                    cm.MapMember(x => x.Price).SetElementName("UnitPrice");
                    cm.MapMember(x => x.Order).SetElementName("OrderID").SetSerializer(new OrderForOrderDetailsSerializer(new BsonClassMap(typeof(Order))));
                    cm.MapMember(x => x.Game).SetElementName("ProductID").SetSerializer(new GameSerializer(new BsonClassMap(typeof(Game))));
                });

            BsonClassMap.RegisterClassMap<Game>(
                cm =>
                {
                    cm.SetIgnoreExtraElements(true);
                    cm.MapCreator(game => new Game(game.ObjectId));

                    cm.MapMember(x => x.Comments).SetDefaultValue(new List<Comment>());

                    cm.MapMember(x => x.Genres).SetElementName("CategoryID").SetSerializer(new GenresFromGameSerializer(new BsonClassMap(typeof(ICollection<Genre>))));

                    cm.MapMember(x => x.Publisher).SetElementName("SupplierID").SetSerializer(new PublisherSerializer(new BsonClassMap(typeof(Publisher))));

                    cm.MapMember(x => x.Description).SetElementName("QuantityPerUnit");

                    cm.MapMember(x => x.UnitsInStock).SetElementName("UnitsInStock");

                    cm.MapMember(x => x.Name).SetElementName("ProductName");

                    cm.MapMember(x => x.Price).SetElementName("UnitPrice");

                    cm.MapMember(x => x.PublishDate).SetDefaultValue(DateTime.UtcNow.AddYears(-1));

                    cm.MapMember(x => x.ReleaseDate).SetDefaultValue(DateTime.UtcNow);

                    cm.MapMember(x => x.PlatformTypes).SetDefaultValue(new List<PlatformType>() { new PlatformType() { IsDeleted = false, Type = TypeOfPlatform.Desktop } });

                    cm.MapMember(x => x.GameLocalization).SetElementName("ProductID")
                    .SetSerializer(new GameLocalizationSerializer(new BsonClassMap(typeof(ICollection<GameTranslate>))));
                });
        }
    }
}
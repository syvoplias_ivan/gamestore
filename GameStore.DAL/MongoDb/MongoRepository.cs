﻿using GameStore.DAL.Abstract;
using GameStore.DAL.Decorator;
using GameStore.DAL.Entities;
using GameStore.DAL.MongoDb.MongoEnums;
using GameStore.DAL.MongoDb.MongoLog;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace GameStore.DAL.MongoDb
{
    public class MongoRepository<TEntity> : GameStoreDecorator<TEntity> where TEntity : class, IEntity, new()
    {
        private IMongoCollection<TEntity> _collection;
        private IMongoNorthwindContext _northwindContext;
        private IMongoLogger _logger;

        public MongoRepository(IRepository<TEntity> repository, IMongoNorthwindContext context, IMongoLogger logger) : base(repository)
        {
            _northwindContext = context;
            _collection = context.Collection<TEntity>();
            _logger = logger;
        }

        public override void Delete(TEntity entity)
        {
            var entityToUpdate = _repository.Get(x => !entity.IsDeleted && x.Id == entity.Id);
            if (entityToUpdate != null)
            {
                _repository.Delete(entityToUpdate);
            }

            SetEntityAsOutOfDate(entity, "Delete");
        }

        public override IEnumerable<TEntity> Enumerable()
        {
            var mongoEntities = GetAllMongoEntities().ToList();
            var logInfo = new LogEventInfo()
            {
                Date = DateTime.UtcNow,
                EntityType = typeof(TEntity).Name,
                ErrorInfo = null,
                EventMessage = "Entities of type " + typeof(TEntity).Name + " get from Mongo Db in Enumerable method",
                EventType = EventType.Get,
                Id = Guid.NewGuid(),
                Version = VersionOfEntity.Default
            };
            _logger.Log(logInfo);

            var sqlEntities = _repository.Enumerable().ToList();

            var same = sqlEntities.Where(x => (x as BaseMongoEntity).ObjectId != null)
                .Select(x => (x as BaseMongoEntity).ObjectId)
                .Intersect(mongoEntities.Select(x => (x as BaseMongoEntity).ObjectId));

            foreach (var sameItem in same)
            {
                var entity = mongoEntities.First(x => (x as BaseMongoEntity).ObjectId == sameItem);
                SetEntityAsOutOfDate(entity, "Enumerable", false);
            }

            return sqlEntities.Concat(mongoEntities.Where(x => !same.Contains((x as BaseMongoEntity).ObjectId)));
        }

        private IQueryable<TEntity> GetAllMongoEntities(Expression<Func<TEntity, bool>> filterExpression = null)
        {
            IQueryable<TEntity> result;
            var mongoType = new TEntity() as BaseMongoEntity;
            var filter = new FilterDefinitionBuilder<TEntity>().Exists(new StringFieldDefinition<TEntity>("OutOfDate"), false);
            if (filterExpression != null)
            {
                result = _collection.Find(filter).ToEnumerable().AsQueryable().Where(filterExpression);
            }
            else
            {
                result = _collection.Find(filter).ToEnumerable().AsQueryable();
            }
            return result;
        }

        public override TEntity Get(Func<TEntity, bool> condition)
        {
            var resultEntity = _repository.Get(condition);

            if (resultEntity == null)
            {
                var filter = new FilterDefinitionBuilder<TEntity>().Exists(new StringFieldDefinition<TEntity>("OutOfDate"), false);
                resultEntity = _collection.Find(filter).ToEnumerable().AsQueryable().FirstOrDefault(condition);

                var logInfo = new LogEventInfo()
                {
                    Date = DateTime.UtcNow,
                    EntityType = typeof(TEntity).Name,
                    ErrorInfo = null,
                    EventMessage = "Entity of type " + typeof(TEntity).Name + " was found from Mongo Db in Select Method",
                    EventType = EventType.Get,
                    Id = Guid.NewGuid(),
                    Version = VersionOfEntity.Default
                };
                _logger.Log(logInfo);
            }

            return resultEntity;
        }

        public override IEnumerable<TEntity> Select(Expression<Func<TEntity, bool>> filter = null, Expression<Func<TEntity, object>> orderBy = null, int? page = null, int? pageSize = null)
        {
            var sqlResult = _repository.Select(filter);
            IQueryable<TEntity> mongoResult = GetAllMongoEntities(filter);

            var mongoEnumerable = mongoResult.AsEnumerable().ToList();

            var unitedResult = sqlResult.Union(mongoEnumerable, new EntitiesEqualityComparer<TEntity>());

            if (orderBy != null)
            {
                unitedResult = unitedResult.OrderBy(orderBy.Compile(), new EntitiesOrderComparer());
            }

            if (page != null && pageSize != null && (page.Value - 1) * pageSize.Value < unitedResult.Count())
            {
                unitedResult = unitedResult.Skip((page.Value - 1) * pageSize.Value).Take(pageSize.Value);
            }

            return unitedResult.ToList();
        }

        public override void Update(TEntity entity)
        {
            var entityToUpdate = _repository.Get(x => !entity.IsDeleted && x.Id == entity.Id);
            if (entityToUpdate != null)
            {
                _repository.Update(entity);
            }
            else
            {
                _repository.Insert(entity);
            }

            SetEntityAsOutOfDate(entity, "Update");
        }

        private void SetEntityAsOutOfDate(TEntity entity, string methodName, bool writeLog = true)
        {
            if (typeof(TEntity).IsSubclassOf(typeof(BaseMongoEntity)))
            {
                var entityMongo = entity as BaseMongoEntity;

                if (!string.IsNullOrEmpty(entityMongo.ObjectId) && entityMongo.Id == default(Guid))
                {
                    var filter = new FilterDefinitionBuilder<BsonDocument>().Eq(new StringFieldDefinition<BsonDocument, ObjectId>("_id"), ObjectId.Parse(entityMongo.ObjectId));

                    var update = new UpdateDefinitionBuilder<BsonDocument>().Push(new StringFieldDefinition<BsonDocument>("OutOfDate"), true);

                    _northwindContext.Database.GetCollection<BsonDocument>(entityMongo.GetMongoName()).UpdateOne(filter, update);

                    if (writeLog)
                    {
                        var logInfo = new LogEventInfo()
                        {
                            Date = DateTime.UtcNow,
                            EntityType = typeof(TEntity).Name,
                            ErrorInfo = null,
                            EventMessage =
                                "Entity of type " + typeof(TEntity).Name + " was set out of date in" + methodName +
                                "method",
                            EventType = EventType.Update,
                            Id = Guid.NewGuid(),
                            Version = VersionOfEntity.New
                        };
                        _logger.Log(logInfo);
                    }
                }
            }
        }

        public override void Insert(TEntity entity)
        {
            var mongoEntity = entity as BaseMongoEntity;

            SetEntityAsOutOfDate(entity, "Insert");

            _repository.Insert(entity);
        }

        public override void InsertRange(IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                var mongoEntity = entity as BaseMongoEntity;

                SetEntityAsOutOfDate(entity, "InsertRange");
            }
            _repository.InsertRange(entities);
        }

        public override int Count()
        {
            var filter = new FilterDefinitionBuilder<TEntity>().Exists(new StringFieldDefinition<TEntity>("OutOfDate"), false);

            var mongoCount = _collection.Find(filter).ToEnumerable().AsQueryable().Count();

            var logInfo = new LogEventInfo()
            {
                Date = DateTime.UtcNow,
                EntityType = typeof(TEntity).Name,
                ErrorInfo = null,
                EventMessage = "Count of entities of type " + typeof(TEntity).Name + " get from Mongo Db",
                EventType = EventType.Get,
                Id = Guid.NewGuid(),
                Version = VersionOfEntity.Default
            };
            _logger.Log(logInfo);

            var repoCount = _repository.Count();
            return mongoCount + repoCount;
        }

        public override int FilteredCount(Expression<Func<TEntity, bool>> filter)
        {
            var filterMongo = new FilterDefinitionBuilder<TEntity>().Exists(new StringFieldDefinition<TEntity>("OutOfDate"), false);

            var mongoCount = _collection.Find(filterMongo).ToEnumerable().AsQueryable().Count(filter);

            var logInfo = new LogEventInfo()
            {
                Date = DateTime.UtcNow,
                EntityType = typeof(TEntity).Name,
                ErrorInfo = null,
                EventMessage = "Count of entoties of type " + typeof(TEntity).Name + " get from Mongo Db in FilteredCount method",
                EventType = EventType.Get,
                Id = Guid.NewGuid(),
                Version = VersionOfEntity.Default
            };
            _logger.Log(logInfo);

            var repoCount = _repository.FilteredCount(filter);
            return mongoCount + repoCount;
        }
    }
}
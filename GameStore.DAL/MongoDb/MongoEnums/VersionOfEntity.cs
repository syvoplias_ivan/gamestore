﻿namespace GameStore.DAL.MongoDb.MongoEnums
{
    public enum VersionOfEntity
    {
        New = 1,
        Old = 2,
        Default = 0
    }
}
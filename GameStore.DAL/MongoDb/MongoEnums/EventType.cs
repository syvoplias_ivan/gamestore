﻿namespace GameStore.DAL.MongoDb.MongoEnums
{
    public enum EventType
    {
        Get,
        Create,
        Update,
        Delete,
        Error
    }
}
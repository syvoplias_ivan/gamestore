﻿using GameStore.DAL.Abstract;
using GameStore.DAL.Entities;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.DAL.MongoDb.EntitySerializers
{
    public class OrderSerializer : BsonClassMapSerializer<Order>
    {
        private readonly IMongoNorthwindContext _northwindContext;

        public OrderSerializer(BsonClassMap classMap) : base(classMap)
        {
            _northwindContext = new MongoContext();
        }

        public override Order Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            var order = new Order();

            int id = context.Reader.ReadInt32();
            var query = Builders<BsonDocument>.Filter.Eq("OrderID", id);
            var queryResult =
                _northwindContext.Database.GetCollection<BsonDocument>(order.GetMongoName()).Find(query).First();

            var deserialized = BsonSerializer.Deserialize<Order>(queryResult);
            return deserialized;
        }
    }
}
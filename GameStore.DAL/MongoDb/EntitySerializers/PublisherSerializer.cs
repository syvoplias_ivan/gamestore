﻿using GameStore.DAL.Abstract;
using GameStore.DAL.Entities;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System.Linq;

namespace GameStore.DAL.MongoDb.EntitySerializers
{
    public class PublisherSerializer : BsonClassMapSerializer<Publisher>
    {
        private IMongoNorthwindContext _northwindContext;

        public PublisherSerializer(BsonClassMap classMap) : base(classMap)
        {
            _northwindContext = new MongoContext();
        }

        public override Publisher Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            var publisher = new Publisher();

            int id = context.Reader.ReadInt32();
            var query = Builders<BsonDocument>.Filter.Eq("SupplierID", id);
            var queryResult =
                _northwindContext.Database.GetCollection<BsonDocument>(publisher.GetMongoName()).Find(query).First();

            var deserialized = BsonSerializer.Deserialize<Publisher>(queryResult);

            return deserialized;
        }
    }
}
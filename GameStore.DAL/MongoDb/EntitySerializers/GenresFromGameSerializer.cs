﻿using GameStore.DAL.Abstract;
using GameStore.DAL.Entities;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;

namespace GameStore.DAL.MongoDb.EntitySerializers
{
    public class GenresFromGameSerializer : BsonClassMapSerializer<ICollection<Genre>>
    {
        private readonly IMongoNorthwindContext _northwindContext;

        public GenresFromGameSerializer(BsonClassMap classMap) : base(classMap)
        {
            _northwindContext = new MongoContext();
        }

        public override ICollection<Genre> Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            var genre = new Genre();
            var list = new List<Genre>();

            while (context.Reader.State == BsonReaderState.Value)
            {
                int id = context.Reader.ReadInt32();
                var query = Builders<BsonDocument>.Filter.Eq("CategoryID", id);
                var genreCollection =
                    _northwindContext.Database.GetCollection<BsonDocument>(genre.GetMongoName()).Find(query).First();
                var deserialized = BsonSerializer.Deserialize<Genre>(genreCollection);

                list.Add(deserialized);
            }

            return list;
        }
    }
}
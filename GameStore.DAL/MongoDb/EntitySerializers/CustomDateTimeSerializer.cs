﻿using GameStore.DAL.Abstract;
using MongoDB.Bson.Serialization;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.DAL.MongoDb.EntitySerializers
{
    public class CustomDateTimeSerializer : BsonClassMapSerializer<DateTime>
    {
        private readonly IMongoNorthwindContext _context;

        public CustomDateTimeSerializer(BsonClassMap classMap) : base(classMap)
        {
            _context = new MongoContext();
        }

        public override DateTime Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            var dateString = context.Reader.ReadString();
            DateTime dateTimeResult;

            DateTime.TryParseExact(dateString, "yyyy-MM-dd hh:mm:ss.FFF", null, DateTimeStyles.AssumeUniversal, out dateTimeResult);

            return dateTimeResult;
        }
    }
}
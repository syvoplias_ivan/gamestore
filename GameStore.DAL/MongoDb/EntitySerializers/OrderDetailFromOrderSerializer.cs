﻿using GameStore.DAL.Abstract;
using GameStore.DAL.Entities;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.DAL.MongoDb.EntitySerializers
{
    public class OrderDetailFromOrderSerializer : BsonClassMapSerializer<ICollection<OrderDetail>>
    {
        private readonly IMongoNorthwindContext _northwindContext;

        public OrderDetailFromOrderSerializer(BsonClassMap classMap) : base(classMap)
        {
            _northwindContext = new MongoContext();
        }

        public override void Serialize(BsonSerializationContext context, BsonSerializationArgs args, ICollection<OrderDetail> value)
        {
            base.Serialize(context, args, null);
        }

        public override ICollection<OrderDetail> Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            var orderDetail = new OrderDetail();
            var list = new List<OrderDetail>();

            while (context.Reader.State == BsonReaderState.Value)
            {
                int id = context.Reader.ReadInt32();
                var query = Builders<BsonDocument>.Filter.Eq("OrderID", id);
                var orderDetailsCollection =
                    _northwindContext.Database.GetCollection<BsonDocument>(orderDetail.GetMongoName()).Find(query).First();
                var deserialized = BsonSerializer.Deserialize<OrderDetail>(orderDetailsCollection);
                list.Add(deserialized);
            }

            return list;
        }
    }
}
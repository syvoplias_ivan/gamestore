﻿using GameStore.Common.Helpers;
using GameStore.DAL.Abstract;
using GameStore.DAL.Entities.Localization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.DAL.MongoDb.EntitySerializers.LocalizationSerializers
{
    public class GenreLocalizationSerializer : BsonClassMapSerializer<ICollection<GenreTranslate>>
    {
        private readonly IMongoNorthwindContext _northwindContext;

        public GenreLocalizationSerializer(BsonClassMap classMap) : base(classMap)
        {
            _northwindContext = new MongoContext();
        }

        public override ICollection<GenreTranslate> Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            var result = new List<GenreTranslate>();
            var languages = LanguageCode.En.GetAllValuesOfEnum().ToList();
            languages.Remove(default(LanguageCode));

            var id = context.Reader.ReadInt32();

            var query = Builders<BsonDocument>.Filter.Eq("CategoryID", id);
            var queryResult =
                _northwindContext.Database.GetCollection<BsonDocument>("categories").Find(query).First();

            var name = queryResult.GetElement("CategoryName").Value.AsString;

            foreach (var language in languages)
            {
                result.Add(new GenreTranslate()
                {
                    IsDeleted = false,
                    Name = name,
                    Language = language
                });
            }

            return result;
        }
    }
}
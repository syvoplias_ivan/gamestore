﻿using GameStore.Common.Helpers;
using GameStore.DAL.Abstract;
using GameStore.DAL.Entities.Localization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.DAL.MongoDb.EntitySerializers.LocalizationSerializers
{
    public class PublisherLocalizationSerializer : BsonClassMapSerializer<ICollection<PublisherTranslate>>
    {
        private readonly IMongoNorthwindContext _northwindContext;

        public PublisherLocalizationSerializer(BsonClassMap classMap) : base(classMap)
        {
            _northwindContext = new MongoContext();
        }

        public override ICollection<PublisherTranslate> Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            var result = new List<PublisherTranslate>();
            var languages = LanguageCode.En.GetAllValuesOfEnum().ToList();
            languages.Remove(default(LanguageCode));

            var id = context.Reader.ReadInt32();

            var query = Builders<BsonDocument>.Filter.Eq("SupplierID", id);
            var queryResult =
                _northwindContext.Database.GetCollection<BsonDocument>("suppliers").Find(query).First();

            var name = queryResult.GetElement("CompanyName").Value.AsString;
            var description = queryResult.GetElement("ContactName").Value.ToString();

            foreach (var language in languages)
            {
                result.Add(new PublisherTranslate()
                {
                    IsDeleted = false,
                    CompanyName = name,
                    Description = description,
                    Language = language
                });
            }

            return result;
        }
    }
}
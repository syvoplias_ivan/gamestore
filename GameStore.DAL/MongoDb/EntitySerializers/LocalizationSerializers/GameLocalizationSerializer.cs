﻿using GameStore.Common.Helpers;
using GameStore.DAL.Abstract;
using GameStore.DAL.Entities.Localization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.DAL.MongoDb.EntitySerializers.LocalizationSerializers
{
    public class GameLocalizationSerializer : BsonClassMapSerializer<ICollection<GameTranslate>>
    {
        private readonly IMongoNorthwindContext _northwindContext;

        public GameLocalizationSerializer(BsonClassMap classMap) : base(classMap)
        {
            _northwindContext = new MongoContext();
        }

        public override ICollection<GameTranslate> Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            var result = new List<GameTranslate>();
            var languages = LanguageCode.En.GetAllValuesOfEnum().ToList();
            languages.Remove(default(LanguageCode));

            var id = context.Reader.ReadInt32();

            var query = Builders<BsonDocument>.Filter.Eq("ProductID", id);
            var queryResult =
                _northwindContext.Database.GetCollection<BsonDocument>("products").Find(query).First();

            var name = queryResult.GetElement("ProductName").Value.AsString;
            var description = queryResult.GetElement("QuantityPerUnit").Value.AsString;

            foreach (var language in languages)
            {
                result.Add(new GameTranslate()
                {
                    IsDeleted = false,
                    Description = description,
                    Name = name,
                    Language = language
                });
            }

            return result;
        }
    }
}
﻿using GameStore.Common;
using GameStore.DAL.Abstract;
using GameStore.DAL.Entities;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System;

namespace GameStore.DAL.MongoDb.EntitySerializers
{
    public class OrderForOrderDetailsSerializer : BsonClassMapSerializer<Order>
    {
        private IMongoNorthwindContext _northwindContext;

        public OrderForOrderDetailsSerializer(BsonClassMap classMap) : base(classMap)
        {
            _northwindContext = new MongoContext();
        }

        public override Order Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            var order = new Order();

            int id = context.Reader.ReadInt32();
            var filter =
                new FilterDefinitionBuilder<BsonDocument>().Eq(new StringFieldDefinition<BsonDocument, int>("OrderID"), id);

            var queryResult =
                _northwindContext.Database.GetCollection<BsonDocument>(order.GetMongoName()).Find(filter).First();

            order.Status = OrderStatus.Paid;
            order.IsDeleted = false;
            order.PaymentType = TypeOfPayment.IBox;
            order.ObjectId = queryResult.GetElement("_id").Value.ToString();
            order.OrderDate = DateTime.Parse(queryResult.GetElement("OrderDate").Value.AsString);
            order.Summ = 0;
            return order;
        }
    }
}
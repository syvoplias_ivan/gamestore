﻿using GameStore.DAL.Abstract;
using GameStore.DAL.Entities;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System.Linq;

namespace GameStore.DAL.MongoDb.EntitySerializers
{
    public class GameSerializer : BsonClassMapSerializer<Game>
    {
        private readonly IMongoNorthwindContext _northwindContext;

        public GameSerializer(BsonClassMap classMap) : base(classMap)
        {
            _northwindContext = new MongoContext();
        }

        public override Game Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            var game = new Game();

            int id = context.Reader.ReadInt32();
            var query = Builders<BsonDocument>.Filter.Eq("ProductID", id);
            var queryResult =
                _northwindContext.Database.GetCollection<BsonDocument>(game.GetMongoName()).Find(query).First();

            var deserialized = BsonSerializer.Deserialize<Game>(queryResult);

            return deserialized;
        }
    }
}
﻿using GameStore.DAL.Abstract;
using GameStore.DAL.Entities;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.DAL.MongoDb
{
    public class MongoContext : IMongoNorthwindContext
    {
        private readonly IMongoClient _client;
        private readonly IMongoDatabase _database;

        public MongoContext()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["MongoDb"].ConnectionString;
            var con = new MongoUrlBuilder(connectionString);

            _client = new MongoClient(connectionString);
            _database = _client.GetDatabase(con.DatabaseName);
        }

        /// <summary>
        /// Method that returns a collection of specified type or null if such collection doesn't exist
        /// </summary>
        /// <typeparam name="T">Type of entity collection to return</typeparam>
        /// <returns>Mongo collection of specified type or null if such collection doesn't exist</returns>
        public IMongoCollection<T> Collection<T>() where T : class, IEntity, new()
        {
            if (IsSameOrSubclass(typeof(BaseMongoEntity), typeof(T)))
            {
                var entity = new T() as BaseMongoEntity;
                if (!string.IsNullOrEmpty(entity.GetMongoName()) && entity.GetMongoName() != "none")
                {
                    return _database.GetCollection<T>(entity.GetMongoName());
                }
            }
            return null;
        }

        public IMongoDatabase Database => _database;

        private bool IsSameOrSubclass(Type potentialBase, Type potentialDescendant)
        {
            return potentialDescendant.IsSubclassOf(potentialBase)
                   || potentialDescendant == potentialBase;
        }
    }
}
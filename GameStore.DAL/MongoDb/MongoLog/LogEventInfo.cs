﻿using GameStore.DAL.Abstract;
using GameStore.DAL.MongoDb.MongoEnums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.DAL.MongoDb.MongoLog
{
    public class LogEventInfo : ILogEventInfo
    {
        [BsonId]
        public Guid Id { get; set; }

        [BsonRepresentation(BsonType.DateTime)]
        public DateTime Date { get; set; }

        [BsonRepresentation(BsonType.String)]
        public string EntityType { get; set; }

        [BsonRepresentation(BsonType.String)]
        public string EventMessage { get; set; }

        [BsonRepresentation(BsonType.Int32)]
        public EventType EventType { get; set; }

        [BsonRepresentation(BsonType.String)]
        public string RawEntity { get; set; }

        [BsonRepresentation(BsonType.Int32)]
        public VersionOfEntity Version { get; set; }

        [BsonRepresentation(BsonType.String)]
        [BsonIgnoreIfDefault]
        public string ErrorInfo { get; set; }

        [BsonRepresentation(BsonType.String)]
        [BsonIgnoreIfDefault]
        public string StackTrace { get; set; }
    }
}
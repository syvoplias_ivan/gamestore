﻿using GameStore.DAL.Abstract;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.DAL.MongoDb.MongoLog
{
    public class MongoLogger : IMongoLogger
    {
        private readonly string _loggerName;
        private readonly IMongoNorthwindContext _context;
        private readonly IMongoCollection<BsonDocument> _collection;

        public MongoLogger(IMongoNorthwindContext context, string name = "MongoLogger")
        {
            _context = context;
            _loggerName = name;

            bool exists = CollectionExists("DbLogs");
            if (!exists)
            {
                _context.Database.CreateCollection("DbLogs");
            }

            _collection = _context.Database.GetCollection<BsonDocument>("DbLogs");
        }

        public void Log(ILogEventInfo logEventInfo)
        {
            var bson = logEventInfo.ToBsonDocument();
            _collection.InsertOne(bson);
        }

        private bool CollectionExists(string collectionName)
        {
            var filter = new BsonDocument("name", collectionName);

            var collections = _context.Database.ListCollections(new ListCollectionsOptions { Filter = filter });

            return collections.Any();
        }

        public override string ToString()
        {
            return _loggerName;
        }
    }
}
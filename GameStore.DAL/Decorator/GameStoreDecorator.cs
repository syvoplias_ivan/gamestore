﻿using GameStore.DAL.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.DAL.Decorator
{
    public abstract class GameStoreDecorator<T> : IRepository<T> where T : class, IEntity
    {
        protected IRepository<T> _repository;

        public GameStoreDecorator(IRepository<T> repository)
        {
            _repository = repository;
        }

        public abstract int Count();

        public abstract void Delete(T entity);

        public abstract IEnumerable<T> Enumerable();

        public abstract int FilteredCount(Expression<Func<T, bool>> filter);

        public abstract void Insert(T entity);

        public abstract void InsertRange(IEnumerable<T> entities);

        public abstract T Get(Func<T, bool> condition);

        public abstract IEnumerable<T> Select(Expression<Func<T, bool>> filter = null, Expression<Func<T, object>> orderBy = null, int? page = null, int? pageSize = null);

        public abstract void Update(T entity);
    }
}
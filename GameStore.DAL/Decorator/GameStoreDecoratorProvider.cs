﻿using GameStore.DAL.Abstract;
using GameStore.DAL.MongoDb;
using GameStore.DAL.Repository;
using System;

namespace GameStore.DAL.Decorator
{
    public class GameStoreDecoratorProvider
    {
        public static IRepository<T> GetDecorator<T>(IGameStoreContext gameStoreContext, IMongoNorthwindContext mongoNorthwindContext, IMongoLogger logger) where T : class, IEntity, new()
        {
            var repository = new Repository<T>(gameStoreContext, logger);
            var decorator = new MongoRepository<T>(repository, mongoNorthwindContext, logger);
            return decorator;
        }
    }
}
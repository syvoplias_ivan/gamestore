// <auto-generated />
namespace GameStore.DAL.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Task6StartMigration : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Task6StartMigration));
        
        string IMigrationMetadata.Id
        {
            get { return "201612080848172_Task6StartMigration"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

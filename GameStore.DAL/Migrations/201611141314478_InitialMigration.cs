namespace GameStore.DAL.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Baskets",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    CustomerId = c.Guid(nullable: false),
                    Summ = c.Decimal(nullable: false, precision: 18, scale: 2),
                    IsDeleted = c.Boolean(nullable: false),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.Orders",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    CustomerId = c.Guid(nullable: false),
                    OrderDate = c.DateTime(nullable: false),
                    PaymentType = c.Int(nullable: false),
                    IsPaid = c.Boolean(nullable: false),
                    BasketId = c.Guid(nullable: false),
                    Summ = c.Decimal(nullable: false, precision: 18, scale: 2),
                    IsDeleted = c.Boolean(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Baskets", t => t.BasketId, cascadeDelete: true)
                .Index(t => t.BasketId);

            CreateTable(
                "dbo.OrderDetails",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    ProductId = c.Guid(nullable: false),
                    Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                    Quantity = c.Short(nullable: false),
                    Discount = c.Single(nullable: false),
                    OrderId = c.Guid(nullable: false),
                    IsDeleted = c.Boolean(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Games", t => t.ProductId, cascadeDelete: true)
                .ForeignKey("dbo.Orders", t => t.OrderId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.OrderId);

            CreateTable(
                "dbo.Games",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Key = c.String(maxLength: 50),
                    Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                    UnitsInStock = c.Short(nullable: false),
                    Discontinued = c.Boolean(nullable: false),
                    ReleaseDate = c.DateTime(nullable: false),
                    Name = c.String(nullable: false),
                    Description = c.String(nullable: false),
                    PublisherId = c.Guid(),
                    IsDeleted = c.Boolean(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Publishers", t => t.PublisherId)
                .Index(t => t.Key, unique: true)
                .Index(t => t.PublisherId);

            CreateTable(
                "dbo.Comments",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Name = c.String(nullable: false),
                    Body = c.String(nullable: false),
                    GameId = c.Guid(nullable: false),
                    ParentId = c.Guid(),
                    Date = c.DateTime(nullable: false),
                    Quote = c.String(),
                    IsDeleted = c.Boolean(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Games", t => t.GameId, cascadeDelete: true)
                .ForeignKey("dbo.Comments", t => t.ParentId)
                .Index(t => t.GameId)
                .Index(t => t.ParentId);

            CreateTable(
                "dbo.Genres",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Name = c.String(nullable: false, maxLength: 50),
                    ParentId = c.Guid(),
                    IsDeleted = c.Boolean(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Genres", t => t.ParentId)
                .Index(t => t.Name, unique: true)
                .Index(t => t.ParentId);

            CreateTable(
                "dbo.PlatformTypes",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Type = c.Int(nullable: false),
                    IsDeleted = c.Boolean(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Type, unique: true);

            CreateTable(
                "dbo.Publishers",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    CompanyName = c.String(nullable: false, maxLength: 40),
                    Description = c.String(storeType: "ntext"),
                    HomePage = c.String(storeType: "ntext"),
                    IsDeleted = c.Boolean(nullable: false),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.GenreGames",
                c => new
                {
                    Genre_Id = c.Guid(nullable: false),
                    Game_Id = c.Guid(nullable: false),
                })
                .PrimaryKey(t => new { t.Genre_Id, t.Game_Id })
                .ForeignKey("dbo.Genres", t => t.Genre_Id, cascadeDelete: true)
                .ForeignKey("dbo.Games", t => t.Game_Id, cascadeDelete: true)
                .Index(t => t.Genre_Id)
                .Index(t => t.Game_Id);

            CreateTable(
                "dbo.PlatformTypeGames",
                c => new
                {
                    PlatformType_Id = c.Guid(nullable: false),
                    Game_Id = c.Guid(nullable: false),
                })
                .PrimaryKey(t => new { t.PlatformType_Id, t.Game_Id })
                .ForeignKey("dbo.PlatformTypes", t => t.PlatformType_Id, cascadeDelete: true)
                .ForeignKey("dbo.Games", t => t.Game_Id, cascadeDelete: true)
                .Index(t => t.PlatformType_Id)
                .Index(t => t.Game_Id);
        }

        public override void Down()
        {
            DropForeignKey("dbo.OrderDetails", "OrderId", "dbo.Orders");
            DropForeignKey("dbo.OrderDetails", "ProductId", "dbo.Games");
            DropForeignKey("dbo.Games", "PublisherId", "dbo.Publishers");
            DropForeignKey("dbo.PlatformTypeGames", "Game_Id", "dbo.Games");
            DropForeignKey("dbo.PlatformTypeGames", "PlatformType_Id", "dbo.PlatformTypes");
            DropForeignKey("dbo.Genres", "ParentId", "dbo.Genres");
            DropForeignKey("dbo.GenreGames", "Game_Id", "dbo.Games");
            DropForeignKey("dbo.GenreGames", "Genre_Id", "dbo.Genres");
            DropForeignKey("dbo.Comments", "ParentId", "dbo.Comments");
            DropForeignKey("dbo.Comments", "GameId", "dbo.Games");
            DropForeignKey("dbo.Orders", "BasketId", "dbo.Baskets");
            DropIndex("dbo.PlatformTypeGames", new[] { "Game_Id" });
            DropIndex("dbo.PlatformTypeGames", new[] { "PlatformType_Id" });
            DropIndex("dbo.GenreGames", new[] { "Game_Id" });
            DropIndex("dbo.GenreGames", new[] { "Genre_Id" });
            DropIndex("dbo.PlatformTypes", new[] { "Type" });
            DropIndex("dbo.Genres", new[] { "ParentId" });
            DropIndex("dbo.Genres", new[] { "Name" });
            DropIndex("dbo.Comments", new[] { "ParentId" });
            DropIndex("dbo.Comments", new[] { "GameId" });
            DropIndex("dbo.Games", new[] { "PublisherId" });
            DropIndex("dbo.Games", new[] { "Key" });
            DropIndex("dbo.OrderDetails", new[] { "OrderId" });
            DropIndex("dbo.OrderDetails", new[] { "ProductId" });
            DropIndex("dbo.Orders", new[] { "BasketId" });
            DropTable("dbo.PlatformTypeGames");
            DropTable("dbo.GenreGames");
            DropTable("dbo.Publishers");
            DropTable("dbo.PlatformTypes");
            DropTable("dbo.Genres");
            DropTable("dbo.Comments");
            DropTable("dbo.Games");
            DropTable("dbo.OrderDetails");
            DropTable("dbo.Orders");
            DropTable("dbo.Baskets");
        }
    }
}
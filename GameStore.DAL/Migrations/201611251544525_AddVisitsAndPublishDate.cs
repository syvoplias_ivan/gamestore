namespace GameStore.DAL.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddVisitsAndPublishDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Games", "PublishDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Games", "VisitsCount", c => c.Int(nullable: false));
        }

        public override void Down()
        {
            DropColumn("dbo.Games", "VisitsCount");
            DropColumn("dbo.Games", "PublishDate");
        }
    }
}
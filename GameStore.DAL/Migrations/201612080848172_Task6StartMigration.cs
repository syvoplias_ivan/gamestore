namespace GameStore.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Task6StartMigration : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Orders", "ObjectId");
            DropColumn("dbo.OrderDetails", "ObjectId");
            DropColumn("dbo.Games", "ObjectId");
            DropColumn("dbo.Genres", "ObjectId");
            DropColumn("dbo.Publishers", "ObjectId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Publishers", "ObjectId", c => c.String());
            AddColumn("dbo.Genres", "ObjectId", c => c.String());
            AddColumn("dbo.Games", "ObjectId", c => c.String());
            AddColumn("dbo.OrderDetails", "ObjectId", c => c.String());
            AddColumn("dbo.Orders", "ObjectId", c => c.String());
        }
    }
}

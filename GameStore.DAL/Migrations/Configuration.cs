﻿using GameStore.Common;
using GameStore.Common.Hasher;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Localization;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace GameStore.DAL.Migrations
{
    using Common.Auth;
    using Entities.Authorizaion;
    using System;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<GameStore.DAL.Context.GameStoreContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(GameStore.DAL.Context.GameStoreContext context)
        {
            #region Base Seed

            var genres = new List<Genre>()
            {
                new Genre()
                {
                    Name = "Strategy",
                    IsDeleted = false
                },
                new Genre()
                {
                    Name = "RPG",
                    IsDeleted = false
                },
                new Genre()
                {
                    Name = "Sport",
                    IsDeleted = false
                },
                new Genre()
                {
                    Name = "Race",
                    IsDeleted = false
                },
                new Genre()
                {
                    Name = "Action",
                    IsDeleted = false
                },
                new Genre()
                {
                    Name = "Adventure",
                    IsDeleted = false
                },
                new Genre()
                {
                    Name = "Puzzle&Skill",
                    IsDeleted = false
                },
                new Genre()
                {
                    Name = "Misc",
                    IsDeleted = false
                },
            };

            context.Genres.AddOrUpdate(x => x.Name, genres.ToArray());
            context.SaveChanges();

            var genreTranslates = new List<GenreTranslate>()
            {
                new GenreTranslate()
                {
                    IsDeleted = false,
                    Language = LanguageCode.Ru,
                    Name = "Разное",
                    GenreId = context.Genres.FirstOrDefault(x => x.Name == "Misc")?.Id ?? default(Guid)
                },
                new GenreTranslate()
                {
                    IsDeleted = false,
                    Language = LanguageCode.Uk,
                    Name = "Різне",
                    GenreId = context.Genres.FirstOrDefault(x => x.Name == "Misc")?.Id ?? default(Guid)
                },
                new GenreTranslate()
                {
                    IsDeleted = false,
                    Language = LanguageCode.Ru,
                    Name = "Паззлы&Скиллы",
                    GenreId = context.Genres.FirstOrDefault(x => x.Name == "Puzzle&Skill")?.Id ?? default(Guid)
                },
                new GenreTranslate()
                {
                    IsDeleted = false,
                    Language = LanguageCode.Uk,
                    Name = "Пазли&Скілли",
                    GenreId = context.Genres.FirstOrDefault(x => x.Name == "Puzzle&Skill")?.Id ?? default(Guid)
                },
                new GenreTranslate()
                {
                    IsDeleted = false,
                    Language = LanguageCode.Ru,
                    Name = "Приключения",
                    GenreId = context.Genres.FirstOrDefault(x => x.Name == "Adventure")?.Id ?? default(Guid)
                },
                new GenreTranslate()
                {
                    IsDeleted = false,
                    Language = LanguageCode.Uk,
                    Name = "Пригоди",
                    GenreId = context.Genres.FirstOrDefault(x => x.Name == "Adventure")?.Id ?? default(Guid)
                },
                new GenreTranslate()
                {
                    IsDeleted = false,
                    Language = LanguageCode.Ru,
                    Name = "Экшен",
                    GenreId = context.Genres.FirstOrDefault(x => x.Name == "Action")?.Id ?? default(Guid)
                },
                new GenreTranslate()
                {
                    IsDeleted = false,
                    Language = LanguageCode.Uk,
                    Name = "Екшен",
                    GenreId = context.Genres.FirstOrDefault(x => x.Name == "Action")?.Id ?? default(Guid)
                },
                new GenreTranslate()
                {
                    IsDeleted = false,
                    Language = LanguageCode.Ru,
                    Name = "Гонки",
                    GenreId = context.Genres.FirstOrDefault(x => x.Name == "Race")?.Id ?? default(Guid)
                },
                new GenreTranslate()
                {
                    IsDeleted = false,
                    Language = LanguageCode.Uk,
                    Name = "Перегони",
                    GenreId = context.Genres.FirstOrDefault(x => x.Name == "Race")?.Id ?? default(Guid)
                },
                new GenreTranslate()
                {
                    IsDeleted = false,
                    Language = LanguageCode.Ru,
                    Name = "Спорт",
                    GenreId = context.Genres.FirstOrDefault(x => x.Name == "Sport")?.Id ?? default(Guid)
                },
                new GenreTranslate()
                {
                    IsDeleted = false,
                    Language = LanguageCode.Uk,
                    Name = "Спорт",
                    GenreId = context.Genres.FirstOrDefault(x => x.Name == "Sport")?.Id ?? default(Guid)
                },
                new GenreTranslate()
                {
                    IsDeleted = false,
                    Language = LanguageCode.Ru,
                    Name = "Стратегия",
                    GenreId = context.Genres.FirstOrDefault(x => x.Name == "Strategy")?.Id ?? default(Guid)
                },
                new GenreTranslate()
                {
                    IsDeleted = false,
                    Language = LanguageCode.Uk,
                    Name = "Стратегія",
                    GenreId = context.Genres.FirstOrDefault(x => x.Name == "Strategy")?.Id ?? default(Guid)
                },
                new GenreTranslate()
                {
                    IsDeleted = false,
                    Language = LanguageCode.Ru,
                    Name = "РПГ",
                    GenreId = context.Genres.FirstOrDefault(x => x.Name == "RPG")?.Id ?? default(Guid)
                },
                new GenreTranslate()
                {
                    IsDeleted = false,
                    Language = LanguageCode.Uk,
                    Name = "РПГ",
                    GenreId = context.Genres.FirstOrDefault(x => x.Name == "RPG")?.Id ?? default(Guid)
                }
            };

            var raceSubNames = new Dictionary<String, List<String>>()
            {
                ["Rally"] = new List<string>()
                {
                    "Ралли", "Раллі"
                },
                ["Arcade"] = new List<string>()
                {
                    "Аркада", "Аркада"
                },
                ["Formula"] = new List<string>()
                {
                    "Формула", "Формула"
                },
                ["Off-road"] = new List<string>()
                {
                    "Бездорожье", "Бездоріжжя"
                }
            };
            var actionSubNames = new Dictionary<String, List<String>>()
            {
                ["FPS"] = new List<string>()
                {
                    "ФПС", "ФПС"
                },
                ["TPS"] = new List<string>()
                {
                    "ТПС", "ТПС"
                },
                ["Misc2"] = new List<string>()
                {
                    "Разное2", "Різне2"
                }
            };

            var stratSubNames = new Dictionary<String, List<String>>()
            {
                ["RTS"] = new List<string>()
                {
                    "РТС", "РТС"
                },
                ["TBS"] = new List<string>()
                {
                    "ТБС", "ТБС"
                }
            };

            foreach (var raceName in raceSubNames.Keys)
            {
                genres.Add(new Genre()
                {
                    Name = raceName,
                    Parent = context.Genres.First(x => x.Name == "Race"),
                    IsDeleted = false
                });

                genreTranslates.AddRange(new[] {new GenreTranslate()
                        {
                            IsDeleted = false,
                            Language = LanguageCode.Ru,
                            Name = raceSubNames[raceName][0],
                            GenreId = context.Genres.FirstOrDefault(x => x.Name == raceName)?.Id ?? default(Guid)
                        },
                        new GenreTranslate()
                        {
                            IsDeleted = false,
                            Language = LanguageCode.Uk,
                            Name = raceSubNames[raceName][1],
                            GenreId = context.Genres.FirstOrDefault(x => x.Name == raceName)?.Id ?? default(Guid)
                        }});
            }

            foreach (var actionSubName in actionSubNames.Keys)
            {
                genres.Add(new Genre()
                {
                    Name = actionSubName,
                    Parent = context.Genres.First(x => x.Name == "Action"),
                    IsDeleted = false
                });

                genreTranslates.AddRange(new[] {new GenreTranslate()
                        {
                            IsDeleted = false,
                            Language = LanguageCode.Ru,
                            Name = actionSubNames[actionSubName][0],
                            GenreId = context.Genres.FirstOrDefault(x => x.Name == actionSubName)?.Id ?? default(Guid)
                        },
                        new GenreTranslate()
                        {
                            IsDeleted = false,
                            Language = LanguageCode.Uk,
                            Name = actionSubNames[actionSubName][1],
                            GenreId = context.Genres.FirstOrDefault(x => x.Name == actionSubName)?.Id ?? default(Guid)
                        }});
            }

            foreach (var stratSubName in stratSubNames.Keys)
            {
                genres.Add(new Genre()
                {
                    Name = stratSubName,
                    Parent = context.Genres.First(x => x.Name == "Strategy"),
                    IsDeleted = false
                });

                genreTranslates.AddRange(new[] {new GenreTranslate()
                        {
                            IsDeleted = false,
                            Language = LanguageCode.Ru,
                            Name = stratSubNames[stratSubName][0],
                            GenreId = context.Genres.FirstOrDefault(x => x.Name == stratSubName)?.Id ?? default(Guid)
                        },
                        new GenreTranslate()
                        {
                            IsDeleted = false,
                            Language = LanguageCode.Uk,
                            Name = stratSubNames[stratSubName][1],
                            GenreId = context.Genres.FirstOrDefault(x => x.Name == stratSubName)?.Id ?? default(Guid)
                        }});
            }

            context.Genres.AddOrUpdate(x => x.Name, genres.ToArray());

            context.SaveChanges();

            foreach (var genreTranslate in genreTranslates)
            {
                var genreTranslateInDb =
                    context.GenreTranslates.FirstOrDefault(
                        x => x.GenreId == genreTranslate.GenreId && x.Language == genreTranslate.Language);
                if (genreTranslateInDb == null)
                {
                    context.GenreTranslates.Add(genreTranslate);
                }
            }
            context.SaveChanges();

            var platforms = new List<PlatformType>()
            {
                new PlatformType()
                {
                    Type = TypeOfPlatform.Mobile,
                    IsDeleted = false
                },
                new PlatformType()
                {
                    Type = TypeOfPlatform.Browser,
                    IsDeleted = false
                },
                new PlatformType()
                {
                    Type = TypeOfPlatform.Desktop,
                    IsDeleted = false
                },
                new PlatformType()
                {
                    Type = TypeOfPlatform.Console,
                    IsDeleted = false
                }
            };

            context.PlatformTypes.AddOrUpdate(x => x.Type, platforms.ToArray());
            context.SaveChanges();

            var publisher =
                new Publisher()
                {
                    CompanyName = "Blizzard",
                    IsDeleted = false
                };

            context.Publishers.AddOrUpdate(x => x.CompanyName, publisher);
            context.SaveChanges();

            var publisherTranlates = new List<PublisherTranslate>()
            {
                new PublisherTranslate()
                {
                    CompanyName = "Blizzard",
                    IsDeleted = false,
                    Language = LanguageCode.Ru,
                    PublisherId =
                        context.Publishers.FirstOrDefault(x => x.CompanyName == "Blizzard")?.Id ?? default(Guid)
                },
                new PublisherTranslate()
                {
                    CompanyName = "Blizzard",
                    IsDeleted = false,
                    Language = LanguageCode.Uk,
                    PublisherId =
                        context.Publishers.FirstOrDefault(x => x.CompanyName == "Blizzard")?.Id ?? default(Guid)
                }
            };

            foreach (var publisherTranslate in publisherTranlates)
            {
                var publTranslateInDb =
                    context.PublisherTranslates.FirstOrDefault(
                        x =>
                            x.PublisherId == publisherTranslate.PublisherId && x.Language == publisherTranslate.Language);
                if (publTranslateInDb == null)
                {
                    context.PublisherTranslates.Add(publisherTranslate);
                }
            }

            context.SaveChanges();

            var game =
                new Game()
                {
                    Key = "game_2",
                    Description = "This game is awesome",
                    Name = "Game",
                    PlatformTypes = new List<PlatformType>()
                    {
                        context.PlatformTypes.FirstOrDefault(x => x.Type == TypeOfPlatform.Desktop),
                        context.PlatformTypes.FirstOrDefault(x => x.Type == TypeOfPlatform.Browser),
                        context.PlatformTypes.FirstOrDefault(x => x.Type == TypeOfPlatform.Console)
                    },
                    Genres = new List<Genre>()
                    {
                        context.Genres.FirstOrDefault(x => x.Name == "Arcade"),
                        context.Genres.FirstOrDefault(x => x.Name == "RPG"),
                        context.Genres.FirstOrDefault(x => x.Name == "Action"),
                        context.Genres.FirstOrDefault(x => x.Name == "Adventure")
                    },
                    IsDeleted = false,
                    Price = 15.0M,
                    Discontinued = false,
                    ReleaseDate = DateTime.UtcNow,
                    PublishDate = DateTime.UtcNow,
                    UnitsInStock = 5,
                    Publisher = context.Publishers.FirstOrDefault()
                };

            if (!Directory.Exists(AppContext.BaseDirectory + "../../../GameStore/GameFiles/"))
            {
                Directory.CreateDirectory(AppContext.BaseDirectory + "../../../GameStore/GameFiles/");
            }

            if (!File.Exists(AppContext.BaseDirectory + "../../../GameStore/GameFiles/game_2.txt"))
            {
                var stream =
                    new FileStream(AppContext.BaseDirectory +
                        "../../../GameStore/GameFiles/game_2.txt",
                        FileMode.CreateNew);
                var txt = new StreamWriter(stream);
                txt.Write("game_2");
                txt.Close();
            }

            context.Games.AddOrUpdate(x => x.Key, game);
            context.SaveChanges();

            var gameTranslate = new List<GameTranslate>()
            {
                new GameTranslate()
                {
                    Description = "Это отличная игра",
                    Language = LanguageCode.Ru,
                    IsDeleted = false,
                    Name = "Игра",
                    GameId = context.Games.FirstOrDefault(x => x.Key == "game_2")?.Id ?? default(Guid)
                },
                new GameTranslate()
                {
                    Description = "Це відмінна гра",
                    Language = LanguageCode.Uk,
                    IsDeleted = false,
                    Name = "Гра",
                    GameId = context.Games.FirstOrDefault(x => x.Key == "game_2")?.Id ?? default(Guid)
                }
            };

            foreach (var translate in gameTranslate)
            {
                var translateInDb =
                    context.GameTranslates.FirstOrDefault(
                        x => x.GameId == translate.GameId && x.Language == translate.Language);
                if (translateInDb == null)
                {
                    context.GameTranslates.Add(translate);
                }
            }

            context.SaveChanges();

            var roles = new List<Role>() {
                new Role() {IsDeleted = false, RoleCode = RoleCode.Guest },
                new Role() { IsDeleted = false, RoleCode = RoleCode.User },
                new Role() { IsDeleted = false, RoleCode = RoleCode.Manager },
                new Role() { IsDeleted = false, RoleCode = RoleCode.Moderator },
                new Role() { IsDeleted = false, RoleCode = RoleCode.Administrator }
            };

            context.Roles.AddOrUpdate(x => x.RoleCode, roles.ToArray());

            context.SaveChanges();

            var hasher = Sha512Hasher.GetInstance();

            var users = new List<User>()
            {
                new User() {
                    Email = "admin@newsite.com",
                    FirstName = "Ivan",
                    Gender = Gender.Male,
                    IsDeleted = false,
                    Lastname = "Bogun",
                    NickName = "IvanBogun",
                    Password = hasher.ComputeHash("qwertylol",null),
                    Roles = new List<Role>() {context.Roles.FirstOrDefault(x => x.RoleCode == RoleCode.Administrator) }
                },
                new User() {
                    Email = "alex@newsite.com",
                    FirstName = "Alex",
                    Gender = Gender.Male,
                    IsDeleted = false,
                    Lastname = "Filonenko",
                    NickName = "Alexashka",
                    Password = hasher.ComputeHash("qwertylol",null),
                    Roles = new List<Role>() {context.Roles.FirstOrDefault(x => x.RoleCode == RoleCode.User) }
                },
                 new User() {
                    Email = "barryallen@newsite.com",
                    FirstName = "Barry",
                    Gender = Gender.Male,
                    IsDeleted = false,
                    Lastname = "Allen",
                    NickName = "TheFlash",
                    Password = hasher.ComputeHash("qwertylol",null),
                    Roles = new List<Role>() {context.Roles.FirstOrDefault(x => x.RoleCode == RoleCode.Moderator) }
                },
                  new User() {
                    Email = "illichenkomaria@newsite.com",
                    FirstName = "Mary",
                    Gender = Gender.Male,
                    IsDeleted = false,
                    Lastname = "Illichenko",
                    NickName = "BartyCrawch",
                    Password = hasher.ComputeHash("qwertylol",null),
                    Roles = new List<Role>() {context.Roles.FirstOrDefault(x => x.RoleCode == RoleCode.Manager) }
                }
            };

            context.Users.AddOrUpdate(x => x.NickName, users.ToArray());
            context.SaveChanges();

            var ctxComments = context.Comments.ToList();
            foreach (var comment in ctxComments)
            {
                comment.UserId = context.Users.FirstOrDefault(x => x.NickName == "IvanBogun")?.Id ?? default(Guid);
            }

            context.Comments.AddOrUpdate(ctxComments.ToArray());
            context.SaveChanges();

            var comments = new List<Comment>()
            {
                new Comment() { UserId = context.Users.FirstOrDefault(x => x.NickName == "IvanBogun")?.Id ?? default(Guid),
                    Body = "Hi, who can say how is this game?",
                    IsDeleted = false,
                    Date = DateTime.UtcNow,
                    Game = context.Games.FirstOrDefault()
                },
                new Comment()
                {
                    UserId = context.Users.FirstOrDefault(x => x.NickName == "Alexashka")?.Id ?? default(Guid),
                    Body = "It's cool I've already played it and say it's awesome",
                    IsDeleted = false,
                    Date = DateTime.UtcNow,
                    Game = context.Games.FirstOrDefault()
                },
                new Comment()
                {
                    UserId = context.Users.FirstOrDefault(x => x.NickName == "TheFlash")?.Id ?? default(Guid),
                    Body = "This is coolest game in the world.",
                    IsDeleted = false,
                    Date = DateTime.UtcNow,
                    Game = context.Games.FirstOrDefault()
                },
                new Comment()
                {
                    UserId = context.Users.FirstOrDefault(x => x.NickName == "IvanBogun")?.Id ?? default(Guid),
                    Body = "Are you kidding me? It's terrible.",
                    IsDeleted = false,
                    Date = DateTime.UtcNow,
                    Game = context.Games.FirstOrDefault()
                },
                new Comment()
                {
                    UserId = context.Users.FirstOrDefault(x => x.NickName == "TheFlash")?.Id ?? default(Guid),
                    Body = "Nice game.",
                    IsDeleted = false,
                    Date = DateTime.UtcNow,
                    Game = context.Games.FirstOrDefault()
                },
                new Comment()
                {
                    UserId = context.Users.FirstOrDefault(x => x.NickName == "IvanBogun")?.Id ?? default(Guid),
                    Body = "My grade is 4-",
                    IsDeleted = false,
                    Date = DateTime.UtcNow,
                    Game = context.Games.FirstOrDefault()
                },
                new Comment()
                {
                    UserId = context.Users.FirstOrDefault(x => x.NickName == "Alexashka")?.Id ?? default(Guid),
                    Body = "Have nothing to add more, than were said above.",
                    IsDeleted = false,
                    Date = DateTime.UtcNow,
                    Game = context.Games.FirstOrDefault()
                },
                new Comment()
                {
                    UserId = context.Users.FirstOrDefault(x => x.NickName == "BartyCrawch")?.Id ?? default(Guid),
                    Body = "Thank you, developers. You are best!",
                    IsDeleted = false,
                    Date = DateTime.UtcNow,
                    Game = context.Games.FirstOrDefault()
                },
                new Comment()
                {
                    UserId = context.Users.FirstOrDefault(x => x.NickName == "IvanBogun")?.Id ?? default(Guid),
                    Body = "Cool!",
                    IsDeleted = false,
                    Date = DateTime.UtcNow,
                    Game = context.Games.FirstOrDefault()
                },
                new Comment()
                {
                    UserId = context.Users.FirstOrDefault(x => x.NickName == "BartyCrawch")?.Id ?? default(Guid),
                    Body = "It's boring...",
                    IsDeleted = false,
                    Date = DateTime.UtcNow,
                    Game = context.Games.FirstOrDefault()
                }
            };

            context.Comments.AddOrUpdate(comments.ToArray());
            context.SaveChanges();

            #endregion Base Seed

            var gameTranslates = new List<GameTranslate>();

            var random = new Random();

            for (int i = 0; i < 25; i++)
            {
                var newGame = new Game()
                {
                    Description = "New game " + (i + 1),
                    Discontinued = false,
                    IsDeleted = false,
                    Genres =
                        new List<Genre>()
                        {
                            context.Genres.FirstOrDefault(x => x.Name == "Arcade"),
                            context.Genres.FirstOrDefault(x => x.Name == "RPG"),
                            context.Genres.FirstOrDefault(x => x.Name == "Action")
                        },
                    PlatformTypes =
                        new List<PlatformType>()
                        {
                            context.PlatformTypes.First(x => x.Type == TypeOfPlatform.Console),
                            context.PlatformTypes.First(x => x.Type == TypeOfPlatform.Desktop),
                            context.PlatformTypes.First(x => x.Type == TypeOfPlatform.Browser)
                        },
                    Key = "new_game_" + (i + 1),
                    Name = "New game " + (i + 1),
                    Price = random.Next(15, 145),
                    PublishDate = DateTime.UtcNow,
                    Publisher = context.Publishers.First(),
                    PublisherId = context.Publishers.First().Id,
                    ReleaseDate = DateTime.UtcNow,
                    UnitsInStock = (short)random.Next(0, 15000),
                    VisitsCount = 0
                };

                gameTranslates.AddRange(new[] {
                    new GameTranslate()
                    {
                        Description = "Новая игра " + (i + 1),
                            Language = LanguageCode.Ru,
                            IsDeleted = false,
                            Name = "Новая игра " + (i + 1),
                            GameId = context.Games.FirstOrDefault(x => x.Key == "new_game_" + (i + 1))?.Id ?? default(Guid)
                        },
                    new GameTranslate()
                    {
                        Description = "Нова гра " + (i + 1),
                        Language = LanguageCode.Uk,
                        IsDeleted = false,
                        Name = "Нова гра " + (i + 1),
                        GameId = context.Games.FirstOrDefault(x => x.Key == "new_game_" + (i + 1))?.Id ?? default(Guid)
                    }
                });

                context.Games.AddOrUpdate(x => x.Key, newGame);
            }

            context.SaveChanges();

            foreach (var translate in gameTranslates)
            {
                var translateInDb =
                    context.GameTranslates.FirstOrDefault(
                        x => x.GameId == translate.GameId && x.Language == translate.Language);
                if (translateInDb == null)
                {
                    context.GameTranslates.Add(translate);
                }
            }
            context.SaveChanges();

            base.Seed(context);
        }
    }
}
namespace GameStore.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MergeTask6WithTask4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "ObjectId", c => c.String());
            AddColumn("dbo.OrderDetails", "ObjectId", c => c.String());
            AddColumn("dbo.Games", "ObjectId", c => c.String());
            AddColumn("dbo.Genres", "ObjectId", c => c.String());
            AddColumn("dbo.Publishers", "ObjectId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Publishers", "ObjectId");
            DropColumn("dbo.Genres", "ObjectId");
            DropColumn("dbo.Games", "ObjectId");
            DropColumn("dbo.OrderDetails", "ObjectId");
            DropColumn("dbo.Orders", "ObjectId");
        }
    }
}

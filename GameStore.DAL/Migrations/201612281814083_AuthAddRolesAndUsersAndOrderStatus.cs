namespace GameStore.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AuthAddRolesAndUsersAndOrderStatus : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Users",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    NickName = c.String(nullable: false, maxLength: 50),
                    Email = c.String(nullable: false),
                    Password = c.String(nullable: false),
                    FirstName = c.String(nullable: false, maxLength: 20),
                    Lastname = c.String(nullable: false, maxLength: 20),
                    Gender = c.Int(nullable: false),
                    MiddleName = c.String(maxLength: 20),
                    IsDeleted = c.Boolean(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.NickName, unique: true);

            CreateTable(
                "dbo.Roles",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    RoleCode = c.Int(nullable: false),
                    IsDeleted = c.Boolean(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.RoleCode, unique: true);

            CreateTable(
                "dbo.RoleUsers",
                c => new
                {
                    Role_Id = c.Guid(nullable: false),
                    User_Id = c.Guid(nullable: false),
                })
                .PrimaryKey(t => new { t.Role_Id, t.User_Id })
                .ForeignKey("dbo.Roles", t => t.Role_Id, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.User_Id, cascadeDelete: true)
                .Index(t => t.Role_Id)
                .Index(t => t.User_Id);

            AddColumn("dbo.Baskets", "UserId", c => c.Guid());
            AddColumn("dbo.Orders", "UserId", c => c.Guid());
            AddColumn("dbo.Orders", "ShippedDate", c => c.DateTime());
            AddColumn("dbo.Orders", "Status", c => c.Int(nullable: false));
            AddColumn("dbo.Comments", "UserId", c => c.Guid(nullable: true));
            CreateIndex("dbo.Baskets", "UserId");
            CreateIndex("dbo.Orders", "UserId");
            CreateIndex("dbo.Comments", "UserId");
            AddForeignKey("dbo.Comments", "UserId", "dbo.Users", "Id");
            AddForeignKey("dbo.Orders", "UserId", "dbo.Users", "Id");
            AddForeignKey("dbo.Baskets", "UserId", "dbo.Users", "Id");
            DropColumn("dbo.Baskets", "CustomerId");
            DropColumn("dbo.Orders", "CustomerId");
            DropColumn("dbo.Orders", "IsPaid");
            DropColumn("dbo.Comments", "Name");
        }

        public override void Down()
        {
            AddColumn("dbo.Comments", "Name", c => c.String(nullable: false));
            AddColumn("dbo.Orders", "IsPaid", c => c.Boolean(nullable: false));
            AddColumn("dbo.Orders", "CustomerId", c => c.Guid(nullable: false));
            AddColumn("dbo.Baskets", "CustomerId", c => c.Guid(nullable: false));
            DropForeignKey("dbo.Baskets", "UserId", "dbo.Users");
            DropForeignKey("dbo.Orders", "UserId", "dbo.Users");
            DropForeignKey("dbo.Comments", "UserId", "dbo.Users");
            DropForeignKey("dbo.RoleUsers", "User_Id", "dbo.Users");
            DropForeignKey("dbo.RoleUsers", "Role_Id", "dbo.Roles");
            DropIndex("dbo.RoleUsers", new[] { "User_Id" });
            DropIndex("dbo.RoleUsers", new[] { "Role_Id" });
            DropIndex("dbo.Roles", new[] { "RoleCode" });
            DropIndex("dbo.Users", new[] { "NickName" });
            DropIndex("dbo.Comments", new[] { "UserId" });
            DropIndex("dbo.Orders", new[] { "UserId" });
            DropIndex("dbo.Baskets", new[] { "UserId" });
            DropColumn("dbo.Comments", "UserId");
            DropColumn("dbo.Orders", "Status");
            DropColumn("dbo.Orders", "ShippedDate");
            DropColumn("dbo.Orders", "UserId");
            DropColumn("dbo.Baskets", "UserId");
            DropTable("dbo.RoleUsers");
            DropTable("dbo.Roles");
            DropTable("dbo.Users");
        }
    }
}
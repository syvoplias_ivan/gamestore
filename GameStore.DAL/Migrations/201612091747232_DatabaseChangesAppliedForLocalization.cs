namespace GameStore.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DatabaseChangesAppliedForLocalization : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.GameTranslates",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        Language = c.Int(nullable: false),
                        GameId = c.Guid(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Games", t => t.GameId, cascadeDelete: true)
                .Index(t => t.GameId);
            
            CreateTable(
                "dbo.GenreTranslates",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(),
                        Language = c.Int(nullable: false),
                        GenreId = c.Guid(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Genres", t => t.GenreId, cascadeDelete: true)
                .Index(t => t.GenreId);
            
            CreateTable(
                "dbo.PublisherTranslates",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        CompanyName = c.String(),
                        Description = c.String(),
                        Language = c.Int(nullable: false),
                        PublisherId = c.Guid(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Publishers", t => t.PublisherId, cascadeDelete: true)
                .Index(t => t.PublisherId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PublisherTranslates", "PublisherId", "dbo.Publishers");
            DropForeignKey("dbo.GenreTranslates", "GenreId", "dbo.Genres");
            DropForeignKey("dbo.GameTranslates", "GameId", "dbo.Games");
            DropIndex("dbo.PublisherTranslates", new[] { "PublisherId" });
            DropIndex("dbo.GenreTranslates", new[] { "GenreId" });
            DropIndex("dbo.GameTranslates", new[] { "GameId" });
            DropTable("dbo.PublisherTranslates");
            DropTable("dbo.GenreTranslates");
            DropTable("dbo.GameTranslates");
        }
    }
}

﻿using GameStore.DAL.Abstract;
using GameStore.DAL.Context;
using GameStore.DAL.Entities;
using GameStore.DAL.MongoDb.MongoEnums;
using GameStore.DAL.MongoDb.MongoLog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace GameStore.DAL.Repository
{
    public class Repository<T> : IRepository<T> where T : class, IEntity
    {
        private IMongoLogger _logger;

        public Repository(IGameStoreContext context, IMongoLogger logger)
        {
            _context = context;
            _logger = logger;
            _dbSet = context.Set<T>();
        }

        public virtual void Delete(T entityToDelete)
        {
            entityToDelete.IsDeleted = true;
            _dbSet.Attach(entityToDelete);
            _context.SetModified<T>(entityToDelete);

            var logInfo = new LogEventInfo()
            {
                Date = DateTime.UtcNow,
                EntityType = typeof(T).Name,
                ErrorInfo = null,
                EventMessage = "Entity of type " + typeof(T).Name + " was deleted in SQL database",
                EventType = EventType.Delete,
                Id = Guid.NewGuid(),
                Version = VersionOfEntity.Default
            };
            _logger.Log(logInfo);
        }

        public virtual void Insert(T entity)
        {
            //_dbSet.Attach(entity);
            //_context.SetAdded<T>(entity);
            _dbSet.Add(entity);

            var logInfo = new LogEventInfo()
            {
                Date = DateTime.UtcNow,
                EntityType = typeof(T).Name,
                ErrorInfo = null,
                EventMessage = "Entity of type " + typeof(T).Name + " was added to SQL Database",
                EventType = EventType.Create,
                Id = Guid.NewGuid(),
                Version = VersionOfEntity.New
            };
            _logger.Log(logInfo);
        }

        public virtual void InsertRange(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                Insert(entity);
            }
        }

        public virtual void Update(T entity)
        {
            _dbSet.Attach(entity);
            _context.SetModified<T>(entity);

            var logInfo = new LogEventInfo()
            {
                Date = DateTime.UtcNow,
                EntityType = typeof(T).Name,
                ErrorInfo = null,
                EventMessage = "Entity of type " + typeof(T).Name + " was updated in SQL database",
                EventType = EventType.Update,
                Id = Guid.NewGuid(),
                Version = VersionOfEntity.New
            };
            _logger.Log(logInfo);
        }

        public IEnumerable<T> Enumerable()
        {
            var logInfo = new LogEventInfo()
            {
                Date = DateTime.UtcNow,
                EntityType = typeof(T).Name,
                ErrorInfo = null,
                EventMessage = "Entities of type " + typeof(T).Name + " get from SQL database",
                EventType = EventType.Get,
                Id = Guid.NewGuid(),
                Version = VersionOfEntity.Default
            };
            _logger.Log(logInfo);
            return _dbSet.Where(x => !x.IsDeleted).ToList();
        }

        public IEnumerable<T> Select(
            Expression<Func<T, bool>> filter = null,
            Expression<Func<T, object>> orderBy = null,
            int? page = null,
            int? pageSize = null)
        {
            IQueryable<T> query = _dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (orderBy != null)
            {
                query = query.OrderBy(orderBy);
            }

            if (page != null && pageSize != null)
            {
                query = query.Skip((page.Value - 1) * pageSize.Value).Take(pageSize.Value);
            }

            var logInfo = new LogEventInfo()
            {
                Date = DateTime.UtcNow,
                EntityType = typeof(T).Name,
                ErrorInfo = null,
                EventMessage = "Entities of type " + typeof(T).Name + " get from SQL Db",
                EventType = EventType.Get,
                Id = Guid.NewGuid(),
                Version = VersionOfEntity.Default
            };
            _logger.Log(logInfo);
            return query.ToList();
        }

        public T Get(Func<T, bool> condition)
        {
            var item = _dbSet.FirstOrDefault(condition);
            var logInfo = new LogEventInfo()
            {
                Date = DateTime.UtcNow,
                EntityType = typeof(T).Name,
                ErrorInfo = null,
                EventMessage = "Entity of type " + typeof(T).Name + " found in SQL Db",
                EventType = EventType.Get,
                Id = Guid.NewGuid(),
                Version = VersionOfEntity.Default
            };
            _logger.Log(logInfo);
            return item;
        }

        public int Count()
        {
            var logInfo = new LogEventInfo()
            {
                Date = DateTime.UtcNow,
                EntityType = typeof(T).Name,
                ErrorInfo = null,
                EventMessage = "Count of games was get from SQL Db",
                EventType = EventType.Delete,
                Id = Guid.NewGuid(),
                Version = VersionOfEntity.New
            };
            _logger.Log(logInfo);

            return _dbSet.AsQueryable().Count(x => !x.IsDeleted);
        }

        public int FilteredCount(Expression<Func<T, bool>> filter)
        {
            var logInfo = new LogEventInfo()
            {
                Date = DateTime.UtcNow,
                EntityType = typeof(T).Name,
                ErrorInfo = null,
                EventMessage = "Count of filtered entities of type " + typeof(T).Name + " get from SQL Db",
                EventType = EventType.Get,
                Id = Guid.NewGuid(),
                Version = VersionOfEntity.Default
            };
            _logger.Log(logInfo);
            return _dbSet.AsQueryable().Count(filter);
        }

        #region Private Fields

        private readonly IGameStoreContext _context;
        private readonly IDbSet<T> _dbSet;

        #endregion Private Fields
    }
}
﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.DAL.Abstract
{
    public interface IMongoNorthwindContext
    {
        IMongoCollection<T> Collection<T>() where T : class, IEntity, new();

        IMongoDatabase Database { get; }
    }
}
﻿using System;

namespace GameStore.DAL.Abstract
{
    public interface IUnitOfWork : IDisposable
    {
        int SaveChanges();

        IRepository<TEntity> Repository<TEntity>() where TEntity : class, IEntity, new();
    }
}
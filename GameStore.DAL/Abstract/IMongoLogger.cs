﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.DAL.Abstract
{
    public interface IMongoLogger
    {
        void Log(ILogEventInfo logEventInfo);
    }
}
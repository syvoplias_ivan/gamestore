﻿using System;
using System.Data.Entity;

namespace GameStore.DAL.Abstract
{
    public interface IGameStoreContext : IDisposable
    {
        IDbSet<T> Set<T>() where T : class, IEntity;

        void SetModified<T>(object entity);

        void SetAdded<T>(object entity);

        void SetDeleted<T>(object entity);

        void SetDetached<T>(object entity);

        void SetUnchanged<T>(object entity);

        EntityState GetEntityState<T>(object entity) where T : class, IEntity;

        int SaveChanges();
    }
}
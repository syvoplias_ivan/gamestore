﻿using GameStore.DAL.MongoDb.MongoEnums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.DAL.Abstract
{
    public interface ILogEventInfo
    {
        Guid Id { get; set; }

        EventType EventType { get; set; }

        string EventMessage { get; set; }

        DateTime Date { get; set; }

        string EntityType { get; set; }

        string RawEntity { get; set; }

        VersionOfEntity Version { get; set; }

        string ErrorInfo { get; set; }

        string StackTrace { get; set; }
    }
}
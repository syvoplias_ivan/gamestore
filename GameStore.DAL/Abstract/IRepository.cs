﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace GameStore.DAL.Abstract
{
    public interface IRepository<TEntity> where TEntity : class
    {
        void Insert(TEntity entity);

        void InsertRange(IEnumerable<TEntity> entities);

        void Update(TEntity entity);

        void Delete(TEntity entity);

        IEnumerable<TEntity> Enumerable();

        TEntity Get(Func<TEntity, bool> condition);

        IEnumerable<TEntity> Select(Expression<Func<TEntity, bool>> filter = null,
            Expression<Func<TEntity, object>> orderBy = null,
            int? page = null,
            int? pageSize = null);

        int Count();

        int FilteredCount(Expression<Func<TEntity, bool>> filter);
    }
}
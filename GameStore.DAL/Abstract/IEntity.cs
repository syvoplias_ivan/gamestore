﻿using System;

namespace GameStore.DAL.Abstract
{
    public interface IEntity
    {
        Guid Id { get; set; }

        bool IsDeleted { get; set; }
    }
}
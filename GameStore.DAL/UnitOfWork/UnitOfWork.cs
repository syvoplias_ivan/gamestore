﻿using GameStore.DAL.Abstract;
using GameStore.DAL.Context;
using GameStore.DAL.Decorator;
using GameStore.DAL.Entities;
using GameStore.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core.Objects;

namespace GameStore.DAL.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private IGameStoreContext _dataContext;
        private IMongoNorthwindContext _northwindContext;
        private bool _disposed;
        private ObjectContext _objectContext;
        private Dictionary<string, object> _repositories;
        private IMongoLogger _logger;

        public UnitOfWork(IGameStoreContext dataContext, IMongoNorthwindContext northwindContext, IMongoLogger logger)
        {
            _dataContext = dataContext;
            _northwindContext = northwindContext;
            _logger = logger;
            _repositories = new Dictionary<string, object>();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public int SaveChanges()
        {
            return _dataContext.SaveChanges();
        }

        public IRepository<TEntity> Repository<TEntity>() where TEntity : class, IEntity, new()
        {
            if (_repositories == null)
            {
                _repositories = new Dictionary<string, object>();
            }

            var type = typeof(TEntity).Name;

            if (_repositories.ContainsKey(type))
            {
                return (IRepository<TEntity>)_repositories[type];
            }

            if (IsSameOrSubclass(typeof(BaseMongoEntity), typeof(TEntity)))
            {
                _repositories.Add(type,
                    GameStoreDecoratorProvider.GetDecorator<TEntity>(_dataContext, _northwindContext, _logger));
            }
            else
            {
                var repositoryType = typeof(Repository<>);
                _repositories.Add(type, Activator.CreateInstance(repositoryType.MakeGenericType(typeof(TEntity)), _dataContext, _logger));
            }

            return (IRepository<TEntity>)_repositories[type];
        }

        // The bulk of the clean-up code is implemented in Dispose(bool)
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // free other managed objects that implement
                    // IDisposable only

                    try
                    {
                        if (_objectContext != null)
                        {
                            if (_objectContext.Connection.State == ConnectionState.Open)
                            {
                                _objectContext.Connection.Close();
                            }

                            _objectContext.Dispose();
                            _objectContext = null;
                        }
                        if (_dataContext != null)
                        {
                            _dataContext.Dispose();
                            _dataContext = null;
                        }
                    }
                    catch (ObjectDisposedException)
                    {
                        // do nothing, the objectContext has already been disposed
                    }

                    if (_repositories != null)
                    {
                        _repositories = null;
                    }
                }

                _disposed = true;
            }
        }

        private bool IsSameOrSubclass(Type potentialBase, Type potentialDescendant)
        {
            return potentialDescendant.IsSubclassOf(potentialBase)
                   || potentialDescendant == potentialBase;
        }
    }
}
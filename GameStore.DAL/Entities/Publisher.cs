﻿using GameStore.DAL.Abstract;
using GameStore.DAL.Entities.Localization;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GameStore.DAL.Entities
{
    [BsonIgnoreExtraElements]
    public class Publisher : BaseMongoEntity
    {
        [Column(TypeName = "nvarchar")]
        [MaxLength(40)]
        [BsonElement("CompanyName")]
        [Required]
        public String CompanyName { get; set; }

        [Column(TypeName = "ntext")]
        [BsonIgnore]
        public String Description { get; set; }

        [Column(TypeName = "ntext")]
        [BsonElement("HomePage")]
        public String HomePage { get; set; }

        [BsonIgnore]
        public virtual ICollection<PublisherTranslate> PublisherLocalization { get; set; }

        public virtual ICollection<Game> Games { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;

            if (obj == this) return true;

            if (obj.GetType() == typeof(Publisher))
            {
                var publisher = (Publisher)obj;

                if (publisher.CompanyName == CompanyName)
                {
                    return true;
                }
            }

            return false;
        }

        public override int GetHashCode()
        {
            return CompanyName.GetHashCode();
        }

        public override string GetMongoName()
        {
            return "suppliers";
        }
    }
}
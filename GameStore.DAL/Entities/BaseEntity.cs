﻿using GameStore.DAL.Abstract;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GameStore.DAL.Entities
{
    public abstract class BaseEntity : IEntity
    {
        [Key]
        [BsonIgnore]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [BsonIgnore]
        public bool IsDeleted { get; set; }
    }
}
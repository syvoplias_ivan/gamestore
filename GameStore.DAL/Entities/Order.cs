﻿using GameStore.Common;
using GameStore.DAL.Entities.Authorizaion;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GameStore.DAL.Entities
{
    public class Order : BaseMongoEntity
    {
        public Guid? UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        [Required]
        public DateTime OrderDate { get; set; }

        public DateTime? ShippedDate { get; set; }

        [Required]
        public TypeOfPayment PaymentType { get; set; }

        [Required]
        [BsonIgnore]
        public OrderStatus Status { get; set; }

        [Required]
        [ForeignKey("Basket")]
        public Guid BasketId { get; set; }

        public virtual Basket Basket { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public decimal Summ { get; set; }

        public virtual ICollection<OrderDetail> OrderDetails { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;

            if (obj == this) return true;

            if (obj.GetType() == typeof(Order))
            {
                var order = (Order)obj;

                if (order.Id == Id || ObjectId == order.ObjectId)
                {
                    return true;
                }
            }

            return false;
        }

        public override int GetHashCode()
        {
            if (Id == default(Guid))
            {
                return ObjectId.GetHashCode();
            }

            return Id.GetHashCode();
        }

        public override string GetMongoName()
        {
            return "orders";
        }
    }
}
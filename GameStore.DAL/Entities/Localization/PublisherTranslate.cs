﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.DAL.Entities.Localization
{
    public class PublisherTranslate : BaseEntity
    {
        public string CompanyName { get; set; }

        public string Description { get; set; }

        public virtual LanguageCode Language { get; set; }

        public Guid PublisherId { get; set; }

        [ForeignKey("PublisherId")]
        public virtual Publisher Publisher { get; set; }
    }
}
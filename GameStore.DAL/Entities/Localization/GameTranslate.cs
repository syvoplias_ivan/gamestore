﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.DAL.Entities.Localization
{
    public class GameTranslate : BaseEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public virtual LanguageCode Language { get; set; }

        public Guid GameId { get; set; }

        [ForeignKey("GameId")]
        public virtual Game Game { get; set; }
    }
}
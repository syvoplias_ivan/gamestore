﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.DAL.Entities.Localization
{
    public class GenreTranslate : BaseEntity
    {
        public string Name { get; set; }

        public virtual LanguageCode Language { get; set; }

        public Guid GenreId { get; set; }

        [ForeignKey("GenreId")]
        public virtual Genre Genre { get; set; }
    }
}
﻿using GameStore.DAL.Abstract;
using GameStore.DAL.Entities.Localization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GameStore.DAL.Entities
{
    [BsonIgnoreExtraElements]
    public class Game : BaseMongoEntity
    {
        [Index(IsUnique = true)]
        [StringLength(50)]
        public string Key { get; set; }

        [DataType(DataType.Currency)]
        public decimal Price { get; set; }

        [Column(TypeName = "smallint")]
        public short UnitsInStock { get; set; }

        [Column(TypeName = "bit")]
        public bool Discontinued { get; set; }

        public Game()
        {
        }

        [BsonConstructor]
        public Game(string objectId)
        {
            Key = objectId;
            ObjectId = objectId;
        }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime ReleaseDate { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime PublishDate { get; set; }

        [Required]
        public int VisitsCount { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<PlatformType> PlatformTypes { get; set; }

        public virtual ICollection<Genre> Genres { get; set; }

        public virtual ICollection<GameTranslate> GameLocalization { get; set; }

        public virtual Publisher Publisher { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;

            if (obj == this) return true;

            if (obj.GetType() == typeof(Game))
            {
                var game = (Game)obj;

                if (game.Key == Key)
                {
                    return true;
                }
            }

            return false;
        }

        public override int GetHashCode()
        {
            return Key.GetHashCode();
        }

        [ForeignKey("Publisher")]
        public Guid? PublisherId { get; set; }

        public override string GetMongoName()
        {
            return "products";
        }
    }
}
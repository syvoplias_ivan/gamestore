﻿using GameStore.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GameStore.DAL.Entities
{
    public class PlatformType : BaseEntity
    {
        [Index(IsUnique = true)]
        [Required]
        public TypeOfPlatform Type { get; set; }

        public virtual ICollection<Game> Games { get; set; }
    }
}
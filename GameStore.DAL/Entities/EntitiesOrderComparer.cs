﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.DAL.Entities
{
    public class EntitiesOrderComparer : IComparer<object>
    {
        public int Compare(object x, object y)
        {
            var properties = x.GetType().GetProperties();

            foreach (var propertyInfo in properties)
            {
                if (propertyInfo.PropertyType == typeof(DateTime))
                {
                    PropertyInfo datePropertyX = x.GetType().GetProperty(propertyInfo.Name);
                    PropertyInfo datePropertyY = y.GetType().GetProperty(propertyInfo.Name);

                    var dateX = (DateTime)datePropertyX.GetValue(x);
                    var dateY = (DateTime)datePropertyY.GetValue(y);
                    return dateX.CompareTo(dateY);
                }

                if (propertyInfo.PropertyType == typeof(decimal))
                {
                    PropertyInfo decimalPropertyX = x.GetType().GetProperty(propertyInfo.Name);
                    PropertyInfo decimalPropertyY = y.GetType().GetProperty(propertyInfo.Name);

                    var decimalX = (decimal)decimalPropertyX.GetValue(x);
                    var decimalY = (decimal)decimalPropertyY.GetValue(y);
                    return decimalX.CompareTo(decimalY);
                }

                if (propertyInfo.PropertyType == typeof(Int32))
                {
                    PropertyInfo intPropertyX = x.GetType().GetProperty(propertyInfo.Name);
                    PropertyInfo intPropertyY = y.GetType().GetProperty(propertyInfo.Name);

                    var intX = (Int32)intPropertyX.GetValue(x);
                    var intY = (Int32)intPropertyY.GetValue(y);
                    return intX.CompareTo(intY);
                }

                if (propertyInfo.PropertyType == typeof(Int64))
                {
                    PropertyInfo intPropertyX = x.GetType().GetProperty(propertyInfo.Name);
                    PropertyInfo intPropertyY = y.GetType().GetProperty(propertyInfo.Name);

                    var intX = (Int64)intPropertyX.GetValue(x);
                    var intY = (Int64)intPropertyY.GetValue(y);
                    return intX.CompareTo(intY);
                }
            }

            return 0;
        }
    }
}
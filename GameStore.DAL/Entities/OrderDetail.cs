﻿using GameStore.DAL.Abstract;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GameStore.DAL.Entities
{
    public class OrderDetail : BaseMongoEntity
    {
        [Required]
        [ForeignKey("Game")]
        public Guid ProductId { get; set; }

        public virtual Game Game { get; set; }

        [Required]
        public decimal Price { get; set; }

        [Required]
        [Column(TypeName = "smallint")]
        public short Quantity { get; set; }

        [Column(TypeName = "real")]
        public float Discount { get; set; }

        [Required]
        [ForeignKey("Order")]
        public Guid OrderId { get; set; }

        public virtual Order Order { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;

            if (obj == this) return true;

            if (obj.GetType() == typeof(OrderDetail))
            {
                var orderDetail = (OrderDetail)obj;

                if (Id == orderDetail.Id || ObjectId == orderDetail.ObjectId)
                {
                    return true;
                }
            }

            return false;
        }

        public override int GetHashCode()
        {
            if (Id == default(Guid))
            {
                return ObjectId.GetHashCode();
            }

            return Id.GetHashCode();
        }

        public override string GetMongoName()
        {
            return "order-details";
        }
    }
}
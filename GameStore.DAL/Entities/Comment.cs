﻿using GameStore.DAL.Entities.Authorizaion;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GameStore.DAL.Entities
{
    public class Comment : BaseEntity
    {
        public Guid? UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        [Required]
        public string Body { get; set; }

        [Required]
        [ForeignKey("Game")]
        public Guid GameId { get; set; }

        public virtual Game Game { get; set; }

        [ForeignKey("Parent")]
        public Guid? ParentId { get; set; }

        public virtual Comment Parent { get; set; }

        [Required]
        public DateTime Date { get; set; }

        public string Quote { get; set; }

        public virtual ICollection<Comment> ChildComments { get; set; }
    }
}
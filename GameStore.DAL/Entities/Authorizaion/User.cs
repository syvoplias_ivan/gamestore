﻿using GameStore.Common.Auth;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.DAL.Entities.Authorizaion
{
    public class User : BaseEntity
    {
        [Required]
        [Index(IsUnique = true)]
        [StringLength(50)]
        public string NickName { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        [StringLength(20)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(20)]
        public string Lastname { get; set; }

        [Required]
        public Gender Gender { get; set; }

        [StringLength(20)]
        public string MiddleName { get; set; }

        public virtual ICollection<Role> Roles { get; set; }
    }
}
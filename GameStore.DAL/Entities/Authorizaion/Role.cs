﻿using GameStore.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GameStore.DAL.Entities.Authorizaion
{
    public class Role : BaseEntity
    {
        [Required]
        [Index(IsUnique = true)]
        public RoleCode RoleCode { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}
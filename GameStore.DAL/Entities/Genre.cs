﻿using GameStore.DAL.Abstract;
using GameStore.DAL.Entities.Localization;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GameStore.DAL.Entities
{
    [BsonIgnoreExtraElements]
    public class Genre : BaseMongoEntity
    {
        [Required]
        [Index(IsUnique = true)]
        [StringLength(50)]
        [BsonElement("CategoryName")]
        public string Name { get; set; }

        [ForeignKey("Parent")]
        public Guid? ParentId { get; set; }

        [BsonIgnore]
        public virtual Genre Parent { get; set; }

        [BsonIgnore]
        public virtual ICollection<GenreTranslate> GenreLocalization { get; set; }

        public virtual ICollection<Game> Games { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;

            if (obj == this) return true;

            if (obj.GetType() == typeof(Genre))
            {
                var genre = (Genre)obj;

                if (genre.Name == Name)
                {
                    return true;
                }
            }

            return false;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        public override string GetMongoName()
        {
            return "categories";
        }
    }
}
﻿using GameStore.DAL.Abstract;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Authorizaion;
using GameStore.DAL.Entities.Localization;
using System.Data.Entity;

namespace GameStore.DAL.Context
{
    public class GameStoreContext : DbContext, IGameStoreContext
    {
        public GameStoreContext() : base("GameStore")
        {
        }

        public DbSet<Comment> Comments { get; set; }
        public DbSet<PlatformType> PlatformTypes { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Publisher> Publishers { get; set; }
        public DbSet<Basket> Baskets { get; set; }
        public DbSet<GameTranslate> GameTranslates { get; set; }
        public DbSet<PublisherTranslate> PublisherTranslates { get; set; }
        public DbSet<GenreTranslate> GenreTranslates { get; set; }
        public DbSet<Role> Roles { get; set; }

        public DbSet<User> Users { get; set; }

        IDbSet<T> IGameStoreContext.Set<T>()
        {
            return Set<T>();
        }

        public void SetModified<T>(object entity)
        {
            Entry(entity).State = EntityState.Modified;
        }

        public void SetAdded<T>(object entity)
        {
            Entry(entity).State = EntityState.Added;
        }

        public void SetDeleted<T>(object entity)
        {
            Entry(entity).State = EntityState.Deleted;
        }

        public void SetDetached<T>(object entity)
        {
            Entry(entity).State = EntityState.Detached;
        }

        public void SetUnchanged<T>(object entity)
        {
            Entry(entity).State = EntityState.Unchanged;
        }

        public EntityState GetEntityState<T>(object entity) where T : class, IEntity
        {
            return Entry(entity).State;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Order>()
                .HasRequired<Basket>(x => x.Basket)
                .WithMany(o => o.Orders)
                .HasForeignKey(x => x.BasketId);
        }
    }
}